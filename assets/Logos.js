export default logos = {
  mentorkonnectLogo:
  "https://firebasestorage.googleapis.com/v0/b/mycollegekonnect-app-efd5d.appspot.com/o/assets%2Fmycollegekonnect-logo.png?alt=media&token=9f7dc269-da6e-4f06-bfb2-18eef6fd2276",
  jntukLogo:
    "https://firebasestorage.googleapis.com/v0/b/mycollegekonnect-app-efd5d.appspot.com/o/assets%2Fjntuk-logo.png?alt=media&token=2aeb560e-f875-4019-a5b9-d4e9c9b33cac",
  mentorKonnectIcon:
    "https://firebasestorage.googleapis.com/v0/b/mycollegekonnect-app-efd5d.appspot.com/o/assets%2FCk%20icon%20(PNG%20Format).png?alt=media&token=cab2b42e-5bb4-45c8-8e26-a55a45e8dc8e",
  defaultProfilePic:
    "https://firebasestorage.googleapis.com/v0/b/collegekonnect-app.appspot.com/o/profile_pic.png?alt=media&token=4152469c-0a75-4704-9da8-bd948944e278",
  bronze:
  "https://firebasestorage.googleapis.com/v0/b/mycollegekonnect-app-efd5d.appspot.com/o/assets%2FBronze.png?alt=media&token=de7ba033-979d-4cce-8191-0f1e15d08ccd",
  gold:
  "https://firebasestorage.googleapis.com/v0/b/mycollegekonnect-app-efd5d.appspot.com/o/assets%2FGold.png?alt=media&token=255ee3d2-f783-40a4-b938-e0a55ca231d4",
  platinum:
  "https://firebasestorage.googleapis.com/v0/b/mycollegekonnect-app-efd5d.appspot.com/o/assets%2FPlatinum.png?alt=media&token=26705ed4-6baa-42cd-97e1-98399e39ed4e",
  silver:
  "https://firebasestorage.googleapis.com/v0/b/mycollegekonnect-app-efd5d.appspot.com/o/assets%2FSilver.png?alt=media&token=305a3bb4-ab92-4348-b0b2-d3b712c6eb9d",
};
