import React from "react";
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  Button,
  AsyncStorage,
  Platform,
  Image,
} from "react-native";
import { Icon } from "react-native-elements";
import Logos from "../../assets/Logos";

export default class HeaderComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { profile: "" };
    AsyncStorage.getItem("profile").then(async (d) => {
      await this.setState({ profile: JSON.parse(d) });
    });
    // .then(() => { console.log(this.state.profile); })
  }

  render() {
    return (
      <View style={styles.headerContainer}>
        <View>
          <Icon
            name="menu"
            color="black"
            size={30}
            containerStyle={{ marginLeft: Platform.OS === "ios" ? 15 : 0 }}
            onPress={() => {
              this.props.navigation.openDrawer();
            }}
          ></Icon>
        </View>
        <View
          style={{
            flex: 1,

            marginLeft: 4, //added
            flexDirection: "row",
            //alignContent: "center",
            justifyContent: "center",
            alignItems: "center", //added
          }}
        >
          <Text style={{ fontWeight: "bold", fontSize: 20 }}>
            {this.props.text != undefined ? this.props.text : ""}
          </Text>
        </View>
        <View style={styles.signOut}>
          <Image
            style={{ width: 40, height: 40, marginEnd: 25 }}
            source={{ uri: Logos.mentorKonnectIcon }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  signOut: {
    //flex: 0.15,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  headerContainer: {
    flex: 1,
    width: Dimensions.get("window").width,
    height: "100%",
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly", //added
  },
});
