import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  ScrollView,
  Platform,
  Image,
  Text,
  Picker,
  TextInput,
  KeyboardAvoidingView,
} from "react-native";
import { Button, CheckBox, Avatar, Icon } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import { FlatList } from "react-native-gesture-handler";
import DatePicker from "react-native-datepicker";
import firebase from "firebase";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import { Toast } from "native-base";
import { urls } from "./../../../env.json";
import PreBuiltEventTypes from "./../Events/PreBuiltEventTypes";
export default class EditProfileMentee extends Component {
  constructor(props) {
    super(props);
    const data = this.props.navigation.getParam("user");
    this.state = {
      userType: "",
      imageLoading: false,
      loading: false,
      uid: data.uid,
      interestmanip: false,
      mentoringInterested: data.mentoring_interested,
      image: data.photo_url,
      available: data.available,
      linkedIn: data.linkedin,
      about: data.about == undefined ? "" : data.about,
      college_after_BTECH:
        data.college_after_BTECH != undefined ? data.college_after_BTECH : "",
      professor: "",
      specialization: "",
      longTermGoal: "",
      credential: "",
      name: data.name,
      course: data.course,
      yearOfGraduation: data.year_passed,
      institutionName: data.institution_name,
      gpa: data.gpa != undefined ? data.gpa : "",
      phoneNumber: data.phone_number,
      subject: "",
      mentoringInterest: "",
      interest: "",
      activity: "",
      shortTermGoal: "",
      rollNumber: data.roll_number,
      email: data.email,
      higherEducation:
        data.higher_education != undefined ? data.higher_education : "None",
      collegeName: data.collegeName,
      branch: data.branch,
      branchesList: [],
      coursesList: [],
      interestsList: data.interests_list,
      shortTermGoalsList: data.short_term_goals,
      activityList: data.extra_curricular_activities,
      longTermGoalsList: data.long_term_goals,
      mentoringInterestList:
        data.mentoring_interest_list != undefined
          ? data.mentoring_interest_list
          : [],
      higherEducationList: [],
      collegesList: [],
      institutionsList: [],
      subjectsList: data.subjects_liked != undefined ? data.subjects_liked : [],
      professorList:
        data.faculty_recommended != undefined ? data.faculty_recommended : [],
      preBuiltEventIndices:
        data.pre_built_events_indices != undefined
          ? data.pre_built_events_indices
          : [], //Used to store the preBuilt event indices

      pre_built_event_count: 0, //to  store the no. of preBuilt Events
      pre_built_list: [], //used to store the values selected in checkBoxes
      no_of_simple_events: data.mentoring_interested
        ? data.no_of_events["no_of_simple_events"]
        : null,
      no_of_bootcamp_events: data.mentoring_interested
        ? data.no_of_events["no_of_bootcamp_events"]
        : null,
      no_of_overview_events: data.mentoring_interested
        ? data.no_of_events["no_of_overview_events"]
        : null,
    };
  }

  async componentDidMount() {
    //getting all the details from database of different academics.
    fetch(urls.getHigherEducation, { method: "GET" })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        let d = Array.from(res);
        this.setState({ higherEducationList: d });
      })
      .catch((error) => {
        console.log(error);
        Toast.show({ text: "Some error occured", buttonText: "Okay" });
      });
    const data = await this.props.navigation.getParam("user");
    let updateInfo = this.props.navigation.getParam("updateInfo");
    await this.setState({
      userType: data.user_type,
      mentoringInterested: data.mentoring_interested,
      image: data.photo_url,
      available: data.available,
      linkedIn: data.linkedin,
      about: data.about == undefined ? "" : data.about,
      college_after_BTECH:
        data.college_after_BTECH != undefined ? data.college_after_BTECH : "",
      name: data.name,
      course: data.course,
      yearOfGraduation: data.year_passed,
      institutionName: data.institution_name,
      gpa: data.gpa != undefined ? data.gpa : "",
      phoneNumber: data.phone_number,
      collegeName: data.college_name,
      course: data.course,
      branch: data.branch,
      email: data.email,
      higherEducation:
        data.higher_education != undefined ? data.higher_education : "None",

      collegeName: data.college_name,
      branch: data.branch,
      interestsList: data.interests_list,
      shortTermGoalsList: data.short_term_goals,
      activityList: data.extra_curricular_activities,
      longTermGoalsList: data.long_term_goals,
      mentoringInterestList:
        data.mentoring_interest_list != undefined
          ? data.mentoring_interest_list
          : [],
      subjectsList: data.subjects_liked != undefined ? data.subjects_liked : [],
      professorList:
        data.faculty_recommended != undefined ? data.faculty_recommended : [],
      updateInfo: updateInfo,
      preBuiltEventIndices:
        data.pre_built_events_indices != undefined
          ? data.pre_built_events_indices
          : [],
      no_of_simple_events:
        data.no_of_events !== undefined && data.no_of_events !== null
          ? data.no_of_events["no_of_simple_events"]
          : null,
      no_of_bootcamp_events:
        data.no_of_events !== undefined && data.no_of_events !== null
          ? data.no_of_events["no_of_bootcamp_events"]
          : null,
      no_of_overview_events:
        data.no_of_events !== undefined && data.no_of_events !== null
          ? data.no_of_events["no_of_overview_events"]
          : null,
    });

    this.setState({ pre_built_event_count: PreBuiltEventTypes.length });
    // console.log("counnt" + this.state.pre_built_event_count);
    await this.setPreEvents();
    fetch(urls.getInstitutions)
      .then((res) => {
        return res.json();
      })
      .then(async (res) => {
        let d = Array.from(res);
        await this.setState({ institutionsList: d });
        await this.setState({
          collegesList: this.state.institutionsList.find(
            (x) => x.name == this.state.institutionName
          )
            ? this.state.institutionsList.find(
                (x) => x.name == this.state.institutionName
              ).colleges
            : [],
        });
        await this.setState({
          coursesList: this.state.collegesList.find(
            (x) => x.name == this.state.collegeName
          )
            ? this.state.collegesList.find(
                (x) => x.name == this.state.collegeName
              ).courses
            : [],
        });
        await this.setState({
          branchesList: this.state.coursesList.find(
            (x) => x.course == this.state.course
          )
            ? this.state.coursesList.find((x) => x.course == this.state.course)
                .branches
            : [],
        });
      })
      .catch((error) => {
        console.log(error);
        Toast.show({ text: "Some error occured", buttonText: "Okay" });
      });

    await this.setState({ institutionName: this.state.institutionName });

    await this.setState({
      collegesList: this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      )
        ? this.state.institutionsList.find(
            (x) => x.name == this.state.institutionName
          ).colleges
        : [],
    });
    await this.setState({
      coursesList: this.state.collegesList.find(
        (x) => x.name == this.state.collegeName
      )
        ? this.state.collegesList.find((x) => x.name == this.state.collegeName)
            .courses
        : [],
    });

    await this.setState({
      branchesList: this.state.coursesList.find(
        (x) => x.course == this.state.course
      )
        ? this.state.coursesList.find((x) => x.course == this.state.course)
            .branches
        : [],
    });
    await this.setState({ collegeName: this.state.collegeName });
    await this.setState({
      coursesList: this.state.collegesList.find(
        (x) => x.name == this.state.collegeName
      )
        ? this.state.collegesList.find((x) => x.name == this.state.collegeName)
            .courses
        : [],
    });
    // console.log(this.state.coursesList);
    await this.setState({
      branchesList: this.state.coursesList.find(
        (x) => x.course == this.state.course
      )
        ? this.state.coursesList.find((x) => x.course == this.state.course)
            .branches
        : [],
    });
  }

  setPreEvents = async () => {
    let preList = this.state.preBuiltEventIndices;
    //console.log(preList);
    let list = []; //0,1,3,4->true,true,false,true,true
    let k = 0;
    var t = this.state.pre_built_event_count;
    //console.log("t"+t);

    for (let i = 0; i < t; i++) {
      if (i == preList[k]) {
        list.push(true);
        k++;
      } else {
        list.push(false);
      }
    }
    //console.log("list"+list);
    await this.setState({ pre_built_list: list });
    //console.log("pre_built_list"+this.state.pre_built_list)
  };

  setFalseWhenNo = () => {
    console.log("No");
    let pre_List = this.state.pre_built_list;
    for (let i = 0; i < pre_List.length; i++) {
      pre_List[i] = false;
    }
    console.log(pre_List);
    this.setState({ pre_built_list: pre_List });
    console.log(this.state.pre_built_list);
  };

  toggle = async (index) => {
    let pre_list = this.state.pre_built_list;
    pre_list[index] = !pre_list[index];
    await this.setState({ pre_built_list: pre_list });
  };
  addMentoringInterest = () => {
    if (this.state.mentoringInterest.trim() == "") {
      Toast.show({
        text: "No mentoring interests to add.",
        buttonText: "Okay",
      });
      return;
    }
    let flag = 0;
    let s = this.state.mentoringInterest.trim();
    let l = this.state.mentoringInterestList;
    let i = 0;
    //checking mentor interest whether already exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      // if mentor interest not present then add into the list.
      let a = this.state.mentoringInterestList;
      a.push({ id: l.length, name: this.state.mentoringInterest.trim() });
      this.setState({ mentoringInterestList: a, mentoringInterest: "" });
    } else {
      Toast.show({
        text: "Mentoring Interest already added",
        buttonText: "Okay",
      });
    }
  };

  delteMentoringInterest = (name) => {
    if (this.state.mentoringInterestList.length == 0) {
      Toast.show({
        text: "No mentoring interests to delete",
        buttonText: "Okay",
      });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.mentoringInterestList;
    var i = 0;
    //check if mentor interest already present
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1); //if present deleting the mentor interest
      this.setState({ mentoringInterestList: l });
      this.setState({ mentoringInterest: "" });
    } else {
      Toast.show({ text: "Mentoring Interest not exists", buttonText: "Okay" });
      this.setState({ mentoringInterest: "" });
    }
  };

  addLongGoal = () => {
    if (this.state.longTermGoal.trim() == "") {
      Toast.show({ text: "No long term goals to add.", buttonText: "Okay" });
      return;
    }
    let flag = 0;
    let s = this.state.longTermGoal.trim();
    let l = this.state.longTermGoalsList;
    let i = 0;
    //checking if goal already present.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      // if goal not already present in list add it
      let a = this.state.longTermGoalsList;
      a.push({ id: l.length, name: this.state.longTermGoal.trim() });
      this.setState({ longTermGoalsList: a });
      this.setState({ longTermGoal: "" });
    } else {
      Toast.show({ text: "Long Term Goal already added", buttonText: "Okay" });
      this.setState({ longTermGoal: "" });
    }
  };

  delteLongGoal = (name) => {
    if (this.state.longTermGoalsList.length == 0) {
      Toast.show({ text: "No long term goals to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.longTermGoalsList;
    var i = 0;
    //checking if goal present.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ longTermGoalsList: l });
    } else {
      Toast.show({ text: "Long term Goal not exists", buttonText: "Okay" });
    }
  };

  addShortGoal = () => {
    if (this.state.shortTermGoal.trim() == "") {
      Toast.show({ text: "No short term goals to add.", buttonText: "Okay" });
      return;
    }
    let flag = 0;
    let s = this.state.shortTermGoal.trim();
    let l = this.state.shortTermGoalsList;
    let i = 0;
    //checking if goal already present.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      // if goal not already present in list add it
      let a = this.state.shortTermGoalsList;
      a.push({ id: l.length, name: this.state.shortTermGoal.trim() });
      this.setState({ shortTermGoalsList: a });
      this.setState({ shortTermGoal: "" });
    } else {
      Toast.show({ text: "Short Term Goal already added", buttonText: "Okay" });
      this.setState({ shortTermGoal: "" });
    }
  };

  delteShortGoal = (name) => {
    if (this.state.shortTermGoalsList.length == 0) {
      Toast.show({ text: "No short term goals to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.shortTermGoalsList;
    var i = 0;
    //check if goal exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ shortTermGoalsList: l });
    } else {
      Toast.show({ text: "Short term Goal not exists", buttonText: "Okay" });
    }
  };

  addActivity = () => {
    if (this.state.activity.trim() == "") {
      Toast.show({
        text: "No extra curricular acitvities to add.",
        buttonText: "Okay",
      });
      return;
    }
    var flag = 0;
    var s = this.state.activity.trim();
    var l = this.state.activityList;
    var i = 0;
    //check if activity exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      //if activity does not exist.
      var a = this.state.activityList;
      a.push({ id: l.length, name: this.state.activity.trim() });
      this.setState({ activityList: a });
      this.setState({ activity: "" });
    } else {
      Toast.show({
        text: "Extra curricular activity already added",
        buttonText: "Okay",
      });
      this.setState({ activity: "" });
    }
  };

  addSubject = () => {
    if (this.state.subject.trim() == "") {
      Toast.show({ text: "No subjects to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.subject.trim();
    var l = this.state.subjectsList;
    var i = 0;
    //check if subject already present in list.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      //if subject not present.
      var a = this.state.subjectsList;
      a.push({ id: l.length, name: this.state.subject.trim() });
      this.setState({ subjectsList: a });
      this.setState({ subject: "" });
    } else {
      this.setState({ subject: "" });
      Toast.show({ text: "Subject already added", buttonText: "Okay" });
    }
  };

  addInterest = () => {
    if (this.state.interest.trim() == "") {
      Toast.show({ text: "No academic interests to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.interest.trim();
    var l = this.state.interestsList;
    var i = 0;
    //check if interest already present.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      //if interest not present
      var a = this.state.interestsList;
      a.push({ id: l.length, name: this.state.interest.trim() });
      this.setState({ interestsList: a });
      this.setState({ interest: "" });
    } else {
      this.setState({ interest: "" });
      Toast.show({ text: "Academic interests added", buttonText: "Okay" });
    }
  };

  addFaculty = () => {
    if (this.state.professor.trim() == "") {
      Toast.show({ text: "No faculty to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.professor.trim();
    var l = this.state.professorList;
    var i = 0;
    //check if faculty already present
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    //if faculty not present.
    if (flag == 0) {
      var a = this.state.professorList;
      a.push({ id: l.length, name: this.state.professor.trim() });
      this.setState({ professorList: a });
      this.setState({ professor: "" });
    } else {
      this.setState({ professor: "" });
      Toast.show({ text: "Faculty already added", buttonText: "Okay" });
    }
  };

  delteActivity = (name) => {
    if (this.state.activityList.length == 0) {
      Toast.show({
        text: "No extra curricular activities to delete",
        buttonText: "Okay",
      });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.activityList;
    var i = 0;
    //check if activity exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ activityList: l });
      this.setState({ activity: "" });
    } else {
      this.setState({ activity: "" });
      Toast.show({
        text: "Extra curricular activity not exists",
        buttonText: "Okay",
      });
    }
  };

  delteSubject = (name) => {
    if (this.state.subjectsList.length == 0) {
      Toast.show({ text: "No subjects to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.subjectsList;
    var i = 0;
    //check if subject exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ subjectList: l });
      this.setState({ subject: "" });
    } else {
      this.setState({ subject: "" });
      Toast.show({ text: "Subject not exists", buttonText: "Okay" });
    }
  };

  deleteInterest = (name) => {
    if (this.state.interestsList.length == 0) {
      Toast.show({
        text: "No academic interests to delete",
        buttonText: "Okay",
      });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.interestsList;
    var i = 0;
    //check if interest exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ interestsList: l });
    } else {
      Toast.show({ text: "Academic interest not exists", buttonText: "Okay" });
    }
  };

  delteFaculty = (name) => {
    if (this.state.professorList.length == 0) {
      Toast.show({ text: "No faculty to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.professorList;
    var i = 0;
    //check if faculty exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ professorList: l });
      this.setState({ professor: "" });
    } else {
      Toast.show({ text: "Faculty not exists", buttonText: "Okay" });
      this.setState({ professor: "" });
    }
  };

  saveButton = () => {
    //They check whether all required fields are filled or not
    if (this.state.name.trim() == "") {
      Toast.show({ text: "Enter name", buttonText: "Okay" });
      return;
    }
    if (this.state.phoneNumber.trim().length == 0) {
      Toast.show({ text: "Enter valid phone Number", buttonText: "Okay" });
      return;
    }
    if (this.state.linkedIn.trim() == "") {
      Toast.show({ text: "Enter your linkedIn Profile", buttonText: "Okay" });
      return;
    }
    if (this.state.linkedIn.trim().indexOf("linkedin.com/in/") == -1) {
      Toast.show({
        text: "LinkedIn url must be of format linkedin.com/in/username",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.rollNumber.trim() == "") {
      Toast.show({ text: "Enter Roll number", buttonText: "Okay" });
      return;
    }
    if (this.state.institutionName.trim() == "None") {
      Toast.show({ text: "Select valid institution", buttonText: "Okay" });
      return;
    }
    if (this.state.collegeName.trim() == "None") {
      Toast.show({ text: "Select valid college", buttonText: "Okay" });
      return;
    }
    if (this.state.branch.trim() == "None") {
      Toast.show({ text: "Select valid branch ", buttonText: "Okay" });
      return;
    }
    if (this.state.course.trim() == "None") {
      Toast.show({ text: "Select valid course", buttonText: "Okay" });
      return;
    }
    if (this.state.yearOfGraduation.trim() === "") {
      Toast.show({ text: "Select graduation date", buttonText: "Okay" });
      return;
    }

    if (this.state.interestsList.length == 0) {
      Toast.show({
        text: "Enter atleast one academic interest",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.shortTermGoalsList.length == 0) {
      Toast.show({
        text: "Enter atleast one short term goal",
        buttonText: "Okay",
      });
      return;
    }

    if (this.state.longTermGoalsList.length == 0) {
      Toast.show({
        text: "Enter atleast one long term goal",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.activityList.length == 0) {
      Toast.show({
        text: "Enter atleast one extra curricular activity",
        buttonText: "Okay",
      });
      return;
    }
    if (
      this.state.mentoringInterested == true &&
      this.state.mentoringInterestList.length == 0
    ) {
      Toast.show({
        text: "Enter atleast one Mentoring Interest",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.imageLoading) {
      Toast.show({
        text: "Please wait image is uploading",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.collegesList.find((x) => x.name == this.state.collegeName)) {
      var collegeId = this.state.collegesList.find(
        (x) => x.name == this.state.collegeName
      ).id;
    }
    if (
      this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      )
    ) {
      var institutionId = this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      ).id;
    }

    let pre_list = this.state.pre_built_list;
    let list = [];
    for (let i = 0; i < pre_list.length; i++) {
      if (pre_list[i] == true) list.push(i);
    }

    let d = {
      last_logged_in: Date().toLocaleString(),
      uid: this.state.uid,
      phone_number: this.state.phoneNumber.trim(),
      available: this.state.available.trim(),
      name: this.state.name.trim(),
      email: this.state.email,
      branch: this.state.branch.trim(),
      photo_url: this.state.image,
      course: this.state.course.trim(),
      year_passed: this.state.yearOfGraduation.trim(),
      gpa: this.state.gpa.trim() === "" ? null : this.state.gpa.trim(),
      user_type: this.state.userType,
      higher_education: this.state.higherEducation,
      interests_list: this.state.interestsList,
      short_term_goals: this.state.shortTermGoalsList,
      long_term_goals: this.state.longTermGoalsList,
      extra_curricular_activities: this.state.activityList,
      mentoring_interest_list: this.state.mentoringInterestList,
      mentoring_interested: this.state.mentoringInterested,
      subjects_liked:
        this.state.subjectsList.length === 0 ? null : this.state.subjectsList,
      faculty_recommended:
        this.state.professorList === 0 ? null : this.state.professorList,
      college_after_BTECH:
        this.state.college_after_BTECH.trim() === ""
          ? null
          : this.state.college_after_BTECH.trim(),
      about: this.state.about.trim() === "" ? null : this.state.about.trim(),
      specialization:
        this.state.specialization.trim() === ""
          ? null
          : this.state.specialization.trim(),
      linkedin: this.state.linkedIn.trim(),
      college_name: this.state.collegeName,
      college_id: collegeId,
      institution_id: institutionId,
      last_updated_by: "user",
      institution_name: this.state.institutionName,
      roll_number: this.state.rollNumber,
      pre_built_events_indices: list.length === 0 ? null : list,
      // adding no_of_events in firebase, when a mentee changed his mentoringInterested to true.
      no_of_events:
        this.state.no_of_simple_events !== null
          ? {
              no_of_simple_events:
                this.state.no_of_simple_events !== null
                  ? this.state.no_of_simple_events
                  : 0,

              no_of_bootcamp_events:
                this.state.no_of_bootcamp_events !== null
                  ? this.state.no_of_bootcamp_events
                  : 0,

              no_of_overview_events:
                this.state.no_of_overview_events !== null
                  ? this.state.no_of_overview_events
                  : 0,
            }
          : this.state.mentoringInterested == true
          ? {
              no_of_simple_events: 0,

              no_of_bootcamp_events: 0,

              no_of_overview_events: 0,
            }
          : null,
    };

    console.log(d);
    console.log(d.about);
    this.setState({ loading: true });
    AsyncStorage.setItem("profile", JSON.stringify(d))
      .then(async () => {
        if (this.state.updateInfo != undefined) await this.state.updateInfo();
        this.props.navigation.navigate("ProfilePage");

        this.setState({ loading: false });
        fetch(urls.updateMentee, {
          method: "POST",
          body: JSON.stringify(d),
          headers: {
            "Content-type": "application/json; charset=UTF-8",
          },
        }).catch((err) => {
          console.log(err);
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  uploadPhoto = async () => {
    try {
      //Asking user for storage permissions.
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status == "granted") {
        //Selecting user from local storage.
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Image,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });

        if (!result.cancelled) {
          this.setState({ image: result.uri, imageLoading: true });
          console.log("RESPONSE", result.uri);
          const response = await fetch(result.uri);

          const blob = await response.blob(); //getting the binary form of image.
          //storing the image binary data in cloud storage.

          var ref = firebase
            .storage()
            .ref()
            .child(`profile_images/${this.state.email}`)
            .put(blob);
          Toast.show({ text: "Uploading Image", buttonText: "Okay" });
          return ref.on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
              // Observe state change events such as progress, pause, and resume
              // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
              let progress =
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              console.log("Upload is " + progress + "% done");
              switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                  console.log("Upload is paused");
                  break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                  console.log("Upload is running");
                  break;
              }
            },
            (error) => {
              // Handle unsuccessful uploads
              console.log("Upload unsuccessfull");
            },
            () => {
              // Handle successful uploads on complete
              // For instance, get the download URL: https://firebasestorage.googleapis.com/...
              Toast.show({ text: "Uploaded Image", buttonText: "Okay" });
              ref.snapshot.ref.getDownloadURL().then((downloadURL) => {
                console.log("File available at", downloadURL);
                this.setState({ image: downloadURL, imageLoading: false });
              });
            }
          );
        }
        // console.log(result);
      } else {
        Alert.alert("Required storage permissions.");
      }
    } catch (E) {
      console.log(E);
    }
  };

  render() {
    return (
      <View style={styles.overall}>
        <KeyboardAvoidingView
          behavior={Platform.Os == "ios" ? "padding" : "height"}
          style={styles.keyboardView}
        >
          <ScrollView
            style={styles.container}
            keyboardShouldPersistTaps="handled"
          >
            <View style={styles.detailsContainer}>
              <View style={styles.profilepic}>
                <Avatar
                  onPress={() => {
                    this.uploadPhoto();
                  }}
                  editButton={{
                    name: "mode-edit",
                    type: "material",
                    color: "#fff",
                    underlayColor: "#0f0",
                  }}
                  showEditButton
                  size={100}
                  rounded
                  source={{ uri: this.state.image }}
                  avatarStyle={{}}
                  icon={{ name: "user", type: "font-awesome" }}
                />
              </View>
              <Text style={styles.heading}>Contact Info:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Name <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Name"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.name}
                  onChangeText={(text) => this.setState({ name: text })}
                />
                <Text style={styles.subtitle}>
                  Email Id <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  editable={false}
                  placeholder="Email"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.email}
                  onChangeText={(text) => this.setState({ email: text })}
                />
                <Text style={styles.subtitle}>
                  Phone Number <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Mobile Number"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  keyboardType={"numeric"}
                  value={this.state.phoneNumber}
                  onChangeText={(text) => this.setState({ phoneNumber: text })}
                />
                <Text style={styles.subtitle}>
                  LinkedIn Profile <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="LinkedIn profile url..linkedin.com/in/username"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.linkedIn}
                  onChangeText={(text) => this.setState({ linkedIn: text })}
                />
              </View>
              <Text style={styles.heading}>College Info:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Roll No. <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Enter roll number in block letters"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={
                    this.state.rollNumber
                      ? this.state.rollNumber.trim().toUpperCase()
                      : ""
                  }
                  onChangeText={(text) =>
                    this.setState({ rollNumber: text.toUpperCase() })
                  }
                />
                <View>
                  <View style={{ marginTop: 5 }}>
                    <Text style={styles.subtitle}>Institution</Text>
                    <Text
                      style={{ fontSize: 17, marginBottom: 5, marginStart: 3 }}
                    >
                      {this.state.institutionName}
                    </Text>
                  </View>
                  <View style={{ marginTop: 5 }}>
                    <Text style={styles.subtitle}>College</Text>
                    <Text
                      style={{ fontSize: 17, marginBottom: 5, marginStart: 3 }}
                    >
                      {this.state.collegeName}
                    </Text>
                  </View>
                </View>
                {/* 
                <Dropdown
                  label="Institution *"
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  animationDuration={0}
                  data={this.state.institutionsList}
                  value={this.state.institutionName}
                  valueExtractor={(item, index) => {
                    return item.name;
                  }}
                  onChangeText={async (v, i, d) => {
                    await this.setState({ institutionName: v });
                    await this.setState({
                      collegeName: "None",
                      course: "None",
                      branch: "None",
                    });
                    await this.setState({
                      collegesList: this.state.institutionsList.find(
                        (x) => x.name == this.state.institutionName
                      )
                        ? this.state.institutionsList.find(
                            (x) => x.name == this.state.institutionName
                          ).colleges
                        : [],
                    });
                    await this.setState({
                      coursesList: this.state.collegesList.find(
                        (x) => x.name == this.state.collegeName
                      )
                        ? this.state.collegesList.find(
                            (x) => x.name == this.state.collegeName
                          ).courses
                        : [],
                    });

                    await this.setState({
                      branchesList: this.state.coursesList.find(
                        (x) => x.course == this.state.course
                      )
                        ? this.state.coursesList.find(
                            (x) => x.course == this.state.course
                          ).branches
                        : [],
                    });
                  }}
                />

                <Dropdown
                  label="College *"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.collegesList}
                  value={this.state.collegeName}
                  valueExtractor={(item, index) => {
                    return item.name;
                  }}
                  onChangeText={async (v, i, d) => {
                    await this.setState({ collegeName: v });
                    await this.setState({ course: "None", branch: "None" });
                    await this.setState({
                      coursesList: this.state.collegesList.find(
                        (x) => x.name == this.state.collegeName
                      )
                        ? this.state.collegesList.find(
                            (x) => x.name == this.state.collegeName
                          ).courses
                        : [],
                    });
                    // console.log(this.state.coursesList);
                    await this.setState({
                      branchesList: this.state.coursesList.find(
                        (x) => x.course == this.state.course
                      )
                        ? this.state.coursesList.find(
                            (x) => x.course == this.state.course
                          ).branches
                        : [],
                    });
                  }}
                /> */}
                <Dropdown
                  label="Course *"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.coursesList}
                  value={this.state.course}
                  valueExtractor={(item, index) => {
                    return item.course;
                  }}
                  onChangeText={async (v, i, d) => {
                    await this.setState({ course: v });
                    await this.setState({ branch: "None" });
                    await this.setState({
                      branchesList: this.state.coursesList.find(
                        (x) => x.course == this.state.course
                      )
                        ? this.state.coursesList.find(
                            (x) => x.course == this.state.course
                          ).branches
                        : [],
                    });
                  }}
                />
                <Dropdown
                  label="Branch *"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.branchesList}
                  value={this.state.branch}
                  valueExtractor={(item, index) => {
                    return item.branch;
                  }}
                  onChangeText={async (v, i, d) => {
                    this.setState({ branch: v });
                  }}
                />
                <Dropdown
                  label="Highest Education"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.higherEducationList}
                  value={this.state.higherEducation}
                  valueExtractor={(item, index) => {
                    return item.name;
                  }}
                  onChangeText={async (v, i, d) => {
                    this.setState({ higherEducation: v });
                  }}
                />

                <Text style={styles.subtitle}>Specialization</Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Specialization"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.specialization}
                  onChangeText={(text) =>
                    this.setState({ specialization: text })
                  }
                />
                <Text style={styles.subtitle}>
                  {" "}
                  Year of Graduation <Text style={styles.req}>*</Text>
                </Text>
                <DatePicker
                  style={{
                    width: Dimensions.get("window").width * 0.9,
                    marginTop: 10,
                    marginBottom: 10,
                  }}
                  date={this.state.yearOfGraduation}
                  mode="date"
                  placeholder="select date"
                  format="YYYY-MM-DD"
                  minDate="1800-01-01"
                  // maxDate="2030-01-01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 4,
                      marginLeft: 0,
                    },
                    dateInput: {
                      marginLeft: 36,
                    },
                  }}
                  onDateChange={(date) => {
                    this.setState({ yearOfGraduation: date });
                  }}
                />
                <Text style={styles.subtitle}>GPA/Percentage</Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="GPA/Percentage"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  keyboardType={"numeric"}
                  value={this.state.gpa}
                  onChangeText={(text) => this.setState({ gpa: text })}
                />
                <Text style={styles.subtitle}>College After B.TECH</Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="College name"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.college_after_BTECH}
                  onChangeText={(text) =>
                    this.setState({ college_after_BTECH: text })
                  }
                />
                <Text style={styles.subtitle}>
                  Subjects liked at your college
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Subject name and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.subject}
                    onChangeText={(text) => this.setState({ subject: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={{
                      backgroundColor: "rgba(25,100,200,1)",
                    }}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addSubject()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.subjectsList.map((l, i) => (
                    <View key={i} style={styles.listsubContainer}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Avatar
                        size="small"
                        overlayContainerStyle={styles.deleteOverlay}
                        rounded
                        icon={{
                          name: "times-circle",
                          color: "black",
                          type: "font-awesome",
                        }}
                        onPress={() => this.delteSubject(l.name)}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
              </View>

              <Text style={styles.heading}>Goal Setting:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Short Term Goals <Text style={styles.req}>*</Text>
                </Text>
                <Text>Goal expected to be completed beyond one year</Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Short Term Goals and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.shortTermGoal}
                    onChangeText={(text) =>
                      this.setState({ shortTermGoal: text })
                    }
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={{
                      backgroundColor: "rgba(25,100,200,1)",
                    }}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addShortGoal()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.shortTermGoalsList.map((l, i) => (
                    <View key={i} style={styles.listsubContainer}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Avatar
                        size="small"
                        overlayContainerStyle={styles.deleteOverlay}
                        rounded
                        icon={{
                          name: "times-circle",
                          color: "black",
                          type: "font-awesome",
                        }}
                        onPress={() => this.delteShortGoal(l.name)}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
                <Text style={styles.subtitle}>
                  Long Term Goals <Text style={styles.req}>*</Text>
                </Text>
                <Text>
                  Something you want to achieve for your successful career{" "}
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Long Term Goals and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.longTermGoal}
                    onChangeText={(text) =>
                      this.setState({ longTermGoal: text })
                    }
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={{
                      backgroundColor: "rgba(25,100,200,1)",
                    }}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addLongGoal()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.longTermGoalsList.map((l, i) => (
                    <View key={i} style={styles.listsubContainer}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Avatar
                        size="small"
                        overlayContainerStyle={styles.deleteOverlay}
                        rounded
                        icon={{
                          name: "times-circle",
                          color: "black",
                          type: "font-awesome",
                        }}
                        onPress={() => this.delteLongGoal(l.name)}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
              </View>

              <Text style={styles.heading}>Mentoring Interests:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Mentoring Interest Areas (If Interested)
                </Text>
                <Text>
                  Topics you are proficient and passionate to share with others
                </Text>
                <View style={styles.interestsView}>
                  <CheckBox
                    center
                    title="Yes"
                    checkedIcon="dot-circle-o"
                    containerStyle={{ backgroundColor: "white" }}
                    uncheckedIcon="circle-o"
                    checked={this.state.mentoringInterested}
                    onPress={() => {
                      this.setState((prev) => {
                        return {
                          mentoringInterested: !prev.mentoringInterested,
                        };
                      });
                    }}
                  />
                  <CheckBox
                    center
                    title="No"
                    checkedIcon="dot-circle-o"
                    containerStyle={{ backgroundColor: "white" }}
                    uncheckedIcon="circle-o"
                    checked={!this.state.mentoringInterested}
                    onPress={() => {
                      this.setFalseWhenNo();
                      this.setState((prev) => {
                        return {
                          mentoringInterested: !prev.mentoringInterested,
                        };
                      });
                    }}
                  />
                </View>
                {this.state.mentoringInterested && (
                  //here
                  <View>
                    <FlatList
                      data={PreBuiltEventTypes}
                      style={{ flex: 1 }}
                      renderItem={({ item, index }) => {
                        return (
                          <CheckBox
                            containerStyle={{ backgroundColor: "white" }}
                            checked={this.state.pre_built_list[index]}
                            title={item.name}
                            onPress={() => {
                              this.toggle(index);
                            }}
                          />
                        );
                      }}
                    />

                    <View style={styles.interestsStyle}>
                      <TextInput
                        selectTextOnFocus={true}
                        placeholder="Enter Mentoring Interests and press + "
                        underlineColorAndroid="transparent"
                        style={styles.adderLists}
                        value={this.state.mentoringInterest}
                        onChangeText={(text) =>
                          this.setState({ mentoringInterest: text })
                        }
                      />
                      <Avatar
                        size="medium"
                        overlayContainerStyle={{
                          backgroundColor: "rgba(25,100,200,1)",
                        }}
                        rounded
                        icon={{
                          name: "plus-circle",
                          color: "white",
                          type: "font-awesome",
                        }}
                        onPress={() => this.addMentoringInterest()}
                        activeOpacity={0.7}
                      />
                    </View>
                    <View style={styles.interestsContentContainer}>
                      {this.state.mentoringInterestList.map((l, i) => (
                        <View key={i} style={styles.listsubContainer}>
                          <Text style={styles.interestsTextStyle}>
                            {l.name}
                          </Text>
                          <Avatar
                            size="small"
                            overlayContainerStyle={styles.deleteOverlay}
                            rounded
                            icon={{
                              name: "times-circle",
                              color: "black",
                              type: "font-awesome",
                            }}
                            onPress={() => this.delteMentoringInterest(l.name)}
                            containerStyle={styles.deleteContainerStyle}
                            activeOpacity={0.7}
                          />
                        </View>
                      ))}
                    </View>
                  </View>
                )}
              </View>
              <Text style={styles.heading}>Additional Info:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Academic Interests <Text style={styles.req}>*</Text>
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Interests and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.interest}
                    onChangeText={(text) => this.setState({ interest: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={{
                      backgroundColor: "rgba(25,100,200,1)",
                    }}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addInterest()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.interestsList.map((l, i) => (
                    <View key={i} style={styles.listsubContainer}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Avatar
                        size="small"
                        overlayContainerStyle={styles.deleteOverlay}
                        rounded
                        icon={{
                          name: "times-circle",
                          color: "black",
                          type: "font-awesome",
                        }}
                        onPress={() => this.deleteInterest(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
                <Text style={styles.subtitle}>
                  Extra Curricular Activities <Text style={styles.req}>*</Text>
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Extra Curricular Activities and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.activity}
                    onChangeText={(text) => this.setState({ activity: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={{
                      backgroundColor: "rgba(25,100,200,1)",
                    }}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addActivity()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.activityList.map((l, i) => (
                    <View key={i} style={styles.listsubContainer}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Avatar
                        size="small"
                        overlayContainerStyle={styles.deleteOverlay}
                        rounded
                        icon={{
                          name: "times-circle",
                          color: "black",
                          type: "font-awesome",
                        }}
                        onPress={() => this.delteActivity(l.name)}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
                <Text style={styles.subtitle}>
                  Faculty you would recommend at your college
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter faculty name and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.professor}
                    onChangeText={(text) => this.setState({ professor: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={{
                      backgroundColor: "rgba(25,100,200,1)",
                    }}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addFaculty()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.professorList.map((l, i) => (
                    <View key={i} style={styles.listsubContainer}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Avatar
                        size="small"
                        overlayContainerStyle={styles.deleteOverlay}
                        rounded
                        icon={{
                          name: "times-circle",
                          color: "black",
                          type: "font-awesome",
                        }}
                        onPress={() => this.delteFaculty(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
                <Text style={styles.subtitle}>Available Timings </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Available timings"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.available}
                  onChangeText={(text) => this.setState({ available: text })}
                />
                <Text style={styles.subtitle}>
                  Additional information about you{" "}
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="About"
                  underlineColorAndroid="transparent"
                  multiline={true}
                  textAlignVertical="top"
                  numberOfLines={10}
                  style={styles.about}
                  value={this.state.about}
                  onChangeText={(text) => this.setState({ about: text })}
                />
              </View>
              <View style={styles.save}>
                <Button
                  icon={
                    <Icon
                      type="font-awesome"
                      name="save"
                      size={20}
                      color="white"
                    />
                  }
                  containerStyle={styles.saveContainerStyle}
                  buttonStyle={styles.saveButtonStyle}
                  onPress={() => {
                    this.saveButton();
                  }}
                  titleStyle={styles.saveButtonTitleStyle}
                  title="Save"
                />
                {this.state.loading && <ActivityIndicator size="large" />}
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  interestsView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  listsubContainer: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    borderRadius: 10,
    borderWidth: 2,
    backgroundColor: "rgba(100,100,255,0.4)",
    alignItems: "center",
  },
  keyboardView: { flex: 1 },
  profilepic: {
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
  },
  overall: { flex: 1, backgroundColor: "white" },
  about: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.9,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    height: 100,
    textAlign: "left",
    backgroundColor: "white",
  },
  deleteContainerStyle: { padding: 0 },
  saveButtonTitleStyle: { padding: 5 },
  saveContainerStyle: { padding: 10 },
  save: { flexDirection: "row", justifyContent: "center" },
  saveButtonStyle: {
    backgroundColor: "rgba(200,175,0,1)",
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    width: Dimensions.get("window").width * 0.4,
  },
  req: { color: "red" },
  dateStyle: {
    width: Dimensions.get("window").width * 0.9,
    marginTop: 10,
    marginBottom: 10,
  },
  interestsStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  interestsContentContainer: { flexDirection: "row", flexWrap: "wrap" },
  interestsSubContent: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    borderRadius: 10,
    borderWidth: 2,
    backgroundColor: "rgba(100,100,255,0.4)",
    alignItems: "center",
  },
  interestsTextStyle: { fontSize: 15, fontWeight: "bold" },
  deleteOverlay: { backgroundColor: "rgba(100,255,255,0)" },
  adderLists: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.76,
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    backgroundColor: "white",
  },
  TextInputStyle: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.9,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    backgroundColor: "white",
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "bold",
  },
  detailsContainer: {
    flexDirection: "column",
    justifyContent: "flex-start",
    margin: Dimensions.get("window").width * 0.03,
    alignContent: "flex-start",
  },
  titleStyle: {
    fontSize: 25,
    fontWeight: "bold",
    textAlign: "center",
  },
  logoContainer: {
    flex: 0.75,
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: "lightblue",
  },
  container: {
    flex: 1,
    flexDirection: "column",
  },
  logoStyle: {
    borderRadius: 5,
    height: 100,
    width: 200,
  },
  dropDownStyle: { paddingBottom: 5, fontWeight: "bold" },

  headingView: {
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "grey",
    padding: 5,
    width: Dimensions.get("window").width * 0.9375,
    backgroundColor: "whitesmoke",
    marginBottom: 5,
  },
  heading: {
    fontSize: 20,
    fontWeight: "bold",
    marginTop: 10,
    color: "black",
  },
});
