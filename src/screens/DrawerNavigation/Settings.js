import React from "react";
import { Notifications } from "expo";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  Platform,
  Switch,
  AsyncStorage,
  StyleSheet,
} from "react-native";
import { urls } from "./../../../env.json";

export default class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isEnabled: false,
      expoPushToken: "",
      isProfileEnabled: false,
    };
  }
  async componentDidMount() {
    let d = await AsyncStorage.getItem("profile");
    console.log(d);
    let d1 = await this.props.navigation.getParam("updateInfo");
    await this.setState({ profile: JSON.parse(d) });
    if (
      this.state.profile.expo_push_token == undefined ||
      this.state.profile.expo_push_token.trim() == ""
    ) {
      await this.setState({
        isEnabled: false,
        expoPushToken: "",
      });
    } else if (this.state.profile.expo_push_token != "") {
      await this.setState({
        isEnabled: true,
        expoPushToken: d.expo_push_token,
      });
    }
    console.log(this.state.profile);
    await this.setState({
      isProfileEnabled: this.state.profile["display_profile"],
    });
  }

  registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        alert("Failed to get push token for push notification!");
        return;
      }
      let token = await Notifications.getExpoPushTokenAsync().catch((err) =>
        console.log(error)
      );
      await this.setState({ expoPushToken: token });
    } else alert("Must use physical device for Push Notifications");

    if (Platform.OS === "android") {
      Notifications.createChannelAndroidAsync("default", {
        name: "default",
        sound: true,
        priority: "max",
        vibrate: [0, 250, 250, 250],
      });
    }
  };

  handleNotificationChange = async (v) => {
    await this.setState({ isEnabled: v });
    if (this.state.isEnabled) await this.registerForPushNotificationsAsync();
    else await this.setState({ expoPushToken: "" });
    if (
      this.state.profile.user_type == "mentee" ||
      this.state.profile.user_type == "license_mentee"
    ) {
      fetch(urls.updateMentee, {
        method: "POST",
        body: JSON.stringify({
          expo_push_token: this.state.expoPushToken,
          uid: this.state.profile.uid,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      })
        .then(async () => {
          let profile = this.state.profile;
          profile["expo_push_token"] = this.state.expoPushToken;
          await AsyncStorage.setItem("profile", JSON.stringify(profile));
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      fetch(urls.updateAlumniJNTUKMentor, {
        method: "POST",
        body: JSON.stringify({
          expo_push_token: this.state.expoPushToken,
          uid: this.state.profile.uid,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      })
        .then(async () => {
          let profile = this.state.profile;
          profile["expo_push_token"] = this.state.expoPushToken;
          await AsyncStorage.setItem("profile", JSON.stringify(profile));
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  render() {
    return (
      <ScrollView style={styles.overall}>
        <View style={styles.container}>
          <Text style={styles.textStyle}>Notifications</Text>
          <View style={styles.toggleView}>
            <Switch
              trackColor={{ false: "#767577", true: "#81b0ff" }}
              thumbColor={this.state.isEnabled ? "#f4f3f4" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={async (v) => {
                await this.handleNotificationChange(v);
              }}
              value={this.state.isEnabled}
            />
          </View>
        </View>
        <View style={styles.container}>
          <Text style={styles.textStyle}>Enable Profile</Text>
          <View style={styles.toggleView}>
            <Switch
              trackColor={{ false: "#767577", true: "#81b0ff" }}
              thumbColor={this.state.isEnabled ? "#f4f3f4" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={async (v) => {
                await this.setState({ isProfileEnabled: v });
                if (
                  this.state.profile.user_type == "mentee" ||
                  this.state.profile.user_type == "license_mentee"
                ) {
                  fetch(urls.updateMentee, {
                    method: "POST",
                    body: JSON.stringify({
                      display_profile: this.state.isProfileEnabled,
                      uid: this.state.profile.uid,
                    }),
                    headers: {
                      "Content-type": "application/json; charset=UTF-8",
                    },
                  })
                    .then(async () => {
                      let profile = this.state.profile;
                      profile["display_profile"] = this.state.isProfileEnabled;
                      await AsyncStorage.setItem(
                        "profile",
                        JSON.stringify(profile)
                      );
                    })
                    .catch((err) => {
                      console.log(err);
                    });
                } else {
                  fetch(urls.updateAlumniJNTUKMentor, {
                    method: "POST",
                    body: JSON.stringify({
                      display_profile: this.state.isProfileEnabled,
                      uid: this.state.profile.uid,
                    }),
                    headers: {
                      "Content-type": "application/json; charset=UTF-8",
                    },
                  })
                    .then(async () => {
                      let profile = this.state.profile;
                      profile["display_profile"] = this.state.isProfileEnabled;
                      await AsyncStorage.setItem(
                        "profile",
                        JSON.stringify(profile)
                      );
                    })
                    .catch((err) => {
                      console.log(err);
                    });
                }
              }}
              value={this.state.isProfileEnabled}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  overall: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff",
  },
  container: {
    flexDirection: "row",
    justifyContent: "flex-start",
    height: Dimensions.get("window").height * 0.07,
    alignItems: "center",
    borderBottomWidth: 0.2,
    borderBottomColor: "black",
  },
  textStyle: {
    flex: 0.75,
    fontSize: 18,
    padding: 20,
  },
  toggleView: {
    flex: 0.25,
    padding: 15,
  },
});
