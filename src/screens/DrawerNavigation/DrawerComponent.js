import React, { Component } from "react";
import {
  View,
  StyleSheet,
  AsyncStorage,
  Dimensions,
  ScrollView,
  ImageBackground,
  Text,
  Alert,
} from "react-native";
import { Avatar, ListItem } from "react-native-elements";
import firebase from "firebase";
import Constants from "expo-constants";

let listToReder = [
  { Home: "Home", icon: "home", title: "Home" },
  { Home: "ProfilePage", icon: "user", title: "Profile" },
  { Home: "SettingsPage", icon: "cogs", title: "Settings" },
];

export default class DraweComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      image:
        "https://firebasestorage.googleapis.com/v0/b/collegekonnect-app.appspot.com/o/profile_pic.png?alt=media&token=4152469c-0a75-4704-9da8-bd948944e278",
    };
  }

  navigateToScreen = (route) => {
    this.getData();
    if (route == "ProfilePage") {
      this.props.navigation.navigate(route, { updateInfo: this.getData });
    } else {
      this.props.navigation.navigate(route);
    }
  };

  getData = () => {
    AsyncStorage.getItem("profile")
      .then((d) => {
        //getting details from storage and setting details of photo url.
        return JSON.parse(d);
      })
      .then((d) => {
        this.setState({ image: d.photo_url });
        this.setState({ user: d });
      })
      .catch((err) => console.log(err));
  };

  componentDidMount() {
    this.getData();

    // StatusBar.setBackgroundColor('#ffffff');
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getData();
      }
    );
  }

  componentWillUnmount() {
    if (this.willFocusSubscription != undefined)
      this.willFocusSubscription.remove();
  }

  signOut = async () => {
    if (this.state.user != null) await firebase.auth().signOut();
    AsyncStorage.setItem("signout", "true")
      .then((d) => {
        this.props.navigation.navigate("AuthScreen");
      })
      .catch((err) => console.log(err));
  };

  keyExtractor = (item, index) => index.toString();

  render() {
    return (
      <ScrollView style={styles.container} stickyHeaderIndices={[0]}>
        <View style={styles.headerContainer}>
          <ImageBackground
            source={require("../../../assets/sideBarBackground.png")}
            style={styles.imageBackground}
          >
            <Text style={styles.titleStyle}>MentorKonnect</Text>
            <View style={styles.imageView}>
              <Avatar
                editButton={{
                  name: "mode-edit",
                  type: "material",
                  color: "#fff",
                  underlayColor: "#0f0",
                }}
                size={Dimensions.get("window").width * 0.35}
                rounded
                source={{ uri: this.state.image }}
                avatarStyle={{}}
                icon={{ name: "user", type: "font-awesome" }}
              />
            </View>
          </ImageBackground>
        </View>
        <ScrollView>
          {listToReder.map((item, index) => {
            return (
              <ListItem
                key={index}
                title={item.title}
                leftAvatar={
                  <Avatar
                    size="small"
                    icon={{ name: item.icon, type: "font-awesome" }}
                    rounded
                    activeOpacity={0.7}
                  />
                }
                containerStyle={{}}
                chevron
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.navigateToScreen(item.Home);
                }}
              />
            );
          })}
          {this.state.user.updateprof == undefined &&
            this.state.user.user_type === "license_mentee" && (
              <View>
                <ListItem
                  key={4}
                  title={"Activate Profile"}
                  leftAvatar={
                    <Avatar
                      size="small"
                      icon={{ name: "check", type: "font-awesome" }}
                      rounded
                      activeOpacity={0.7}
                    />
                  }
                  containerStyle={{}}
                  chevron
                  onPress={() => {
                    this.props.navigation.toggleDrawer();
                    this.navigateToScreen("ActivatePage");
                  }}
                />
              </View>
            )}
          
          <ListItem
            key={5}
            title={"FAQ's"}
            leftAvatar={
              <Avatar
                size="small"
                icon={{ name: "question-circle", type: "font-awesome" }}
                rounded
                activeOpacity={0.7}
              />
            }
            chevron
            onPress={() => {
              this.props.navigation.toggleDrawer();
              this.navigateToScreen("FaqPage");
            }}
          ></ListItem>
          <ListItem
            key={6}
            title={"About"}
            leftAvatar={
              <Avatar
                size="small"
                icon={{ name: "circle", type: "font-awesome" }}
                rounded
                activeOpacity={0.7}
              />
            }
            chevron
            onPress={() => {
              this.props.navigation.toggleDrawer();
              this.navigateToScreen("AboutPage");
            }}
          ></ListItem>
          <ListItem
            key={7}
            title={"Signout"}
            leftAvatar={
              <Avatar
                size="small"
                icon={{ name: "sign-out", type: "font-awesome" }}
                rounded
                activeOpacity={0.7}
              />
            }
            chevron
            onPress={() => {
              this.props.navigation.toggleDrawer();
              Alert.alert(
                "Sign out",
                "Are you sure you want to signout?",
                [
                  {
                    text: "Cancel",
                    style: "cancel",
                  },
                  { text: "OK", onPress: () => this.signOut() },
                ],
                { cancelable: false }
              );
            }}
          ></ListItem>
        </ScrollView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  imageView: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  titleStyle: {
    fontSize: 25,
    color: "orange",
    fontWeight: "bold",
    marginBottom: 10,
  },
  statusBar: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    width: Dimensions.get("window").width * 0.7,
    flexDirection: "column",
  },
  imageBackground: {
    paddingVertical: 15,
    alignItems: "center",
    height: Dimensions.get("window").height * 0.3,
  },
  headerText: {
    textTransform: "capitalize",
    fontSize: 30,
    fontWeight: "bold",
    color: "orange",
  },
  headerContainer: {
    flex: 1,
    flexDirection: "column",
    alignContent: "center",
  },
});
