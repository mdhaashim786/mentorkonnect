import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  AsyncStorage,
  Linking,
  Alert,
  ImageBackground,
  ActivityIndicator,
  Share,
  Button,
  Modal,
  TouchableOpacity,
} from "react-native";
import firebase from "firebase";
import { Icon, Avatar } from "react-native-elements";
import {
  ScrollView,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import ViewShot from "react-native-view-shot";
import { Ionicons } from "@expo/vector-icons";
import Logos from "../../../assets/Logos";
import PreBuiltEventTypes from "./../Events/PreBuiltEventTypes";
import * as MediaLibrary from "expo-media-library";
import * as Font from "expo-font";
import { AppLoading } from "expo";
import * as Permissions from "expo-permissions";
let customFonts = {
  LobsterRegular: require("../../../assets/fonts/LobsterRegular-rR8O.ttf"),
};
export default class ProfileDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: null,
      badgeview: null,
      modalVisible: false,
      showme: false,
      imageuri: "",
      badgelink: "",
      fontsLoaded: false,
    };
  }
  async _loadFontsAsync() {
    await Font.loadAsync(customFonts);
    this.setState({ fontsLoaded: true });
  }
  getData = async () => {
    let d = await AsyncStorage.getItem("profile");
    console.log("d" + d);
    let updateInfo = await this.props.navigation.getParam("updateInfo");
    await this.setState({ updateInfo: updateInfo });
    await this.setState({ profile: JSON.parse(d) });
  };

  componentDidMount = async () => {
    //TO load the fonts
    this._loadFontsAsync();
    await this.getData();

    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getData();
      }
    );
  };

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  download = async () => {
    const imagee = this.state.imageuri;
    //console.log("IMAGE", imagee);
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status === "granted") {
      const asset = await MediaLibrary.createAssetAsync(imagee);
      //console.log("ASSESt", asset);
      await MediaLibrary.createAlbumAsync("Download", asset, false).then(() => {
        alert("Download Successful");
      });
    } else {
      alert("Required storage permissions.");
    }
  };
  onShare = async () => {
    const imagee1 = this.state.imageuri;
    const response = await fetch(imagee1);
    //console.log("MUY URL", imagee1);

    const blob = await response.blob(); //getting the binary form of image.
    //storing the image binary data in cloud storage.
    var ref = firebase
      .storage()
      .ref()
      .child(`badge_link/${this.state.profile.email}`)
      .put(blob);
    await firebase
      .storage()
      .ref()
      .child(`badge_link/${this.state.profile.email}`)
      .getDownloadURL()
      .then((downloadURL) => {
        //console.log("File available at", downloadURL);
        this.setState({ badgelink: downloadURL });
      })
      .catch(function (error) {
        // Handle any errors
        console.log("Error", error);
      });

    //console.log("BADGE LINK", this.state.badgelink);
    this.share();
  };
  share = async () => {
    try {
      const badgelink = this.state.badgelink;

      const result = await Share.share({
        message: badgelink,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  // For showing the modal
  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  };
  showmeVisible = (v) => {
    this.setState({ showme: v });
  };
  //for capturing the viewshot
  onCapture = (uri) => {
    this.setState({ imageuri: uri });
    //console.log("do something with ", this.state.imageuri);
  };
  render() {
    const { modalVisible } = this.state;
    if (this.state.profile != null) {
      var badge = null;
      if (this.state.profile.no_of_events != undefined) {
        //assigning badges to mentors based on no.of simple events,no of bootcamp events and no of overview events completed by the mentor.

        const simple = this.state.profile.no_of_events["no_of_simple_events"]; //getting no of simple evnts of the mentor.
        const overview = this.state.profile.no_of_events[
          "no_of_overview_events"
        ]; //getting no of overview evnts of the mentor.
        const bootcamp = this.state.profile.no_of_events[
          "no_of_bootcamp_events"
        ]; //getting no of bootcamp evnts of the mentor.
        // based on the conditions,respective badge is stored in badge variable.
        if (simple >= 20 || overview >= 7 || bootcamp >= 3)
          badge = Logos.platinum;
        else if (
          (simple >= 10 && simple < 20) ||
          (overview >= 4 && overview < 7) ||
          bootcamp == 2
        )
          badge = Logos.gold;
        else if (
          (simple >= 5 && simple < 10) ||
          (overview >= 1 && overview < 4) ||
          bootcamp == 1
        )
          badge = Logos.silver;
        else if (simple >= 1 && simple < 5) badge = Logos.bronze;
        else badge = null;
      }
      if (this.state.fontsLoaded) {
        return (
          <ScrollView>
            <View style={styles.imageStyle}>
              <View style={styles.containerStyle}>
                <Icon
                  raised
                  name="edit"
                  type="font-awesome"
                  color="#f50"
                  onPress={() => {
                    if (this.state.profile.user_type == "mentor")
                      this.props.navigation.navigate("EditProfileAlumniPage", {
                        user: this.state.profile,
                        updateInfo: this.state.updateInfo,
                      });
                    else
                      this.props.navigation.navigate("EditProfileMenteePage", {
                        user: this.state.profile,
                        updateInfo: this.state.updateInfo,
                      });
                  }}
                />
              </View>
              <View style={styles.imageContainer}>
                <View style={styles.imageViewStyle}>
                  <Avatar
                    size={100}
                    rounded
                    source={{ uri: this.state.profile.photo_url }}
                    avatarStyle={{}}
                    icon={{ name: "user", type: "font-awesome" }}
                  />
                </View>
                <View style={styles.imageBesideText}>
                  <Text
                    style={{
                      ...styles.subTextStyle,
                      textTransform: "capitalize",
                    }}
                  >
                    {this.state.profile.name}
                  </Text>

                  {this.state.profile.roll_number && (
                    <Text
                      style={{
                        ...styles.subTextStyle,
                        textTransform: "uppercase",
                      }}
                    >
                      {this.state.profile.roll_number}
                    </Text>
                  )}
                  <Text style={styles.subTextStyle}>
                    {this.state.profile.email}
                  </Text>
                  <Text style={styles.subTextStyle}>
                    {this.state.profile.branch}
                  </Text>
                  <Text style={styles.subTextStyle}>
                    {this.state.profile.college_name}
                  </Text>
                </View>
              </View>
            </View>
            {badge != null && (
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  height: 160,

                  marginTop: 3,
                }}
              >
                <View
                  style={{
                    borderWidth: 2,
                    height: 160,
                    flexDirection: "row",
                    alignItems: "center",
                    margin: 4,
                    justifyContent: "space-around",
                  }}
                >
                  <View style={{ justifyContent: "flex-start" }}>
                    <Text
                      style={{
                        fontSize: 26,
                        fontWeight: "bold",
                        textTransform: "capitalize",
                        marginEnd: 10,
                      }}
                    >
                      My Achievements
                    </Text>

                    <Text>(Long press badge to download and share)</Text>
                  </View>
                  <View style={styles.badgeViewStyle}>
                    <View>
                      <TouchableWithoutFeedback
                        onLongPress={() => this.setModalVisible(true)}
                      >
                        <ImageBackground
                          style={{
                            width: 70,
                            height: 140,
                          }}
                          resizeMode="contain"
                          source={{ uri: badge }}
                        >
                          <View style={styles.badge_1stview}>
                            <View style={styles.badge_2ndview}>
                              <Text style={styles.nameonbadge}>
                                {this.state.profile.name.split(" ", 1)}
                              </Text>
                            </View>
                          </View>
                        </ImageBackground>
                      </TouchableWithoutFeedback>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                      {this.state.modalVisible && (
                        <View style={styles.centeredView}>
                          <Modal
                            animationType="slide"
                            transparent={true}
                            visible={modalVisible}
                            onRequestClose={() => {
                              Alert.alert("Modal has been closed.");
                            }}
                          >
                            <View style={styles.centeredView}>
                              <View style={styles.modalView}>
                                <Ionicons
                                  name="ios-close"
                                  size={38}
                                  color="black"
                                  style={{ alignSelf: "flex-end" }}
                                  onPress={() => {
                                    this.setModalVisible(false);
                                  }}
                                />
                                <ViewShot
                                  onCapture={this.onCapture}
                                  captureMode="continuous"
                                  options={{ format: "png", quality: 1 }}
                                >
                                  <ImageBackground
                                    style={{
                                      width: 220,
                                      height: 310,
                                      marginTop: 6,
                                    }}
                                    resizeMode="contain"
                                    source={{ uri: badge }}
                                  >
                                    <View style={styles.modalbadge_1stview}>
                                      <View style={styles.modalbadge_2ndview}>
                                        <Text style={styles.name_onmodalbadge}>
                                          {this.state.profile.name}
                                        </Text>
                                      </View>
                                    </View>
                                  </ImageBackground>
                                </ViewShot>

                                <View
                                  style={{
                                    flexDirection: "column",
                                    justifyContent: "space-evenly", // added
                                  }}
                                >
                                  <View style={{ marginVertical: 10 }}>
                                    <Button
                                      style={{
                                        ...styles.openButton,
                                        backgroundColor: "#2196F3",
                                      }}
                                      onPress={() => {
                                        this.setModalVisible(false);
                                        this.download();
                                      }}
                                      title="DOWNLOAD"
                                    ></Button>
                                  </View>
                                  <View style={{ marginVertical: 10 }}>
                                    <Button
                                      style={{
                                        ...styles.openButton,
                                        backgroundColor: "#2196F3",
                                      }}
                                      onPress={() => {
                                        this.setModalVisible(false);
                                        this.onShare();
                                      }}
                                      title="SHARE"
                                    ></Button>
                                  </View>
                                </View>
                              </View>
                            </View>
                          </Modal>
                        </View>
                      )}
                    </View>
                  </View>
                </View>
              </View>
            )}
            <View style={styles.details}>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ ...styles.titleStyle, marginEnd: 10 }}>
                  {this.state.profile.name}
                </Text>
                <TouchableOpacity
                  style={{
                    borderWidth: 2,
                    borderColor: "#4682b4",
                    borderRadius: 6,
                  }}
                  onPress={() =>
                    Linking.openURL(`${this.state.profile.linkedin}`)
                  }
                >
                  <Image
                    source={require("../../../assets/linkedin-icon.png")}
                    style={{
                      width: 28,
                      height: 28,
                      margin: 0.4,
                    }}
                  />
                </TouchableOpacity>
              </View>
              {this.state.profile.current_working_title != undefined && (
                <View>
                  <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                    Working Title
                  </Text>
                  <Text style={styles.about}>
                    {this.state.profile.current_working_title}
                  </Text>
                </View>
              )}
              {this.state.profile.current_working_company != undefined && (
                <View>
                  <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                    Working Company
                  </Text>
                  <Text style={styles.about}>
                    {this.state.profile.current_working_company}
                  </Text>
                </View>
              )}
              {this.state.profile.about != "" &&
                this.state.profile.about != undefined && (
                  <View>
                    <Text style={styles.subHeading}>About</Text>
                    <Text style={styles.about}>{this.state.profile.about}</Text>
                  </View>
                )}
              {this.state.profile.gpa != undefined &&
                this.state.profile.gpa != "" && (
                  <View>
                    <Text style={styles.subHeading}>GPA</Text>
                    <Text
                      style={{ ...styles.about, marginBottom: 5, fontSize: 15 }}
                    >
                      {this.state.profile.gpa}
                    </Text>
                  </View>
                )}

              {this.state.profile.year_passed != "" &&
                this.state.profile.year_passed != undefined && (
                  <View>
                    <Text style={styles.subHeading}>Year of Graduation</Text>
                    <Text style={styles.about}>
                      {this.state.profile.year_passed}
                    </Text>
                  </View>
                )}
              {this.state.profile.course != undefined &&
                this.state.profile.course != "None" && (
                  <View>
                    <Text style={styles.subHeading}>Course</Text>
                    <Text style={styles.about}>
                      {this.state.profile.course}
                    </Text>
                  </View>
                )}

              {this.state.profile.college_after_BTECH != "" &&
                this.state.profile.college_after_BTECH != undefined && (
                  <View>
                    <Text style={styles.subHeading}>College After B.TECH</Text>
                    <Text style={styles.about}>
                      {this.state.profile.college_after_BTECH}
                    </Text>
                  </View>
                )}
              {this.state.profile.higher_education != undefined &&
                this.state.profile.higher_education != "None" &&
                this.state.profile.higher_education != "" && (
                  <View>
                    <Text style={styles.subHeading}>Higher Education</Text>
                    <Text style={styles.about}>
                      {this.state.profile.higher_education}
                    </Text>
                  </View>
                )}
              {this.state.profile.specialization != undefined &&
                this.state.profile.specialization != "" && (
                  <View>
                    <Text style={styles.subHeading}>Specialization</Text>
                    <Text style={styles.about}>
                      {this.state.profile.specialization}
                    </Text>
                  </View>
                )}
              {this.state.profile.available != undefined &&
                this.state.profile.available != "" && (
                  <View>
                    <Text style={styles.subHeading}>Available Time</Text>
                    <Text style={styles.about}>
                      {this.state.profile.available}
                    </Text>
                  </View>
                )}
              {this.state.profile.phone_number != undefined &&
                this.state.profile.phone_number != "" && (
                  <View>
                    <Text style={styles.subHeading}>Phone Number</Text>
                    <Text style={styles.about}>
                      {this.state.profile.phone_number}
                    </Text>
                  </View>
                )}
              <Text style={styles.subHeading}>Academic Interests</Text>
              <View style={styles.renderContainer}>
                {this.state.profile.interests_list.map((l, i) => (
                  <View key={i} style={styles.insideView}>
                    <Text style={styles.subTexttitle}>{l.name}</Text>
                  </View>
                ))}
              </View>
              {this.state.profile.mentoring_interest_list != undefined &&
                this.state.profile.mentoring_interest_list.length != 0 && (
                  <View>
                    <Text style={styles.subHeading}>Mentoring Interests</Text>
                    <View style={styles.renderContainer}>
                      {this.state.profile.mentoring_interest_list.map(
                        (l, i) => (
                          <View key={i} style={styles.insideView}>
                            <Text style={styles.subTexttitle}>{l.name}</Text>
                          </View>
                        )
                      )}
                      {/* Used to display preBuilt Events when profile is opened */}

                      {this.state.profile.pre_built_events_indices !=
                        undefined &&
                        this.state.profile.pre_built_events_indices.length !=
                          0 &&
                        this.state.profile.pre_built_events_indices.map(
                          (l, i) => (
                            <View key={i} style={styles.insideView}>
                              {PreBuiltEventTypes.map((item, id) => (
                                <View key={id}>
                                  {l == id && (
                                    <Text style={styles.subTexttitle}>
                                      {item.name}
                                    </Text>
                                  )}
                                </View>
                              ))}
                            </View>
                          )
                        )}
                    </View>
                  </View>
                )}

              {this.state.profile.patents != undefined && (
                <View>
                  <Text style={styles.subHeading}>Patents</Text>
                  <View style={styles.renderContainer}>
                    {this.state.profile.patents.map((l, i) => (
                      <View key={i} style={styles.insideView}>
                        <Text style={styles.subTexttitle}>{l.name}</Text>
                      </View>
                    ))}
                  </View>
                </View>
              )}
              {this.state.profile.extra_curricular_activities != undefined && (
                <View>
                  <Text style={styles.subHeading}>
                    Extra Curricular Activites
                  </Text>
                  <View style={styles.renderContainer}>
                    {this.state.profile.extra_curricular_activities.map(
                      (l, i) => (
                        <View key={i} style={styles.insideView}>
                          <Text style={styles.subTexttitle}>{l.name}</Text>
                        </View>
                      )
                    )}
                  </View>
                </View>
              )}
              {this.state.profile.short_term_goals != undefined && (
                <View>
                  <Text style={styles.subHeading}>Short Term Goals</Text>
                  <View style={styles.renderContainer}>
                    {this.state.profile.short_term_goals.map((l, i) => (
                      <View key={i} style={styles.insideView}>
                        <Text style={styles.subTexttitle}>{l.name}</Text>
                      </View>
                    ))}
                  </View>
                </View>
              )}
              {this.state.profile.long_term_goals != undefined &&
                this.state.profile.long_term_goals.length != 0 && (
                  <View>
                    <Text style={styles.subHeading}>Long Term Goals</Text>
                    <View style={styles.renderContainer}>
                      {this.state.profile.long_term_goals.map((l, i) => (
                        <View key={i} style={styles.insideView}>
                          <Text style={styles.subTexttitle}>{l.name}</Text>
                        </View>
                      ))}
                    </View>
                  </View>
                )}
              {this.state.profile.subjects_liked != undefined &&
                this.state.profile.subjects_liked.length != 0 && (
                  <View>
                    <Text style={styles.subHeading}>
                      Subjects Liked At your college
                    </Text>
                    <View style={styles.renderContainer}>
                      {this.state.profile.subjects_liked.map((l, i) => (
                        <View key={i} style={styles.insideView}>
                          <Text style={styles.subTexttitle}>{l.name}</Text>
                        </View>
                      ))}
                    </View>
                  </View>
                )}
              {this.state.profile.faculty_recommended != undefined &&
                this.state.profile.faculty_recommended.length != 0 && (
                  <View>
                    <Text style={styles.subHeading}>
                      Faculty Recommended At your college
                    </Text>
                    <View style={styles.renderContainer}>
                      {this.state.profile.faculty_recommended.map((l, i) => (
                        <View key={i} style={styles.insideView}>
                          <Text style={styles.subTexttitle}>{l.name}</Text>
                        </View>
                      ))}
                    </View>
                  </View>
                )}
            </View>
          </ScrollView>
        );
      } else {
        return <AppLoading />;
      }
    } else {
      return (
        <View style={styles.indicator}>
          <ActivityIndicator size="large"></ActivityIndicator>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  renderContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  subTexttitle: {
    fontSize: 15,
    fontWeight: "bold",
  },
  indicator: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  imageStyle: {
    flex: 1,
    flexDirection: "column",
    height: Dimensions.get("window").height * 0.3,
    backgroundColor: "#a3c0e1",
  },
  modalbadge_1stview: {
    borderRadius: 50,
    flexWrap: "wrap",
    marginLeft: 65.3,
    marginTop: 86.8,
    height: 90,
    width: 90,
    justifyContent: "center",
    alignItems: "center",
  },
  modalbadge_2ndview: {
    height: 80,
    width: 80,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10.1,
  },
  name_onmodalbadge: {
    fontSize: 14,
    color: "#fffff0",
    fontWeight: "900",
    fontFamily: "LobsterRegular",
    textTransform: "capitalize",
    textAlign: "center",
  },
  badge_1stview: {
    borderRadius: 50,
    flexWrap: "wrap",
    marginLeft: 12.8,
    marginTop: 36.8,
    height: 45,
    width: 45,
    justifyContent: "center",
    alignItems: "center",
  },
  badge_2ndview: {
    height: 35,
    width: 35,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
  },
  nameonbadge: {
    fontSize: 8,
    color: "#fffff0",
    fontWeight: "900",
    textAlign: "center",
    fontFamily: "LobsterRegular",
    textTransform: "capitalize",
  },
  insideView: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    borderRadius: 10,
    borderWidth: 2,
    alignItems: "center",
    borderColor: "grey",
  },
  subTextStyle: {
    color: "white",
    fontWeight: "bold",
    marginBottom: 5,
    fontSize: 15,
  },
  containerStyle: {
    flexDirection: "row",
    justifyContent: "flex-end",
    height: Dimensions.get("window").height * 0.03,
    marginBottom: 10,
  },
  subHeading: {
    fontSize: 20,
    fontWeight: "bold",
    marginVertical: 5,
    marginStart: 3,
  },
  about: {
    fontSize: 17,
    marginBottom: 5,
    marginStart: 3,
  },
  imageContainer: {
    height: Dimensions.get("window").height * 0.25,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  details: {
    flexDirection: "column",
    padding: 10,
  },
  imageViewStyle: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: Dimensions.get("window").height * 0.01,
    paddingLeft: 10,
    paddingRight: 20,
  },
  badgeViewStyle: {
    marginRight: Dimensions.get("window").height * 0.01,
  },
  titleStyle: {
    fontSize: 23,
    fontWeight: "bold",
    textTransform: "capitalize",
    marginStart: 3,
  },
  emailStyle: {
    color: "rgba(200,0,0,0.8)",
    fontSize: 15,
  },
  imageBesideText: {
    flex: 2,
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "center",
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    backgroundColor: "#fffafa",
    borderRadius: 20,

    paddingHorizontal: Dimensions.get("window").width * 0.03,

    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    height: Dimensions.get("window").height * 0.7,
    width: Dimensions.get("window").width * 0.66,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
});
