import React from "react";
import { Text, View, StyleSheet, Dimensions, ScrollView } from "react-native";
import { FontAwesome } from "@expo/vector-icons";

export default class AppContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ishandle: false,
      ishandle1: false,
      ishandle2: false,
      ishandle3: false,
      ishandle4: false,
      ishandle5: false,
      ishandle6: false,
      ishandle7: false,
      ishandle8: false,
      ishandle9: false,
    };
  }
  render() {
    return (
      <View style={styles.overAllView}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            {this.state.ishandle == true && (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <Text style={{ paddingRight: 15, fontWeight: "700" }}>
                      1.{""}
                    </Text>
                  </View>
                  <Text
                    style={{
                      ...styles.fieldBoldStyle,
                      marginRight: Dimensions.get("window").width * 0.23,
                    }}
                  >
                    What is the MentorKonnect ?
                  </Text>

                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-up"
                      size={27}
                      color="black"
                      style={{ marginTop: 5 }}
                      onPress={() => {
                        if (this.state.ishandle == false) {
                          this.setState({ ishandle: true });
                        } else {
                          this.setState({ ishandle: false });
                        }
                      }}
                    />
                  </View>
                </View>
                <Text style={styles.fieldStyle}>
                  MentorKonnect is a platform that connects Mentors and Mentees.
                  In the world of MentorKonnect, mentors are people who offer
                  mentoring to current students (mentees).
                </Text>
              </View>
            )}
            {this.state.ishandle == false && (
              <View style={styles.cardStyle}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View
                    style={{ paddingRight: 15, marginRight: 5, marginLeft: 2 }}
                  >
                    <Text style={{ fontWeight: "700" }}>1.</Text>
                  </View>
                  <View style={{ marginRight: 1 }}>
                    <Text
                      style={{
                        ...styles.fieldBoldStyle,  
                        marginRight: Dimensions.get("window").width * 0.25,
                      }}
                    >
                      What is the MentorKonnect ?
                    </Text>
                  </View>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-down"
                      size={27}
                      color="black"
                      style={{ marginTop: 5, marginBottom: 10 }}
                      onPress={() => {
                        if (this.state.ishandle == false) {
                          this.setState({ ishandle: true });
                        } else {
                          this.setState({ ishandle: false });
                        }
                      }}
                    />
                  </View>
                </View>
              </View>
            )}
          </View>
          <View style={styles.container}>
            {this.state.ishandle1 == true && (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <View>
                      <Text style={{ fontWeight: "700", paddingRight: 15 }}>
                        2.
                      </Text>
                    </View>
                  </View>
                  <Text
                    style={{
                      ...styles.fieldBoldStyle,
                      marginRight: Dimensions.get("window").width * 0.21,
                    }}
                  >
                    Who qualifies to be a mentor ?
                  </Text>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-up"
                      size={27}
                      color="black"
                      style={{ marginTop: 5 }}
                      onPress={() => {
                        if (this.state.ishandle1 == false) {
                          this.setState({ ishandle1: true });
                        } else {
                          this.setState({ ishandle1: false });
                        }
                      }}
                    />
                  </View>
                </View>
                <Text style={styles.fieldStyle}>
                  MentorKonnect has alumni of any college, industry experienced
                  professionals in their field as mentors.
                </Text>
              </View>
            )}
            {this.state.ishandle1 == false && (
              <View style={styles.cardStyle}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ paddingRight: 15, marginRight: 5, marginLeft: 2}}>
                    <Text style={{ fontWeight: "700" }}>2.</Text>
                  </View>
                  <View style={{  marginRight: 1 }}>
                    <Text
                      style={{
                        ...styles.fieldBoldStyle,

                        marginRight: Dimensions.get("window").width * 0.225,
                      }}
                    >
                      Who qualifies to be a mentor ?
                    </Text>
                  </View>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-down"
                      size={27}
                      color="black"
                      style={{ marginTop: 5, marginBottom: 10 }}
                      onPress={() => {
                        if (this.state.ishandle1 == false) {
                          this.setState({ ishandle1: true });
                        } else {
                          this.setState({ ishandle1: false });
                        }
                      }}
                    />
                  </View>
                </View>
              </View>
            )}
          </View>
          <View style={styles.container}>
            {this.state.ishandle2 == true && (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <View>
                      <Text style={{ fontWeight: "700", paddingRight: 15 }}>
                        3.
                      </Text>
                    </View>
                  </View>
                  <Text style={{
                       ...styles.fieldBoldStyle,
                        marginRight: Dimensions.get("window").width *0.21,
                      }}>
                    What are the different types of mentoring events?
                  </Text>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-up"
                      size={27}
                      color="black"
                      style={{ marginTop: 5 }}
                      onPress={() => {
                        if (this.state.ishandle2 == false) {
                          this.setState({ ishandle2: true });
                        } else {
                          this.setState({ ishandle2: false });
                        }
                      }}
                    />
                  </View>
                </View>
                <Text style={styles.fieldStyle}>
                  Mentoring events are 3 types.
                </Text>
                <View
                  style={{
                    paddingRight: 15,
                    marginRight: 5,
                  }}
                >
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 1. </Text>
                    <Text style={styles.mattertext}>
                      Simple event is a one-to-one type. Example: First/second
                      year student (mentee in this case) asking final year
                      students (mentors in this case) for guidance while
                      choosing electives.
                      {"\n"}
                    </Text>
                  </View>
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 2. </Text>
                    <Text style={styles.mattertext}>
                      Orientation/ Guest lecture event type is generally a
                      one-time group meeting where a mentor gives an overview of
                      a topic/subject. Example: “Trends in computer networks
                      industry” – A mentoring event by an alumni student
                      graduated in 1990 - 1994.
                      {"\n"}
                    </Text>
                  </View>
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 3. </Text>

                    <Text style={styles.mattertext}>
                      Bootcamp event type is a recurring intensive training to
                      improve a specific skill significantly. This usually spans
                      across 5-15 weeks. Example: “Mobile Application
                      development using ReactJS” – A mentoring event by an
                      alumni student graduated in 1990 - 1994.
                    </Text>
                  </View>
                </View>
              </View>
            )}
            {this.state.ishandle2 == false && (
              <View style={styles.cardStyle}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View
                    style={{ paddingRight: 15, marginRight: 5, marginLeft: 2 }}
                  >
                    <Text style={{ fontWeight: "700" }}>3.</Text>
                  </View>
                  <View style={{ marginRight: 1 }}>
                    <Text style={{
                         ...styles.fieldBoldStyle,
                        marginRight: Dimensions.get("window").width *0.22,
                      }}>
                      What are the different types of mentoring events?
                    </Text>
                  </View>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-down"
                      size={27}
                      color="black"
                      style={{ marginTop: 5, marginBottom: 10 }}
                      onPress={() => {
                        if (this.state.ishandle2 == false) {
                          this.setState({ ishandle2: true });
                        } else {
                          this.setState({ ishandle2: false });
                        }
                      }}
                    />
                  </View>
                </View>
              </View>
            )}
          </View>
          <View style={styles.container}>
            {this.state.ishandle3 == true && (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <Text style={{ paddingRight: 15, fontWeight: "700" }}>
                      4.{""}
                    </Text>
                  </View>
                  <Text
                    style={{
                      ...styles.fieldBoldStyle,
                      marginRight: Dimensions.get("window").width * 0.13,
                    }}
                  >
                    How do I put a mentoring request?
                  </Text>

                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-up"
                      size={27}
                      color="black"
                      style={{ marginTop: 5 }}
                      onPress={() => {
                        if (this.state.ishandle3 == false) {
                          this.setState({ ishandle3: true });
                        } else {
                          this.setState({ ishandle3: false });
                        }
                      }}
                    />
                  </View>
                </View>

                <View
                  style={{
                    paddingRight: 15,
                    marginRight: 5,
                  }}
                >
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 1. </Text>
                    <Text style={styles.mattertext}>
                      Search for a mentor / mentoring interest {"\n"}
                    </Text>
                  </View>
                  <Text style={styles.orstyle}>OR</Text>
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 2. </Text>
                    <Text style={styles.mattertext}>
                      Connect to a mentor in his/her profile page or simply
                      click on ‘+’ button in the event page and fill the
                      required details.{" "}
                    </Text>
                  </View>
                </View>
              </View>
            )}
            {this.state.ishandle3 == false && (
              <View style={styles.cardStyle}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View
                    style={{ paddingRight: 15, marginRight: 5, marginLeft: 2 }}
                  >
                    <Text style={{ fontWeight: "700" }}>4.</Text>
                  </View>
                  <View style={{ marginRight: 1 }}>
                    <Text
                      style={{
                        ...styles.fieldBoldStyle,
                        marginRight: Dimensions.get("window").width * 0.16,
                      }}
                    >
                      How do I put a mentoring request?
                    </Text>
                  </View>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-down"
                      size={27}
                      color="black"
                      style={{ marginTop: 5, marginBottom: 10 }}
                      onPress={() => {
                        if (this.state.ishandle3 == false) {
                          this.setState({ ishandle3: true });
                        } else {
                          this.setState({ ishandle3: false });
                        }
                      }}
                    />
                  </View>
                </View>
              </View>
            )}
          </View>
          <View style={styles.container}>
            {this.state.ishandle4 == true && (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <Text style={{ paddingRight: 15, fontWeight: "700" }}>
                      5.{""}
                    </Text>
                  </View>
                  <Text style={{
                         ...styles.fieldBoldStyle,
                        marginRight: Dimensions.get("window").width * 0.05,
                      }}>
                    What is the main goal of MentorKonnect? What do mentors and
                    mentees get from this platform?
                  </Text>

                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-up"
                      size={27}
                      color="black"
                      style={{ marginTop: 5 }}
                      onPress={() => {
                        if (this.state.ishandle4 == false) {
                          this.setState({ ishandle4: true });
                        } else {
                          this.setState({ ishandle4: false });
                        }
                      }}
                    />
                  </View>
                </View>
                <Text style={styles.fieldStyle}>
                  Main goal of the MentorKonnect platform is to promote
                  collaboration between industry and academia. It is a platform
                  that connects experts (mentors) with current students
                  (mentees). For mentors, this is one more way to share their
                  knowledge with current students. For mentees, this platform
                  provides opportunities to learn from / work with seniors.
                </Text>
              </View>
            )}
            {this.state.ishandle4 == false && (
              <View style={styles.cardStyle}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View
                    style={{ paddingRight: 15, marginRight: 5, marginLeft: 2 }}
                  >
                    <Text style={{ fontWeight: "700" }}>5.</Text>
                  </View>
                  <View style={{ marginRight: 1 }}>
                    <Text style={{
                         ...styles.fieldBoldStyle,

                        marginRight: Dimensions.get("window").width * 0.13,
                      }}>
                      What is the main goal of MentorKonnect? What do mentors
                      and mentees get from this platform?
                    </Text>
                  </View>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-down"
                      size={27}
                      color="black"
                      style={{ marginTop: 5, marginBottom: 10 }}
                      onPress={() => {
                        if (this.state.ishandle4 == false) {
                          this.setState({ ishandle4: true });
                        } else {
                          this.setState({ ishandle4: false });
                        }
                      }}
                    />
                  </View>
                </View>
              </View>
            )}
          </View>
          <View style={styles.container}>
            {this.state.ishandle5 == true && (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <Text style={{ paddingRight: 15, fontWeight: "700" }}>
                      6.{""}
                    </Text>
                  </View>
                  <Text
                    style={{
                      ...styles.fieldBoldStyle,
                      marginRight: Dimensions.get("window").width * 0.07,
                    }}
                  >
                    Is there any way to preview an event?
                  </Text>

                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-up"
                      size={27}
                      color="black"
                      style={{ marginTop: 5 }}
                      onPress={() => {
                        if (this.state.ishandle5 == false) {
                          this.setState({ ishandle5: true });
                        } else {
                          this.setState({ ishandle5: false });
                        }
                      }}
                    />
                  </View>
                </View>
                <Text style={styles.fieldStyle}>
                  No, there is no such thing to preview an event.
                </Text>
              </View>
            )}
            {this.state.ishandle5 == false && (
              <View style={styles.cardStyle}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View
                    style={{ paddingRight: 15, marginRight: 5, marginLeft: 2 }}
                  >
                    <Text style={{ fontWeight: "700" }}>6.</Text>
                  </View>
                  <View style={{ marginRight: 1 }}>
                    <Text
                      style={{
                        ...styles.fieldBoldStyle,
                        marginRight: Dimensions.get("window").width * 0.09,
                      }}
                    >
                      Is there any way to preview an event?
                    </Text>
                  </View>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-down"
                      size={27}
                      color="black"
                      style={{ marginTop: 5, marginBottom: 10 }}
                      onPress={() => {
                        if (this.state.ishandle5 == false) {
                          this.setState({ ishandle5: true });
                        } else {
                          this.setState({ ishandle5: false });
                        }
                      }}
                    />
                  </View>
                </View>
              </View>
            )}
          </View>
          <View style={styles.container}>
            {this.state.ishandle6 == true && (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <Text style={{ paddingRight: 15, fontWeight: "700" }}>
                      7.{""}
                    </Text>
                  </View>
                  <Text style={{
                        ...styles.fieldBoldStyle,

                        marginRight: Dimensions.get("window").width * 0.24,
                      }}>
                    What If I don't like a mentor's mentoring ?
                  </Text>

                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-up"
                      size={27}
                      color="black"
                      style={{ marginTop: 5 }}
                      onPress={() => {
                        if (this.state.ishandle6 == false) {
                          this.setState({ ishandle6: true });
                        } else {
                          this.setState({ ishandle6: false });
                        }
                      }}
                    />
                  </View>
                </View>
                <Text style={styles.fieldStyle}>
                  If you don't like a mentor's mentoring you can directly write
                  a feedback to that mentor or else switch to another mentor.
                </Text>
              </View>
            )}
            {this.state.ishandle6 == false && (
              <View style={styles.cardStyle}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View
                    style={{ paddingRight: 15, marginRight: 5, marginLeft: 2 }}
                  >
                    <Text style={{ fontWeight: "700" }}>7.</Text>
                  </View>
                  <View style={{ marginRight: 1 }}>
                    <Text style={{
                        ...styles.fieldBoldStyle,
                        marginRight: Dimensions.get("window").width * 0.25,
                      }}>
                      What If I don't like a mentor's mentoring ?
                    </Text>
                  </View>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-down"
                      size={27}
                      color="black"
                      style={{ marginTop: 5, marginBottom: 10 }}
                      onPress={() => {
                        if (this.state.ishandle6 == false) {
                          this.setState({ ishandle6: true });
                        } else {
                          this.setState({ ishandle6: false });
                        }
                      }}
                    />
                  </View>
                </View>
              </View>
            )}
          </View>
          <View style={styles.container}>
            {this.state.ishandle7 == true && (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <Text style={{ paddingRight: 15, fontWeight: "700" }}>
                      8.{""}
                    </Text>
                  </View>
                  <Text
                    style={{
                      ...styles.fieldBoldStyle,
                      marginRight: Dimensions.get("window").width * 0.11,
                    }}
                  >
                    What if a mentor rejects an event ?
                  </Text>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-up"
                      size={27}
                      color="black"
                      style={{ marginTop: 5 }}
                      onPress={() => {
                        if (this.state.ishandle7 == false) {
                          this.setState({ ishandle7: true });
                        } else {
                          this.setState({ ishandle7: false });
                        }
                      }}
                    />
                  </View>
                </View>
                <Text style={styles.fieldStyle}>
                  If so the displayed status is open with a rejected reason, and
                  you can select another mentor.
                </Text>
              </View>
            )}
            {this.state.ishandle7 == false && (
              <View style={styles.cardStyle}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View
                    style={{ paddingRight: 15, marginRight: 5, marginLeft: 2 }}
                  >
                    <Text style={{ fontWeight: "700" }}>8.</Text>
                  </View>
                  <View style={{ marginRight: 1 }}>
                    <Text
                      style={{
                        ...styles.fieldBoldStyle,
                        marginRight: Dimensions.get("window").width * 0.13,
                      }}
                    >
                      What if a mentor rejects an event ?
                    </Text>
                  </View>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-down"
                      size={27}
                      color="black"
                      style={{ marginTop: 5, marginBottom: 10 }}
                      onPress={() => {
                        if (this.state.ishandle7 == false) {
                          this.setState({ ishandle7: true });
                        } else {
                          this.setState({ ishandle7: false });
                        }
                      }}
                    />
                  </View>
                </View>
              </View>
            )}
          </View>
          <View style={styles.container}>
            {this.state.ishandle8 == true && (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <View>
                      <Text style={{ fontWeight: "700", paddingRight: 15 }}>
                        9.
                      </Text>
                    </View>
                  </View>
                  <Text style={{
                         ...styles.fieldBoldStyle,
                        marginRight: Dimensions.get("window").width * 0.19,
                      }}>
                    How would a mentor acquires a badge?
                  </Text>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-up"
                      size={27}
                      color="black"
                      style={{ marginTop: 5 }}
                      onPress={() => {
                        if (this.state.ishandle8 == false) {
                          this.setState({ ishandle8: true });
                        } else {
                          this.setState({ ishandle8: false });
                        }
                      }}
                    />
                  </View>
                </View>
                <Text style={styles.fieldStyle}>Mentor can obtain a badge whenever he/she completes a specific no. of mentoring events. </Text>
                <Text style={styles.fieldStyle}>Badges are 4 types:</Text>
                <View
                  style={{
                    paddingRight: 15,
                    marginRight: 5,
                  }}
                >
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 1. </Text>
                    <Text style={styles.mattertext}>
                      Bronze : mentor must complete atleast one simple event.
                      {"\n"}
                    </Text>
                  </View>
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 2. </Text>
                    <Text style={styles.mattertext}>
                      Silver : mentor must complete five simple events or one
                      overview event or one bootcamp event.
                      {"\n"}
                    </Text>
                  </View>
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 3. </Text>

                    <Text style={styles.mattertext}>
                      Gold : mentor must complete ten simple events or four
                      overview events or two bootcamp events. {"\n"}
                    </Text>
                  </View>
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 4. </Text>

                    <Text style={styles.mattertext}>
                      Platinum : mentor must complete twenty simple events or
                      seven overview events or three bootcamp events.{" "}
                    </Text>
                  </View>
                </View>
              </View>
            )}
            {this.state.ishandle8 == false && (
              <View style={styles.cardStyle}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View
                    style={{ paddingRight: 15, marginRight: 5, marginLeft: 2 }}
                  >
                    <Text style={{ fontWeight: "700" }}>9.</Text>
                  </View>
                  <View style={{ marginRight: 1 }}>
                    <Text style={{
                         ...styles.fieldBoldStyle,

                        marginRight: Dimensions.get("window").width * 0.2,
                      }}>
                      How would a mentor acquires a badge?
                    </Text>
                  </View>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-down"
                      size={27}
                      color="black"
                      style={{ marginTop: 5, marginBottom: 10 }}
                      onPress={() => {
                        if (this.state.ishandle8 == false) {
                          this.setState({ ishandle8: true });
                        } else {
                          this.setState({ ishandle8: false });
                        }
                      }}
                    />
                  </View>
                </View>
              </View>
            )}
          </View>
          <View style={styles.container}>
            {this.state.ishandle9 == true && (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <Text style={{ paddingRight: 15, fontWeight: "700" }}>
                      10.{""}
                    </Text>
                  </View>
                  <Text
                    style={{
                      ...styles.fieldBoldStyle,

                      marginRight: Dimensions.get("window").width * 0.23,
                    }}
                  >
                    How can I share my badge on LinkedIn?{" "}
                  </Text>

                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-up"
                      size={27}
                      color="black"
                      style={{ marginTop: 5 }}
                      onPress={() => {
                        if (this.state.ishandle9 == false) {
                          this.setState({ ishandle9: true });
                        } else {
                          this.setState({ ishandle9: false });
                        }
                      }}
                    />
                  </View>
                </View>

                <View
                  style={{
                    paddingRight: 15,
                    marginRight: 5,
                  }}
                >
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 1. </Text>
                    <Text style={styles.mattertext}>
                      Download it and share it from the phone's gallery. {"\n"}
                    </Text>
                  </View>
                  <Text style={styles.orstyle}>OR</Text>
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 2. </Text>
                    <Text style={styles.mattertext}>
                      Directly share it using the share button.{" "}
                    </Text>
                  </View>
                </View>
              </View>
            )}
            {this.state.ishandle9 == false && (
              <View style={styles.cardStyle}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View
                    style={{
                      paddingRight: 15,
                      marginRight: 5,
                      marginLeft: 2,
                    }}
                  >
                    <Text style={{ fontWeight: "700" }}>10.</Text>
                  </View>
                  <View style={{ marginRight: 1 }}>
                    <Text
                      style={{
                        ...styles.fieldBoldStyle,

                        marginRight: Dimensions.get("window").width * 0.22,
                      }}
                    >
                      How can I share my badge on LinkedIn?
                    </Text>
                  </View>
                  <View style={{ justifyContent: "flex-end", marginLeft: 5 }}>
                    <FontAwesome
                      name="angle-down"
                      size={27}
                      color="black"
                      style={{ marginTop: 5, marginBottom: 10 }}
                      onPress={() => {
                        if (this.state.ishandle9 == false) {
                          this.setState({ ishandle9: true });
                        } else {
                          this.setState({ ishandle9: false });
                        }
                      }}
                    />
                  </View>
                </View>
              </View>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  feedback: {
    fontSize: 25,
    color: "black",
    fontWeight: "bold",
  },
  icons1: {
    flexDirection: "row",
    paddingTop: 10,
  },
  container1: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 20,
    marginBottom: 20,
  },
  eventstype: { flexDirection: "row", paddingRight: 10 },
  overAllView: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    marginLeft: Dimensions.get("window").width * 0.03,
    marginRight: Dimensions.get("window").width * 0.03,
  },
  container: {
    margin: 5,
    flexDirection: "column",
    borderBottomWidth: 2,
    borderBottomColor: "#afeeee",
  },
  containerselected: {
    elevation: 3,
    marginBottom: 2,
    shadowColor: "#00ffff",
    backgroundColor: "#f0f8ff",
    shadowOpacity: 0.5,
    borderWidth: 0,
    paddingHorizontal: 17,
    paddingBottom: 10,
    paddingTop: 10,
    alignItems: "flex-start",
    alignContent: "space-between",
    justifyContent: "flex-start",
  },
  cardStyle: {
    paddingHorizontal: 15,
    paddingBottom: 0,
    paddingTop: 10,
  },
  orstyle: {
    alignSelf: "center",
    margin: 10,
    marginTop: -17,
    fontSize: 17,
    color: "black",
  },
  titleStyle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 5,
  },
  mattertext: {
    fontSize: 16,
    color: "#2f4f4f",
    fontWeight: "600",
  },
  fieldBoldStyle: {
    fontSize: 16,
    marginBottom: 3,
    color: "#212121",
    fontWeight: "700",
   
  },
  fieldStyle: {
    fontSize: 15,
    marginBottom: 3,
    color: "#2f4f4f",
    paddingHorizontal: 3,
  },
});
