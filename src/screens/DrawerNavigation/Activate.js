import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Keyboard,
  Image,
  TextInput,
  KeyboardAvoidingView,
  Dimensions,
  ActivityIndicator,
  Platform,
  AsyncStorage,
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { Toast } from "native-base";
import * as firebase from "firebase";
import Logos from "../../../assets/Logos";
import { urls } from "./../../../env.json";

export default class AddAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      code: "",
      institute: null,
      college: null,
      uid: null,
      activated: false,
    };
  }
  componentDidMount() {
    AsyncStorage.getItem("profile")
      .then((d) => {
        return JSON.parse(d);
      })
      .then((d) => {
        this.setState({
          activated: d.License_code_verified,
        });
      });
  }

  reviewDetails = () => {
    Keyboard.dismiss();
    this.setState({ loading: true });
    //checking the auth details.
    if (this.state.code.trim().length == 0) {
      Toast.show({ text: "Enter license code", buttonText: "Okay" });
    }
    firebase
      .database()
      .ref("mentees")
      .orderByChild("license_code")
      .equalTo(this.state.code)
      .once("value")
      .then((snapshot) => {
        if (snapshot.exists()) {
          this.setState({ code: "", loading: false, activated: true });
          Toast.show({
            text: "Entered license code is correct",
            buttonText: "Okay",
          });
          snapshot.forEach((item) => {
            let d = item.val();
            d = { ...d };
            d.License_code_verified = true;
            console.log("new", d);
            fetch(
              urls.updateMentee, //To save mentee details to database.
              {
                method: "POST",
                body: JSON.stringify(d),
                headers: {
                  "Content-type": "application/json; charset=UTF-8",
                },
              }
            ).then((res) => {
              this.setState({ loading: false });
              AsyncStorage.setItem("profile", JSON.stringify(d));
            });
          });
        } else {
          Toast.show({
            text: "Entered license code is Incorrect",
            buttonText: "Okay",
          });
          this.setState({ loading: false });
        }
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView
          behavior={Platform.Os == "ios" ? "padding" : "height"}
          style={styles.keyboardView}
        >
          <ScrollView keyboardShouldPersistTaps="handled">
            <View style={styles.logoContainer}>
              <Image
                style={styles.imageStyle}
                source={{ uri: Logos.mentorKonnectIcon }}
              />
              <Text style={styles.titleStyle}>Activate Profile</Text>
            </View>
            {!this.state.activated && (
              <View>
                <View style={styles.detailsContainer}>
                  <Text style={styles.subtitle}>
                    Enter License Code <Text style={{ color: "red" }}>*</Text>
                  </Text>
                  <TextInput
                    selectTextOnFocus={true}
                    autoCapitalize="none"
                    placeholder="License Code"
                    underlineColorAndroid="transparent"
                    style={styles.TextInputStyle}
                    value={`${this.state.code}`}
                    onChangeText={(text) => this.setState({ code: text })}
                  />
                </View>
                <View style={styles.grantButtonView}>
                  <Button
                    icon={
                      <Icon
                        type="font-awesome"
                        name="check"
                        size={20}
                        color="white"
                      />
                    }
                    containerStyle={styles.grantButtonContainer}
                    buttonStyle={styles.grantButtonStyle}
                    onPress={() => {
                      this.setState({ loading: false });
                      this.reviewDetails();
                    }}
                    titleStyle={styles.buttonText}
                    title="Activate"
                  />
                  {this.state.loading && <ActivityIndicator size="large" />}
                </View>
              </View>
            )}
            {this.state.activated && (
              <View style={{ flexDirection: "column" }}>
                <Image
                  style={styles.ActivateStyle}
                  source={{
                    uri:
                      "https://firebasestorage.googleapis.com/v0/b/mycollegekonnect-app-efd5d.appspot.com/o/assets%2Factivate.png?alt=media&token=d6e41e40-87cd-4f1a-8a6c-ec7521e434a1",
                  }}
                />
                <Text
                  style={{
                    fontSize: 20,
                    color: "green",
                    textAlign: "center",
                  }}
                >
                  Your Profile is ACTIVATED
                </Text>
              </View>
            )}
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonText: { padding: 5 },
  ActivateStyle: {
    height: 120,
    width: 120,
    marginLeft: Dimensions.get("window").width / 2 - 60,
    marginTop: Dimensions.get("window").height / 5 - 60,
  },
  grantButtonStyle: {
    borderRadius: 30,
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    width: Dimensions.get("window").width * 0.7,
  },
  grantButtonContainer: { padding: 0 },
  grantButtonView: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: 10,
  },
  imageStyle: {
    height: 120,
    width: 120,
  },
  keyboardView: {
    flex: 1,
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "bold",
  },
  detailsContainer: {
    flexDirection: "column",
    justifyContent: "center",
    margin: 30,
    padding: 30,
    alignContent: "center",
    borderWidth: 0.5,
    borderRadius: 2,
  },
  TextInputStyle: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.7,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
  },
  logoContainer: {
    marginTop: 50,
    alignItems: "center",
  },
  titleStyle: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  logoStyle: {
    borderRadius: 5,
    height: 175,
    width: 300,
  },
});
