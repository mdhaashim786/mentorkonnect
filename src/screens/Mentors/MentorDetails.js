import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  Dimensions,
  AsyncStorage,
  Linking,
  ActivityIndicator,
} from "react-native";
import Constants from "expo-constants";
import { Toast } from "native-base";
import firebase from "firebase";
import { Icon, Avatar, Button } from "react-native-elements";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { urls } from "./../../../env.json";
import Logos from "../../../assets/Logos";
import PreBuiltEventTypes from "../Events/PreBuiltEventTypes";
import * as Font from "expo-font";

var list;
let customFonts = {
  LobsterRegular: require("../../../assets/fonts/LobsterRegular-rR8O.ttf"),
};
export default class MenteeDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontsLoaded: false,
      mentor: null,
      profile: null,
      preBuiltEventsList: [],
      device_id: "",
    };
  }
  async _loadFontsAsync() {
    await Font.loadAsync(customFonts);
    this.setState({ fontsLoaded: true });
  }
  getData = async () => {
    let d = await this.props.navigation.getParam("mentor");
    await this.setState({ mentor: d });

    await AsyncStorage.getItem("profile").then((data) => {
      this.setState({
        profile: JSON.parse(data),
      });
    });
  };

  componentDidMount() {
    //TO load the fonts
    this._loadFontsAsync();
    this.getData();
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getData();
      }
    );
    var userId = firebase.auth().currentUser.uid;
    firebase
      .database()
      .ref("mentees/" + userId)
      .once("value")
      .then((snapshot) => {
        if (snapshot.exists()) {
          let value = snapshot.val();
          if (value.device_id != undefined) {
            console.log(value.device_id);
            this.setState({ device_id: value.device_id });
          } else {
            this.setState({ device_id: "anonymous" });
          }
        }
      });
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  render() {
    // Navigating from AdminMentorCard and Mentor Details(in AdminEventDetails) Screen,
    // sending parameter "admin" as true
    let admin = this.props.navigation.getParam("admin");
    // If navigating from other screens, assign false to admin parameter
    if (admin === undefined) admin = false;

    // Page variable is the screen to which the mentor data is send.
    let page = this.props.navigation.getParam("page");
    // When mentor is connected directly from mentor list without going to event creation
    console.log("PAGE", page);
    let directly = false;
    if (page === undefined) {
      page = "AddMentorRoute";
      directly = true;
    }
    //assigning badges to mentors based on no.of simple events,no of bootcamp events and no of overview events completed by the mentor.
    let mentorBadge = this.props.navigation.getParam("mentor");
    var badge = null;
    if (mentorBadge.no_of_events != undefined) {
      const simple = mentorBadge.no_of_events["no_of_simple_events"]; //getting no of simple evnts of the mentor.
      const overview = mentorBadge.no_of_events["no_of_overview_events"]; //getting no of overview evnts of the mentor.
      const bootcamp = mentorBadge.no_of_events["no_of_bootcamp_events"]; //getting no of bootcamp evnts of the mentor.
      // based on the conditions,respective badge is stored in badge variable.
      if (simple >= 20 || overview >= 7 || bootcamp >= 3)
        badge = Logos.platinum;
      else if (
        (simple >= 10 && simple < 20) ||
        (overview >= 4 && overview < 7) ||
        bootcamp == 2
      )
        badge = Logos.gold;
      else if (
        (simple >= 5 && simple < 10) ||
        (overview >= 1 && overview < 4) ||
        bootcamp == 1
      )
        badge = Logos.silver;
      else if (simple >= 1 && simple < 5) badge = Logos.bronze;
      else badge = null;
    }
    if (
      this.state.mentor != null &&
      this.state.profile != null &&
      this.state.fontsLoaded
    ) {
      return (
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.imageContainer}>
              <View style={styles.imageViewStyle}>
                <Avatar
                  size={100}
                  rounded
                  source={{ uri: this.state.mentor.photo_url }}
                  icon={{ name: "user", type: "font-awesome" }}
                />
              </View>
              <View style={styles.imageBesideText}>
                <View style={styles.badgeStyle}>
                  <Text
                    style={{
                      color: "white",
                      fontWeight: "bold",
                      marginBottom: 5,
                      marginRight: 3,
                      fontSize: 15,
                    }}
                  >
                    {this.state.mentor.name}
                  </Text>
                </View>

                <Text
                  style={{
                    color: "white",
                    fontWeight: "bold",
                    marginBottom: 5,
                    marginRight: 3,
                    fontSize: 15,
                  }}
                  selectable={true}
                >
                  {this.state.mentor.email}
                </Text>
                <Text
                  style={{
                    color: "white",
                    fontWeight: "bold",
                    marginBottom: 5,
                    fontSize: 15,
                  }}
                >
                  {this.state.mentor.branch}
                </Text>
                <Text
                  style={{
                    color: "white",
                    fontWeight: "bold",
                    marginBottom: 5,
                    fontSize: 15,
                  }}
                >
                  {this.state.mentor.college_name}
                </Text>
              </View>
              {badge != null && (
                <View style={styles.badgeViewStyle}>
                  <ImageBackground
                    style={{
                      width: 70,
                      height: 140, //Dimensions.get("window").height * 0.2,
                    }}
                    resizeMode="contain"
                    source={{ uri: badge }}
                  >
                    <View style={styles.badge_1stview}>
                      <View style={styles.badge_2ndview}>
                        <Text style={styles.nameonbadge}>
                          {this.state.mentor.name.split(" ", 1)}
                        </Text>
                      </View>
                    </View>
                  </ImageBackground>
                </View>
              )}
            </View>
          </View>
          
          <View style={styles.details}>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ ...styles.titleStyle, marginEnd: 10 }}>
                {this.state.mentor.name}
              </Text>
              <TouchableOpacity
                style={{
                  borderWidth: 2,
                  borderColor: "#4682b4",
                  borderRadius: 6,
                }}
                onPress={() =>
                  Linking.openURL(`${this.state.mentor.linkedin}`)
                }
              >
                <Image
                  source={require("../../../assets/linkedin-icon.png")}
                  style={{
                    width: 28,
                    height: 28,
                    margin: 0.4,
                  }}
                  
                />
              </TouchableOpacity>
            </View>
            {this.state.mentor.current_working_title != undefined && (
              <View>
                <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                  Working Title
                </Text>
                <Text style={styles.about}>
                  {this.state.mentor.current_working_title}
                </Text>
              </View>
            )}
            {this.state.mentor.current_working_company != undefined && (
              <View>
                <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                  Working Company
                </Text>
                <Text style={styles.about}>
                  {this.state.mentor.current_working_company}
                </Text>
              </View>
            )}

            {this.state.mentor.about != "" &&
              this.state.mentor.about != undefined && (
                <View>
                  <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                    About
                  </Text>
                  <Text style={styles.about}>{this.state.mentor.about}</Text>
                </View>
              )}
            {this.state.mentor.gpa != undefined && this.state.mentor.gpa != "" && (
              <View>
                <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                  GPA
                </Text>
                <Text
                  style={{ ...styles.about, marginBottom: 5, fontSize: 15 }}
                >
                  {this.state.mentor.gpa}
                </Text>
              </View>
            )}
            {this.state.mentor.year_passed != "" &&
              this.state.mentor.year_passed != undefined && (
                <View>
                  <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                    Year of Graduation
                  </Text>
                  <Text style={styles.about}>
                    {this.state.mentor.year_passed}
                  </Text>
                </View>
              )}
            {this.state.mentor.college_after_BTECH != "" &&
              this.state.mentor.college_after_BTECH != undefined && (
                <View>
                  <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                    College After B.TECH
                  </Text>
                  <Text style={styles.about}>
                    {this.state.mentor.college_after_BTECH}
                  </Text>
                </View>
              )}
            {this.state.mentor.higher_education != undefined &&
              this.state.mentor.higher_education != "" && (
                <View>
                  <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                    Highest Education
                  </Text>
                  <Text style={styles.about}>
                    {this.state.mentor.higher_education}
                  </Text>
                </View>
              )}
            {this.state.mentor.specialization != undefined &&
              this.state.mentor.specialization != "" && (
                <View>
                  <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                    Specialization
                  </Text>
                  <Text style={styles.about}>
                    {this.state.mentor.specialization}
                  </Text>
                </View>
              )}
            {this.state.mentor.available != undefined &&
              this.state.mentor.available != "" && (
                <View>
                  <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                    Available Time
                  </Text>
                  <Text style={styles.about}>
                    {this.state.mentor.available}
                  </Text>
                </View>
              )}
            {this.state.mentor.phone_number != undefined &&
              this.state.mentor.phone_number != "" && (
                <View>
                  <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                    Phone Number
                  </Text>
                  <Text style={styles.about}>
                    {this.state.mentor.phone_number}
                  </Text>
                </View>
              )}
            <Text style={styles.subHeading}>Interests</Text>
            <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
              {this.state.mentor.interests_list.map((l, i) => (
                <View
                  key={i}
                  style={{
                    flexDirection: "row",
                    margin: 5,
                    padding: 5,
                    borderRadius: 10,
                    borderWidth: 2,
                    alignItems: "center",
                  }}
                >
                  <Text style={{ fontSize: 15, fontWeight: "bold" }}>
                    {l.name}
                  </Text>
                </View>
              ))}
            </View>
            {this.state.mentor.mentoring_interest_list != undefined && (
              <View>
                <Text style={styles.subHeading}>Mentoring Interests</Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  {this.state.mentor.mentoring_interest_list.map((l, i) => (
                    <View key={i} style={styles.listViewStyle}>
                      <Text style={{ fontSize: 17, fontWeight: "bold" }}>
                        {l.name}
                      </Text>
                    </View>
                  ))}
                  {/* Used to display preBuilt Events when mentordetails are opned */}

                  {this.state.mentor.pre_built_events_indices != undefined &&
                    this.state.mentor.pre_built_events_indices.length != 0 &&
                    this.state.mentor.pre_built_events_indices.map((l, i) => (
                      <View key={i} style={styles.listViewStyle}>
                        {PreBuiltEventTypes.map((item, id) => (
                          <View key={id}>
                            {l == id && (
                              <Text
                                style={{ fontSize: 17, fontWeight: "bold" }}
                              >
                                {item.name}
                              </Text>
                            )}
                          </View>
                        ))}
                      </View>
                    ))}
                </View>
              </View>
            )}
            {this.state.mentor.patents != undefined && (
              <View>
                <Text style={styles.subHeading}>Patents</Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  {this.state.mentor.patents.map((l, i) => (
                    <View key={i} style={styles.listViewStyle}>
                      <Text style={{ fontSize: 17, fontWeight: "bold" }}>
                        {l.name}
                      </Text>
                    </View>
                  ))}
                </View>
              </View>
            )}
            {this.state.mentor.extra_curricular_activities != undefined && (
              <View>
                <Text style={styles.subHeading}>
                  Extra Curricular Activites
                </Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  {this.state.mentor.extra_curricular_activities.map((l, i) => (
                    <View key={i} style={styles.listViewStyle}>
                      <Text style={{ fontSize: 17, fontWeight: "bold" }}>
                        {l.name}
                      </Text>
                    </View>
                  ))}
                </View>
              </View>
            )}
            {this.state.mentor.short_term_goals != undefined && (
              <View>
                <Text style={styles.subHeading}>Short Term Goals</Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  {this.state.mentor.short_term_goals.map((l, i) => (
                    <View key={i} style={styles.listViewStyle}>
                      <Text style={{ fontSize: 17, fontWeight: "bold" }}>
                        {l.name}
                      </Text>
                    </View>
                  ))}
                </View>
              </View>
            )}
            {this.state.mentor.long_term_goals != undefined &&
              this.state.mentor.long_term_goals.length != 0 && (
                <View>
                  <Text style={styles.subHeading}>Long Term Goals</Text>
                  <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                    {this.state.mentor.long_term_goals.map((l, i) => (
                      <View key={i} style={styles.listViewStyle}>
                        <Text style={{ fontSize: 17, fontWeight: "bold" }}>
                          {l.name}
                        </Text>
                      </View>
                    ))}
                  </View>
                </View>
              )}
            {this.state.mentor.subjects_liked != undefined &&
              this.state.mentor.subjects_liked.length != 0 && (
                <View>
                  <Text style={styles.subHeading}>
                    Subjects Liked at your college
                  </Text>
                  <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                    {this.state.mentor.subjects_liked.map((l, i) => (
                      <View key={i} style={styles.listViewStyle}>
                        <Text style={{ fontSize: 17, fontWeight: "bold" }}>
                          {l.name}
                        </Text>
                      </View>
                    ))}
                  </View>
                </View>
              )}
            {this.state.mentor.faculty_recommended != undefined &&
              this.state.mentor.faculty_recommended.length != 0 && (
                <View>
                  <Text style={styles.subHeading}>
                    Faculty Recommended at your college
                  </Text>
                  <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                    {this.state.mentor.faculty_recommended.map((l, i) => (
                      <View key={i} style={styles.listViewStyle}>
                        <Text style={{ fontSize: 17, fontWeight: "bold" }}>
                          {l.name}
                        </Text>
                      </View>
                    ))}
                  </View>
                </View>
              )}
            {admin == false &&
              this.state.profile.user_type == "mentee" &&
              this.state.profile.status == "approved" && (
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 20,
                    marginBottom: 20,
                  }}
                >
                  <Button
                    title="Connect"
                    containerStyle={{ width: 150 }}
                    onPress={async () => {
                      if (
                        Constants.deviceId != this.state.device_id &&
                        this.state.device_id != "anonymous"
                      ) {
                        // //signout
                        await firebase
                          .auth()
                          .signOut()
                          .then(() => {
                            this.props.navigation.navigate("AuthScreen");
                          })
                          .then(() => {
                            Toast.show({
                              text:
                                "You have been logged out as you have logged in another device!  ",
                              buttonText: "Okay",
                            });
                          })
                          .catch((err) => {
                            console.log(err);
                          });
                      } else {
                        if (directly) {
                          this.props.navigation.navigate(page, {
                            //push
                            mentor: this.state.mentor,
                            profile: this.state.profile,
                            eventType: "Others",
                          });
                        } else {
                          this.props.navigation.state.params.onGoBack(
                            this.state.mentor
                          );
                          this.props.navigation.navigate(page);
                        }
                      }
                    }}
                  />
                </View>
              )}
            {admin == false &&
              this.state.profile.user_type == "license_mentee" &&
              this.state.profile.super_admin_verified == true && (
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 20,
                    marginBottom: 20,
                  }}
                >
                  <Button
                    title="Connect"
                    containerStyle={{ width: 150 }}
                    onPress={async () => {
                      if (
                        Constants.deviceId != this.state.device_id &&
                        this.state.device_id != "anonymous"
                      ) {
                        // //signout
                        await firebase
                          .auth()
                          .signOut()
                          .then(() => {
                            this.props.navigation.navigate("AuthScreen");
                          })
                          .then(() => {
                            Toast.show({
                              text:
                                "You have been logged out as you have logged in another device!  ",
                              buttonText: "Okay",
                            });
                          })
                          .catch((err) => {
                            console.log(err);
                          });
                      } else {
                        if (directly) {
                          this.props.navigation.navigate(page, {
                            //push
                            mentor: this.state.mentor,
                            profile: this.state.profile,
                            eventType: "Others",
                          });
                        } else {
                          this.props.navigation.state.params.onGoBack(
                            this.state.mentor
                          );
                          this.props.navigation.navigate(page);
                        }
                      }
                    }}
                  />
                </View>
              )}
          </View>
        </ScrollView>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <ActivityIndicator size="large" />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: Dimensions.get("window").height * 0.25,
    backgroundColor: "#a3c0e1",
  },
  subHeading: {
    fontSize: 20,
    fontWeight: "bold",
    marginVertical: 5,
  },
  about: {
    fontSize: 17,
    marginBottom: 5,
  },
  imageContainer: {
    height: Dimensions.get("window").height * 0.25,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  details: {
    flexDirection: "column",
    padding: 10,
  },
  imageViewStyle: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: Dimensions.get("window").height * 0.01,
    paddingLeft: 10,
    paddingRight: 10,
    marginRight: Dimensions.get("window").height * 0.01,
  },
  badge_1stview: {
    borderRadius: 50,
    flexWrap: "wrap",
    marginLeft: 12.8,
    marginTop: 36.8,
    height: 45,
    width: 45,
    justifyContent: "center",
    alignItems: "center",
  },
  badge_2ndview: {
    height: 35,
    width: 35,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
  },
  nameonbadge: {
    fontSize: 8,
    color: "#fffff0",
    fontWeight: "900",
    textAlign: "center",
    fontFamily: "LobsterRegular",
    textTransform: "capitalize",
  },
  badgeViewStyle: {
    paddingLeft: 5,
    paddingRight: 5,
  },
  titleStyle: {
    fontSize: 23,
    fontWeight: "bold",
    textTransform: "capitalize",
  },
  emailStyle: {
    color: "rgba(200,0,0,0.8)",
    fontSize: 15,
  },
  badgeStyle: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 10,
  },
  imageBesideText: {
    flex: 2,
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "center",
  },
  listViewStyle: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    borderRadius: 10,
    borderWidth: 2,
    alignItems: "center",
  },
});
