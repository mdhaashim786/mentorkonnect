import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Linking,
  ImageBackground,
  Dimensions,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Avatar } from "react-native-elements";
import Logos from "../../../assets/Logos";
import { urls } from "./../../../env.json";
import PreBuiltEventTypes from "./../Events/PreBuiltEventTypes";
import { AppLoading } from "expo";
import * as Font from "expo-font";
let customFonts = {
  LobsterRegular: require("../../../assets/fonts/LobsterRegular-rR8O.ttf"),
};
export class MentorCard extends React.Component {
  state = {
    //to load fonts
    fontsLoaded: false,
  };
  async _loadFontsAsync() {
    await Font.loadAsync(customFonts);
    this.setState({ fontsLoaded: true });
  }
  componentDidMount() {
    this._loadFontsAsync();
  }

  render() {
    const { mentor, navigation } = this.props;
    // console.log(mentoringInterests);
    //assigning badges to mentors based on no.of simple events,no of bootcamp events and no of overview events completed by the mentor.
    var badge = null;
    if (mentor.no_of_events != undefined) {
      const simple = mentor.no_of_events["no_of_simple_events"]; //getting no of simple evnts of the mentor.
      const overview = mentor.no_of_events["no_of_overview_events"]; //getting no of overview evnts of the mentor.
      const bootcamp = mentor.no_of_events["no_of_bootcamp_events"]; //getting no of bootcamp evnts of the mentor.
      // based on the conditions,respective badge is stored in badge variable.
      if (simple >= 20 || overview >= 7 || bootcamp >= 3)
        badge = Logos.platinum;
      else if (
        (simple >= 10 && simple < 20) ||
        (overview >= 4 && overview < 7) ||
        bootcamp == 2
      )
        badge = Logos.gold;
      else if (
        (simple >= 5 && simple < 10) ||
        (overview >= 1 && overview < 4) ||
        bootcamp == 1
      )
        badge = Logos.silver;
      else if (simple >= 1 && simple < 5) badge = Logos.bronze;
      else badge = null;
    }
    if (this.state.fontsLoaded) {
      return (
        <View style={styles.overall}>
          <TouchableOpacity
            activeOpacity={0.7}
            //on pressing mentor card his details are visible.
            onPress={() => {
              navigation.navigate("MentorDetailsPage", {
                mentor: mentor,
                navigation: navigation,
              });
            }}
          >
            <View style={styles.listItem}>
              <Avatar
                size={80}
                rounded
                source={{ uri: mentor.photo_url }}
                icon={{ name: "user", type: "font-awesome" }}
              />
              <View style={styles.cardRight}>
                <View style={styles.cardRightSub}>
                  <Text style={styles.text}>{mentor.name}</Text>
                </View>
                {mentor.current_working_title != undefined &&
                  mentor.current_working_title != "" && (
                    <Text style={{ fontSize: 17, fontWeight: "500" }}>
                      {mentor.current_working_title}{" "}
                    </Text>
                  )}
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: "bold",
                  }}
                >
                  Mentoring Interests:
                </Text>
                {mentor.mentoring_interest_list.map((item, index) => {
                  return (
                    <Text style={{ fontSize: 14 }} key={index}>
                      {item.name}
                    </Text>
                  );
                })}
                {mentor.pre_built_events_indices != undefined &&
                  mentor.pre_built_events_indices.length != 0 &&
                  mentor.pre_built_events_indices.map((l, i) => (
                    <View key={i}>
                      {PreBuiltEventTypes.map((item, id) => (
                        <View key={id}>
                          {l == id && (
                            <Text style={styles.subTexttitle}>{item.name}</Text>
                          )}
                        </View>
                      ))}
                    </View>
                  ))}
              </View>
              {badge != null && (
                <View>
                  <ImageBackground
                    style={{
                      width: 70, //60
                      height: 140, //125
                    }}
                    resizeMode="contain"
                    source={{ uri: badge }}
                  >
                    <View style={styles.badge_1stview}>
                      <View style={styles.badge_2ndview}>
                        <Text style={styles.nameonbagde}>
                          {mentor.name.split(" ", 1)}
                        </Text>
                      </View>
                    </View>
                  </ImageBackground>
                </View>
              )}
            </View>
          </TouchableOpacity>
        </View>
      );
    } else {
      return <AppLoading />;
    }
  }
}

const styles = StyleSheet.create({
  overall: { marginTop: 1 },
  cardRight: { flex: 1, alignItems: "flex-start", marginLeft: 15 },
  cardRightSub: { flexDirection: "row", alignItems: "center" },
  text: {
    fontWeight: "bold",
    textTransform: "capitalize",
    fontSize: 18,
    marginEnd: 5,
  },

  listItem: {
    flexDirection: "row",
    alignItems: "center",
    margin: 5,
    borderRadius: 1,
    paddingVertical: 10,
    paddingLeft:15,
    paddingRight:2,
    //paddingStart: 20,
    borderWidth: 0.5,
    marginBottom: 2,
  },
  badge_1stview: {
    borderRadius: 50,
    flexWrap: "wrap",
    marginLeft: 12.8,
    marginTop: 36.8,
    height: 45,
    width: 45,
    justifyContent: "center",
    alignItems: "center",
  },
  badge_2ndview: {
    height: 35,
    width: 35,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
  },
  nameonbagde: {
    fontSize: 8,
    color: "#fffff0",
    fontWeight: "900",
    textAlign: "center",
    fontFamily: "LobsterRegular",
    textTransform: "capitalize",
  },
});
