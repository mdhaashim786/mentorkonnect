import React from "react";
import {
  StyleSheet,
  ActivityIndicator,
  Text,
  View,
  AsyncStorage,
  Image,
  FlatList,
  Button,
} from "react-native";
import { SearchBar } from "react-native-elements";
import { MentorCard } from "./MentorCard";
import { urls } from "./../../../env.json";
import PreBuiltEventTypes from "./../Events/PreBuiltEventTypes";
var list;
var preList;
export default class Mentors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      dataloaded: false,
      refreshing: false,
      profile: null,
      badge: null,
    };
  }
  SearchFilterFunction = async (text) => {
    //console.log("p"+list);
    const newData = this.state.data.filter(function (item) {
      const nameData = item.name ? item.name.toUpperCase() : "".toUpperCase(); //checking if the search string is substring of any of the names of mentors.
      const textnameData = text.toUpperCase();
      if (nameData.indexOf(textnameData) != -1) {
        //console.log(nameData, textnameData, nameData.indexOf(textnameData));
        return nameData.indexOf(textnameData) > -1;
      } else {
        for (var i = 0; i < item.mentoring_interest_list.length; i++) {
          const itemData = item.mentoring_interest_list[i].name
            ? item.mentoring_interest_list[i].name.toUpperCase()
            : "".toUpperCase();
          const textData = text.toUpperCase();
          if (itemData.indexOf(textData) != -1) {
            return itemData.indexOf(textData) > -1;
          }
        }
      }
    });
    //console.log("p"+preList);
    const preData = this.state.data.filter((item) => {
      if (
        item.pre_built_events_indices !== undefined &&
        item.pre_built_events_indices.length != 0
      ) {
        for (var x = 0; x < item.pre_built_events_indices.length; x++) {
          for (var y = 0; y < PreBuiltEventTypes.length; y++) {
            if (item.pre_built_events_indices[x] == y) {
              const XData = PreBuiltEventTypes[y].name
                ? PreBuiltEventTypes[y].name.toUpperCase()
                : "".toUpperCase();
              const tData = text.toUpperCase();
              if (XData.indexOf(tData) != -1) {
                return XData.indexOf(tData) > -1;
              }
            }
          }
        }
      }
    });
    let searchArray = [...preData, ...newData]; //can contain values which can be duplicate
    let myset = new Set(searchArray); //removing the duplicate values

    await this.setState({
      search: text,
      dupdata: [...myset], //converting the set to array using spread operator
    });
  };

  getData = async () => {
    fetch(urls.getMentors, {
      method: "GET",
    }) //fetching all the mentors data
      .then((response) => {
        return response.json();
      })
      .then(async (responseJSON) => {
        let d = Array.from(responseJSON);
        // console.log(d);
        let idToRemove = null;
        await AsyncStorage.getItem("profile").then((data) => {
          this.setState({
            profile: JSON.parse(data),
          });
        });
        if (this.state.profile.user_type == "mentee") {
          idToRemove = this.state.profile.uid;
          d = d.filter((item) => item.uid !== idToRemove);
        } else {
          idToRemove = this.state.profile.uid;
          d = d.filter((item) => item.uid !== idToRemove);
        }
        let k = d.filter((item) => {
          if (
            item.user_type === "mentor" &&
            item.institution_id != this.state.profile.institution_id &&
            item.mentoring_interest_other_college !== undefined
          ) {
            return item.mentoring_interest_other_college === true;
          }
        });

         let l = d.filter((item) => {
          if (
            item.user_type === "mentor" &&
            item.college_id != this.state.profile.college_id &&
            item.institution_id == this.state.profile.institution_id &&
            item.mentoring_interest_other_college !== undefined
          ) {
            return item.mentoring_interest_other_college === true;
          }
        });
        l = l.filter((item) => item.display_profile === true);
        k = k.filter((item) => item.display_profile === true);
        d = d.filter(
          (item) =>
            item.college_id === this.state.profile.college_id &&
            item.institution_id === this.state.profile.institution_id
        );

        d = d.filter((item) => item.display_profile === true);
        d = [...d, ...k,...l];
        let with_badge = d.filter((item) => {
          if (
            item.no_of_events != undefined &&
            (item.no_of_events["no_of_simple_events"] != 0 ||
              item.no_of_events["no_of_overview_events"] != 0 ||
              item.no_of_events["no_of_bootcamp_events"] != 0)
          ) {
            return true;
          }
        });

        let plat = with_badge.filter((item) => {
          if (
            item.no_of_events != undefined &&
            (item.no_of_events["no_of_simple_events"] >= 20 ||
              item.no_of_events["no_of_overview_events"] >= 7 ||
              item.no_of_events["no_of_bootcamp_events"] >= 3)
          ) {
            return true;
          }
        });
        let gold = with_badge.filter((item) => {
          if (
            (item.no_of_events != undefined &&
              item.no_of_events["no_of_simple_events"] >= 10 &&
              item.no_of_events["no_of_simple_events"] < 20) ||
            (item.no_of_events["no_of_overview_events"] >= 4 &&
              item.no_of_events["no_of_overview_events"] < 7) ||
            item.no_of_events["no_of_bootcamp_events"] == 2
          ) {
            return true;
          }
        });
        let silver = with_badge.filter((item) => {
          if (
            (item.no_of_events != undefined &&
              item.no_of_events["no_of_simple_events"] >= 5 &&
              item.no_of_events["no_of_simple_events"] < 10) ||
            (item.no_of_events["no_of_overview_events"] >= 1 &&
              item.no_of_events["no_of_overview_events"] < 4) ||
            item.no_of_events["no_of_bootcamp_events"] == 1
          ) {
            return true;
          }
        });
        let bronze = with_badge.filter((item) => {
          if (
            item.no_of_events != undefined &&
            item.no_of_events["no_of_simple_events"] >= 1 &&
            item.no_of_events["no_of_simple_events"] < 5
          ) {
            return true;
          }
        });
        let without_badge = d.filter((item) => {
          if (
            item.no_of_events == undefined ||
            (item.no_of_events["no_of_simple_events"] == 0 &&
              item.no_of_events["no_of_overview_events"] == 0 &&
              item.no_of_events["no_of_bootcamp_events"] == 0)
          ) {
            return true;
          }
        });
        //Sorting the mentors list using badges
        let badges_profiles = [...plat, ...gold, ...silver, ...bronze];
        let mentors_array = new Set(badges_profiles);
        d = [...mentors_array, ...without_badge];

        this.setState({
          data: d,
          dataloaded: true,
          dupdata: d,
          refreshing: false,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  async componentDidMount() {
    await this.getData();

    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getData();
      }
    );
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true, search: "" });
    await this.componentDidMount();
  };

  render() {
    if (this.state.dataloaded) {
      return (
        <View style={styles.container}>
          <SearchBar
            round
            inputContainerStyle={styles.searchContainer}
            showLoadingIcon={true}
            inputStyle={styles.searchinput}
            containerStyle={styles.searchSubContainer}
            searchIcon={{ size: 20 }}
            onChangeText={(text) => this.SearchFilterFunction(text)}
            onClear={(text) => this.SearchFilterFunction("")}
            placeholder="Enter name/interest to search..."
            value={this.state.search}
          ></SearchBar>
          <FlatList
            refreshing={this.state.refreshing}
            data={this.state.dupdata}
            onRefresh={this._onRefresh}
            renderItem={({ item }) => {
              return (
                <MentorCard
                  mentor={item}
                  navigation={this.props.navigation}
                  key={item.phone_number}
                /> //for each mentor we render mentorcard
              );
            }}
            keyExtractor={(item, index) => item.email}
          />
        </View>
      );
    } else {
      return (
        <View style={styles.indicator}>
          <ActivityIndicator size="large"></ActivityIndicator>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  searchSubContainer: {
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "white",
    borderBottomWidth: 0,
    borderTopWidth: 0,
  },
  searchinput: {
    color: "black",
  },
  searchContainer: {
    backgroundColor: "white",
    marginRight: 6,
    width: "100%",
    borderWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 5,
  },
  indicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  search: {
    margin: 0,
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    paddingHorizontal: 3,
    paddingVertical: 10,
    justifyContent: "center",
  },
  signOut: {
    alignSelf: "flex-end",
    marginRight: 20,
  },
  listItem: {
    flexDirection: "row",
    margin: 10,
    shadowRadius: 0.3,
    shadowOpacity: 0.5,
    elevation: 1,
    borderRadius: 2,
    padding: 20,
  },
});
