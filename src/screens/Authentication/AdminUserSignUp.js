import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  ScrollView,
  Platform,
  Image,
  Text,
  Picker,
  TextInput,
  KeyboardAvoidingView,
} from "react-native";
import { Button, Avatar, Icon } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import { Notifications } from "expo";
import firebase from "firebase";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import { Toast } from "native-base";
import { urls } from "../../../env.json";
import Logos from "../../../assets/Logos";

export default class adminUserLogin extends Component {
  constructor(props) {
    super(props);
    this.interestInputRef = React.createRef();
    this.multiSelect = React.createRef();
    this.state = {
      prof: null,
      license_bucket: [],
      profile: null,
      loading: false,
      interestmanip: false,
      mentoringInterested: false,
      image: Logos.defaultProfilePic,
      available: "",
      linkedIn: "",
      about: "",
      college_after_BTECH: "",
      collegeName: "None",
      professor: "",
      specialization: "",
      longTermGoal: "",
      credential: "",
      name: "",
      course: "None",
      yearOfGraduation: "",
      gpa: "",
      phoneNumber: "",
      result: "",
      subject: "",
      mentoringInterest: "",
      interest: "",
      activity: "",
      shortTermGoal: "",
      institutionName: "None",
      email: "",
      rollNumber: "",
      higherEducation: "None",
      branch: "None",
      branchesList: [],
      coursesList: [],
      interestsList: [],
      shortTermGoalsList: [],
      activityList: [],
      longTermGoalsList: [],
      mentoringInterestList: [],
      higherEducationList: [],
      subjectsList: [],
      professorList: [],
      collegesList: [],
      institutionsList: [],
      expo_push_token: "", //used to store expo push token
      preBuiltEventsList: [], //getting all the pre built event types from the database
      pre_built_event_type: "",
      pre_built_event_index: null, //used to store the no.of prebuilt event types available
      pre_built_list: [], //storing values of events the mentor/mentee is interested to mentor
    };
  }

  async componentDidMount() {
    console.log("adminsignup");
    //The dummy data of the college admin
    const data = await this.props.navigation.getParam("data");
    this.setState({ prof: data });

    //we temporarily delete the user because he may not fill the sign up form and leave only if he presses save then we login him again.
    var signedInUser = firebase.auth().currentUser; //we get details of user from navigation
    //setting all the details
    console.log("signedInUser", signedInUser);

    await this.setState({
      image:
        signedInUser.photoURL != null
          ? signedInUser.photoURL
          : this.state.image,
    });
    await this.setState({
      name: signedInUser.displayName != null ? signedInUser.displayName : "",
    });
    await this.setState({ email: signedInUser.email });
    await this.setState({
      phoneNumber:
        signedInUser.phoneNumber === null ? "" : signedInUser.phoneNumber,
    });
    //getting all the details from database of different academics.
    fetch(urls.getHigherEducation, { method: "GET" })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        let d = Array.from(res);
        this.setState({ higherEducationList: d });
      })
      .catch((error) => {
        console.log(error);
        Toast.show({
          text: "Unable to show higher educations list",
          buttonText: "Okay",
        });
      });
    fetch(urls.getInstitutions)
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        let d = Array.from(res);
        this.setState({ institutionsList: d });
      })
      .catch((error) => {
        console.log(error);
        Toast.show({ text: "Unable to show institutions", buttonText: "Okay" });
      });

    await this.registerForPushNotificationsAsync(); //to create a new pushToken for every user
  }

  validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      //used to create expoPushToken for every user
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        alert("Failed to get push token for push notification!");
        return;
      }
      try {
        var token = await Notifications.getExpoPushTokenAsync();
        console.log(token);
        await this.setState({
          expo_push_token: token,
        });
      } catch (err) {
        console.log(err);
      }
    } else alert("Must use physical device for Push Notifications");

    if (Platform.OS === "android") {
      Notifications.createChannelAndroidAsync("default", {
        name: "default",
        sound: true,
        priority: "max",
        vibrate: [0, 250, 250, 250],
      });
    }
    // return token;
  };
  saveButton = async () => {
    //They check whether all required fields are filled or not
    if (this.state.name.trim() == "") {
      Toast.show({ text: "Enter name", buttonText: "Okay" });
      return;
    }
    if (this.state.email.trim() == "") {
      Toast.show({ text: "Enter email id", buttonText: "Okay" });
      return;
    }
    if (!this.validateEmail(this.state.email)) {
      Toast.show({ text: "Enter valid email id", buttonText: "Okay" });
      return;
    }
    if (this.state.phoneNumber.trim().length == 0) {
      Toast.show({ text: "Enter valid phone Number", buttonText: "Okay" });
      return;
    }
    if (this.state.linkedIn.trim() == "") {
      Toast.show({ text: "Enter your linkedIn Profile", buttonText: "Okay" });
      return;
    }
    if (this.state.linkedIn.trim().indexOf("linkedin.com/in/") == -1) {
      Toast.show({
        text: "LinkedIn url must be of format linkedin.com/in/username",
        buttonText: "Okay",
        duration: 2000,
      });
      return;
    }
    if (this.state.rollNumber.trim() == "") {
      Toast.show({ text: "Enter Roll number", buttonText: "Okay" });
      return;
    }
    if (this.state.institutionName.trim() == "None") {
      Toast.show({ text: "Select valid instituion", buttonText: "Okay" });
      return;
    }
    if (this.state.collegeName.trim() == "None") {
      Toast.show({ text: "Select valid college", buttonText: "Okay" });
      return;
    }
    if (this.state.branch.trim() == "None") {
      Toast.show({ text: "Select valid branch", buttonText: "Okay" });
      return;
    }
    if (this.state.course.trim() == "None") {
      Toast.show({ text: "Select valid course", buttonText: "Okay" });
      return;
    }
    if (this.state.collegesList.find((x) => x.name == this.state.collegeName)) {
      var collegeId = this.state.collegesList.find(
        (x) => x.name == this.state.collegeName
      ).id;
    }
    if (
      this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      )
    ) {
      var institutionId = this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      ).id;
    }

    const navigation = await this.props.navigation.getParam("navigation");
    // console.log(this.state.pre_built_list);

    let d = {
      last_logged_in: Date().toLocaleString(),
      phone_number: this.state.phoneNumber.trim(),
      name: this.state.name.trim(),
      email: this.state.email,
      branch: this.state.branch.trim(),
      photo_url: this.state.image,
      course: this.state.course.trim(),
      user_type: "college_admin",
      linkedin: this.state.linkedIn.trim(),
      authorized: false,
      license_code_verified: false,
      college_name: this.state.collegeName,
      college_id: collegeId,
      institution_id: institutionId,
      expo_push_token: this.state.expo_push_token, //token is stored into the
      last_updated_by: "user",
      institution_name: this.state.institutionName,
      display_profile: true,
      roll_number: this.state.rollNumber,
    };
    this.setState({ loading: true });
    d.uid = firebase.auth().currentUser.uid;
    //Fetchs the license data from the institutions section of the db and concatinates with the admin's profile
    firebase
      .database()
      .ref("institutions/" + d.institution_id + "/colleges/" + d.college_id)
      .once("value")
      .then(async (snapshot) => {
        if (snapshot.exists()) {
          console.log("snap", snapshot.val());
          let k = snapshot.val().collegeadmin;
          this.state.license_bucket.push(k.license_buckets[0]);
          d = {
            ...d,
            license_buckets: this.state.license_bucket,
            license_code: k.license_code,
            logo_url: snapshot.val().logo_url,
          };
          console.log("User logged in succesfully");
          let obj = this.state.prof;
          //to remove the dummy data of collegeadmin pushed by the push key
          await firebase
            .database()
            .ref("collegeadmins/" + obj.key)
            .remove()
            .then(() => {
              console.log("reference data removed");
              fetch(urls.addLicenseadmin, {
                method: "POST",
                body: JSON.stringify(d),
                headers: {
                  "Content-type": "application/json; charset=UTF-8",
                },
              })
                .then((res) => {
                  let obj = { roles: ["college_admin"] };
                  //Setting the user_role of collegeadmin
                  fetch(urls.setUserRole, {
                    method: "POST",
                    body: JSON.stringify({
                      role: obj,
                      uid: firebase.auth().currentUser.uid,
                    }),
                    headers: {
                      "Content-type": "application/json; charset=UTF-8",
                    },
                  })
                    .then((res) => {
                      console.log(res.text);
                      AsyncStorage.setItem("profile", JSON.stringify(d)); //set the profile in async storage
                      AsyncStorage.getItem("profile").then((s) => {
                        this.props.navigation.navigate("adminPage");
                        console.log("data saved", s);
                      });
                      //navigating to the appPage.
                    })
                    .catch((err) => {
                      console.log(err);
                    });
                })
                .catch((error) => {
                  console.log(
                    "There has been a problem with your fetch operation: " +
                      error.msg
                  );
                  // ADD THIS THROW error
                });
            });
        } else {
          console.log("failed");
        }
      });
  };

  uploadPhoto = async () => {
    try {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL); //Asking user for storage permissions.
      if (status == "granted") {
        let result = await ImagePicker.launchImageLibraryAsync({
          //Selecting user from local storage.
          mediaTypes: ImagePicker.MediaTypeOptions.Image,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
        if (!result.cancelled) {
          this.setState({ image: result.uri, imageLoading: true });
          const response = await fetch(result.uri);
          const blob = await response.blob(); //getting the binary form of image.
          var ref = firebase
            .storage()
            .ref()
            .child(`profile_images/${this.state.email}`)
            .put(blob); //storing the image binary data in cloud storage.
          Toast.show({ text: "Uploading Image", buttonText: "Okay" });
          return ref.on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
              // Observe state change events such as progress, pause, and resume
              // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
              progress =
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              console.log("Upload is " + progress + "% done");
              switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                  console.log("Upload is paused");
                  break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                  console.log("Upload is running");
                  break;
              }
            },
            (error) => {
              // Handle unsuccessful uploads
              console.log("Upload unsuccessfull");
            },
            () => {
              // Handle successful uploads on complete
              // For instance, get the download URL: https://firebasestorage.googleapis.com/...
              Toast.show({ text: "Uploaded Image", buttonText: "Okay" });
              ref.snapshot.ref.getDownloadURL().then((downloadURL) => {
                // console.log('File available at', downloadURL);
                this.setState({ image: downloadURL, imageLoading: false });
              });
            }
          );
        }
        // console.log(result);
      } else {
        Alert.alert("Required storage permissions.");
      }
    } catch (E) {
      console.log(E);
    }
  };

  render() {
    return (
      <View style={styles.overall}>
        <KeyboardAvoidingView
          behavior={Platform.Os == "ios" ? "padding" : "height"}
          style={{ flex: 1 }}
        >
          <ScrollView
            style={styles.container}
            keyboardShouldPersistTaps="handled"
          >
            <View style={styles.detailsContainer}>
              {/* <Text style={styles.subtitle}>Photo<Text style={styles.req}>*</Text></Text> */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignContent: "center",
                }}
              >
                <Avatar
                  onPress={() => {
                    this.uploadPhoto();
                  }}
                  editButton={{
                    name: "mode-edit",
                    type: "material",
                    color: "#fff",
                    underlayColor: "#0f0",
                  }}
                  showEditButton
                  size={100}
                  rounded
                  source={{ uri: this.state.image }}
                  avatarStyle={{}}
                  icon={{ name: "user", type: "font-awesome" }}
                />
              </View>
              <Text style={styles.subtitle}>
                Name <Text style={styles.req}>*</Text>
              </Text>
              <TextInput
                selectTextOnFocus={true}
                placeholder="Name"
                underlineColorAndroid="transparent"
                style={styles.TextInputStyle}
                value={this.state.name}
                onChangeText={(text) => this.setState({ name: text })}
              />
              <Text style={styles.subtitle}>
                Email Id <Text style={styles.req}>*</Text>
              </Text>
              <TextInput
                selectTextOnFocus={true}
                editable={false}
                autoCapitalize="none"
                placeholder="Email"
                underlineColorAndroid="transparent"
                style={styles.TextInputStyle}
                value={this.state.email}
                onChangeText={(text) => this.setState({ email: text })}
              />
              <Text style={styles.subtitle}>
                Phone Number <Text style={styles.req}>*</Text>
              </Text>
              <TextInput
                selectTextOnFocus={true}
                placeholder="Mobile Number"
                underlineColorAndroid="transparent"
                style={styles.TextInputStyle}
                keyboardType={"numeric"}
                value={this.state.phoneNumber}
                onChangeText={(text) => this.setState({ phoneNumber: text })}
              />
              <Text style={styles.subtitle}>
                LinkedIn Profile <Text style={styles.req}>*</Text>
              </Text>
              <TextInput
                selectTextOnFocus={true}
                autoCapitalize="none"
                placeholder="LinkedIn profile url..linkedin.com/in/username"
                underlineColorAndroid="transparent"
                style={styles.TextInputStyle}
                value={this.state.linkedIn.toLowerCase()}
                onChangeText={(text) =>
                  this.setState({ linkedIn: text.toLowerCase() })
                }
              />
              <Text style={styles.subtitle}>
                Roll No <Text style={styles.req}>*</Text>
              </Text>
              <TextInput
                selectTextOnFocus={true}
                autoCapitalize="characters"
                placeholder="Enter roll number in block letters"
                underlineColorAndroid="transparent"
                style={styles.TextInputStyle}
                value={this.state.rollNumber}
                onChangeText={(text) => this.setState({ rollNumber: text })}
              />
              <Dropdown
                label="Institution *"
                labelFontSize={18}
                itemCount={6}
                labelTextStyle={styles.dropDownStyle}
                animationDuration={0}
                data={this.state.institutionsList}
                value={this.state.institutionName}
                valueExtractor={(item, index) => {
                  return item.name;
                }}
                onChangeText={async (v, i, d) => {
                  await this.setState({ institutionName: v });
                  await this.setState({
                    collegeName: "None",
                    course: "None",
                    branch: "None",
                  });
                  await this.setState({
                    collegesList: this.state.institutionsList.find(
                      (x) => x.name == this.state.institutionName
                    )
                      ? this.state.institutionsList.find(
                          (x) => x.name == this.state.institutionName
                        ).colleges
                      : [],
                  });
                  await this.setState({
                    coursesList: this.state.collegesList.find(
                      (x) => x.name == this.state.collegeName
                    )
                      ? this.state.collegesList.find(
                          (x) => x.name == this.state.collegeName
                        ).courses
                      : [],
                  });
                  await this.setState({
                    branchesList: this.state.coursesList.find(
                      (x) => x.course == this.state.course
                    )
                      ? this.state.coursesList.find(
                          (x) => x.course == this.state.course
                        ).branches
                      : [],
                  });
                }}
              />
              <Dropdown
                label="College *"
                labelFontSize={18}
                itemCount={6}
                labelTextStyle={styles.dropDownStyle}
                data={this.state.collegesList}
                value={this.state.collegeName}
                valueExtractor={(item, index) => {
                  return item.name;
                }}
                onChangeText={async (v, i, d) => {
                  await this.setState({ collegeName: v });
                  await this.setState({ course: "None", branch: "None" });
                  await this.setState({
                    coursesList: this.state.collegesList.find(
                      (x) => x.name == this.state.collegeName
                    )
                      ? this.state.collegesList.find(
                          (x) => x.name == this.state.collegeName
                        ).courses
                      : [],
                  });
                  // console.log(this.state.coursesList);
                  await this.setState({
                    branchesList: this.state.coursesList.find(
                      (x) => x.course == this.state.course
                    )
                      ? this.state.coursesList.find(
                          (x) => x.course == this.state.course
                        ).branches
                      : [],
                  });
                }}
              />
              <Dropdown
                label="Course *"
                labelFontSize={18}
                itemCount={6}
                labelTextStyle={styles.dropDownStyle}
                data={this.state.coursesList}
                value={this.state.course}
                valueExtractor={(item, index) => {
                  return item.course;
                }}
                onChangeText={async (v, i, d) => {
                  await this.setState({ course: v });
                  await this.setState({ branch: "None" });
                  await this.setState({
                    branchesList: this.state.coursesList.find(
                      (x) => x.course == this.state.course
                    )
                      ? this.state.coursesList.find(
                          (x) => x.course == this.state.course
                        ).branches
                      : [],
                  });
                  // console.log(this.state.branchesList)
                }}
              />
              <Dropdown
                label="Branch *"
                labelFontSize={18}
                itemCount={6}
                labelTextStyle={styles.dropDownStyle}
                data={this.state.branchesList}
                value={this.state.branch}
                valueExtractor={(item, index) => {
                  return item.branch;
                }}
                onChangeText={async (v, i, d) => {
                  this.setState({ branch: v });
                }}
              />

              <View style={styles.saveView}>
                <Button
                  icon={
                    <Icon
                      type="font-awesome"
                      name="save"
                      size={20}
                      color="white"
                    />
                  }
                  containerStyle={styles.saveContainerStyle}
                  buttonStyle={styles.saveButtonStyle}
                  onPress={() => {
                    this.saveButton();
                  }}
                  titleStyle={styles.saveButtonTitleStyle}
                  title="Save "
                />
                {this.state.loading && <ActivityIndicator size="large" />}
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  overall: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 10,
  },
  interestsView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  deleteOverlayContainer: {
    backgroundColor: "rgba(25,100,200,1)",
  },
  deleteContainerStyle: {
    padding: 7,
  },
  saveButtonTitleStyle: {
    padding: 5,
  },
  saveContainerStyle: {
    padding: 10,
  },
  about: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.9,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    height: 100,
    textAlign: "left",
  },
  saveButtonStyle: {
    backgroundColor: "rgba(200,175,0,1)",
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    width: Dimensions.get("window").width * 0.4,
  },
  saveView: {
    flexDirection: "row",
    justifyContent: "center",
  },
  subView: {
    flexDirection: "row",
    borderRadius: 10,
    borderWidth: 2,
    backgroundColor: "rgba(100,100,255,0.4)",
    alignItems: "center",
  },
  adderLists: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.76,
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
  },
  TextInputStyle: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.9,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 5,
    color: "black",
  },
  detailsContainer: {
    flexDirection: "column",
    justifyContent: "flex-start",
    margin: 15,
    alignContent: "flex-start",
  },
  titleStyle: {
    fontSize: 25,
    fontWeight: "bold",
    textAlign: "center",
  },
  logoContainer: {
    flex: 0.75,
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: "white",
  },
  container: {
    flex: 1,
    flexDirection: "column",
  },
  logoStyle: {
    borderRadius: 5,
    height: 110,
    width: 200,
  },
  dropDownStyle: {
    paddingBottom: 5,
    fontWeight: "bold",
  },
  req: {
    color: "red",
  },
  dateStyle: {
    width: Dimensions.get("window").width * 0.9,
    marginTop: 10,
    marginBottom: 10,
  },
  interestsStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  interestsContentContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  interestsSubContent: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    paddingStart: 10,
    borderRadius: 10,
    borderWidth: 2,
    backgroundColor: "rgba(100,100,255,0.4)",
    alignItems: "center",
  },
  interestsTextStyle: {
    fontSize: 15,
    fontWeight: "bold",
  },
  deleteOverlay: {
    backgroundColor: "rgba(100,255,255,0.1)",
    padding: 0,
  },
});
