import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  Linking,
  Alert,
  BackHandler,
  Dimensions,
} from "react-native";
import { SocialIcon, Button, Icon } from "react-native-elements";
import { Toast } from "native-base";
import firebase from "firebase";
import * as google from "expo-google-app-auth";
import { googleConfig } from "../../../env.json";
import { urls } from "../../../env.json";
import * as Crypto from "expo-crypto";
import * as AppleAuthentication from "expo-apple-authentication";
import Constants from "expo-constants";
import { TouchableOpacity } from "react-native-gesture-handler";
import VersionCheck from "react-native-version-check-expo";
/*

Below is the login flow

> Any user logins with signup with google button.
> selects his email and then authenticates to firebase with google credentials.
>  if his record is already added by the admin(ALUMNI) then you will be redireted to AlumniDetailsSignUp if his is signing in first time or else redirected to mentorapppage.
     (or)
   if his record is in collegeadmins section redirects him to the AdminUserSignup if he logs in for the first time else he will be redirected to adminapppage.
      (or)  
   if his record not exists in mentors section in database then he will be considered as a mentee and then checks for his role if he is a admin or a mentee.
     *if he is a mentee then
        -->if he is a new user then redirect him to NonAlumniusersignup page. 
        -->if he is a old user update his last loggedin in database and redirect to mentee app page.     
     *if he is an admin then redirect to admin page updating his last loggedin in database. 
>note:by default any user will be redirected to nonalumnidetailssignup page.


*/

export default class AuthScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      result: "",
      checked: "Yes",
      alumni: null,
      loading: true,
      laodingauth: false,
      studentAuthDisabled: false,
      alumniAuthDisabled: false,
      alumniPresent: false,
      loadingauthApple: false,
      email: "",
      authcheckadmin: false,
      authdeletedadmin: false,
      collegeadminchecked: false,
      device_id: "",
    };
  }

  async componentDidMount() {
    //checking whether a mentee already present
    //added
    console.disableYellowBox = true;
    const loginAvailable = await AppleAuthentication.isAvailableAsync();

    //console.log(loginAvailable)
    this.setState({ appleLogin: loginAvailable });
    let store;
    let link;
    if (loginAvailable) {
      store = "appStore";
      link = "https://apps.apple.com/in/app/mentorkonnect/id1514969823";
    } else {
      store = "playStore";
      link =
        "https://play.google.com/store/apps/details?id=com.mycollegekonnect.mentorkonnect";
    }
    VersionCheck.getLatestVersion({
      provider: store,
    }).then((latestVersion) => {
      VersionCheck.needUpdate({
        currentVersion: VersionCheck.getCurrentVersion(),
        latestVersion: latestVersion,
      }).then((res) => {
        console.log(res.isNeeded); // true
        if (res.isNeeded) {
          Alert.alert(
            "App Update",
            "Dear User, you will have to update your app to the latest version to continue using",
            [
              {
                text: "Update Now",
                onPress: () => {
                  BackHandler.exitApp();
                  Linking.openURL(link);
                },
              },
            ],
            { cancelable: false }
          );
        } else {
          var unsubscribe = firebase.auth().onAuthStateChanged((user) => {
            //This listener must only check once whether user exists or not so we stop it from listening more than once by calling unsubscribe()

            unsubscribe();
            console.log(user);
            if (user) {
              firebase
                .database()
                .ref(`mentees/${user.uid}`)
                .once("value")
                .then((snap) => {
                  console.log(snap.exists());
                  if (!snap.exists()) {
                    this.setState({ loadingauth: false });
                    this.setState({ studentAuthDisabled: false });
                    this.checkforalumni();
                    return false;
                  }
                  return true;
                })
                .then((res) => {
                  if (res) {
                    fetch(urls.isAdminv2, {
                      method: "POST",
                      body: JSON.stringify({
                        uid: firebase.auth().currentUser.uid,
                      }),
                      headers: {
                        "Content-type": "application/json; charset=UTF-8",
                      },
                    })
                      .then((res) => res.json())
                      .then((res) => {
                        if (res.role == "admin") {
                          this.props.navigation.navigate("adminPage");
                        } else if (res.role == "mentee") {
                          console.log(
                            "User already signed in Firebase navigating to mentee page."
                          );
                          this.props.navigation.navigate("MenteeAppPage");
                        } else {
                          this.setState({ loadingauth: false });
                          this.setState({ studentAuthDisabled: false });
                        }
                      });
                  }
                });
            } else {
              this.setState({ loading: false });
            }
          });
        }
      });
    });
  }
  checkforalumni = () => {
    fetch(urls.getAlumnibyEmail, {
      method: "POST",
      body: JSON.stringify({
        email: firebase.auth().currentUser.email,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then(async (res) => {
        //res.found will be true if his/her records is found in mentors section of database
        if (res.found == true) {
          if (
            res.profile.authorized == true &&
            res.profile.updateprof == true
          ) {
            console.log("user already signed in navigationg to mentorpage");
            this.props.navigation.navigate("MentorAppPage");
          }
        } else {
          this.setState({ loadingauth: false }); //, loading: false });
          this.setState({ studentAuthDisabled: false });
          this.checkforcollegeadmin();
        }
      });
  };
  checkforcollegeadmin = () => {
    //check for collegeadmin
    fetch(urls.findAdmin, {
      method: "POST",
      body: JSON.stringify({
        email: firebase.auth().currentUser.email,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        if (
          res.Adminfound === false ||
          (res.Adminfound === true && res.profile.authorized === undefined)
        ) {
          this.setState({ loadingauth: false }); //, loading: false
          this.setState({ studentAuthDisabled: false });

          this.props.navigation.navigate("PreviewAppPage");
        } else {
          let profileobj = res.profile;
          console.log("database", profileobj);
          profileobj.last_logged_in = Date().toLocaleString();
          fetch(
            urls.updateLicenseadmin, //To save admin details to database.
            {
              method: "POST",
              body: JSON.stringify(profileobj),
              headers: {
                "Content-type": "application/json; charset=UTF-8",
              },
            }
          ).then((res) => {
            AsyncStorage.setItem("profile", JSON.stringify(profileobj)).then(
              () => {
                this.props.navigation.navigate("adminPage");
              }
            );
          });
        }
        return true;
      });
  };

  firebaseGoogleSignIn = async (data) => {
    //console.log('auser' + JSON.stringify(data));
    var googleUser = data.auser;
    var provider = data.provider;

    var unsubscribe = firebase
      .auth()
      .onAuthStateChanged(async (firebaseUser) => {
        unsubscribe();
        // user signs in already but comes back to main page without filling sign up details
        //checking whether user is already firebase authenticated.
        if (!this.isUserEqual(googleUser, firebaseUser)) {
          //if user is not firebase authenticated this below code executes
          // Through expo google login we get access token and id token then we form a GoogleAuthProviders credential
          //var provider = googleUser.provider;
          var credential;
          //console.log('Provider:' + provider);
          if (provider == "Apple") {
            const oauthProvider = new firebase.auth.OAuthProvider("apple.com");
            credential = oauthProvider.credential({
              idToken: googleUser.identityToken,
              rawNonce: data.rawNonce,
            });
          } else {
            credential = firebase.auth.GoogleAuthProvider.credential(
              googleUser.idToken,
              googleUser.accessToken
            );
          }
          //Sign in through firebase using above credential
          firebase
            .auth()
            .signInWithCredential(credential)
            .then(async (result) => {
              console.log(
                "signing in with your credentials into firebase",
                result
              );
              //differentiating whether u r alumni or not
              console.log("lookingfor Alumni");
              fetch(urls.getAlumnibyEmail, {
                method: "POST",
                body: JSON.stringify({
                  email: firebase.auth().currentUser.email,
                }),
                headers: {
                  "Content-type": "application/json; charset=UTF-8",
                },
              })
                .then((res) => res.json())
                .then(async(res) => {
                  console.log(res);
                  //res.found will be true if his/her records is found in mentors section of database
                  if (res.found == true) {
                    console.log("alumini present");

                    console.log(res);
                    //redirecting to the Details Sign up Page.
                    if (res.profile.authorized == false) {
                      this.props.navigation.navigate("AlumniDetailsSignUp", {
                        data: res.profile,
                        navigation: this.props.navigation.getParam(
                          "navigation"
                        ),
                      });
                    } else {
                      //cheking whether your profile isreplacedwith uid or not for alumni
                      if (res.profile.updateprof == undefined) {
                        console.log(
                          "deleting your old profile and updating with your uid"
                        );
                        var d = {
                          ...res.profile,
                          uid: firebase.auth().currentUser.uid,
                          updateprof: true,
                          mentoring_interest_other_college: false,
                          no_of_events: {
                            no_of_bootcamp_events: 0,
                            no_of_overview_events: 0,
                            no_of_simple_events: 0,
                          },
                        };
                        d.last_logged_in = Date().toLocaleString();
                        d.flag = true; //flag indicates whether to update reg code or not
                        console.log(d);

                        await firebase
                          .database()
                          .ref(`mentors/${res.profile.phone_number}`)
                          .remove()
                          .then(() => {
                            console.log("Removed successfully");
                          })
                          .catch((error) => {
                            console.log(error);
                          });
                        let info = {
                          id: res.profile.phone_number,
                          uid: firebase.auth().currentUser.uid,
                        };
                        //to change the old events to get updated i.e changing mentor id from phonenumber to uid
                        fetch(urls.updateAlumniEventRequests, {
                          method: "POST",
                          body: JSON.stringify(info),
                          headers: {
                            "Content-type": "application/json; charset=UTF-8",
                          },
                        })
                          .then(() => {
                            console.log("Event STATUS UPDATED");
                            //return res.json();
                          })
                          .catch((err) => {
                            console.log(err);
                          });

                        AsyncStorage.setItem("profile", JSON.stringify(d))
                          .then(async () => {
                            try {
                             await fetch(urls.addAlumniJNTUKMentor, {
                                method: "POST",
                                body: JSON.stringify(d),
                                headers: {
                                  "Content-type":
                                    "application/json; charset=UTF-8",
                                },
                              })
                                .then((res) => {
                                  console.log("STATUS UPDATED", res.statusText);
                                  //return res.json();
                                })
                                .then((res) => {
                                  let obj = { roles: ["alumni"] };

                                  fetch(urls.setUserRole, {
                                    method: "POST",
                                    body: JSON.stringify({
                                      role: obj,
                                      uid: firebase.auth().currentUser.uid,
                                    }),
                                    headers: {
                                      "Content-type":
                                        "application/json; charset=UTF-8",
                                    },
                                  })
                                    .then((res) => {
                                      console.log(res.text);
                                      this.props.navigation.navigate(
                                        "MentorAppPage"
                                      ); //navigating to the appPage.
                                    })
                                    .catch((err) => {
                                      console.log(err);
                                    });
                                  console.log("updated successfull");
                                })
                                .catch((err) => {
                                  console.log(err);
                                });
                            } catch (err) {
                              console.log(err);
                            }
                          })
                          .catch((err) => console.log(err));
                      } else {
                        console.log(
                          "your profile has already pushed with uid updating your lastlogged in"
                        );
                        Toast.show({
                          text: "Welcome back " + res.profile.name,
                          buttonText: "Okay",
                        });

                        let d = res.profile;
                        console.log(d);
                        d.last_logged_in = Date().toLocaleString();
                        d.flag = true; //flag indicates whether to update reg code or not
                        AsyncStorage.setItem("profile", JSON.stringify(d))
                          .then(async () => {
                            try {
                              this.props.navigation.navigate("MentorAppPage");

                              fetch(urls.updateAlumniJNTUKMentor, {
                                //updating logged in information in database
                                method: "POST",
                                body: JSON.stringify(d),
                                headers: {
                                  "Content-type":
                                    "application/json; charset=UTF-8",
                                },
                              })
                                .then((res) => {
                                  console.log("STATUS", res.statusText);
                                  console.log("MENTOR PAGE");
                                })
                                .catch((err) => {
                                  console.log(err);
                                });
                            } catch (err) {
                              console.log(err);
                            }
                          })
                          .catch((err) => console.log(err));
                      }
                      this.setState({ loading: false });
                    }
                  } else {
                    if (res.found == false) {
                      console.log("looking for college admin");
                      //determining whether you are a college admin or not.
                      fetch(urls.findAdmin, {
                        method: "POST",
                        body: JSON.stringify({
                          email: firebase.auth().currentUser.email,
                        }),
                        headers: {
                          "Content-type": "application/json; charset=UTF-8",
                        },
                      })
                        .then((res) => {
                          return res.json();
                        })
                        .then((res) => {
                          console.log("res", res);
                          if (res.Adminfound == true) {
                            //checking whether the collegeadmin is a new user or not.
                            if (res.profile.authorized == undefined) {
                              let obj = res.profile;
                              console.log("objectt", obj);

                              this.props.navigation.navigate(
                                "AdminUserSignUp",
                                {
                                  navigation: this.props.navigation,
                                  data: obj,
                                }
                              );
                              this.setState({ loadingauth: false });
                              this.setState({ studentAuthDisabled: false });
                            } else {
                              console.log("old college admin signing");

                              let profileobj = res.profile;
                              console.log("database", profileobj);
                              profileobj.last_logged_in = Date().toLocaleString();
                              fetch(
                                urls.updateLicenseadmin, //To save admin details to database.
                                {
                                  method: "POST",
                                  body: JSON.stringify(profileobj),
                                  headers: {
                                    "Content-type":
                                      "application/json; charset=UTF-8",
                                  },
                                }
                              ).then((res) => {
                                AsyncStorage.setItem(
                                  "profile",
                                  JSON.stringify(profileobj)
                                ).then(() => {
                                  this.props.navigation.navigate("adminPage");
                                });
                              });
                            }
                          } else {
                            //if he is a new user on the firebase we need to take his signup details..
                            //console.log('Firebase signin result' + JSON.stringify(result));

                            if (result.additionalUserInfo.isNewUser) {
                              //Navigating to the NonAlumniUserSignUp.
                              var user = firebase.auth().currentUser;
                              if (user) {
                                this.props.navigation.navigate(
                                  "PreviewAppPage",
                                  {
                                    navigation: this.props.navigation,
                                  }
                                );
                                this.setState({ loadingauth: false });
                                this.setState({ studentAuthDisabled: false });
                              }
                            }
                            //user exists.
                            else {
                              var user = firebase.auth().currentUser;

                              firebase
                                .database()
                                .ref(`mentees/${user.uid}`)
                                .once("value")
                                .then((snap) => {
                                  if (!snap.exists()) {
                                    this.props.navigation.navigate(
                                      "PreviewAppPage",
                                      {
                                        navigation: this.props.navigation,
                                      }
                                    );
                                    this.setState({ loadingauth: false });
                                    this.setState({
                                      studentAuthDisabled: false,
                                    });
                                    return false;
                                  }

                                  return true;
                                })
                                .then((res) => {
                                  if (res) {
                                    fetch(urls.isAdminv2, {
                                      method: "POST",
                                      body: JSON.stringify({
                                        uid: firebase.auth().currentUser.uid,
                                      }),
                                      headers: {
                                        "Content-type":
                                          "application/json; charset=UTF-8",
                                      },
                                    })
                                      .then((res) => res.json())
                                      .then(async (d) => {
                                        //console.log(d);
                                        if (d.role == "admin") {
                                          Toast.show({
                                            text: "you are signing in as admin",
                                            buttonText: "Okay",
                                          });
                                          await AsyncStorage.setItem(
                                            "profile",
                                            JSON.stringify(d.data)
                                          );
                                          this.props.navigation.navigate(
                                            "adminPage"
                                          );
                                          await AsyncStorage.setItem(
                                            "signout",
                                            "false"
                                          );
                                          console.log(
                                            "Admin signed in, updating logged in time in db"
                                          );

                                          console.log("Login Success.");
                                          this.setState({
                                            laodingauth: false,
                                            loadingauthApple: false,
                                            studentAuthDisabled: false,
                                          });
                                          return;
                                        } else if (d.role == "mentee") {
                                          Toast.show({
                                            text:
                                              "you are signing in as mentee",
                                            buttonText: "Okay",
                                          });
                                          if (
                                            d.data["device_id"] != undefined &&
                                            d.data["device_id"] != null &&
                                            d.data["device_id"] != ""
                                          ) {
                                            let new_deviceId =
                                              Constants.deviceId;
                                            /*If the device_id already exists  in db then , find the new_device id in which the user logs in
                                    and compare it with the one in db ,
                                     -->if it is same then navigate to menteeAppPage
                                     --->else update the deviceId with the new device Id and then navigate to menteeAppPage */

                                            if (
                                              new_deviceId !=
                                              d.data["device_id"]
                                            ) {
                                              d.data[
                                                "device_id"
                                              ] = new_deviceId;
                                              const url = `${urls.updateDeviceId}?device_id=${new_deviceId}`;
                                              fetch(url, {
                                                method: "POST",
                                                headers: {
                                                  Accept: "application/json",
                                                  "Content-Type":
                                                    "application/json",
                                                },
                                                body: JSON.stringify(d.data),
                                              });
                                              //if user logs in with new device then his device id is updated
                                            }
                                            //then naviagted to menteeApppage after updating profile
                                            await AsyncStorage.setItem(
                                              "profile",
                                              JSON.stringify(d.data)
                                            );
                                            this.props.navigation.navigate(
                                              "MenteeAppPage"
                                            );
                                            await AsyncStorage.setItem(
                                              "signout",
                                              "false"
                                            );
                                            console.log(
                                              "existing user signed in, updating logged in time in db,updated the deviceId"
                                            );
                                          } else {
                                            /*if an existing user logs in and if his device_id is null and not defined yet
                                  then, we have to find the device id and update it in his profile and then navigate him to menteeAppPage*/
                                            let deviceId = Constants.deviceId;
                                            this.setState({
                                              device_id: deviceId,
                                            });

                                            d.data[
                                              "device_id"
                                            ] = this.state.device_id;
                                            //console.log(d);
                                            const url = `${urls.updateDeviceId}?device_id=${this.state.device_id}`;
                                            fetch(url, {
                                              method: "POST",
                                              headers: {
                                                Accept: "application/json",
                                                "Content-Type":
                                                  "application/json",
                                              },
                                              body: JSON.stringify(d.data),
                                            });
                                            //console.log(d.data);
                                            await AsyncStorage.setItem(
                                              "profile",
                                              JSON.stringify(d.data)
                                            );
                                            this.props.navigation.navigate(
                                              "MenteeAppPage"
                                            );
                                            await AsyncStorage.setItem(
                                              "signout",
                                              "false"
                                            );
                                            console.log(
                                              "existing user signed in, updating logged in time in db"
                                            );

                                            console.log("Login Success.");
                                          }
                                          this.setState({
                                            loadingauthApple: false,
                                            laodingauth: false,
                                            studentAuthDisabled: false,
                                          });
                                        } else {
                                          await AsyncStorage.setItem(
                                            "profile",
                                            d.data
                                          );
                                          await AsyncStorage.setItem(
                                            "signout",
                                            "false"
                                          );
                                          console.log(
                                            "User already signed in Firebase."
                                          );
                                          this.props.navigation.navigate(
                                            "MenteeAppPage"
                                          );
                                        }
                                      })

                                      .catch(function (error) {
                                        var errorCode = error.code;
                                        var errorMessage = error.message;
                                        // The email of the user's account used.
                                        var email = error.email;

                                        console.log(
                                          "error code:" +
                                            errorCode +
                                            " error msg:" +
                                            errorMessage
                                        );
                                      });
                                  }
                                })
                                .catch(function (error) {
                                  var errorCode = error.code;
                                  var errorMessage = error.message;
                                  // The email of the user's account used.
                                  var email = error.email;

                                  console.log(
                                    "error code:" +
                                      errorCode +
                                      " error msg:" +
                                      errorMessage
                                  );
                                });
                            }
                          }
                        });
                    }
                  }
                })
                .catch((err) => {
                  console.log(err);
                });
            });
        }
      });
  };
  //to check whether a user is already firebase authenticated
  isUserEqual = (googleUser, firebaseUser) => {
    if (firebaseUser) {
      var providerData = firebaseUser.providerData;
      for (var i = 0; i < providerData.length; i++) {
        if (
          providerData[i].providerId ===
            firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
          providerData[i].uid === googleUser.user.id
        ) {
          return true;
        }
        return false;
      }
    }
  };

  expoGoogleSignUp = async () => {
    console.disableYellowBox = true;
    //deleting your firebase authentication as you are not active in database
    //this is the case where user signs in and then closes his/her app without entering into database or filling the form(for mentee) or saving the details(for alumni)
    var user = firebase.auth().currentUser;
    if (user) {
      fetch(urls.getAlumnibyEmail, {
        method: "POST",
        body: JSON.stringify({ email: firebase.auth().currentUser.email }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      })
        .then((res) => {
          return res.json();
        })
        .then(async (res) => {
          //response will be false if your record is not available in mentors
          if (res.found == false) {
            //checking whether you are a collegeadmin or not
            fetch(urls.findAdmin, {
              method: "POST",
              body: JSON.stringify({
                email: firebase.auth().currentUser.email,
              }),
              headers: {
                "Content-type": "application/json; charset=UTF-8",
              },
            })
              .then((res) => {
                return res.json();
              })
              .then(async (res) => {
                //response will be false if your record is not available in collegeadmins
                if (res.Adminfound == true) {
                  if (res.profile.authorized == undefined) {
                    firebase
                      .auth()
                      .signOut()
                      .then(() => {
                        user
                          .delete()
                          .then((res) => {
                            console.log("user deleted");
                            console.log("Admin Routee");
                            this.setState({
                              loadingauth: false,
                              loading: false,
                              authdeletedadmin: true,
                            });
                            this.setState({ studentAuthDisabled: false });
                          })
                          .catch((err) => {
                            console.log(err);
                          });
                      });
                  }
                } else {
                  //if you are not a collegeadmin then it is the case of a menteee
                  firebase
                    .auth()
                    .signOut()
                    .then(() => {
                      console.log("user signed out");
                      firebase
                        .database()
                        .ref(`mentees/${user.uid}`)
                        .once("value")
                        .then((snap) => {
                          if (!snap.exists()) {
                            user
                              .delete()
                              .then((res) => {
                                console.log("user deleted");
                                console.log("Mentee Routee");
                                this.setState({
                                  loadingauth: false,
                                  loading: false,
                                });
                                this.setState({ studentAuthDisabled: false });
                              })
                              .catch((err) => {
                                console.log(err);
                              });
                          }
                        });
                    })
                    .catch((err) => console.log(err));
                }
              });
          } else {
            firebase
              .auth()
              .signOut()
              .then(() => {
                console.log("user signed out");
                if (res.profile.authorized == false) {
                  user
                    .delete()
                    .then((res) => {
                      console.log("user deleted");
                      console.log("Mentor Routee");
                      this.setState({ loadingauth: false, loading: false });
                      this.setState({ studentAuthDisabled: false });
                    })
                    .catch((err) => {
                      console.log(err);
                    });
                }
              })
              .catch((err) => console.log(err));
          }
        });
    }

    try {
      //Sign in through google redirecting to website to login
      let result = await google.logInAsync(googleConfig);

      console.log(result);
      if (result.type == "success") {
        this.setState({ result: result });
        //if success log in through firebase also.

        const nonce = Math.random().toString(36).substring(2, 10);

        await this.firebaseGoogleSignIn({
          auser: result,
          provider: "Google",
          rawNonce: nonce,
        });
      } else {
        Toast.show({ text: "Login Failed!", buttonText: "Okay" });
        this.setState({ loadingauth: false });
        this.setState({ studentAuthDisabled: false });
        console.log("Login Failed.");
      }
    } catch (error) {
      console.log(error);
      Toast.show({ text: "Something error occured", buttonText: "Okay" });
      this.setState({ loadingauth: false });
      this.setState({ studentAuthDisabled: false });
    }
  };

  // Use this to conditionally display the Apple SignIn Button
  //isAppleLoginAvailable = AppleAuthentication.isAvailableAsync();

  loginWithApple = async () => {
    var user = firebase.auth().currentUser;
    if (user) {
      fetch(urls.getAlumnibyEmail, {
        method: "POST",
        body: JSON.stringify({ email: firebase.auth().currentUser.email }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      })
        .then((res) => {
          return res.json();
        })
        .then(async (res) => {
          //response will be false if your record is not available in mentors
          if (res.found == false) {
            console.log("looking for college admin");
            fetch(urls.findAdmin, {
              method: "POST",
              body: JSON.stringify({
                email: firebase.auth().currentUser.email,
              }),
              headers: {
                "Content-type": "application/json; charset=UTF-8",
              },
            })
              .then((res) => {
                return res.json();
              })
              .then(async (res) => {
                if (res.Adminfound == true) {
                  if (res.profile.authorized == undefined) {
                    console.log("college admin is trying to signin");
                    firebase
                      .auth()
                      .signOut()
                      .then(() => {
                        user
                          .delete()
                          .then((res) => {
                            console.log("user deleted");
                            console.log("Admin Routee");
                            this.setState({
                              loadingauth: false,
                              loading: false,
                              authdeletedadmin: true,
                            });
                            this.setState({ studentAuthDisabled: false });
                          })
                          .catch((err) => {
                            console.log(err);
                          });
                      });
                  }
                } else {
                  firebase
                    .auth()
                    .signOut()
                    .then(() => {
                      console.log("user signed out");
                      firebase
                        .database()
                        .ref(`mentees/${user.uid}`)
                        .once("value")
                        .then((snap) => {
                          console.log(snap.exists());
                          if (!snap.exists()) {
                            user
                              .delete()
                              .then((res) => {
                                console.log("user deleted");
                                console.log("Mentee Routee");
                                this.setState({
                                  loadingauth: false,
                                  loading: false,
                                });
                                this.setState({ studentAuthDisabled: false });
                              })
                              .catch((err) => {
                                console.log(err);
                              });
                          }
                        });
                    })
                    .catch((err) => console.log(err));
                }
              });
          } else {
            firebase
              .auth()
              .signOut()
              .then(() => {
                console.log("user signed out");
                if (res.profile.authorized == false) {
                  user
                    .delete()
                    .then((res) => {
                      console.log("user deleted");
                      console.log("Mentor Routee");
                      this.setState({ loadingauth: false, loading: false });
                      this.setState({ studentAuthDisabled: false });
                    })
                    .catch((err) => {
                      console.log(err);
                    });
                }
              })
              .catch((err) => console.log(err));
          }
        });
    }

    try {
      const csrf = Math.random().toString(36).substring(2, 15);
      const nonce = Math.random().toString(36).substring(2, 10);
      const hashedNonce = await Crypto.digestStringAsync(
        Crypto.CryptoDigestAlgorithm.SHA256,
        nonce
      );
      const appleCredential = await AppleAuthentication.signInAsync({
        requestedScopes: [
          AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
          AppleAuthentication.AppleAuthenticationScope.EMAIL,
        ],
        state: csrf,
        nonce: hashedNonce,
      });
      console.log("Apple Credential" + JSON.stringify(appleCredential));
      const { identityToken, email, state } = appleCredential;
      // add the credential to react state
      this.setState({ idToken: identityToken, rawNonce: hashedNonce });
      //this.firebaseGoogleSignIn ({ idToken: identityToken, rawNonce: nonce, provider: "Apple" });
      await this.firebaseGoogleSignIn({
        auser: appleCredential,
        provider: "Apple",
        rawNonce: nonce,
      });
      //console.log("Apple ID Token:" + identityToken);
      //console.log("Apple Email:" + email);
      //console.log("Apple ID State:" + state);
    } catch (error) {
      console.log(error);
      Toast.show({ text: "Something error occured", buttonText: "Okay" });
    }
  };

  sendMail = () => {
    const subject = "";
    Linking.openURL(`mailto:myCollegekonnect.app@gmail.com`);
  };

  render() {
    if (!this.state.loading) {
      return (
        <View style={styles.totalContainer}>
          <ScrollView
            style={styles.container}
            showsVerticalScrollIndicator={false}
          >
            <View style={styles.logoContainer}>
              <Image
                style={{
                  height: 200,
                  width: 350,
                  alignSelf: "center",
                  marginRight: 15,
                  marginVertical: 10,
                }}
                source={require("../../../assets/mycollegekonnect-logo.png")}
              />
              <Text style={styles.titleStyle}>MentorKonnect</Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: "white",
                borderRadius: 17,
                borderColor: "black",
                borderWidth: 1,
                margin: 12,
              }}
            >
              <View style={styles.authContainer}>
                <View style={styles.authContainer1}>
                  <Text style={styles.authSubContainer1}>Login/SignUp</Text>
                  <View style={{ flexDirection: "row", marginTop: 10 }}>
                    <SocialIcon
                      title="Sign Up/In With Google"
                      button
                      onPress={() => {
                        this.setState({
                          loadingauth: true,
                          studentAuthDisabled: true,
                        });
                        this.expoGoogleSignUp();
                      }}
                      style={styles.menteeAuthButton}
                      type="google"
                    />
                    {this.state.loadingauth && (
                      <ActivityIndicator size="small" />
                    )}
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    {this.state.appleLogin === true && (
                      <View style={{ alignItems: "center" }}>
                        <AppleAuthentication.AppleAuthenticationButton
                          buttonType={
                            AppleAuthentication.AppleAuthenticationButtonType
                              .SIGN_IN
                          }
                          buttonStyle={
                            AppleAuthentication.AppleAuthenticationButtonStyle
                              .BLACK
                          }
                          cornerRadius={30}
                          style={{
                            width: Dimensions.get("window").width * 0.7,
                            height: 50,
                          }}
                          onPress={() => {
                            this.setState({
                              loadingauthApple: true,
                              studentAuthDisabled: true,
                            });

                            this.loginWithApple();
                          }}
                        />
                      </View>
                    )}
                    {this.state.appleLogin && this.state.loadingauthApple && (
                      <ActivityIndicator size="small" />
                    )}
                  </View>
                </View>
              </View>
              <View
                style={{
                  marginTop: Dimensions.get("window").height * 0.04,
                  height: Dimensions.get("window").height * 0.2,

                  margin: 20,
                  borderColor: "#9d7d61",
                  borderWidth: 10,
                  backgroundColor: "#c9eea1",
                }}
              >
                <Text
                  style={{
                    fontSize: 17,
                    fontWeight: "bold",
                    alignSelf: "center",
                    marginBottom: 10,
                  }}
                >
                  Instructions :
                </Text>
                <Text style={{ marginLeft: 5 }}>
                  1.Any user, Sign up/in with google.{"\n"}
                </Text>
                <Text style={{ marginLeft: 5 }}>
                  2.For more information and for any issues please contact us.
                </Text>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  alignSelf: "center",
                  marginTop: 10,
                }}
              >
                <View style={{ alignSelf: "center" }}>
                  <TouchableOpacity
                    onPress={() => {
                      Linking.openURL(
                        "https://www.linkedin.com/in/collegekonnect/"
                      );
                    }}
                  >
                    <Image
                      source={require("../../../assets/linkedin-icon.png")}
                      style={{ width: 35, height: 35, alignSelf: "center" }}
                    />
                    <Text style={{ color: "blue" }}>follow us</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ alignSelf: "center", marginLeft: 60 }}>
                  <TouchableOpacity
                    onPress={() => {
                      Linking.openURL("https://www.mycollegekonnect.com/");
                    }}
                  >
                    <Image
                      source={require("../../../assets/website.png")}
                      style={{ width: 35, height: 35, alignSelf: "center" }}
                    />
                    <Text style={{ color: "green" }}>Visit us</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ marginLeft: 60 }}>
                  <TouchableOpacity onPress={() => this.sendMail()}>
                    <Image
                      source={require("../../../assets/mail.png")}
                      style={{ width: 35, height: 35, alignSelf: "center" }}
                    />

                    <Text style={{ color: "red" }}>Contact us</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.footerContainer}>
                <Text style={styles.footerStyle}>
                  mycollegekonnect.com © All rights reserved.
                </Text>
              </View>
            </View>
          </ScrollView>
        </View>
      );
    } else {
      return (
        <View style={styles.activityIndicatorViewStyle}>
          <Image
            style={{
              width: Dimensions.get("window").width,
              height: Dimensions.get("window").height * 0.8,
            }}
            source={require("./loader2.gif")}
          />
          <Image
            style={{ width: 50, height: 50, alignSelf: "center" }}
            source={require("./200.gif")}
          />
        </View>
      );
    }
  }
}
const styles = StyleSheet.create({
  imageStyle: { width: 120, height: 120 },
  titleStyle: {
    fontSize: 25,
    fontWeight: "bold",
    textAlign: "center",
  },
  lottie: {
    width: 300,
    height: 60,
  },
  logoContainer: {
    marginTop: Dimensions.get("window").height * 0.08,
    alignItems: "center",
    margin: 10,
  },
  titleStyle: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
    marginTop: 20,
  },
  container: {
    marginTop: 27,
  },
  logoStyle: {
    borderRadius: 5,
    height: 175,
    width: 300,
  },
  footerContainer: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    marginBottom: Dimensions.get("window").height * 0.01,
  },
  footerStyle: {
    marginTop: 15,
    fontSize: 15,
    color: "black",
    alignSelf: "center",
    alignContent: "flex-end",
    justifyContent: "flex-end",
  },
  signInButton: {
    flexDirection: "row",
    alignContent: "center",
    margin: 5,
    backgroundColor: "rgba(0,250,0,0.2)",
  },
  totalContainer: {
    backgroundColor: "#CBDEF4",
    flex: 1,
    justifyContent: "center",
  },

  authContainer1: {
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 0,
    marginTop: 15,
  },
  authContainer: {
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 0,
    marginTop: 15,
    backgroundColor: "white",
  },
  authSubContainer1: { fontSize: 17, fontWeight: "bold", marginBottom: 5 },
  menteeAuthButton: { width: Dimensions.get("window").width * 0.7 },
  instructionsViewContainer: {
    flexDirection: "column",
    margin: 15,
    marginBottom: 5,
    marginTop: Dimensions.get("window").height * 0.08,
  },
  instructionsText: { fontWeight: "bold", fontSize: 18, marginLeft: 10 },
  instructionsSubContainer: { flexDirection: "column", margin: 10 },
  instructionsField: { fontSize: 15, marginBottom: 20 },
  activityIndicatorViewStyle: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#F9FBFC",
    alignContent: "center",
    alignItems: "center",
  },
});
