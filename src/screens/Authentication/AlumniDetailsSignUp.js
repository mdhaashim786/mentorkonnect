import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Alert,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  ScrollView,
  Keyboard,
  Platform,
  Image,
  Text,
  Picker,
  TextInput,
  KeyboardAvoidingView,
} from "react-native";
import { Button, Avatar, Icon, CheckBox } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import DatePicker from "react-native-datepicker";
import { Notifications } from "expo";
import firebase from "firebase";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import { Toast } from "native-base";
import { urls } from "../../../env.json";
import Logos from "../../../assets/Logos";
import { FlatList } from "react-native-gesture-handler";
import PreBuiltEventTypes from "./../Events/PreBuiltEventTypes";
export default class AlumniDetailsSignUp extends Component {
  constructor(props) {
    super(props);

    //getting the data already stored on database.
    const data = this.props.navigation.getParam("data");

    this.state = {
      loading: false,
      interestmanip: false,
      imageLoading: false,
      professor: "",
      specialization: data.specialization,
      college_after_BTECH:
        data.college_after_BTECH == undefined ? "" : data.college_after_BTECH,
      about: data.about == undefined ? "" : data.about,
      patent: "",
      credential: "",
      expo_push_token: "",
      mentoringInterestOtherCollege: data.mentoring_interest_other_college,
      collegeName: data.college_name,
      name: data.name,
      course: data.course,
      institutionName: data.institution_name,
      yearOfGraduation: data.year_passed,
      phoneNumber: data.phone_number,
      result: "",
      collegeId:data.college_id,
      institutionId :data.institution_id,
      current_working_title:
        data.current_working_title != undefined
          ? data.current_working_title
          : "",
      current_working_company:
        data.current_working_company != undefined
          ? data.current_working_company
          : "",
      subject: "",
      mentoringInterest: "",
      interest: "",
      activity: "",
      available:
        data.available != undefined && data.available != ""
          ? data.available
          : "",
      image: Logos.defaultProfilePic,
      about: data.about == undefined ? "" : data.about,
      linkedIn: data.linkedin,
      email: data.email,
      higherEducation:
        data.higher_education != undefined ? data.higher_education : "None",
      branch: "Computer Science and Engineering (CSE)",
      branchesList: [],
      coursesList: [],
      interestsList:
        data.interests_list == undefined ? [] : data.interests_list,
      activityList: [],
      patentsList: data.patents == undefined ? [] : data.patents,
      collegesList: [],
      mentoringInterestList:
        data.mentoring_interest_list == undefined
          ? []
          : data.mentoring_interest_list,
      higherEducationList: [],
      institutionsList: [],
      subjectsList: data.subjects_liked == undefined ? [] : data.subjects_liked,
      professorList:
        data.professor_list == undefined ? [] : data.professor_list,
      key: "",
      preBuiltEventsList:
        data.pre_built_events_indices != undefined
          ? data.pre_built_events_indices
          : [], //getting all the pre built event types from the database
      pre_built_event_type: "",
      pre_built_event_index: null, //used to store the no.of prebuilt event types available
      pre_built_list: [], //storing values of events the mentor/mentee is intrested to mentor
    };
  }

  async componentDidMount() {
    //Notifications ON bydefault
    await this.registerForPushNotificationsAsync();
    var signedInUser = firebase.auth().currentUser; //we get details of user from navigation
    //setting all the details
    console.log("signedInUser", signedInUser);

    this.setState({
      image:
        signedInUser.photoURL != null
          ? signedInUser.photoURL
          : this.state.image,
    });
    //getting all the academic details.

    fetch(urls.getHigherEducation, { method: "GET" })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        let d = Array.from(res);
        // console.log(d)
        this.setState({ higherEducationList: d });
      })
      .catch((error) => {
        console.log(error);
        Toast.show({ text: "Some error occured", buttonText: "Okay" });
      });

    const data = await this.props.navigation.getParam("data");

    await this.setState({
      specialization: data.specialization,
      college_after_BTECH:
        data.college_after_BTECH == undefined ? "" : data.college_after_BTECH,
      about: data.about == undefined ? "" : data.about,
      patent: data.patent == undefined ? "" : data.patent,
      name: data.name,
      mentoringInterestOtherCollege: data.mentoring_interest_other_college,
      course: data.course,
      yearOfGraduation: data.year_passed,
      phoneNumber: data.phone_number,
      result: "",
      collegeId:data.college_id,

      current_working_title:
        data.current_working_title != undefined
          ? data.current_working_title
          : "",
      current_working_company:
        data.current_working_company != undefined
          ? data.current_working_company
          : "",

      available:
        data.available != undefined && data.available != ""
          ? data.available
          : "",
      image:
        signedInUser.photoURL != null
          ? signedInUser.photoURL
          : this.state.image,
      about: data.about == undefined ? "" : data.about,
      linkedIn: data.linkedin,
      institutionName: data.institution_name,
      email: data.email,
      institutionId :data.institution_id,

      higherEducation: data.higher_education,
      collegeName: data.college_name,
      institution_name: data.institution_name,
      branch: data.branch,
      course: data.course,
      interestsList:
        data.interests_list == undefined ? [] : data.interests_list,
      mentoringInterestList:
        data.mentoring_interest_list == undefined
          ? []
          : data.mentoring_interest_list,
      subjectsList: data.subjects_liked == undefined ? [] : data.subjects_liked,
      professorList:
        data.faculty_recommended == undefined ? [] : data.faculty_recommended,
      key: data.key,
      preBuiltEventIndices:
        data.pre_built_events_indices != undefined
          ? data.pre_built_events_indices
          : [],
    });
    this.setState({ pre_built_event_index: PreBuiltEventTypes.length });
    await this.setPreEvents();
    fetch(urls.getInstitutions)
      .then((res) => {
        return res.json();
      })
      .then(async (res) => {
        let d = Array.from(res);
        this.setState({ institutionsList: d });

        await this.setState({
          collegesList: this.state.institutionsList.find(
            (x) => x.name == this.state.institutionName
          )
            ? this.state.institutionsList.find(
                (x) => x.name == this.state.institutionName
              ).colleges
            : [],
        });
        await this.setState({
          coursesList: this.state.collegesList.find(
            (x) => x.name == this.state.collegeName
          )
            ? this.state.collegesList.find(
                (x) => x.name == this.state.collegeName
              ).courses
            : [],
        });
        await this.setState({
          branchesList: this.state.coursesList.find(
            (x) => x.course == this.state.course
          )
            ? this.state.coursesList.find((x) => x.course == this.state.course)
                .branches
            : [],
        });
      })
      .catch((error) => {
        console.log(error);
        Toast.show({ text: "Some error occured", buttonText: "Okay" });
      });
    await this.setState({ institutionName: data.institution_name });

    await this.setState({
      collegesList: this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      )
        ? this.state.institutionsList.find(
            (x) => x.name == this.state.institutionName
          ).colleges
        : [],
    });
    await this.setState({
      coursesList: this.state.collegesList.find(
        (x) => x.name == this.state.collegeName
      )
        ? this.state.collegesList.find((x) => x.name == this.state.collegeName)
            .courses
        : [],
    });
    await this.setState({
      branchesList: this.state.coursesList.find(
        (x) => x.course == this.state.course
      )
        ? this.state.coursesList.find((x) => x.course == this.state.course)
            .branches
        : [],
    });
    await this.setState({ collegeName: data.college_name });
    await this.setState({
      coursesList: this.state.collegesList.find(
        (x) => x.name == this.state.collegeName
      )
        ? this.state.collegesList.find((x) => x.name == this.state.collegeName)
            .courses
        : [],
    });
    await this.setState({
      branchesList: this.state.coursesList.find(
        (x) => x.course == this.state.course
      )
        ? this.state.coursesList.find((x) => x.course == this.state.course)
            .branches
        : [],
    });
  }
  setPreEvents = async () => {
    let preList = this.state.preBuiltEventIndices;
    //console.log(preList);
    let list = []; //0,1,3,4->true,true,false,true,true
    let k = 0;
    var t = this.state.pre_built_event_index;
    //console.log("t"+t);

    for (let i = 0; i < t; i++) {
      if (i == preList[k]) {
        list.push(true);
        k++;
      } else {
        list.push(false);
      }
    }
    //console.log("list"+list);
    await this.setState({ pre_built_list: list });
    //console.log("pre_built_list"+this.state.pre_built_list)
  };

  toggle = async (index) => {
    let pre_list = this.state.pre_built_list;
    pre_list[index] = !pre_list[index];
    await this.setState({ pre_built_list: pre_list });
  };
  //all the below functions work similar to the one's in NonAlumniUserSignUp.
  addMentoringInterest = () => {
    if (this.state.mentoringInterest.trim() == "") {
      Toast.show({
        text: "No mentoring interests to add.",
        buttonText: "Okay",
      });
      return;
    }
    let flag = 0;
    let s = this.state.mentoringInterest.trim();
    let l = this.state.mentoringInterestList;
    let i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      let a = this.state.mentoringInterestList;
      a.push({ id: l.length, name: this.state.mentoringInterest.trim() });
      this.setState({ mentoringInterestList: a, mentoringInterest: "" });
    } else {
      Toast.show({
        text: "Mentoring Interest already added",
        buttonText: "Okay",
      });
    }
  };

  deleteMentoringInterest = (name) => {
    if (this.state.mentoringInterestList.length == 0) {
      Toast.show({
        text: "No mentoring interests to delete",
        buttonText: "Okay",
      });
      return;
    }

    var flag = 0;
    var s = name.trim();
    var l = this.state.mentoringInterestList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ mentoringInterestList: l });
      this.setState({ mentoringInterest: "" });
    } else {
      Toast.show({ text: "Mentoring Interest not exists", buttonText: "Okay" });
      this.setState({ mentoringInterest: "" });
    }
  };

  addPatent = () => {
    if (this.state.patent.trim() == "") {
      Toast.show({ text: "Patent empty to add.", buttonText: "Okay" });
      return;
    }
    let flag = 0;
    let s = this.state.patent.trim();
    let l = this.state.patentsList;
    let i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      let a = this.state.patentsList;
      a.push({ id: l.length, name: this.state.patent.trim() });
      this.setState({ patentsList: a });
      this.setState({ patent: "" });
    } else {
      Toast.show({ text: "patent already added", buttonText: "Okay" });
      this.setState({ patent: "" });
    }
  };

  deletePatent = (name) => {
    if (this.state.patentsList.length == 0) {
      Toast.show({ text: "patents empty to delete", buttonText: "Okay" });
      return;
    }

    var flag = 0;
    var s = name;
    var l = this.state.patentsList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ patentsList: l });
    } else {
      Toast.show({ text: "Patent not exists", buttonText: "Okay" });
    }
  };

  addSubject = () => {
    if (this.state.subject.trim() == "") {
      Toast.show({ text: "No subjects to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.subject.trim();
    var l = this.state.subjectsList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      var a = this.state.subjectsList;
      a.push({ id: l.length, name: this.state.subject.trim() });
      this.setState({ subjectsList: a });
      this.setState({ subject: "" });
    } else {
      this.setState({ subject: "" });
      Toast.show({ text: "Subject already added", buttonText: "Okay" });
    }
  };

  addInterest = () => {
    if (this.state.interest.trim() == "") {
      Toast.show({ text: "No academic interests to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.interest.trim();
    var l = this.state.interestsList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      var a = this.state.interestsList;
      a.push({ id: l.length, name: this.state.interest.trim() });
      this.setState({ interestsList: a });
      this.setState({ interest: "" });
    } else {
      this.setState({ interest: "" });
      Toast.show({ text: "Academic interests added", buttonText: "Okay" });
    }
  };

  addFaculty = () => {
    if (this.state.professor.trim() == "") {
      Toast.show({ text: "No faculty to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.professor.trim();
    var l = this.state.professorList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      var a = this.state.professorList;
      a.push({ id: l.length, name: this.state.professor.trim() });
      this.setState({ professorList: a });
      this.setState({ professor: "" });
    } else {
      this.setState({ professor: "" });
      Toast.show({ text: "Faculty already added", buttonText: "Okay" });
    }
  };

  deleteSubject = (name) => {
    if (this.state.subjectsList.length == 0) {
      Toast.show({ text: "No subjects to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.subjectsList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ subjectList: l });
      this.setState({ subject: "" });
    } else {
      this.setState({ subject: "" });
      Toast.show({ text: "Subject not exists", buttonText: "Okay" });
    }
  };

  deleteInterest = (name) => {
    if (this.state.interestsList.length == 0) {
      Toast.show({
        text: "No academic interests to delete",
        buttonText: "Okay",
      });
      return;
    }

    var flag = 0;
    var s = name.trim();
    var l = this.state.interestsList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ interestsList: l });
    } else {
      Toast.show({ text: "Academic interest not exists", buttonText: "Okay" });
    }
  };

  deleteFaculty = (name) => {
    if (this.state.professorList.length == 0) {
      Toast.show({ text: "No faculty to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.professorList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ professorList: l });
      this.setState({ professor: "" });
    } else {
      Toast.show({ text: "Faculty not exists", buttonText: "Okay" });
      this.setState({ professor: "" });
    }
  };
  registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        alert("Failed to get push token for push notification!");
        return;
      }
      try {
        var token = await Notifications.getExpoPushTokenAsync();
        await this.setState({
          expo_push_token: token,
        });
      } catch (err) {
        console.log(err);
      }
    } else alert("Must use physical device for Push Notifications");

    if (Platform.OS === "android") {
      Notifications.createChannelAndroidAsync("default", {
        name: "default",
        sound: true,
        priority: "max",
        vibrate: [0, 250, 250, 250],
      });
    }
    // return token;
  };
  saveButton = async () => {
    if (this.state.name.trim() == "") {
      Toast.show({ text: "Enter name", buttonText: "Okay" });
      return;
    }
    if (this.state.phoneNumber.trim().length == 0) {
      Toast.show({ text: "Enter valid phone Number", buttonText: "Okay" });
      return;
    }

    if (this.state.linkedIn.trim().indexOf("linkedin.com/in/") == -1) {
      Toast.show({
        text: "LinkedIn url must be of format linkedin.com/in/username",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.institutionName.trim() == "None") {
      Toast.show({ text: "Select valid institution", buttonText: "Okay" });
      return;
    }
    if (this.state.collegeName.trim() == "None") {
      Toast.show({ text: "Select valid college", buttonText: "Okay" });
      return;
    }
    if (this.state.branch.trim() == "None") {
      Toast.show({ text: "Select valid branch ", buttonText: "Okay" });
      return;
    }
    if (this.state.course.trim() == "None") {
      Toast.show({ text: "Select valid course", buttonText: "Okay" });
      return;
    }

    if (this.state.yearOfGraduation.trim() === "") {
      Toast.show({ text: "Select graduation date", buttonText: "Okay" });
      return;
    }
    if (this.state.specialization.trim() === "") {
      Toast.show({ text: "Fill in specialization", buttonText: "Okay" });
      return;
    }
    if (this.state.interestsList.length == 0) {
      Toast.show({
        text: "Enter atleast one academic interest",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.available.trim() == "") {
      Toast.show({ text: "Fill available Time", buttonText: "Okay" });
      return;
    }
    if (this.state.current_working_title.trim() == "") {
      Toast.show({
        text: "Enter current job working title",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.current_working_company.trim() == "") {
      Toast.show({ text: "Enter current working company", buttonText: "Okay" });
      return;
    }

    if (this.state.mentoringInterestList.length == 0) {
      Toast.show({
        text: "Enter atleast one Mentoring Interest",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.higherEducation.trim() == "") {
      Toast.show({ text: "Select higher Education", buttonText: "Okay" });
      return;
    }
    if (this.state.imageLoading) {
      Toast.show({
        text: "Please wait image is uploading",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.collegesList.find((x) => x.name == this.state.collegeName)) {
      var collegeId = this.state.collegesList.find(
        (x) => x.name == this.state.collegeName
      ).id;
    }
    if (
      this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      )
    ) {
      var institutionId = this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      ).id;
    }
    let pre_list = this.state.pre_built_list;
    let list = [];
    for (let i = 0; i < pre_list.length; i++) {
      if (pre_list[i] == true) list.push(i);
    }
    let d = {
      last_logged_in: Date().toLocaleString(),
      photo_url: this.state.image,
      college_after_BTECH: this.state.college_after_BTECH.trim(),
      name: this.state.name.trim(),
      available: this.state.available.trim(),
      about: this.state.about.trim() === "" ? null : this.state.about.trim(),
      email: this.state.email.trim(),
      branch: this.state.branch.trim(),
      course: this.state.course.trim(),
      year_passed: this.state.yearOfGraduation.trim(),
      specialization:
        this.state.specialization.trim() === ""
          ? null
          : this.state.specialization.trim(),
      higher_education:
        this.state.higherEducation.trim() === ""
          ? null
          : this.state.higherEducation.trim(),
      linkedin: this.state.linkedIn.trim(),
      user_type: "mentor",
      mentoring_interest_other_college: this.state
        .mentoringInterestOtherCollege,
      interests_list: this.state.interestsList,
      expo_push_token: this.state.expo_push_token,
      phone_number: this.state.phoneNumber,
      patents:
        this.state.patentsList.length === 0 ? null : this.state.patentsList,
      mentoring_interest_list: this.state.mentoringInterestList,
      subjects_liked:
        this.state.subjectsList.length === 0 ? null : this.state.subjectsList,
      faculty_recommended:
        this.state.professorList.length === 0 ? null : this.state.professorList,
      authorized: true,
      status: "approved",
      institution_name: this.state.institutionName,
      current_working_title: this.state.current_working_title,
      college_name: this.state.collegeName,
      college_id: this.state.collegeId,
      institution_id: this.state.institutionId,
      current_working_company: this.state.current_working_company,
      last_updated_by: "user",
      display_profile: true,
      flag: true,
      key: this.state.key,
      pre_built_events_indices: list.length === 0 ? null : list,
      no_of_events: {
        no_of_simple_events: 0,
        no_of_bootcamp_events: 0,
        no_of_overview_events: 0,
      },
    };
    //pushing your profile with your uid deleting the record which admin entered
    if (d.updateprof == undefined) {
      d = { ...d, uid: firebase.auth().currentUser.uid, updateprof: true };
      console.log(d);
      // await firebase.auth().signOut().then(()=>{console.log("user signed out");})
      firebase
        .database()
        .ref(`mentors/${d.key}`)
        .remove()
        .then(() => {
          console.log("Removed successfully");
        })
        .catch((error) => {
          console.log(error);
        });

      this.setState({ loading: true });
      // const response = await fetch(this.state.image);
      Alert.alert(
        "Notifications",
        "You will get frequent updates from MentorKonnect App. If you don’t like to receive notifications, please turn off notifications in settings",
        [
          {
            text: "OK",
            onPress: () => {
              console.log("OK");
            },
          },
        ]
      );
      AsyncStorage.setItem("profile", JSON.stringify(d)).then(() => {
        this.props.navigation.navigate("MentorAppPage", { profile: d });
      });

      console.log("Async profile");
      AsyncStorage.getItem("profile").then(async (k) => {
        console.log(k);
      });
      fetch(urls.updateAlumniJNTUKMentor, {
        method: "POST",
        body: JSON.stringify(d),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      })
        .then((res) => {
          // console.log(d);
          const url = `${urls.sendWelcomeMail}?name=${d.name}&email=${d.email}&type=mentor`;
          // console.log(url);
          fetch(url)
            .then((res) => {
              console.log(res.json());
            })
            .catch((err) => console.log(err));
        })
        .then(() => {
          console.log("registration confirmation sent as mail.");
          let obj = { roles: ["alumni"] };

          fetch(urls.setUserRole, {
            method: "POST",
            body: JSON.stringify({
              role: obj,
              uid: firebase.auth().currentUser.uid,
            }),
            headers: {
              "Content-type": "application/json; charset=UTF-8",
            },
          })
            .then((res) => {
              console.log(res.text);
              this.props.navigation.navigate("MentorAppPage"); //navigating to the appPage.
            })
            .catch((err) => {
              console.log(err);
            });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  uploadPhoto = async () => {
    try {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL); //Asking for the permissions.
      if (status == "granted") {
        //Reading the local storage image.

        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Image,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
        if (!result.cancelled) {
          this.setState({ image: result.uri, imageLoading: true });
          const response = await fetch(result.uri);
          const blob = await response.blob(); //getting the binary form of image.
          var ref = firebase
            .storage()
            .ref()
            .child(`profile_images/${this.state.email}`)
            .put(blob); //storing the image binary data in cloud storage.
          Toast.show({ text: "Uploading Image", buttonText: "Okay" });

          return ref.on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
              // Observe state change events such as progress, pause, and resume
              // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
              progress =
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              console.log("Upload is " + progress + "% done");
              switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                  console.log("Upload is paused");
                  break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                  console.log("Upload is running");
                  break;
              }
            },
            (error) => {
              // Handle unsuccessful uploads
              console.log("Upload unsuccessfull");
            },
            () => {
              // Handle successful uploads on complete
              // For instance, get the download URL: https://firebasestorage.googleapis.com/...
              Toast.show({ text: "Uploaded Image", buttonText: "Okay" });
              ref.snapshot.ref.getDownloadURL().then((downloadURL) => {
                console.log("File available at", downloadURL);
                this.setState({ image: downloadURL, imageLoading: false });
              });
            }
          );
        } else {
        }
        console.log(result);
      } else {
        Alert.alert("Required storage permissions.");
      }
    } catch (E) {
      console.log(E);
    }
  };

  render() {
    return (
      <View style={styles.overall}>
        <KeyboardAvoidingView
          behavior={Platform.Os == "ios" ? "padding" : "height"}
          style={styles.keyboradViewStyle}
        >
          <ScrollView
            style={styles.container}
            keyboardShouldPersistTaps="handled"
          >
            <View style={styles.detailsContainer}>
              <View style={styles.photoContentStyle}>
                <Avatar
                  onPress={() => {
                    this.uploadPhoto();
                  }}
                  editButton={{
                    name: "mode-edit",
                    type: "material",
                    color: "#fff",
                    underlayColor: "#0f0",
                  }}
                  showEditButton
                  size={100}
                  rounded
                  source={{ uri: this.state.image }}
                  avatarStyle={{}}
                  icon={{ name: "user", type: "font-awesome" }}
                />
              </View>
              <Text style={styles.heading}>Contact Info:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Name <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Name"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.name}
                  onChangeText={(text) => this.setState({ name: text })}
                />
                <Text style={styles.subtitle}>
                  Email Id <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  editable={false}
                  placeholder="Email"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.email}
                  onChangeText={(text) => this.setState({ email: text })}
                />
                <Text style={styles.subtitle}>
                  Phone Number <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Mobile Number"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  keyboardType={"numeric"}
                  value={this.state.phoneNumber}
                  onChangeText={(text) => this.setState({ phoneNumber: text })}
                />
                <Text style={styles.subtitle}>
                  LinkedIn Profile <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  editable={this.state.editable}
                  placeholder="LinkedIn profile url..linkedin.com/in/username"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.linkedIn}
                  onChangeText={(text) => this.setState({ linkedIn: text })}
                />
              </View>
              <Text style={styles.heading}>College Info:</Text>
              <View style={styles.headingView}>
                <View>
                  <View style={{ marginTop: 5 }}>
                    <Text style={styles.subtitle}>Institution</Text>
                    <Text
                      style={{ fontSize: 17, marginBottom: 5, marginStart: 3 }}
                    >
                      {this.state.institutionName}
                    </Text>
                  </View>
                  <View style={{ marginTop: 5 }}>
                    <Text style={styles.subtitle}>College</Text>
                    <Text
                      style={{ fontSize: 17, marginBottom: 5, marginStart: 3 }}
                    >
                      {this.state.collegeName}
                    </Text>
                  </View>
                </View>
                {/* <Dropdown
                  label="Institution *"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  animationDuration={0}
                  data={this.state.institutionsList}
                  value={this.state.institutionName}
                  valueExtractor={(item, index) => {
                    return item.name;
                  }}
                  onChangeText={async (v, i, d) => {
                    await this.setState({ institutionName: v });
                    await this.setState({
                      collegeName: "None",
                      course: "None",
                      branch: "None",
                    });
                    await this.setState({
                      collegesList: this.state.institutionsList.find(
                        (x) => x.name == this.state.institutionName
                      )
                        ? this.state.institutionsList.find(
                            (x) => x.name == this.state.institutionName
                          ).colleges
                        : [],
                    });
                    await this.setState({
                      coursesList: this.state.collegesList.find(
                        (x) => x.name == this.state.collegeName
                      )
                        ? this.state.collegesList.find(
                            (x) => x.name == this.state.collegeName
                          ).courses
                        : [],
                    });
                    await this.setState({
                      branchesList: this.state.coursesList.find(
                        (x) => x.course == this.state.course
                      )
                        ? this.state.coursesList.find(
                            (x) => x.course == this.state.course
                          ).branches
                        : [],
                    });
                  }}
                />
                <Dropdown
                  label="College *"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.collegesList}
                  value={this.state.collegeName}
                  valueExtractor={(item, index) => {
                    return item.name;
                  }}
                  onChangeText={async (v, i, d) => {
                    await this.setState({ collegeName: v });
                    await this.setState({ course: "None", branch: "None" });
                    await this.setState({
                      coursesList: this.state.collegesList.find(
                        (x) => x.name == this.state.collegeName
                      )
                        ? this.state.collegesList.find(
                            (x) => x.name == this.state.collegeName
                          ).courses
                        : [],
                    });
                    await this.setState({
                      branchesList: this.state.coursesList.find(
                        (x) => x.course == this.state.course
                      )
                        ? this.state.coursesList.find(
                            (x) => x.course == this.state.course
                          ).branches
                        : [],
                    });
                  }}
                /> */}
                <Dropdown
                  label="Course *"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.coursesList}
                  value={this.state.course}
                  valueExtractor={(item, index) => {
                    return item.course;
                  }}
                  onChangeText={async (v, i, d) => {
                    await this.setState({ course: v });
                    await this.setState({ branch: "None" });
                    await this.setState({
                      branchesList: this.state.coursesList.find(
                        (x) => x.course == this.state.course
                      )
                        ? this.state.coursesList.find(
                            (x) => x.course == this.state.course
                          ).branches
                        : [],
                    });
                    console.log(this.state.branchesList);
                  }}
                />
                <Dropdown
                  label="Branch *"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.branchesList}
                  value={this.state.branch}
                  valueExtractor={(item, index) => {
                    return item.branch;
                  }}
                  onChangeText={async (v, i, d) => {
                    this.setState({ branch: v });
                  }}
                />
                <Dropdown
                  label="Highest Education"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.higherEducationList}
                  value={this.state.higherEducation}
                  valueExtractor={(item, index) => {
                    return item.name;
                  }}
                  onChangeText={async (v, i, d) => {
                    this.setState({ higherEducation: v });
                  }}
                />
                <Text style={styles.subtitle}>
                  Specialization <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Specialization"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.specialization}
                  onChangeText={(text) =>
                    this.setState({ specialization: text })
                  }
                />
                <Text style={styles.subtitle}>
                  {" "}
                  Year of Graduation <Text style={styles.req}>*</Text>
                </Text>
                <DatePicker
                  style={styles.dateStyle}
                  date={this.state.yearOfGraduation}
                  mode="date"
                  placeholder="select date"
                  format="YYYY-MM-DD"
                  minDate="1800-01-01"
                  // maxDate="2030-01-01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 4,
                      marginLeft: 0,
                    },
                    dateInput: {
                      marginLeft: 36,
                    },
                  }}
                  onDateChange={(date) => {
                    this.setState({ yearOfGraduation: date });
                  }}
                />
                <Text style={styles.subtitle}>College After B.TECH</Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="College name"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.college_after_BTECH}
                  onChangeText={(text) =>
                    this.setState({ college_after_BTECH: text })
                  }
                />
              </View>

              <Text style={styles.heading}>Work Related Info:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Current Working Company <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="your present company"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.current_working_company}
                  onChangeText={(text) =>
                    this.setState({ current_working_company: text })
                  }
                />
                <Text style={styles.subtitle}>
                  Current Working Title <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="your present job title"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.current_working_title}
                  onChangeText={(text) =>
                    this.setState({ current_working_title: text })
                  }
                />
                <Text style={styles.subtitle}>Patents/Awards</Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Patents and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.patent}
                    onChangeText={(text) => this.setState({ patent: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={styles.deleteoverlayContainer}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addPatent()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.patentsList.map((l, i) => (
                    <View key={i} style={styles.interestsSubContent}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Icon
                        name={"times-circle"}
                        type={"font-awesome"}
                        onPress={() => this.deletePatent(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
              </View>
              <Text style={styles.heading}>Mentoring Interests:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Would you like to offer mentoring to students from other
                  college also?
                </Text>

                <View style={styles.interestsView}>
                  <CheckBox
                    center
                    title="Yes"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    containerStyle={{ backgroundColor: "white" }}
                    checked={this.state.mentoringInterestOtherCollege}
                    onPress={() => {
                      this.setState((prev) => {
                        return {
                          mentoringInterestOtherCollege: !prev.mentoringInterestOtherCollege,
                        };
                      });
                    }}
                  />
                  <CheckBox
                    center
                    title="No"
                    checkedIcon="dot-circle-o"
                    containerStyle={{ backgroundColor: "white" }}
                    uncheckedIcon="circle-o"
                    checked={!this.state.mentoringInterestOtherCollege}
                    onPress={() => {
                      this.setState((prev) => {
                        return {
                          mentoringInterestOtherCollege: !prev.mentoringInterestOtherCollege,
                        };
                      });
                    }}
                  />
                </View>
                <Text style={styles.subtitle}>
                  Mentoring Interest Areas <Text style={styles.req}>*</Text>
                </Text>
                <View>
                  <FlatList
                    data={PreBuiltEventTypes}
                    style={{ flex: 1 }}
                    renderItem={({ item, index }) => {
                      return (
                        <CheckBox
                          checked={this.state.pre_built_list[index]}
                          title={item.name}
                          containerStyle={{ backgroundColor: "white" }}
                          onPress={() => {
                            this.toggle(index);
                          }}
                        />
                      );
                    }}
                  />
                </View>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Mentoring Interests and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.mentoringInterest}
                    onChangeText={(text) =>
                      this.setState({ mentoringInterest: text })
                    }
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={styles.deleteoverlayContainer}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addMentoringInterest()}
                    activeOpacity={0.7}
                  />
                </View>

                <View style={styles.interestsContentContainer}>
                  {this.state.mentoringInterestList.map((l, i) => (
                    <View key={i} style={styles.interestsSubContent}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Icon
                        name={"times-circle"}
                        type={"font-awesome"}
                        onPress={() => this.deleteMentoringInterest(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
              </View>

              <Text style={styles.heading}>Additional Info:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Interests <Text style={styles.req}>*</Text>
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Interests and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.interest}
                    onChangeText={(text) => this.setState({ interest: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={styles.deleteoverlayContainer}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addInterest()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.interestsList.map((l, i) => (
                    <View key={i} style={styles.interestsSubContent}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Icon
                        name={"times-circle"}
                        type={"font-awesome"}
                        onPress={() => this.deleteInterest(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
                <Text style={styles.subtitle}>
                  Subjects liked at your college
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Subjects and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.subject}
                    onChangeText={(text) => this.setState({ subject: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={styles.deleteoverlayContainer}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addSubject()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.subjectsList.map((l, i) => (
                    <View key={i} style={styles.interestsSubContent}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Icon
                        name={"times-circle"}
                        type={"font-awesome"}
                        onPress={() => this.deleteSubject(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
                <Text style={styles.subtitle}>
                  Faculty you would recommend at your college
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter faculty and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.professor}
                    onChangeText={(text) => this.setState({ professor: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={styles.deleteoverlayContainer}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addFaculty()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.professorList.map((l, i) => (
                    <View key={i} style={styles.interestsSubContent}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Icon
                        name={"times-circle"}
                        type={"font-awesome"}
                        onPress={() => this.deleteFaculty(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
                <Text style={styles.subtitle}>
                  Available Timings <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Available timings"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.available}
                  onChangeText={(text) => this.setState({ available: text })}
                />
                <Text style={styles.subtitle}>
                  Additional Information about you
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Additional Information about you"
                  underlineColorAndroid="transparent"
                  textAlignVertical="top"
                  multiline={true}
                  numberOfLines={10}
                  style={styles.about}
                  value={this.state.about}
                  onChangeText={(text) => this.setState({ about: text })}
                />
              </View>
              <View style={styles.save}>
                <Button
                  icon={
                    <Icon
                      type="font-awesome"
                      name="save"
                      size={20}
                      color="white"
                    />
                  }
                  containerStyle={styles.saveContainerStyle}
                  buttonStyle={styles.saveButtonStyle}
                  onPress={() => {
                    Keyboard.dismiss();
                    this.saveButton();
                  }}
                  titleStyle={styles.saveButtonTitleStyle}
                  title="Save"
                />
                {this.state.loading && <ActivityIndicator size="large" />}
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  deleteoverlayContainer: { backgroundColor: "rgba(25,100,200,1)" },
  overall: { flex: 1, backgroundColor: "white" },
  about: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.9,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    height: 100,
    textAlign: "left",
    backgroundColor: "white",
  },
  deleteContainerStyle: { padding: 7 },
  saveButtonTitleStyle: { padding: 5 },
  saveContainerStyle: { padding: 10 },
  save: { flexDirection: "row", justifyContent: "center" },
  saveButtonStyle: {
    backgroundColor: "rgba(200,175,0,1)",
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    width: Dimensions.get("window").width * 0.4,
  },
  interestsView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  adderLists: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.76,
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    backgroundColor: "white",
  },
  TextInputStyle: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.9,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    backgroundColor: "white",
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "bold",
  },
  detailsContainer: {
    flexDirection: "column",
    justifyContent: "flex-start",
    margin: Dimensions.get("window").width * 0.03,
    alignContent: "flex-start",
  },
  titleStyle: {
    fontSize: 25,
    fontWeight: "bold",
    textAlign: "center",
  },
  logoContainer: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  container: {
    flex: 1,
  },
  logoStyle: {
    borderRadius: 5,
    height: 110,
    width: 200,
  },
  dropDownStyle: { paddingBottom: 5, fontWeight: "bold" },
  keyboradViewStyle: { flex: 1 },
  photoContentStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
  },
  req: { color: "red" },
  dateStyle: {
    width: Dimensions.get("window").width * 0.9,
    marginTop: 10,
    marginBottom: 10,
  },
  interestsStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  interestsContentContainer: { flexDirection: "row", flexWrap: "wrap" },
  interestsSubContent: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    borderRadius: 10,
    borderWidth: 2,
    backgroundColor: "rgba(100,100,255,0.4)",
    alignItems: "center",
  },
  interestsTextStyle: { fontSize: 15, fontWeight: "bold" },
  deleteOverlay: { backgroundColor: "rgba(100,255,255,0.1)", padding: 0 },

  headingView: {
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "grey",
    padding: 5,
    width: Dimensions.get("window").width * 0.9375,
    backgroundColor: "whitesmoke",
    marginBottom: 5,
  },
  heading: {
    fontSize: 20,
    fontWeight: "bold",
    marginTop: 10,
    color: "black",
  },
});
