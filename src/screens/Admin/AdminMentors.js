import React from "react";
import {
  StyleSheet,
  ActivityIndicator,
  Text,
  View,
  ImageBackground,
  Image,
  FlatList,
  Button,
  AsyncStorage,
} from "react-native";
import { SearchBar } from "react-native-elements";
import MentorCard from "./AdminMentorCard";
import { urls } from "./../../../env.json";
import { Toast } from "native-base";

export default class AdminMentors extends React.Component {
  constructor(props) {
    super(props);
    this.state = { search: "", dataloaded: false, refreshing: false };
  }

  SearchFilterFunction(text) {
    const newData = this.state.data.filter(function (item) {
      const itemData = item.name ? item.name.toUpperCase() : "".toUpperCase(); //checking if the search string is substring of any of the names of mentors.
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      search: text,
      dupdata: newData,
    });
  }

  getData = async () => {
    AsyncStorage.getItem("profile")
      .then((d) => {
        
        return JSON.parse(d);
      })
      .then((d) => {
       
        if (d.user_type === "college_admin") {
          if (d.authorized == false) {
            Toast.show({
              text:
                "You can only perform tasks when you are activated by Mentoring association",
              buttonText: "Okay",
            });
            this.setState({
              dataloaded: true,
              refreshing: false,
            });
          } else {
            let obj = { college: d.college_id, institute: d.institution_id };
            //To fetch their own college alumni
            fetch(urls.getCollegeAlumni, {
              method: "POST",
              body: JSON.stringify(obj),
              headers: {
                "Content-type": "application/json; charset=UTF-8",
              },
            })
              .then((response) => {
                let d = response.json();
                return d;
              })
              .then((responseJSON) => {
                let d = Array.from(responseJSON);
                this.setState({
                  data: d,
                  dataloaded: true,
                  dupdata: d,
                  refreshing: false,
                });
                console.log("list", this.state.dupdata);
              })
              .catch((error) => {
                console.log(error);
              });
          }
        } else {
          let obj = { college: d.college_id, institute: d.institution_id };
          fetch(urls.getCollegeAlumni, {
            method: "POST",
            body: JSON.stringify(obj),
            headers: {
              "Content-type": "application/json; charset=UTF-8",
            },
          }) //fetching all the alumni data
            .then((response) => {
              let d = response.json();
              return d;
            })
            .then((responseJSON) => {
              let d = Array.from(responseJSON);
              this.setState({
                data: d,
                dataloaded: true,
                dupdata: d,
                refreshing: false,
              });
              console.log("list", this.state.dupdata);
            })
            .catch((error) => {
              console.log(error);
            });
        }
      })
      .catch((err) => console.log(err));
  };

  componentDidMount() {
    this.getData();
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getData();
      }
    );
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true, search: "" });
    await this.getData();
    this.setState({ refreshing: false });
  };

  render() {
    if (this.state.dataloaded) {
      return (
        <View style={styles.container}>
          <SearchBar
            round
            inputContainerStyle={styles.searchContainer}
            showLoadingIcon={true}
            inputStyle={styles.searchinput}
            containerStyle={styles.searchSubContainer}
            searchIcon={{ size: 20 }}
            onChangeText={(text) => this.SearchFilterFunction(text)}
            onClear={(text) => this.SearchFilterFunction("")}
            placeholder="Enter mentor name to search..."
            value={this.state.search}
          ></SearchBar>
          <FlatList
            refreshing={this.state.refreshing}
            data={this.state.dupdata}
            onRefresh={this._onRefresh}
            renderItem={({ item }) => {
              console.log(item.name);
              return (
                <MentorCard
                  mentor={item}
                  navigation={this.props.navigation}
                  getData={this.getData}
                />
              );
            }}
            keyExtractor={(item, index) => item.email}
          />
        </View>
      );
    } else {
      return (
        <View style={styles.indicator}>
          <ActivityIndicator size="large"></ActivityIndicator>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  searchSubContainer: {
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "white",
    borderBottomWidth: 0,
    borderTopWidth: 0,
  },
  searchinput: {
    color: "black",
  },
  searchContainer: {
    backgroundColor: "white",
    padding: 0,
    marginRight: 6,
    width: "100%",
    borderWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 5,
  },
  indicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  search: {
    margin: 0,
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 7,
    paddingTop: 10,
    justifyContent: "center",
  },
  signOut: {
    alignSelf: "flex-end",
    marginRight: 20,
  },
  listItem: {
    flexDirection: "row",
    margin: 10,
    shadowRadius: 0.3,
    shadowOpacity: 0.5,
    elevation: 1,
    borderRadius: 2,
    padding: 20,
  },
});
