import React from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Avatar, Button } from "react-native-elements";
import { urls } from "./../../../env.json";

export default class collegementeeCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      approveButtonFlag: false,
      disapproveButtonFlag: false,
    };
  }

  handleApprove = async (status, uid, email, name, mentoring) => {
    const { getData } = this.props;
    const { admin } = this.props;
    const { adminuser } = this.props;
    var user = "license_mentee";
    console.log(user);

    const url =
      urls.handleApprovalsv2 +
      `?user=${user}&status=${status}&uid=${uid}&email=${email}&name=${name}&mentoring=${mentoring}`;
    fetch(url)
      .then(async () => {
        this.setState({
          approveButtonFlag: false,
          disapproveButtonFlag: false,
        });
        console.log("yes");
        await getData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  sendPushNotification = async (msg, token, status) => {
    const message = {
      to: token,
      sound: "default",
      title: "Mentoring Event",
      body: msg,
      data: { data: status },
      _displayInForeground: true,
    };
    fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Accept-encoding": "gzip, deflate",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(message),
    }).catch((err) => {
      console.log(err);
    });
  };

  render() {
    const { mentee, navigation } = this.props;
    let flag;
    if (mentee.mentoring_interest_list != undefined) {
      flag = "true";
    } else {
      flag = "false";
    }
    return (
      <View style={styles.overall}>
        <TouchableOpacity
          activeOpacity={0.7}
          //on pressing mentee card his details are visible.
          onPress={() => {
            navigation.navigate("MenteeDetailsValidation", {
              user: mentee,
              navigation: navigation,
            });
          }}
        >
          <View style={styles.listItem}>
            <Avatar
              size={100}
              rounded
              avatarStyle={{}}
              source={{ uri: mentee.photo_url }}
              icon={{ name: "user", type: "font-awesome" }}
            />
            <View style={styles.cardRight}>
              <Text style={styles.text}>{mentee.name}</Text>
              <Text style={styles.subtext}>{mentee.course}</Text>
              <Text style={styles.subtext}>
                Mentoring Interested:{" "}
                {mentee.mentoring_interest_list != undefined ? "Yes" : "No"}
              </Text>
              <Text style={styles.subtext}>
                Status:{" "}
                {mentee["status"] == "approved" ? "Approved" : "Not Approved"}
              </Text>
              {mentee.bucket_id != undefined && (
                <Text style={styles.subtext}>
                  Bucket ID: {mentee.bucket_id + 1}
                </Text>
              )}
            </View>
          </View>
        </TouchableOpacity>

        {mentee.License_code_verified === true && (
          <View style={styles.buttons}>
            <Button
              loading={this.state.approveButtonFlag}
              disabled={mentee.super_admin_verified == true}
              title="Activate"
              buttonStyle={styles.activate}
              containerStyle={styles.buttonContainer}
              onPress={async () => {
                await this.setState({
                  approveButtonFlag: true,
                });
                await this.handleApprove(
                  "approved",
                  mentee.uid,
                  mentee.email,
                  mentee.name,
                  flag
                );
                let msg = `Hi ${mentee.name} you  have been activated as mentee by Admin of CollegeKonnect.`;
              }}
            />
            <Button
              loading={this.state.disapproveButtonFlag}
              disabled={mentee.super_admin_verified == false}
              title="Deactivate"
              buttonStyle={styles.deactivate}
              containerStyle={{ ...styles.buttonContainer, marginStart: 10 }}
              onPress={async () => {
                await this.setState({
                  disapproveButtonFlag: true,
                });
                await this.handleApprove(
                  "not_approved",
                  mentee.uid,
                  mentee.email,
                  mentee.name,
                  flag
                );
                let msg = `Hi ${mentee.name} you  have been deactivated as mentee by Admin of CollegeKonnect. Please contact our council team for the reason.`;
              }}
            />
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttons: {
    flexDirection: "row",
    justifyContent: "center",
    margin: 5,
    marginTop: 0,
  },
  activate: {
    backgroundColor: "rgba(0,178,0,1)",
  },
  deactivate: {
    backgroundColor: "rgba(230,0,0,0.8)",
  },
  buttonContainer: {
    marginLeft: 5,
    width: Dimensions.get("window").width * 0.4,
  },
  subtext: {
    marginBottom: 5,
  },
  overall: {
    marginTop: 10,
    flexDirection: "column",
    elevation: 1,
    borderRadius: 2,
    borderWidth: 0.5,
  },
  cardRight: {
    flex: 1,
    alignItems: "flex-start",
    marginLeft: 15,
  },
  cardRightSub: {
    flexDirection: "row",
    alignItems: "center",
  },
  text: {
    fontWeight: "bold",
    textTransform: "capitalize",
    marginBottom: 3,
    fontSize: 18,
  },
  listItem: {
    flexDirection: "row",
    alignItems: "center",
    margin: 5,
    padding: 10,
    paddingBottom: 0,
  },
});
