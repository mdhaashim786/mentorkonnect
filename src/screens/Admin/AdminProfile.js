import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  AsyncStorage,
  Linking,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity
} from "react-native";
import Logos from "../../../assets/Logos";
import { Icon, Avatar, Button } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import { urls } from "./../../../env.json";
import { Toast } from "native-base";
import PreBuiltEventTypes from "./../Events/PreBuiltEventTypes";
import * as Font from "expo-font";
import { AppLoading } from "expo";
let customFonts = {
  LobsterRegular: require("../../../assets/fonts/LobsterRegular-rR8O.ttf"),
};
export default class ProfileDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = { profile: null, loading: false, fontsLoaded: false };
  }
  //To load the fonts file from assests
  async _loadFontsAsync() {
    await Font.loadAsync(customFonts);
    this.setState({ fontsLoaded: true });
  }
  getData = async () => {
    let d = await this.props.navigation.getParam("user");
    await this.setState({ profile: d });
  };

  componentDidMount = async () => {
    //TO load the fonts
    this._loadFontsAsync();
    await this.getData();
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getData();
      }
    );
  };

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  resendCode = async () => {
    fetch(
      urls.resendRegsitrationCode +
        `/?reg_code=${this.state.profile.registration_code}&email=${this.state.profile.email}&name=${this.state.profile.name}`
    )
      .then((res) => res.json())
      .then(async (res) => {
        Toast.show({ text: res.data, buttonText: "Okay" });
        await this.setState({ loading: false });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    if (this.state.profile != null) {
      var badge = null;
      if (this.state.profile.no_of_events != undefined) {
        //assigning badges to mentors based on no.of simple events,no of bootcamp events and no of overview events completed by the mentor.

        const simple = this.state.profile.no_of_events["no_of_simple_events"]; //getting no of simple evnts of the mentor.
        const overview = this.state.profile.no_of_events[
          "no_of_overview_events"
        ]; //getting no of overview evnts of the mentor.
        const bootcamp = this.state.profile.no_of_events[
          "no_of_bootcamp_events"
        ]; //getting no of bootcamp evnts of the mentor.
        // based on the conditions,respective badge is stored in badge variable.
        if (simple >= 20 || overview >= 7 || bootcamp >= 3)
          badge = Logos.platinum;
        else if (
          (simple >= 10 && simple < 20) ||
          (overview >= 4 && overview < 7) ||
          bootcamp == 2
        )
          badge = Logos.gold;
        else if (
          (simple >= 5 && simple < 10) ||
          (overview >= 1 && overview < 4) ||
          bootcamp == 1
        )
          badge = Logos.silver;
        else if (simple >= 1 && simple < 5) badge = Logos.bronze;
        else badge = null;
      }
      if (this.state.fontsLoaded) {
        return (
          <ScrollView>
            <View style={styles.imageStyle}>
              {this.state.profile.user_type !== "college_admin" && (
                <View style={styles.containerStyle}>
                  <Icon
                    raised
                    name="edit"
                    type="font-awesome"
                    color="#f50"
                    onPress={() => {
                      if (this.state.profile.user_type == "mentor")
                        this.props.navigation.navigate(
                          "AdminMentorEditProfileRoute",
                          {
                            user: this.state.profile,
                            updateInfo: this.state.updateInfo,
                          }
                        );
                      else
                        this.props.navigation.navigate(
                          "AdminMenteeEditProfileRoute",
                          {
                            user: this.state.profile,
                            updateInfo: this.state.updateInfo,
                          }
                        );
                    }}
                  />
                </View>
              )}
              <View style={styles.imageContainer}>
                <View style={styles.imageViewStyle}>
                  <Avatar
                    size={120}
                    rounded
                    source={{ uri: this.state.profile.photo_url }}
                    avatarStyle={{}}
                    icon={{ name: "user", type: "font-awesome" }}
                  />
                </View>
                <View style={styles.imageBesideText}>
                  <Text style={styles.subTextStyle}>
                    {this.state.profile.name}
                  </Text>
                  {this.state.profile.roll_number != undefined && (
                    <Text
                      style={{
                        ...styles.subTextStyle,
                        textTransform: "uppercase",
                      }}
                    >
                      {this.state.profile.roll_number}
                    </Text>
                  )}
                  <Text style={styles.subTextStyle}>
                    {this.state.profile.branch}
                  </Text>
                  <Text style={styles.subTextStyle}>
                    {this.state.profile.college_name}
                  </Text>
                </View>
              </View>
            </View>
            {badge != null && (
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  height: 160,
                  marginTop: 3,
                }}
              >
                <View
                  style={{
                    borderWidth: 2,
                    height: 160,
                    flexDirection: "row",
                    alignItems: "center",
                    margin: 4,
                    justifyContent: "space-around",
                  }}
                >
                  <View style={{ justifyContent: "flex-start" }}>
                    <Text
                      style={{
                        fontSize: 26,
                        textTransform: "capitalize",
                        marginEnd: 10,
                        fontWeight: "bold",
                      }}
                    >
                      My Achievements
                    </Text>
                  </View>
                  <View style={styles.badgeViewStyle}>
                    <ImageBackground
                      style={{
                        width: 70,
                        height: 140,
                      }}
                      resizeMode="contain"
                      source={{ uri: badge }}
                    >
                      <View style={styles.badge_1stview}>
                        <View style={styles.badge_2ndview}>
                          <Text style={styles.nameonbadge}>
                            {this.state.profile.name.split(" ", 1)}
                          </Text>
                        </View>
                      </View>
                    </ImageBackground>
                  </View>
                </View>
              </View>
            )}
            <View style={styles.details}>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ ...styles.titleStyle, marginEnd: 10 }}>
                {this.state.profile.name}
              </Text>
              <TouchableOpacity
                  style={{
                    borderWidth: 2,
                    borderColor: "#4682b4",
                    borderRadius: 6,
                  }}
                  onPress={() =>
                    Linking.openURL(`${this.state.profile.linkedin}`)
                  }
                >
                  <Image
                    source={require("../../../assets/linkedin-icon.png")}
                    style={{
                      width: 28,
                      height: 28,
                      margin: 0.4,
                    }}
                  />
                </TouchableOpacity>
            </View>
              {this.state.profile.about != "" &&
                this.state.profile.about != undefined && (
                  <View>
                    <Text style={styles.subHeading}>About</Text>
                    <Text style={styles.about}>{this.state.profile.about}</Text>
                  </View>
                )}

              {this.state.profile.email != undefined &&
                this.state.profile.email != "" && (
                  <View>
                    <Text style={styles.subHeading} selectable={true}>
                      Email
                    </Text>
                    <Text style={styles.about}>{this.state.profile.email}</Text>
                  </View>
                )}

              {/* {this.state.profile.registration_code != undefined &&
              this.state.profile.registration_code != "" && (
                <View>
                  <Text style={styles.subHeading}>Registration Code</Text>

                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "flex-start",
                      alignItems: "center",
                      margin: 5,
                    }}
                  >
                    <Text
                      style={{
                        ...styles.about,
                        marginBottom: 5,
                        marginEnd: 10,
                      }}
                    >
                      {this.state.profile.registration_code}
                    </Text>
                    <Button
                      onPress={async () => {
                        await this.setState({ loading: true });
                        await this.resendCode();
                      }}
                      title="Resend Code"
                    />
                    {this.state.loading && <ActivityIndicator size="large" />}
                  </View>
                </View>
              )} */}
              {this.state.profile.current_working_title != undefined && (
                <View>
                  <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                    Working Title
                  </Text>
                  <Text style={styles.about}>
                    {this.state.profile.current_working_title}
                  </Text>
                </View>
              )}
              {this.state.profile.current_working_company != undefined && (
                <View>
                  <Text style={{ ...styles.subHeading, marginBottom: 2 }}>
                    Working Company
                  </Text>
                  <Text style={styles.about}>
                    {this.state.profile.current_working_company}
                  </Text>
                </View>
              )}
              {this.state.profile.gpa != undefined &&
                this.state.profile.gpa != "" && (
                  <View>
                    <Text style={styles.subHeading}>GPA</Text>
                    <Text
                      style={{ ...styles.about, marginBottom: 5, fontSize: 15 }}
                    >
                      {this.state.profile.gpa}
                    </Text>
                  </View>
                )}
              {this.state.profile.year_passed != "" &&
                this.state.profile.year_passed != undefined && (
                  <View>
                    <Text style={styles.subHeading}>Year of Graduation</Text>
                    <Text style={styles.about}>
                      {this.state.profile.year_passed}
                    </Text>
                  </View>
                )}
              {this.state.profile.course != undefined &&
                this.state.profile.course != "None" && (
                  <View>
                    <Text style={styles.subHeading}>Course</Text>
                    <Text style={styles.about}>
                      {this.state.profile.course}
                    </Text>
                  </View>
                )}
              {this.state.profile.college_after_BTECH != "" &&
                this.state.profile.college_after_BTECH != undefined && (
                  <View>
                    <Text style={styles.subHeading}>College After B.TECH</Text>
                    <Text style={styles.about}>
                      {this.state.profile.college_after_BTECH}
                    </Text>
                  </View>
                )}
              {this.state.profile.higher_education != undefined &&
                this.state.profile.higher_education != "None" &&
                this.state.profile.higher_education != "" && (
                  <View>
                    <Text style={styles.subHeading}>Higher Education</Text>
                    <Text style={styles.about}>
                      {this.state.profile.higher_education}
                    </Text>
                  </View>
                )}
              {this.state.profile.specialization != undefined &&
                this.state.profile.specialization != "" && (
                  <View>
                    <Text style={styles.subHeading}>Specialization</Text>
                    <Text style={styles.about}>
                      {this.state.profile.specialization}
                    </Text>
                  </View>
                )}
              {this.state.profile.available != undefined &&
                this.state.profile.available != "" && (
                  <View>
                    <Text style={styles.subHeading}>Available Time</Text>
                    <Text style={styles.about}>
                      {this.state.profile.available}
                    </Text>
                  </View>
                )}
              {this.state.profile.phone_number != undefined &&
                this.state.profile.phone_number != "" && (
                  <View>
                    <Text style={styles.subHeading}>Phone Number</Text>
                    <Text style={styles.about}>
                      {this.state.profile.phone_number}
                    </Text>
                  </View>
                )}
              {this.state.profile.interests_list != undefined && (
                <Text style={styles.subHeading}>Academic Interests</Text>
              )}
              <View style={styles.renderContainer}>
                {this.state.profile.interests_list != undefined &&
                  this.state.profile.interests_list.map((l, i) => (
                    <View key={i} style={styles.insideView}>
                      <Text style={styles.subTexttitle}>{l.name}</Text>
                    </View>
                  ))}
              </View>
              {this.state.profile.mentoring_interest_list != undefined &&
                this.state.profile.mentoring_interest_list.length != 0 && (
                  <View>
                    <Text style={styles.subHeading}>Mentoring Interests</Text>
                    <View style={styles.renderContainer}>
                      {this.state.profile.mentoring_interest_list.map(
                        (l, i) => (
                          <View key={i} style={styles.insideView}>
                            <Text style={styles.subTexttitle}>{l.name}</Text>
                          </View>
                        )
                      )}

                      {this.state.profile.pre_built_events_indices !=
                        undefined &&
                        this.state.profile.pre_built_events_indices.length !=
                          0 &&
                        this.state.profile.pre_built_events_indices.map(
                          (l, i) => (
                            <View key={i}>
                              {PreBuiltEventTypes.map((item, id) => (
                                <View key={id}>
                                  {l == id && (
                                    <View style={styles.insideView}>
                                      <Text style={styles.subTexttitle}>
                                        {item.name}
                                      </Text>
                                    </View>
                                  )}
                                </View>
                              ))}
                            </View>
                          )
                        )}
                    </View>
                  </View>
                )}

              {this.state.profile.patents != undefined && (
                <View>
                  <Text style={styles.subHeading}>Patents</Text>
                  <View style={styles.renderContainer}>
                    {this.state.profile.patents.map((l, i) => (
                      <View key={i} style={styles.insideView}>
                        <Text style={styles.subTexttitle}>{l.name}</Text>
                      </View>
                    ))}
                  </View>
                </View>
              )}
              {this.state.profile.extra_curricular_activities != undefined && (
                <View>
                  <Text style={styles.subHeading}>
                    Extra Curricular Activites
                  </Text>
                  <View style={styles.renderContainer}>
                    {this.state.profile.extra_curricular_activities.map(
                      (l, i) => (
                        <View key={i} style={styles.insideView}>
                          <Text style={styles.subTexttitle}>{l.name}</Text>
                        </View>
                      )
                    )}
                  </View>
                </View>
              )}
              {this.state.profile.short_term_goals != undefined && (
                <View>
                  <Text style={styles.subHeading}>Short Term Goals</Text>
                  <View style={styles.renderContainer}>
                    {this.state.profile.short_term_goals.map((l, i) => (
                      <View key={i} style={styles.insideView}>
                        <Text style={styles.subTexttitle}>{l.name}</Text>
                      </View>
                    ))}
                  </View>
                </View>
              )}
              {this.state.profile.long_term_goals != undefined &&
                this.state.profile.long_term_goals.length != 0 && (
                  <View>
                    <Text style={styles.subHeading}>Long Term Goals</Text>
                    <View style={styles.renderContainer}>
                      {this.state.profile.long_term_goals.map((l, i) => (
                        <View key={i} style={styles.insideView}>
                          <Text style={styles.subTexttitle}>{l.name}</Text>
                        </View>
                      ))}
                    </View>
                  </View>
                )}
              {this.state.profile.subjects_liked != undefined &&
                this.state.profile.subjects_liked.length != 0 && (
                  <View>
                    <Text style={styles.subHeading}>
                      Subjects Liked At your college
                    </Text>
                    <View style={styles.renderContainer}>
                      {this.state.profile.subjects_liked.map((l, i) => (
                        <View key={i} style={styles.insideView}>
                          <Text style={styles.subTexttitle}>{l.name}</Text>
                        </View>
                      ))}
                    </View>
                  </View>
                )}
              {this.state.profile.faculty_recommended != undefined &&
                this.state.profile.faculty_recommended.length != 0 && (
                  <View>
                    <Text style={styles.subHeading}>
                      Faculty Recommended At your college
                    </Text>
                    <View style={styles.renderContainer}>
                      {this.state.profile.faculty_recommended.map((l, i) => (
                        <View key={i} style={styles.insideView}>
                          <Text style={styles.subTexttitle}>{l.name}</Text>
                        </View>
                      ))}
                    </View>
                  </View>
                )}
            </View>
          </ScrollView>
        );
      } else {
        return <AppLoading />;
      }
    } else {
      return (
        <View style={styles.indicator}>
          <ActivityIndicator size="large"></ActivityIndicator>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  renderContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  subTexttitle: {
    fontSize: 15,
    fontWeight: "bold",
  },
  indicator: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  imageStyle: {
    flex: 1,
    flexDirection: "column",
    height: Dimensions.get("window").height * 0.3,
    backgroundColor: "#a3c0e1",
  },
  insideView: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    borderRadius: 10,
    borderWidth: 2,
    alignItems: "center",
    borderColor: "grey",
  },
  subTextStyle: {
    color: "white",
    fontWeight: "bold",
    marginBottom: 5,
    fontSize: 15,
    textTransform: "capitalize",
  },
  containerStyle: {
    flexDirection: "row",
    justifyContent: "flex-end",
    height: Dimensions.get("window").height * 0.03,
    marginBottom: 10,
  },
  subHeading: {
    fontSize: 20,
    fontWeight: "bold",
    marginVertical: 5,
    marginStart: 3,
  },
  about: {
    fontSize: 17,
    marginBottom: 5,
    marginStart: 3,
  },
  nameonbadge: {
    fontSize: 8,
    color: "#fffff0",
    fontWeight: "900",
    textAlign: "center",
    fontFamily: "LobsterRegular",
    textTransform: "capitalize",
  },
  imageContainer: {
    height: Dimensions.get("window").height * 0.25,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  details: {
    flexDirection: "column",
    padding: 10,
  },
  imageViewStyle: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: Dimensions.get("window").height * 0.01,
    paddingLeft: 10,
    paddingRight: 20,
  },
  titleStyle: {
    fontSize: 23,
    fontWeight: "bold",
    textTransform: "capitalize",
    marginStart: 3,
  },
  emailStyle: {
    color: "rgba(200,0,0,0.8)",
    fontSize: 15,
  },
  imageBesideText: {
    flex: 2,
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "center",
  },
  badge_1stview: {
    borderRadius: 50,
    flexWrap: "wrap",
    marginLeft: 12.8,
    marginTop: 36.8,
    height: 45,
    width: 45,
    justifyContent: "center",
    alignItems: "center",
    //backgroundColor: "grey",
  },
  badge_2ndview: {
    height: 35,
    width: 35,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    //backgroundColor: "tomato",
  },
  nameonbagde: {
    fontSize: 8,
    color: "#fffff0",
    fontWeight: "900",
    textAlign: "center",
    fontFamily: "LobsterRegular",
    textTransform: "capitalize",
  },
});
