import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
  ScrollView,
  Platform,
  Image,
  Text,
  Alert,
  Picker,
  TextInput,
  KeyboardAvoidingView,
  AsyncStorage,
} from "react-native";
import { Button, CheckBox } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import DatePicker from "react-native-datepicker";
import { Toast } from "native-base";
import { Avatar, Icon } from "react-native-elements";
import { urls } from "./../../../env.json";
import Logos from "../../../assets/Logos";
import * as firebase from "firebase";
import { FlatList } from "react-native-gesture-handler";
import PreBuiltEventTypes from "./../Events/PreBuiltEventTypes";
/* This file functionality is similar to AlumniDetailsSignUp but instead of updating details
   here we create alumni details*/

export default class NonAlumniUserLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adminprofile: null,
      loading: false,
      interestmanip: false,
      mentoringInterestOtherCollege: false,
      professor: "",
      specialization: "",
      college_after_BTECH: "",
      institutionName: "None",
      about: "",
      patent: "",
      credential: "",
      name: "",
      course: "None",
      yearOfGraduation: "",
      phoneNumber: "",
      result: "",
      collegeName: "None",
      subject: "",
      mentoringInterest: "",
      interest: "",
      activity: "",
      available: "",
      image: Logos.defaultProfilePic,
      about: "",
      linkedIn: "",
      email: "",
      current_working_company: "",
      current_working_title: "",
      higherEducation: "None",
      branch: "None",
      branchesList: [],
      coursesList: [],
      collegesList: [],
      interestsList: [],
      activityList: [],
      patentsList: [],
      mentoringInterestList: [],
      higherEducationList: [],
      institutionsList: [],
      subjectsList: [],
      professorList: [],
      saveDetailsButtonFlag: false,

      pre_built_event_type: "",
      pre_built_event_index: null, //used to store the no.of prebuilt event types available
      pre_built_list: [], //storing values of events the mentor/mentee is intrested to mentor
    };
  }

  componentDidMount() {
    //getting all the academic details.
    fetch(urls.getHigherEducation, { method: "GET" })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        let d = Array.from(res);
        this.setState({ higherEducationList: d });
      })
      .catch((error) => {
        console.log(error);
        Toast.show({ text: "Some error occured", buttonText: "Okay" });
      });
    fetch(urls.getInstitutions)
      .then((res) => {
        return res.json();
      })
      .then(async (res) => {
        let d = Array.from(res);
        this.setState({ institutionsList: d });
        await this.setState({
          collegesList: this.state.institutionsList.find(
            (x) => x.name == this.state.institutionName
          )
            ? this.state.institutionsList.find(
                (x) => x.name == this.state.institutionName
              ).colleges
            : [],
        });
        await this.setState({
          branchesList: this.state.collegesList.find(
            (x) => x.name == this.state.collegeName
          )
            ? this.state.collegesList.find(
                (x) => x.name == this.state.collegeName
              ).branches
            : [],
        });
        await this.setState({
          coursesList: this.state.collegesList.find(
            (x) => x.name == this.state.collegeName
          )
            ? this.state.collegesList.find(
                (x) => x.name == this.state.collegeName
              ).courses
            : [],
        });
      })
      .catch((error) => {
        console.log(error);
        Toast.show({ text: "Some error occured", buttonText: "Okay" });
      });
    AsyncStorage.getItem("profile")
      .then((data) => {
        //getting details from storage and setting details of photo url.
        return JSON.parse(data);
      })
      .then(async (data) => {
        this.setState({ adminprofile: data });
        if (data.user_type == "college_admin") {
          await this.setState({ institutionName: data.institution_name });
          await this.setState({
            collegeName: "None",
            course: "None",
            branch: "None",
          });
          await this.setState({
            collegesList: this.state.institutionsList.find(
              (x) => x.name == this.state.institutionName
            )
              ? this.state.institutionsList.find(
                  (x) => x.name == this.state.institutionName
                ).colleges
              : [],
          });
          await this.setState({
            coursesList: this.state.collegesList.find(
              (x) => x.name == this.state.collegeName
            )
              ? this.state.collegesList.find(
                  (x) => x.name == this.state.collegeName
                ).courses
              : [],
          });

          await this.setState({
            branchesList: this.state.coursesList.find(
              (x) => x.course == this.state.course
            )
              ? this.state.coursesList.find(
                  (x) => x.course == this.state.course
                ).branches
              : [],
          });
          await this.setState({ collegeName: data.college_name });
          await this.setState({
            course: "None",
            branch: "None",
          });
          await this.setState({
            coursesList: this.state.collegesList.find(
              (x) => x.name == this.state.collegeName
            )
              ? this.state.collegesList.find(
                  (x) => x.name == this.state.collegeName
                ).courses
              : [],
          });
          // console.log(this.state.coursesList);
          await this.setState({
            branchesList: this.state.coursesList.find(
              (x) => x.course == this.state.course
            )
              ? this.state.coursesList.find(
                  (x) => x.course == this.state.course
                ).branches
              : [],
          });
        }
      });

    this.setState({ pre_built_event_index: PreBuiltEventTypes.length });
    this.setFalseForAllPreBuiltEvents();
  }
  setFalseForAllPreBuiltEvents = () => {
    let count = this.state.pre_built_event_index;
    for (let i = 0; i < count; i++) this.state.pre_built_list.push(false); //pushing all values as false intitially
    // console.log("setFalse"+this.state.pre_built_list);
  };

  toggle = async (index) => {
    let pre_list = this.state.pre_built_list; //used to temporarily store pre_built_list
    pre_list[index] = !pre_list[index]; //toggling the value at the index selected
    await this.setState({ pre_built_list: pre_list }); //assigning 'pre_list' back into 'pre_built_list'
  };
  //all the below functions work similar to the one's in NonAlumniUserSignUp.
  addMentoringInterest = () => {
    if (this.state.mentoringInterest.trim() == "") {
      Toast.show({
        text: "No mentoring interests to add.",
        buttonText: "Okay",
      });
      return;
    }
    let flag = 0;
    let s = this.state.mentoringInterest.trim();
    let l = this.state.mentoringInterestList;
    let i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      let a = this.state.mentoringInterestList;
      a.push({ id: l.length, name: this.state.mentoringInterest.trim() });
      this.setState({ mentoringInterestList: a, mentoringInterest: "" });
    } else {
      Toast.show({
        text: "Mentoring Interest already added",
        buttonText: "Okay",
      });
    }
  };

  deleteMentoringInterest = (name) => {
    if (this.state.mentoringInterestList.length == 0) {
      Toast.show({
        text: "No Mentoring Interests to delete",
        buttonText: "Okay",
      });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.mentoringInterestList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ mentoringInterestList: l });
      this.setState({ mentoringInterest: "" });
    } else {
      Toast.show({ text: "Mentoring Interest not exists", buttonText: "Okay" });
      this.setState({ mentoringInterest: "" });
    }
  };

  addPatent = () => {
    if (this.state.patent.trim() == "") {
      Toast.show({ text: "No patents to add.", buttonText: "Okay" });
      return;
    }
    let flag = 0;
    let s = this.state.patent.trim();
    let l = this.state.patentsList;
    let i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      let a = this.state.patentsList;
      a.push({ id: l.length, name: this.state.patent.trim() });
      this.setState({ patentsList: a });
      this.setState({ patent: "" });
    } else {
      Toast.show({ text: "Patent already added", buttonText: "Okay" });
      this.setState({ patent: "" });
    }
  };

  deletePatent = (name) => {
    if (this.state.patentsList.length == 0) {
      Toast.show({ text: "No patents to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name;
    var l = this.state.patentsList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ patentsList: l });
    } else {
      Toast.show({ text: "Patent not exists", buttonText: "Okay" });
    }
  };

  addSubject = () => {
    if (this.state.subject.trim() == "") {
      Toast.show({ text: "No subjects to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.subject.trim();
    var l = this.state.subjectsList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      var a = this.state.subjectsList;
      a.push({ id: l.length, name: this.state.subject.trim() });
      this.setState({ subjectsList: a });
      this.setState({ subject: "" });
    } else {
      this.setState({ subject: "" });
      Toast.show({ text: "Subject already added", buttonText: "Okay" });
    }
  };

  addInterest = () => {
    if (this.state.interest.trim() == "") {
      Toast.show({ text: "No academic interests to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.interest.trim();
    var l = this.state.interestsList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      var a = this.state.interestsList;
      a.push({ id: l.length, name: this.state.interest.trim() });
      this.setState({ interestsList: a });
      this.setState({ interest: "" });
    } else {
      this.setState({ interest: "" });
      Toast.show({ text: "Academic interests added", buttonText: "Okay" });
    }
  };

  addFaculty = () => {
    if (this.state.professor.trim() == "") {
      Toast.show({ text: "No faculty to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.professor.trim();
    var l = this.state.professorList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      var a = this.state.professorList;
      a.push({ id: l.length, name: this.state.professor.trim() });
      this.setState({ professorList: a });
      this.setState({ professor: "" });
    } else {
      this.setState({ professor: "" });
      Toast.show({ text: "Faculty already added", buttonText: "Okay" });
    }
  };

  deleteSubject = (name) => {
    if (this.state.subjectsList.length == 0) {
      Toast.show({ text: "No subjects to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.subjectsList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ subjectList: l });
      this.setState({ subject: "" });
    } else {
      this.setState({ subject: "" });
      Toast.show({ text: "Subject not exists", buttonText: "Okay" });
    }
  };

  deleteInterest = (name) => {
    if (this.state.interestsList.length == 0) {
      Toast.show({
        text: "No academic interests to delete",
        buttonText: "Okay",
      });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.interestsList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ interestsList: l });
    } else {
      Toast.show({ text: "Academic interest not exists", buttonText: "Okay" });
    }
  };

  deleteFaculty = (name) => {
    if (this.state.professorList.length == 0) {
      Toast.show({ text: "No faculty to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.professorList;
    var i = 0;
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ professorList: l });
      this.setState({ professor: "" });
    } else {
      Toast.show({ text: "Faculty not exists", buttonText: "Okay" });
      this.setState({ professor: "" });
    }
  };

  validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  saveButton = async () => {
    //All the details must be filled by Admin.
    if (this.state.name.trim() == "") {
      Toast.show({ text: "Enter name", buttonText: "Okay" });
      return;
    }
    if (this.state.email.trim() == "") {
      Toast.show({ text: "Enter email id", buttonText: "Okay" });
      return;
    }
    if (!this.validateEmail(this.state.email)) {
      Toast.show({ text: "Enter valid email id", buttonText: "Okay" });
      return;
    }
    if (this.state.phoneNumber.trim().length == 0) {
      Toast.show({ text: "Enter valid phone Number", buttonText: "Okay" });
      return;
    }
    if (this.state.linkedIn.trim().indexOf("linkedin.com/in/") == -1) {
      Toast.show({
        text: "LinkedIn url must be of format linkedin.com/in/username",
        buttonText: "Okay",
        duration: 2000,
      });
      return;
    }
    if (this.state.current_working_title.trim() == "") {
      Toast.show({
        text: "Enter current job working title",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.current_working_company.trim() == "") {
      Toast.show({ text: "Enter current working company", buttonText: "Okay" });
      return;
    }
    if (this.state.institutionName.trim() == "None") {
      Toast.show({ text: "Select valid instituion", buttonText: "Okay" });
      return;
    }
    if (this.state.collegeName.trim() == "None") {
      Toast.show({ text: "Select valid college", buttonText: "Okay" });
      return;
    }
    if (this.state.branch.trim() == "None") {
      Toast.show({ text: "Select valid branch", buttonText: "Okay" });
      return;
    }
    if (this.state.course.trim() == "None") {
      Toast.show({ text: "Select valid course", buttonText: "Okay" });
      return;
    }
    if (this.state.specialization.trim() === "") {
      Toast.show({ text: "Enter specialization", buttonText: "Okay" });
      return;
    }
    if (this.state.yearOfGraduation.trim() === "") {
      Toast.show({ text: "Select year of graduation", buttonText: "Okay" });
      return;
    }
    if (this.state.interestsList.length == 0) {
      Toast.show({ text: "Enter atleast one interest", buttonText: "Okay" });
      return;
    }
    if (this.state.available.trim() == "") {
      Toast.show({ text: "Enter available timings", buttonText: "Okay" });
      return;
    }
    if (this.state.mentoringInterestList.length == 0) {
      Toast.show({
        text: "Enter atleast one mentoring Interest",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.higherEducation.trim() == "") {
      Toast.show({ text: "Select higher Education", buttonText: "Okay" });
      return;
    }
    if (this.state.collegesList.find((x) => x.name == this.state.collegeName)) {
      var collegeId = this.state.collegesList.find(
        (x) => x.name == this.state.collegeName
      ).id;
    }
    if (
      this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      )
    ) {
      var institutionId = this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      ).id;
    }
    this.setState({
      saveDetailsButtonFlag: true,
    });
    let pre_list = this.state.pre_built_list; //used to temporarily store pre_built_list
    let event_indices_list = []; //keeps track of all indices which are selected
    for (let i = 0; i < pre_list.length; i++) {
      if (pre_list[i] == true)
        //if selected i.e true
        event_indices_list.push(i); //then we are pushing the index into the array
    }
    let d = {
      last_logged_in: Date().toLocaleString(),
      photo_url: this.state.image,
      college_after_BTECH:
        this.state.college_after_BTECH.trim() === ""
          ? null
          : this.state.college_after_BTECH.trim(),
      name: this.state.name.trim(),
      available: this.state.available.trim(),
      about: this.state.about.trim() === "" ? null : this.state.about.trim(),
      email: this.state.email.trim(),
      branch: this.state.branch.trim(),
      course: this.state.course.trim(),
      year_passed: this.state.yearOfGraduation.trim(),
      specialization:
        this.state.specialization.trim() === ""
          ? null
          : this.state.specialization.trim(),
      higher_education:
        this.state.higherEducation.trim() === ""
          ? null
          : this.state.higherEducation.trim(),
      linkedin: this.state.linkedIn.trim(),
      user_type: "mentor",
      interests_list: this.state.interestsList,
      mentoring_interest_other_college: this.state
        .mentoringInterestOtherCollege,
      phone_number: this.state.phoneNumber,
      patents:
        this.state.patentsList.length === 0 ? null : this.state.patentsList,
      mentoring_interest_list: this.state.mentoringInterestList,
      subjects_liked:
        this.state.subjectsList.length === 0 ? null : this.state.subjectsList,
      faculty_recommended:
        this.state.professorList === 0 ? null : this.state.professorList,
      authorized: false,
      status: "approved",
      current_working_title: this.state.current_working_title,
      current_working_company: this.state.current_working_company,
      college_name: this.state.collegeName,
      college_id:
        this.state.adminprofile.user_type === "college_admin"
          ? this.state.adminprofile.college_id
          : collegeId,
      last_updated_by: "admin",

      institution_id:
        this.state.adminprofile.user_type === "college_admin"
          ? this.state.adminprofile.institution_id
          : institutionId,
      institution_name: this.state.institutionName,
      display_profile: true,
      pre_built_events_indices:
        event_indices_list.length === 0 ? null : event_indices_list,
    };

    fetch(urls.isAlumniPresent, {
      method: "POST",
      body: JSON.stringify({email: d.email }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.alreadyPresent) {
          Toast.show({
            text: "A Record of Alumni already exists with same phone number.",
            buttonText: "Okay",
          });
          this.setState({
            saveDetailsButtonFlag: false,
          });
          return;
        } else {
          if (
            (this.state.adminprofile.user_type === "college_admin" &&
              this.state.adminprofile.authorized == true) ||
            this.state.adminprofile.user_type !== "college_admin"
          ) {
            fetch(urls.addAlumniJNTUKadmin, {
              method: "POST",
              body: JSON.stringify(d),
              headers: {
                "Content-type": "application/json; charset=UTF-8",
              },
            })
              .then((res) => {
                return res.json();
              })
              .then((res) => {
                console.log(res);
                if (res.error == undefined) {
                  Toast.show({ text: "Alumni Added", buttonText: "Okay" });
                  this.setState({
                    loading: false,
                    interestmanip: false,
                    professor: "",
                    specialization: "",
                    college_after_BTECH: "",
                    institutionName: "None",
                    about: "",
                    patent: "",
                    credential: "",
                    name: "",
                    course: "None",
                    yearOfGraduation: "",
                    phoneNumber: "",
                    result: "",
                    collegeName: "None",
                    subject: "",
                    mentoringInterest: "",
                    interest: "",
                    activity: "",
                    available: "",
                    image: Logos.defaultProfilePic,
                    about: "",
                    linkedIn: "",
                    email: "",
                    current_working_company: "",
                    current_working_title: "",
                    higherEducation: "None",
                    branch: "None",
                    interestsList: [],
                    activityList: [],
                    patentsList: [],
                    mentoringInterestList: [],
                    subjectsList: [],
                    professorList: [],
                    saveDetailsButtonFlag: false,
                  });
                } else {
                  Toast.show({ text: res.error, buttonText: "okay" });
                }
              })
              .then(() => {
                this.setState({
                  saveDetailsButtonFlag: false,
                });
              })
              .catch((err) => console.log(err));
          } else {
            if (
              this.state.adminprofile.user_type === "college_admin" &&
              this.state.adminprofile.authorized == false
            ) {
              Alert.alert(
                "Warning!",
                "You can only perform actions when you are activated by Mentoring association ,please contact MentorKonnect Council Team for further details.",
                [{ text: "OK" }]
              );
              this.setState({
                loading: false,
                interestmanip: false,
                professor: "",
                specialization: "",
                college_after_BTECH: "",
                institutionName: "None",
                about: "",
                patent: "",
                credential: "",
                name: "",
                course: "None",
                yearOfGraduation: "",
                phoneNumber: "",
                result: "",
                collegeName: "None",
                subject: "",
                mentoringInterest: "",
                interest: "",
                activity: "",
                available: "",
                image: Logos.defaultProfilePic,
                about: "",
                linkedIn: "",
                email: "",
                current_working_company: "",
                current_working_title: "",
                higherEducation: "None",
                branch: "None",
                interestsList: [],
                activityList: [],
                patentsList: [],
                mentoringInterestList: [],
                subjectsList: [],
                professorList: [],
                saveDetailsButtonFlag: false,
              });
            }
          }
        }
      })
      .catch((err) => console.log(err));
    this.setFalse();
  };
  setFalse = () => {
    //console.log("No");
    let pre_List = this.state.pre_built_list;
    for (let i = 0; i < pre_List.length; i++) {
      //used to set all the values of checkBoxes as false when NO is clicked for mentoringInterest
      pre_List[i] = false;
    }
    //console.log(pre_List);
    this.setState({ pre_built_list: pre_List });
    //console.log(this.state.pre_built_list);
  };

  render() {
    if (this.state.adminprofile !== null) {
      return (
        <View style={styles.overall}>
          <KeyboardAvoidingView
            behavior={Platform.Os == "ios" ? "padding" : "height"}
            style={styles.keyboardView}
          >
            <ScrollView
              style={styles.container}
              keyboardShouldPersistTaps="handled"
            >
              <View style={styles.logoContainer}>
                <Image
                  style={styles.logoStyle}
                  source={{ uri: Logos.mentorkonnectLogo }}
                />
              </View>
              <View style={styles.detailsContainer}>
                <Text style={styles.heading}>Contact Info:</Text>
                <View style={styles.headingView}>
                  <Text style={styles.subtitle}>
                    Name <Text style={styles.req}>*</Text>
                  </Text>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Name"
                    underlineColorAndroid="transparent"
                    style={styles.TextInputStyle}
                    value={this.state.name}
                    onChangeText={(text) => this.setState({ name: text })}
                  />
                  <Text style={styles.subtitle}>
                    Email Id <Text style={styles.req}>*</Text>
                  </Text>
                  <TextInput
                    selectTextOnFocus={true}
                    autoCapitalize="none"
                    placeholder="Email"
                    underlineColorAndroid="transparent"
                    style={styles.TextInputStyle}
                    value={this.state.email}
                    onChangeText={(text) => this.setState({ email: text })}
                  />
                  <Text style={styles.subtitle}>
                    Phone Number <Text style={styles.req}>*</Text>
                  </Text>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Mobile Number"
                    underlineColorAndroid="transparent"
                    style={styles.TextInputStyle}
                    keyboardType={"numeric"}
                    value={this.state.phoneNumber}
                    onChangeText={(text) =>
                      this.setState({ phoneNumber: text })
                    }
                  />
                  <Text style={styles.subtitle}>
                    LinkedIn Profile <Text style={styles.req}>*</Text>
                  </Text>
                  <TextInput
                    selectTextOnFocus={true}
                    autoCapitalize="none"
                    editable={this.state.editable}
                    placeholder="LinkedIn profile url..linkedin.com/in/username"
                    underlineColorAndroid="transparent"
                    style={styles.TextInputStyle}
                    value={this.state.linkedIn}
                    onChangeText={(text) => this.setState({ linkedIn: text })}
                  />
                </View>
                <Text style={styles.heading}>College Info:</Text>
                <View style={styles.headingView}>
                  {this.state.adminprofile.user_type !== "college_admin" && (
                    <View>
                      <Dropdown
                        label="Institution *"
                        labelFontSize={18}
                        itemCount={6}
                        labelTextStyle={styles.dropDownStyle}
                        animationDuration={0}
                        data={this.state.institutionsList}
                        value={this.state.institutionName}
                        valueExtractor={(item, index) => {
                          return item.name;
                        }}
                        onChangeText={async (v, i, d) => {
                          await this.setState({ institutionName: v });
                          await this.setState({
                            collegeName: "None",
                            course: "None",
                            branch: "None",
                          });
                          await this.setState({
                            collegesList: this.state.institutionsList.find(
                              (x) => x.name == this.state.institutionName
                            )
                              ? this.state.institutionsList.find(
                                  (x) => x.name == this.state.institutionName
                                ).colleges
                              : [],
                          });
                          await this.setState({
                            coursesList: this.state.collegesList.find(
                              (x) => x.name == this.state.collegeName
                            )
                              ? this.state.collegesList.find(
                                  (x) => x.name == this.state.collegeName
                                ).courses
                              : [],
                          });

                          await this.setState({
                            branchesList: this.state.coursesList.find(
                              (x) => x.course == this.state.course
                            )
                              ? this.state.coursesList.find(
                                  (x) => x.course == this.state.course
                                ).branches
                              : [],
                          });
                        }}
                      />

                      <Dropdown
                        label="College *"
                        labelFontSize={18}
                        itemCount={6}
                        labelTextStyle={styles.dropDownStyle}
                        data={this.state.collegesList}
                        value={this.state.collegeName}
                        valueExtractor={(item, index) => {
                          return item.name;
                        }}
                        onChangeText={async (v, i, d) => {
                          await this.setState({ collegeName: v });
                          await this.setState({
                            course: "None",
                            branch: "None",
                          });
                          await this.setState({
                            coursesList: this.state.collegesList.find(
                              (x) => x.name == this.state.collegeName
                            )
                              ? this.state.collegesList.find(
                                  (x) => x.name == this.state.collegeName
                                ).courses
                              : [],
                          });
                          // console.log(this.state.coursesList);
                          await this.setState({
                            branchesList: this.state.coursesList.find(
                              (x) => x.course == this.state.course
                            )
                              ? this.state.coursesList.find(
                                  (x) => x.course == this.state.course
                                ).branches
                              : [],
                          });
                        }}
                      />
                    </View>
                  )}

                  <Dropdown
                    label="Course *"
                    labelFontSize={18}
                    itemCount={6}
                    labelTextStyle={styles.dropDownStyle}
                    data={this.state.coursesList}
                    value={this.state.course}
                    valueExtractor={(item, index) => {
                      return item.course;
                    }}
                    onChangeText={async (v, i, d) => {
                      await this.setState({ course: v });
                      await this.setState({ branch: "None" });
                      await this.setState({
                        branchesList: this.state.coursesList.find(
                          (x) => x.course == this.state.course
                        )
                          ? this.state.coursesList.find(
                              (x) => x.course == this.state.course
                            ).branches
                          : [],
                      });
                      // console.log(this.state.branchesList)
                    }}
                  />
                  <Dropdown
                    label="Branch *"
                    labelFontSize={18}
                    itemCount={6}
                    labelTextStyle={styles.dropDownStyle}
                    data={this.state.branchesList}
                    value={this.state.branch}
                    valueExtractor={(item, index) => {
                      return item.branch;
                    }}
                    onChangeText={async (v, i, d) => {
                      this.setState({ branch: v });
                    }}
                  />
                  <Dropdown
                    label="Highest Education"
                    labelFontSize={18}
                    itemCount={6}
                    labelTextStyle={styles.dropDownStyle}
                    data={this.state.higherEducationList}
                    value={this.state.higherEducation}
                    valueExtractor={(item, index) => {
                      return item.name;
                    }}
                    onChangeText={async (v, i, d) => {
                      this.setState({ higherEducation: v });
                    }}
                  />
                  <Text style={styles.subtitle}>
                    Specialization <Text style={styles.req}>*</Text>
                  </Text>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Specialization"
                    underlineColorAndroid="transparent"
                    style={styles.TextInputStyle}
                    value={this.state.specialization}
                    onChangeText={(text) =>
                      this.setState({ specialization: text })
                    }
                  />
                  <Text style={styles.subtitle}>
                    Year of Graduation <Text style={styles.req}>*</Text>
                  </Text>
                  <DatePicker
                    style={styles.dateStyle}
                    date={this.state.yearOfGraduation}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    minDate="1800-01-01"
                    // maxDate="2020-01-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: "absolute",
                        left: 0,
                        top: 4,
                        marginLeft: 0,
                      },
                      dateInput: {
                        marginLeft: 36,
                      },
                    }}
                    onDateChange={(date) => {
                      this.setState({ yearOfGraduation: date });
                    }}
                  />
                  <Text style={styles.subtitle}>College After B.TECH</Text>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="College name"
                    underlineColorAndroid="transparent"
                    style={styles.TextInputStyle}
                    value={this.state.college_after_BTECH}
                    onChangeText={(text) =>
                      this.setState({ college_after_BTECH: text })
                    }
                  />
                </View>
                <Text style={styles.heading}>Work Related Info:</Text>
                <View style={styles.headingView}>
                  <Text style={styles.subtitle}>
                    Current Working Company <Text style={styles.req}>*</Text>
                  </Text>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter current working company"
                    underlineColorAndroid="transparent"
                    style={styles.TextInputStyle}
                    value={this.state.current_working_company}
                    onChangeText={(text) =>
                      this.setState({ current_working_company: text })
                    }
                  />

                  <Text style={styles.subtitle}>
                    Current Working Title <Text style={styles.req}>*</Text>
                  </Text>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter current working job role"
                    underlineColorAndroid="transparent"
                    style={styles.TextInputStyle}
                    value={this.state.current_working_title}
                    onChangeText={(text) =>
                      this.setState({ current_working_title: text })
                    }
                  />
                  <Text style={styles.subtitle}>Patents/Awards</Text>
                  <View style={styles.interestsStyle}>
                    <TextInput
                      selectTextOnFocus={true}
                      placeholder="Enter Patents"
                      underlineColorAndroid="transparent"
                      style={styles.adderLists}
                      value={this.state.patent}
                      onChangeText={(text) => this.setState({ patent: text })}
                    />
                    <Avatar
                      size="medium"
                      overlayContainerStyle={{
                        backgroundColor: "rgba(25,100,200,1)",
                      }}
                      rounded
                      icon={{
                        name: "plus-circle",
                        color: "white",
                        type: "font-awesome",
                      }}
                      onPress={() => this.addPatent()}
                      activeOpacity={0.7}
                    />
                  </View>
                  <View style={styles.interestsContentContainer}>
                    {this.state.patentsList.map((l, i) => (
                      <View key={i} style={styles.interestsSubContent}>
                        <Text style={styles.interestsTextStyle}>{l.name}</Text>
                        <Icon
                          name={"times-circle"}
                          type={"font-awesome"}
                          onPress={() => this.deletePatent(l.name)}
                          containerStyle={styles.deleteContainerStyle}
                          activeOpacity={0.7}
                        />
                      </View>
                    ))}
                  </View>
                </View>

                <Text style={styles.heading}>Mentoring Interests:</Text>
                <View style={styles.headingView}>
                  <Text style={styles.subtitle}>
                    Would you like to offer mentoring to students from other
                    college also?
                  </Text>

                  <View style={styles.interestsView}>
                    <CheckBox
                      center
                      title="Yes"
                      checkedIcon="dot-circle-o"
                      uncheckedIcon="circle-o"
                      containerStyle={{ backgroundColor: "white" }}
                      checked={this.state.mentoringInterestOtherCollege}
                      onPress={() => {
                        this.setState((prev) => {
                          return {
                            mentoringInterestOtherCollege: !prev.mentoringInterestOtherCollege,
                          };
                        });
                      }}
                    />
                    <CheckBox
                      center
                      title="No"
                      checkedIcon="dot-circle-o"
                      uncheckedIcon="circle-o"
                      containerStyle={{ backgroundColor: "white" }}
                      checked={!this.state.mentoringInterestOtherCollege}
                      onPress={() => {
                        this.setState((prev) => {
                          return {
                            mentoringInterestOtherCollege: !prev.mentoringInterestOtherCollege,
                          };
                        });
                      }}
                    />
                  </View>
                  <Text style={styles.subtitle}>
                    Mentoring Interest Areas <Text style={styles.req}>*</Text>
                  </Text>
                  <FlatList
                    data={PreBuiltEventTypes}
                    style={{ flex: 1 }}
                    renderItem={({ item, index }) => {
                      return (
                        <CheckBox
                          containerStyle={{ backgroundColor: "white" }}
                          checked={this.state.pre_built_list[index]}
                          title={item.name}
                          onPress={() => {
                            this.toggle(index);
                          }}
                        />
                      );
                    }}
                  />
                  <View style={styles.interestsStyle}>
                    <TextInput
                      selectTextOnFocus={true}
                      placeholder="Enter Mentoring Interests"
                      underlineColorAndroid="transparent"
                      style={styles.adderLists}
                      value={this.state.mentoringInterest}
                      onChangeText={(text) =>
                        this.setState({ mentoringInterest: text })
                      }
                    />
                    <Avatar
                      size="medium"
                      overlayContainerStyle={{
                        backgroundColor: "rgba(25,100,200,1)",
                      }}
                      rounded
                      icon={{
                        name: "plus-circle",
                        color: "white",
                        type: "font-awesome",
                      }}
                      onPress={() => this.addMentoringInterest()}
                      activeOpacity={0.7}
                    />
                  </View>
                  <View style={styles.interestsContentContainer}>
                    {this.state.mentoringInterestList.map((l, i) => (
                      <View key={i} style={styles.interestsSubContent}>
                        <Text style={styles.interestsTextStyle}>{l.name}</Text>
                        <Icon
                          name={"times-circle"}
                          type={"font-awesome"}
                          onPress={() => this.deleteMentoringInterest(l.name)}
                          containerStyle={styles.deleteContainerStyle}
                          activeOpacity={0.7}
                        />
                      </View>
                    ))}
                  </View>
                </View>
                <Text style={styles.heading}>Additional Info:</Text>
                <View style={styles.headingView}>
                  <Text style={styles.subtitle}>
                    Academic Interests <Text style={styles.req}>*</Text>
                  </Text>
                  <View style={styles.interestsStyle}>
                    <TextInput
                      selectTextOnFocus={true}
                      placeholder="Enter Interests"
                      underlineColorAndroid="transparent"
                      style={styles.adderLists}
                      value={this.state.interest}
                      onChangeText={(text) => this.setState({ interest: text })}
                    />
                    <Avatar
                      size="medium"
                      overlayContainerStyle={{
                        backgroundColor: "rgba(25,100,200,1)",
                      }}
                      rounded
                      icon={{
                        name: "plus-circle",
                        color: "white",
                        type: "font-awesome",
                      }}
                      onPress={() => this.addInterest()}
                      activeOpacity={0.7}
                    />
                  </View>
                  <View style={styles.interestsContentContainer}>
                    {this.state.interestsList.map((l, i) => (
                      <View key={i} style={styles.interestsSubContent}>
                        <Text style={styles.interestsTextStyle}>{l.name}</Text>
                        <Icon
                          name={"times-circle"}
                          type={"font-awesome"}
                          onPress={() => this.deleteInterest(l.name)}
                          containerStyle={styles.deleteContainerStyle}
                          activeOpacity={0.7}
                        />
                      </View>
                    ))}
                  </View>
                  <Text style={styles.subtitle}>
                    Subjects liked at your college
                  </Text>
                  <View style={styles.interestsStyle}>
                    <TextInput
                      selectTextOnFocus={true}
                      placeholder="Enter Subjects"
                      underlineColorAndroid="transparent"
                      style={styles.adderLists}
                      value={this.state.subject}
                      onChangeText={(text) => this.setState({ subject: text })}
                    />
                    <Avatar
                      size="medium"
                      overlayContainerStyle={{
                        backgroundColor: "rgba(25,100,200,1)",
                      }}
                      rounded
                      icon={{
                        name: "plus-circle",
                        color: "white",
                        type: "font-awesome",
                      }}
                      onPress={() => this.addSubject()}
                      activeOpacity={0.7}
                    />
                  </View>
                  <View style={styles.interestsContentContainer}>
                    {this.state.subjectsList.map((l, i) => (
                      <View key={i} style={styles.interestsSubContent}>
                        <Text style={styles.interestsTextStyle}>{l.name}</Text>
                        <Icon
                          name={"times-circle"}
                          type={"font-awesome"}
                          onPress={() => this.deleteSubject(l.name)}
                          containerStyle={styles.deleteContainerStyle}
                          activeOpacity={0.7}
                        />
                      </View>
                    ))}
                  </View>
                  <Text style={styles.subtitle}>
                    Faculty you would recommend at your college
                  </Text>
                  <View style={styles.interestsStyle}>
                    <TextInput
                      selectTextOnFocus={true}
                      placeholder="Enter faculty"
                      underlineColorAndroid="transparent"
                      style={styles.adderLists}
                      value={this.state.professor}
                      onChangeText={(text) =>
                        this.setState({ professor: text })
                      }
                    />
                    <Avatar
                      size="medium"
                      overlayContainerStyle={{
                        backgroundColor: "rgba(25,100,200,1)",
                      }}
                      rounded
                      icon={{
                        name: "plus-circle",
                        color: "white",
                        type: "font-awesome",
                      }}
                      onPress={() => this.addFaculty()}
                      activeOpacity={0.7}
                    />
                  </View>
                  <View style={styles.interestsContentContainer}>
                    {this.state.professorList.map((l, i) => (
                      <View key={i} style={styles.interestsSubContent}>
                        <Text style={styles.interestsTextStyle}>{l.name}</Text>
                        <Icon
                          name={"times-circle"}
                          type={"font-awesome"}
                          onPress={() => this.deleteFaculty(l.name)}
                          containerStyle={styles.deleteContainerStyle}
                          activeOpacity={0.7}
                        />
                      </View>
                    ))}
                  </View>
                  <Text style={styles.subtitle}>
                    Available Timings <Text style={styles.req}>*</Text>
                  </Text>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Available time"
                    underlineColorAndroid="transparent"
                    style={styles.TextInputStyle}
                    value={this.state.available}
                    onChangeText={(text) => this.setState({ available: text })}
                  />
                  <Text style={styles.subtitle}>Additional information</Text>
                  <TextInput
                    selectTextOnFocus={true}
                    textAlignVertical="top"
                    placeholder="About"
                    underlineColorAndroid="transparent"
                    multiline={true}
                    numberOfLines={10}
                    style={styles.about}
                    value={this.state.about}
                    onChangeText={(text) => this.setState({ about: text })}
                  />
                </View>
                <View style={styles.save}>
                  <Button
                    loading={this.state.saveDetailsButtonFlag}
                    icon={
                      <Icon
                        type="font-awesome"
                        name="save"
                        size={20}
                        color="white"
                      />
                    }
                    containerStyle={styles.saveContainerStyle}
                    buttonStyle={styles.saveButtonStyle}
                    onPress={() => {
                      this.saveButton();
                    }}
                    titleStyle={styles.saveButtonTitleStyle}
                    title="Save"
                  />
                  {this.state.loading && <ActivityIndicator size="large" />}
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      );
    } else {
      return (
        <View style={styles.indicator}>
          <ActivityIndicator size="large"></ActivityIndicator>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  indicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  overall: { flex: 1, backgroundColor: "white" },
  keyboardView: { flex: 1 },
  deleteContainerStyle: { padding: 7 },
  about: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.9,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    height: 100,
    backgroundColor: "white",
    textAlign: "left",
  },
  saveButtonTitleStyle: { padding: 5 },
  saveContainerStyle: { padding: 10 },
  save: { flexDirection: "row", justifyContent: "center" },
  saveButtonStyle: {
    backgroundColor: "rgba(200,175,0,1)",
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    width: Dimensions.get("window").width * 0.4,
  },
  adderLists: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.76,
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    backgroundColor: "white",
  },
  interestsView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  TextInputStyle: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.9,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    backgroundColor: "white",
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "bold",
  },
  detailsContainer: {
    flexDirection: "column",
    justifyContent: "flex-start",
    margin: 12,
    alignContent: "flex-start",
  },
  titleStyle: {
    fontSize: 25,
    fontWeight: "bold",
    textAlign: "center",
  },
  logoContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    flex: 1,
  },
  logoStyle: {
    borderRadius: 5,
    height: 120,
    width: 200,
    marginTop: 30,
    marginBottom: 20,
  },
  dropDownStyle: { paddingBottom: 5, fontWeight: "bold" },
  req: { color: "red" },
  dateStyle: {
    width: Dimensions.get("window").width * 0.9,
    marginTop: 10,
    marginBottom: 10,
  },
  interestsStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  interestsContentContainer: { flexDirection: "row", flexWrap: "wrap" },
  interestsSubContent: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    borderRadius: 10,
    borderWidth: 2,
    backgroundColor: "rgba(100,100,255,0.4)",
    alignItems: "center",
  },
  interestsTextStyle: { fontSize: 15, fontWeight: "bold" },
  deleteOverlay: { backgroundColor: "rgba(100,255,255,0.1)", padding: 0 },

  heading: {
    fontSize: 20,
    fontWeight: "bold",
    marginTop: 10,
    color: "black",
  },
  headingView: {
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "grey",
    padding: 5,
    width: Dimensions.get("window").width * 0.9375,
    backgroundColor: "whitesmoke",
    marginBottom: 5,
  },
});
