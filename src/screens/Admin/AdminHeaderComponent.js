import React from "react";
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  Platform,
  Image,
  AsyncStorage,
} from "react-native";
import { Icon, Avatar } from "react-native-elements";
import Logos from "../../../assets/Logos";

export default class AdminHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = { profile: "" };
    AsyncStorage.getItem("profile")
      .then(async (d) => {
        await this.setState({
          profile: JSON.parse(d),
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <View style={styles.headerContainer}>
        <View>
          <Icon
            name="menu"
            color="black"
            size={30}
            containerStyle={{ marginLeft: Platform.OS === "ios" ? 15 : 0 }}
            onPress={() => {
              this.props.navigation.openDrawer();
            }}
          ></Icon>
        </View>
        {this.props.text !== undefined && (
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              alignContent: "center",
              justifyContent: "center",
            }}
          >
            <Text style={{ fontWeight: "bold", fontSize: 20 }}>
              {this.props.text}
            </Text>
          </View>
        )}
        <View style={styles.signOut}>
          <Image
            style={{ width: 40, height: 40, marginEnd: 25 }}
            source={{ uri: Logos.mentorKonnectIcon }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  signOut: {
    flex: 0.15,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  headerContainer: {
    width: Dimensions.get("window").width,
    height: "100%",
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
  },
});
