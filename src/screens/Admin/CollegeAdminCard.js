//TO display  college admin details in the card for activating and deactivating

import React from "react";
import { StyleSheet, Text, View, Dimensions, Linking } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Avatar, Button } from "react-native-elements";
import { urls } from "./../../../env.json";

export default class CollegeAdminCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      approveButtonFlag: false,
      disapproveButtonFlag: false,
    };
  }

  handleApprove = async (status, uid, email, name) => {
    const { getData } = this.props;
    let user = "collegeadmin";
    const url =
      urls.handleApprovalsv2 +
      `?user=${user}&status=${status}&uid=${uid}&email=${email}&name=${name}`;
    fetch(url)
      .then(async () => {
        this.setState({
          disapproveButtonFlag: false,
          approveButtonFlag: false,
        });
        getData();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  sendPushNotification = async (msg, token, status) => {
    const message = {
      to: token,
      sound: "default",
      title: "Mentoring Event",
      body: msg,
      data: { data: status },
      _displayInForeground: true,
    };
    fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Accept-encoding": "gzip, deflate",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(message),
    }).catch((err) => {
      console.log(err);
    });
  };

  render() {
    const { admin, navigation } = this.props;
    return (
      <View style={styles.overall}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            navigation.navigate("AdminDetailsValidation", {
              user: admin,
              navigation: navigation,
              admin: true,
            }); //on pressing mentor card his details are visible.
          }}
        >
          <View style={styles.listItem}>
            <Avatar
              size={80}
              rounded
              avatarStyle={{}}
              source={{ uri: admin.photo_url }}
              icon={{ name: "user", type: "font-awesome" }}
            />
            <View style={styles.cardRight}>
              <Text style={{ ...styles.text, fontSize: 15 }}>{admin.name}</Text>

              <Text
                style={{ ...styles.subText, fontSize: 20, fontWeight: "bold" }}
              >
                {admin.college_name}
              </Text>
              {
                <Text style={styles.subText}>
                  Status:{" "}
                  {admin["authorized"] == true ? "verified" : "Not Verified"}
                </Text>
              }
            </View>
          </View>
        </TouchableOpacity>
        {admin.license_code_verified === true && (
          <View style={styles.buttons}>
            <Button
              loading={this.state.approveButtonFlag}
              disabled={admin.authorized == true}
              title="Activate"
              buttonStyle={styles.activate}
              containerStyle={styles.buttonContainer}
              onPress={async () => {
                this.setState({
                  approveButtonFlag: true,
                });
                {
                  await this.handleApprove(
                    "approved",
                    admin.uid,
                    admin.email,
                    admin.name
                  );
                  /*let msg = `Hi ${admin.name} you  have been activated as College admin by Admin of CollegeKonnect.`
                await this.sendPushNotification(msg, mentor.expo_push_token, "approved");*/
                }
              }}
            />
            <Button
              loading={this.state.disapproveButtonFlag}
              disabled={admin.authorized == false}
              title="Deactivate"
              buttonStyle={styles.deactivate}
              containerStyle={{ ...styles.buttonContainer, marginStart: 10 }}
              onPress={async () => {
                this.setState({
                  disapproveButtonFlag: true,
                });
                {
                  await this.handleApprove(
                    "not_approved",
                    admin.uid,
                    admin.email,
                    admin.name
                  );
                  /* let msg = `Hi ${admin.name} you  have been deactivated as College admin by Admin of CollegeKonnect. Please contact our council team for the reason.`
                await this.sendPushNotification(msg, mentor.expo_push_token, "not_approved");*/
                }
              }}
            />
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttons: {
    flexDirection: "row",
    justifyContent: "center",
    margin: 5,
    marginTop: 0,
  },
  activate: {
    backgroundColor: "rgba(0,178,0,1)",
  },
  deactivate: {
    backgroundColor: "rgba(230,0,0,0.8)",
  },
  buttonContainer: {
    marginBottom: 3,
    width: Dimensions.get("window").width * 0.4,
  },
  overall: {
    marginTop: 5,
    flexDirection: "column",
    elevation: 1,
    borderRadius: 2,
    borderWidth: 0.5,
  },
  cardRight: {
    flex: 1,
    alignItems: "flex-start",
    marginLeft: 15,
  },
  cardRightSub: {
    flexDirection: "row",
    alignItems: "center",
  },
  text: {
    fontWeight: "bold",
    textTransform: "capitalize",
    marginBottom: 3,
    fontSize: 18,
  },
  subText: {
    marginBottom: 3,
    fontSize: 15,
  },
  listItem: {
    flexDirection: "row",
    alignItems: "center",
    padding: 10,
  },
});
