// Functionality: Creating new Mentoring Event by mentee
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  Dimensions,
  AsyncStorage,
} from "react-native";
import { TextInput, ScrollView } from "react-native-gesture-handler";
import { Button, CheckBox } from "react-native-elements";
import { Toast } from "native-base";
import { urls } from "./../../../env.json";

export default class CreateNewEvent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: "",
      eventName: "",
      eventSummary: "",
      preWorkDone: "",
      additionalDetails: "",
      schedulePreferences: "",
      mentor: null,
      sumbitButtonFlag: false,
      registered_mentees: [], //array is created to keep track of registered mentees of the group event
      google_classroom_link: "", //used to store google classroom link
      google_classroom_code: "", //used to store the google classRoom code
      groupeventType: false,
    };
  }

  componentDidMount() {
    // Get the mentor param from MentorDetails page
    let mentor = this.props.navigation.getParam("mentor");
    if (mentor != undefined) {
      this.setState({
        mentor: mentor != undefined ? mentor : null,
      });
    }
  }

  submit = () => {
    // Check whether the required fields are entered or not
    if (this.state.eventName.trim() == "")
      Toast.show({ text: "Enter event name", buttonText: "Okay" });
    else if (this.state.eventSummary.trim() == "")
      Toast.show({ text: "Enter event summary", buttonText: "Okay" });
    else if (this.state.google_classroom_link.trim() == "")
      Toast.show({
        text: "Enter Link for Google Classroom",
        buttonText: "Okay",
      });
    else if (this.state.google_classroom_code.trim() == "")
      Toast.show({
        text: "Enter Code for Google Classroom",
        buttonText: "Okay",
      });
    else if (this.state.preWorkDone.trim() == "")
      Toast.show({ text: "Enter pre work done", buttonText: "Okay" });
    else if (this.state.schedulePreferences.trim() == "")
      Toast.show({ text: "Enter timings", buttonText: "Okay" });
    else {
      this.setState({
        sumbitButtonFlag: true,
      });
      this.addEvent();
    }
  };

  sendPushNotification = async (msg, token, status) => {
    const message = {
      to: token,
      sound: "default",
      title: "Mentoring Event",
      body: msg,
      data: { data: status },
      _displayInForeground: true,
    };
    fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Accept-encoding": "gzip, deflate",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(message),
    }).catch((err) => {
      console.log(err);
    });
  };

  addEvent = async () => {
    const profile = this.props.navigation.getParam("profile");
    this.state.registered_mentees.push(profile.uid); //we are initially pushing the admin's uid as admin is also the mentee of the groupEvent
    let eventObject = {
      date_of_last_action: Date.now(),
      mentor_id:
        this.state.mentor != null
          ? this.state.mentor.uid != undefined
            ? this.state.mentor.uid
            : this.state.mentor.phone_number
          : null,
      mentee_id: profile.uid,
      event_name: this.state.eventName.trim(),
      event_summary: this.state.eventSummary.trim(),
      prework_done: this.state.preWorkDone.trim(),
      additional_details_mentee:
        this.state.additionalDetails.trim() == ""
          ? null
          : this.state.additionalDetails.trim(),
      schedule_preferences:
        this.state.schedulePreferences.trim() == ""
          ? null
          : this.state.schedulePreferences.trim(),
      status: "Open",
      mentor_push_token:
        this.state.mentor == undefined
          ? null
          : this.state.mentor.expo_push_token,
      mentee_push_token: profile.expo_push_token,
      last_updated_by: "admin",
      event_approved: true,
      group_event: true,
      registered_mentees: this.state.registered_mentees, //it stores all the details of mentees who want to register
      google_classroom_link: this.state.google_classroom_link, //storing the google classRoom Link
      google_classroom_code: this.state.google_classroom_code, //storing the google classRoom Code
      event_type: this.state.groupeventType
        ? "Bootcamp Event"
        : "Overview Event", //storing event type whether it is bootcamp or overview.
      college_id: profile.college_id,
      institution_id: profile.institution_id,
    };
    //console.log("My event obj"+eventObject.registered_mentees);
    const url = urls.createNewMentoringEvent;
    return fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(eventObject),
    })
      .then((response) => response.json())
      .then(async (response) => {
        this.props.navigation.goBack();
        if (this.state.mentor != null) {
          let mentee_message = `Hi ${profile.name} , you have successfully sent a mentoring event request to ${this.state.mentor.name} on ${eventObject.event_name}`;
          await this.sendPushNotification(
            mentee_message,
            eventObject.mentee_push_token,
            "Open"
          );
          let mentor_message = `Hi ${this.state.mentor.name} , you have received a mentoring event request from ${profile.name} on ${eventObject.event_name}`;
          await this.sendPushNotification(
            mentor_message,
            eventObject.mentor_push_token,
            "Open"
          );
        } else {
          let mentee_message = `Hi ${profile.name} , you have successfully created mentoring event request ${eventObject.event_name}`;
          await this.sendPushNotification(
            mentee_message,
            eventObject.mentee_push_token,
            "Open"
          );
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  selectMentor = () => {
    this.props.navigation.navigate("SelectMentorRoute", {
      // When mentee navigates back from "SelectMentor" screen, refresh() will be called
      onGoBack: this.refresh,
      page: "CreateNewEventRoute",
      eventType: "Others",
    });
  };

  // Recieving mentor data from 'SelectMentor' screen
  //  we need to capture the selected mentor
  refresh = async (data) => {
    await this.setState({
      mentor: data,
    });
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="height">
        <ScrollView
          style={styles.scrollViewContainer}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
        >
          {/* Mentoring event name heading */}
          <Text style={styles.titleStyle}>
            Mentoring event name <Text style={{ color: "red" }}>*</Text>
          </Text>

          {/* Text input to enter event name */}
          <TextInput
            placeholder={"Enter event name"}
            style={styles.fieldStyle}
            onChangeText={(name) => {
              this.setState({ eventName: name });
            }}
          ></TextInput>

          {/* Specific learning objective heading */}
          <Text style={styles.titleStyle}>
            Specific learning objective <Text style={{ color: "red" }}>*</Text>
          </Text>

          {/* Text input to enter Specific learning objective */}
          <TextInput
            multiline
            placeholder={
              "Enter specifc details about the topic in which you need mentoring"
            }
            style={styles.fieldLargeStyle}
            onChangeText={(summary) => {
              this.setState({ eventSummary: summary });
            }}
          ></TextInput>
          {/* checkboxes to specify the type of event,whether it is overview event or bootcamp event */}
          <Text style={styles.titleStyle}>Select Event Type</Text>
          <CheckBox
            title="Bootcamp Event"
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checked={this.state.groupeventType}
            onPress={() => {
              this.setState((prev) => {
                return { groupeventType: !prev.groupeventType };
              });
            }}
          />
          <CheckBox
            title="Overview Event"
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checked={!this.state.groupeventType}
            onPress={() => {
              this.setState((prev) => {
                return { groupeventType: !prev.groupeventType };
              });
            }}
          />

          {/* Select mentor heading */}
          <Text style={styles.titleStyle}>Select Mentor</Text>

          {/* Button for selecting mentors */}
          {this.state.mentor == null && (
            <View style={styles.buttonContainer}>
              <Button
                title="Show mentors"
                onPress={() => {
                  this.selectMentor();
                }}
                containerStyle={styles.bigButton}
              />
            </View>
          )}

          {/* Button for changing selected mentor */}
          {this.state.mentor != null && (
            <View style={{ marginBottom: 10 }}>
              <Text style={styles.addedMentorTextStyle}>
                {this.state.mentor.name}
              </Text>
              <View style={{ flexDirection: "row" }}>
                <View style={{ marginEnd: 10 }}>
                  <Button
                    title="Remove"
                    onPress={() => {
                      this.setState({
                        mentor: null,
                      });
                    }}
                    containerStyle={styles.smallButton}
                    buttonStyle={{ backgroundColor: "#90A4AE" }}
                  />
                </View>
                <Button
                  title="Change Mentor"
                  onPress={() => {
                    this.selectMentor();
                  }}
                  containerStyle={styles.bigButton}
                  buttonStyle={{ backgroundColor: "#03A9F4" }}
                />
              </View>
            </View>
          )}

          {/* Google-Class Room Link */}
          <Text style={styles.titleStyle}>
            Google Classroom Link <Text style={{ color: "red" }}>*</Text>
          </Text>

          {/* Text input to enter Google ClassRoom Link */}
          <TextInput
            multiline
            keyboardType="url"
            placeholder={"Provide the Google ClassRoom Link"}
            style={styles.fieldStyle}
            onChangeText={(GoogleClassroom) => {
              this.setState({ google_classroom_link: GoogleClassroom });
            }}
          ></TextInput>
          {/* Google-Class Room code */}
          <Text style={styles.titleStyle}>
            Google Classroom Code <Text style={{ color: "red" }}>*</Text>
          </Text>

          {/* Text input to enter Google ClassRoom Code*/}
          <TextInput
            multiline
            placeholder={"Provide the Google ClassRoom Code"}
            style={styles.fieldStyle}
            onChangeText={(GoogleClassroomCode) => {
              this.setState({ google_classroom_code: GoogleClassroomCode });
            }}
          ></TextInput>

          {/* Pre-work done heading */}
          <Text style={styles.titleStyle}>
            Pre-work done <Text style={{ color: "red" }}>*</Text>
          </Text>

          {/* Text input to enter pre-work */}
          <TextInput
            multiline
            placeholder={"Describe your work or projects on this topic"}
            style={styles.fieldLargeStyle}
            onChangeText={(preWork) => {
              this.setState({ preWorkDone: preWork });
            }}
          ></TextInput>

          {/* Scheduled preferences heading */}
          <Text style={styles.titleStyle}>
            Timings <Text style={{ color: "red" }}>*</Text>
          </Text>

          {/* Text input to enter scheduled preferences */}
          <TextInput
            placeholder={"Ex: 10AM - 12PM IST"}
            style={styles.fieldStyle}
            onChangeText={(schedulePreferences) => {
              this.setState({ schedulePreferences: schedulePreferences });
            }}
          ></TextInput>

          {/* Additional Comments heading */}
          <Text style={styles.titleStyle}>Additional Comments</Text>

          {/* Text input to enter additional comments */}
          <TextInput
            multiline
            placeholder={`Any additional details...\nWrite your friends names if they also need mentoring in the same topic`}
            style={styles.fieldLargeStyle}
            onChangeText={(additional) => {
              this.setState({ additionalDetails: additional });
            }}
          ></TextInput>

          {/* Buttons for submitting the request */}
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <View style={styles.submitContainer}>
              <Button
                title="Cancel"
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                containerStyle={styles.smallButton}
                buttonStyle={{ backgroundColor: "#E57373" }}
              />
            </View>
            <View style={styles.submitContainer}>
              <Button
                title="Submit"
                loading={this.state.sumbitButtonFlag}
                onPress={() => {
                  this.submit();
                }}
                containerStyle={styles.smallButton}
                buttonStyle={{ backgroundColor: "#66BB6A" }}
              />
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  scrollViewContainer: {
    marginHorizontal: 15,
    backgroundColor: "white",
  },
  titleStyle: {
    fontSize: 20,
    fontWeight: "700",
    marginBottom: 10,
  },
  fieldStyle: {
    fontSize: 17,
    padding: 5,
    paddingStart: 13,
    borderWidth: 1,
    marginBottom: 15,
    height: 50,
  },
  addedMentorTextStyle: {
    fontSize: 20,
    marginBottom: 15,
    marginStart: 10,
  },
  fieldLargeStyle: {
    fontSize: 17,
    padding: 5,
    paddingStart: 13,
    borderWidth: 1,
    marginBottom: 15,
    height: 120,
    textAlignVertical: "top",
  },
  buttonContainer: {
    borderRadius: 25,
    alignItems: "flex-start",
    marginBottom: 15,
  },
  submitContainer: {
    borderRadius: 10,
    alignItems: "center",
    width: 120,
    marginBottom: 15,
    marginTop: 5,
    marginEnd: 20,
  },
  buttonText: {
    fontSize: 17,
    padding: 10,
  },
  bigButton: {
    width: Dimensions.get("window").width * 0.5,
  },
  smallButton: {
    width: Dimensions.get("window").width * 0.3,
  },
});
