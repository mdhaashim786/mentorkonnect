import React from "react";
import { StyleSheet, View } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class Feedback extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ratings: 0,
    };
  }

  componentDidMount() {
    this.setState({
      ratings: this.props.ratings,
    });
  }

  render() {
    let stars = [];
    const status = this.props.status;
    // In this case, user needs to select ratings so we are using TouchableOpacity and updates
    // the ratings when he made changes.
    if (status == "Waiting for feedback" || status == "Scheduled") {
      for (let i = 1; i <= 5; i++) {
        if (i <= this.state.ratings) {
          stars.push(
            <TouchableOpacity
              key={i}
              onPress={() => {
                this.setState({
                  ratings: i,
                });
                // Calling the method in parent .i.e. EventDetails for updating ratings
                // whenever user changes ratings
                this.props.updateRating(i);
              }}
            >
              <Icon
                name="ios-star"
                size={50}
                color="#FBC02D"
                style={{ marginEnd: 10 }}
              />
            </TouchableOpacity>
          );
        } else {
          stars.push(
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  ratings: i,
                });
                this.props.updateRating(i);
              }}
            >
              <Icon
                name="ios-star-outline"
                size={50}
                color="#FBC02D"
                style={{ marginEnd: 10 }}
              />
            </TouchableOpacity>
          );
        }
      }
    }
    // When the event is completed, the user cannot make any changes
    else if (status == "Completed") {
      for (let i = 1; i <= 5; i++) {
        if (i <= this.state.ratings) {
          stars.push(
            <Icon
              name="ios-star"
              size={50}
              color="#FBC02D"
              style={{ marginEnd: 10 }}
            />
          );
        } else {
          stars.push(
            <Icon
              name="ios-star-outline"
              size={50}
              color="#FBC02D"
              style={{ marginEnd: 10 }}
            />
          );
        }
      }
    }

    return <View style={styles.container}>{stars}</View>;
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    height: "100%",
    width: "100%",
    flexDirection: "row",
    alignContent: "space-around",
  },
});
