//In License History tab, to display the colleges who have licenses
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Linking,
  Image,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { urls } from "./../../../env.json";

export default class CollegeCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      approveButtonFlag: false,
      disapproveButtonFlag: false,
    };
  }

  handleApprove = async (status, uid, email, name) => {
    const { getData } = this.props;
    let user = "collegeadmin";
    const url =
      urls.handleApprovalsv2 +
      `?user=${user}&status=${status}&uid=${uid}&email=${email}&name=${name}`;
    fetch(url)
      .then(async () => {
        this.setState({
          disapproveButtonFlag: false,
          approveButtonFlag: false,
        });
        await getData();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  sendPushNotification = async (msg, token, status) => {
    const message = {
      to: token,
      sound: "default",
      title: "Mentoring Event",
      body: msg,
      data: { data: status },
      _displayInForeground: true,
    };
    fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Accept-encoding": "gzip, deflate",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(message),
    }).catch((err) => {
      console.log(err);
    });
  };

  render() {
    const { admin, navigation } = this.props;
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          navigation.navigate("LicenseDetails", {
            user: admin,
            navigation: navigation,
            admin: true,
          }); //on pressing mentor card his details are visible.
        }}
      >
        <View
          style={{
            marginTop: 5,
            flexDirection: "column",
            elevation: 1,
            borderRadius: 2,
            borderWidth: 3,
            height: 120,
            backgroundColor: "white",
            flex: 1,
            alignItems: "center",
            justifyContent: "center",

            width: "100%",
          }}
        >
          <View
            style={{
              flexDirection: "row",
              paddingLeft: 15,
              width: Dimensions.get("window").width * 0.9,
            }}
          >
            <Image
              source={{ uri: `${admin.logo_url}` }}
              style={{ height: 70, width: 45 }}
            ></Image>
            <Text
              numberOfLines={2}
              style={{
                marginLeft: 10,
                marginBottom: 3,
                fontSize: 15,
                fontSize: 20,
                fontWeight: "bold",
                color: "black",
                flex:1
              }}
            >
              {admin.college_name}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
