import React from "react";
import {
  StyleSheet,
  View,
  RefreshControl,
  ActivityIndicator,
  Text,
  AsyncStorage,
} from "react-native";
import { FlatList, ScrollView } from "react-native-gesture-handler";
import { SearchBar } from "react-native-elements";
import AdminEventCard from "./AdminEventCard";
import { Avatar } from "react-native-elements";
import { urls } from "./../../../env.json";
import firebase from "firebase";

export default class GroupEvents extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      requestsLoaded: false,
      refreshing: false,
      text: "",
      requestsList: [], //used to store all the events
      requestsOpenList: [], //used to store all the open events
      requestsScheduledList: [], //used to store all  scheduled  events
      requestsRejectedList: [], //used to store all  the Rejected events
      requestsCancelledList: [], //used to store all the Cancelled events
      requestsCompletedList: [], //used to store all the  Completed events
      requestsWaitingForFeedbackList: [], //used to store all the  waiting for feedback events
      profile: null,
    };
  }

  getData = async (value) => {
    AsyncStorage.getItem("profile")
      .then((data) => {
        this.setState({
          profile: JSON.parse(data),
        });
      })
      .catch((error) => console.log(error));
    let response = value;
    await this.setState({
      requestsLoaded: true,
      requestsList: response,
    });
    await this.setState({
      requestsOpenList: this.state.requestsList.filter(
        (item) => item.status == "Open"
      ),
      requestsScheduledList: this.state.requestsList.filter(
        (item) => item.status == "Scheduled"
      ),
      requestsRejectedList: this.state.requestsList.filter(
        (item) => item.status == "Rejected"
      ),
      requestsCancelledList: this.state.requestsList.filter(
        (item) => item.status == "Cancelled"
      ),
      requestsCompletedList: this.state.requestsList.filter(
        (item) => item.status == "Completed"
      ),
      requestsWaitingForFeedbackList: this.state.requestsList.filter(
        (item) => item.status == "Waiting for feedback"
      ),
    });
  };
  listener = async () => {
    var call = (value) => {
      this.getData(value);
    };
    //This listener fetchs all the group events of all colleges
    await firebase
      .database()
      .ref("events/")
      .orderByChild("group_event")
      .equalTo(true)
      .on("value", function (snapshot) {
        let list = [];
        snapshot.forEach((item) => {
          list.push(item.val());
        });

        call(list);
      });
  };

  componentDidMount() {
    this.listener();
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.listener();
      }
    );
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  _onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.listener()
      .then(() => {
        this.setState({ refreshing: false });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.requestsLoaded && this.state.requestsList.length != 0 && (
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />
            }
          >
            {this.state.requestsScheduledList.map((item, index) => {
              return (
                <AdminEventCard
                  event={item}
                  navigation={this.props.navigation}
                  profile={this.state.profile}
                  key={item.event_id}
                  getData={this.getData.bind(this)}
                />
              );
            })}
            {this.state.requestsOpenList.map((item, index) => {
              return (
                <AdminEventCard
                  event={item}
                  navigation={this.props.navigation}
                  profile={this.state.profile}
                  key={item.event_id}
                  getData={this.getData.bind(this)}
                />
              );
            })}
            {this.state.requestsRejectedList.map((item, index) => {
              return (
                <AdminEventCard
                  event={item}
                  navigation={this.props.navigation}
                  profile={this.state.profile}
                  key={item.event_id}
                  getData={this.getData.bind(this)}
                />
              );
            })}
            {this.state.requestsWaitingForFeedbackList.map((item, index) => {
              return (
                <AdminEventCard
                  event={item}
                  navigation={this.props.navigation}
                  profile={this.state.profile}
                  key={item.event_id}
                  getData={this.getData.bind(this)}
                />
              );
            })}

            {this.state.requestsCompletedList.map((item, index) => {
              return (
                <AdminEventCard
                  event={item}
                  navigation={this.props.navigation}
                  profile={this.state.profile}
                  key={item.event_id}
                  getData={this.getData.bind(this)}
                />
              );
            })}
            {this.state.requestsCancelledList.map((item, index) => {
              return (
                <AdminEventCard
                  event={item}
                  navigation={this.props.navigation}
                  profile={this.state.profile}
                  key={item.event_id}
                  getData={this.getData.bind(this)}
                />
              );
            })}
          </ScrollView>
        )}

        {this.state.requestsLoaded && (
          <View style={styles.floatingButtonContainer}>
            <Avatar
              size="medium"
              rounded
              title="+"
              onPress={() => {
                this.props.navigation.navigate("CreateNewEventRoute", {
                  profile: this.state.profile,
                });
              }}
              overlayContainerStyle={{ backgroundColor: "#106CC8" }}
            />
          </View>
        )}

        {this.state.requestsLoaded && this.state.requestsList.length == 0 && (
          <ScrollView
            contentContainerStyle={styles.msgContainer}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />
            }
          >
            <Text>No group events to show</Text>
            <View style={styles.floatingButtonContainer}>
              <Avatar
                size="medium"
                rounded
                title="+"
                onPress={() => {
                  this.props.navigation.navigate("CreateNewEventRoute", {
                    profile: this.state.profile,
                  });
                }}
                overlayContainerStyle={{ backgroundColor: "#106CC8" }}
              />
            </View>
          </ScrollView>
        )}
        {!this.state.requestsLoaded && (
          <View style={styles.msgContainer}>
            <ActivityIndicator size={40} />
          </View>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0,
    backgroundColor: "#fff",
  },
  msgContainer: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  floatingButtonContainer: {
    alignItems: "flex-end",
    position: "absolute",
    right: 20,
    bottom: 20,
    justifyContent: "flex-end",
  },
});
