//In collegeadmin drawer navigator,,To know the license details of he purchased
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  AsyncStorage,
  Linking,
  ImageBackground,
  ActivityIndicator,
  FlatList,
} from "react-native";
import { Icon, Avatar, Button } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import { urls } from "./../../../env.json";
import { Toast } from "native-base";
import LicenseBucketCard from "./LicenseBucketCard";

export default class ProfileDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = { profile: null, loading: false, dupdata: null };
  }

  getData = async () => {
    AsyncStorage.getItem("profile")
      .then(async (data) => {
        return JSON.parse(data);
      })
      .then(async (d) => {
        await this.setState({ profile: d });
        await this.setState({ dupdata: d.license_buckets });
      });
  };

  componentDidMount = async () => {
    await this.getData();
    //
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getData();
      }
    );
  };

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  render() { 
    if (this.state.profile != null) {
      return (
        <ScrollView>
          <View style={styles.imageStyle}>
            <View style={styles.containerStyle}>
              <Text
                style={{ fontSize: 20, fontWeight: "bold", color: "white" }}
              >
                My Details
              </Text>
            </View>
            <View style={styles.imageContainer}>
              <View style={styles.imageViewStyle}>
                <Avatar
                  size={120}
                  rounded
                  source={{ uri: this.state.profile.photo_url }}
                  avatarStyle={{}}
                  icon={{ name: "user", type: "font-awesome" }}
                />
              </View>

              <View style={styles.imageBesideText}>
                <Text style={styles.subTextStyle}>
                  {this.state.profile.name}
                </Text>
                {this.state.profile.roll_number != undefined && (
                  <Text
                    style={{
                      ...styles.subTextStyle,
                      textTransform: "uppercase",
                    }}
                  >
                    {this.state.profile.roll_number}
                  </Text>
                )}
                <Text style={styles.subTextStyle}>
                  {this.state.profile.branch}
                </Text>
                <Text style={styles.subTextStyle}>
                  {this.state.profile.college_name}
                </Text>
                <Text
                  style={{ ...styles.subTextStyle, textTransform: "lowercase" }}
                >
                  {this.state.profile.email}
                </Text>
                <Text style={styles.subTextStyle}>
                  {this.state.profile.phone_number}
                </Text>
              </View>
            </View>
          </View>

          <FlatList
            refreshing={this.state.refreshing}
            data={this.state.dupdata}
            onRefresh={this._onRefresh}
            renderItem={({ item, index }) => {
              console.log(item.name);
              return (
                <LicenseBucketCard
                  bucket={item}
                  bucketno={index}
                  navigation={this.props.navigation}
                  getData={this.getData}
                />
              );
            }}
            keyExtractor={(item, index) => item.email}
          />
        </ScrollView>
      );
    } else {
      return (
        <View style={styles.indicator}>
          <ActivityIndicator size="large"></ActivityIndicator>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  renderContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  subTexttitle: {
    fontSize: 15,
    fontWeight: "bold",
  },
  indicator: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  imageStyle: {
    flex: 1,
    flexDirection: "column",
    height: Dimensions.get("window").height * 0.35,
    backgroundColor: "#A9A9A9",
  },
  insideView: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    borderRadius: 10,
    borderWidth: 2,
    alignItems: "center",
    borderColor: "grey",
  },
  subTextStyle: {
    color: "white",
    fontWeight: "bold",
    marginBottom: 5,
    fontSize: 15,
    textTransform: "capitalize",
  },
  containerStyle: {
    flexDirection: "row",

    height: Dimensions.get("window").height * 0.02,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  subHeading: {
    fontSize: 20,
    fontWeight: "bold",
    marginVertical: 5,
    marginStart: 3,
  },
  about: {
    fontSize: 17,
    marginBottom: 5,
    marginStart: 3,
  },
  imageContainer: {
    height: Dimensions.get("window").height * 0.3,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  details: {
    flexDirection: "column",
    padding: 10,
  },
  imageViewStyle: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: Dimensions.get("window").height * 0.01,
    paddingLeft: 10,
    paddingRight: 20,
  },
  titleStyle: {
    fontSize: 23,
    fontWeight: "bold",
    textTransform: "capitalize",
    marginStart: 3,
  },
  emailStyle: {
    color: "rgba(200,0,0,0.8)",
    fontSize: 15,
  },
  imageBesideText: {
    flex: 2,
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "center",
    marginRight: 10,
  },
});
