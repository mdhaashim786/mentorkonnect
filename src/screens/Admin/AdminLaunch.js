import React from "react";
import {
  StyleSheet,
  View,
  RefreshControl,
  ActivityIndicator,
  Text,
  AsyncStorage,
} from "react-native";
import { FlatList, ScrollView } from "react-native-gesture-handler";
import { SearchBar } from "react-native-elements";
import AdminEventCard from "./AdminEventCard";
import { Toast } from "native-base";
import { urls } from "./../../../env.json";
import firebase from "firebase";

export default class AdminLaunch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      requestsLoaded: false,
      refreshing: false,
      text: "",
      requestsList: [], //used to store all the events
      requestsOpenList: [], //used to store all the open events
      requestsScheduledList: [], //used to store all  scheduled  events
      requestsRejectedList: [], //used to store all  the Rejected events
      requestsCancelledList: [], //used to store all the Cancelled events
      requestsCompletedList: [], //used to store all the  Completed events
      requestsWaitingForFeedbackList: [], //used to store all the  waiting for feedback events
      profile: null,
    };
  }
  getCollegeEvents = async (value) => {
    let response = value;
    await this.setState({
      requestsLoaded: true,
      requestsList: response,
    });
    await this.setState({
      requestsOpenList: this.state.requestsList.filter(
        (item) => item.status == "Open"
      ), //storing Open events
      requestsScheduledList: this.state.requestsList.filter(
        (item) => item.status == "Scheduled"
      ), //storing scheduled events
      requestsRejectedList: this.state.requestsList.filter(
        (item) => item.status == "Rejected"
      ), //storing rejected events
      requestsCancelledList: this.state.requestsList.filter(
        (item) => item.status == "Cancelled"
      ), //storing cancelled events
      requestsCompletedList: this.state.requestsList.filter(
        (item) => item.status == "Completed"
      ), //storing completed events
      requestsWaitingForFeedbackList: this.state.requestsList.filter(
        (item) => item.status == "Waiting for feedback"
      ),
    }); //storing waiting for feedback
    this.setState({
      requestsLoaded: true,
      dataloaded: true,
      refreshing: false,
    });
  };
  getAllEvents = async (value) => {
    let response = value;
    await this.setState({
      requestsLoaded: true,
      requestsList: response,
    });
    await this.setState({
      requestsOpenList: this.state.requestsList.filter(
        (item) => item.status == "Open"
      ), //storing Open events
      requestsScheduledList: this.state.requestsList.filter(
        (item) => item.status == "Scheduled"
      ), //storing scheduled events
      requestsRejectedList: this.state.requestsList.filter(
        (item) => item.status == "Rejected"
      ), //storing rejected events
      requestsCancelledList: this.state.requestsList.filter(
        (item) => item.status == "Cancelled"
      ), //storing cancelled events
      requestsCompletedList: this.state.requestsList.filter(
        (item) => item.status == "Completed"
      ), //storing completed events
      requestsWaitingForFeedbackList: this.state.requestsList.filter(
        (item) => item.status == "Waiting for feedback"
      ),
    }); //storing waiting for feedback
    this.setState({
      requestsLoaded: true,
      dataloaded: true,
      refreshing: false,
    });
  };

  getData = async () => {
    AsyncStorage.getItem("profile")
      .then((d) => {
        return JSON.parse(d);
      })
      .then(async (d) => {
        this.setState({ profile: d });

        if (d.user_type === "college_admin") {
          let college = d.college_id;

          if (d.authorized == true) {
            var call = (value) => {
              this.getCollegeEvents(value);
            };
            //This listener fetchs the specific college events of their college only
            await firebase
              .database()
              .ref("events/")
              .orderByChild("college_id")
              .equalTo(college)
              .on("value", function (snapshot) {
                let list = [];
                snapshot.forEach((item) => {
                  if (
                    item.val().group_event === false &&
                    item.val().institution_id === d.institution_id
                  ) {
                    list.push(item.val());
                  }
                });
                list.filter((item) => {
                  return item.group_event === false;
                });
                call(list);
              });
          } else {
            Toast.show({
              text:
                "You can only perform tasks when you are activated by Mentoring association",
              buttonText: "Okay",
            });
            this.setState({
              requestsLoaded: true,
              dataloaded: true,
              refreshing: false,
            });
          }
        } else {
          var call = (value) => {
            this.getAllEvents(value);
          };
          //This listener fetchs the all events
          await firebase
            .database()
            .ref("events/")
            .orderByChild("group_event")
            .equalTo(false)
            .on("value", function (snapshot) {
              let list = [];
              snapshot.forEach((item) => {
                list.push(item.val());
              });

              call(list);
            });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  listener = async () => {
    await AsyncStorage.getItem("profile")
      .then((data) => {
        return JSON.parse(data);
      })
      .then(async (data) => {
        if (data.user_type === "college_admin") {
          let id = data.uid;
          var callProfile = () => {
            this.getData();
          };
          //This listener monitors the college admin profile , if any change in the college admin profile . It sets in the  Async storage
          await firebase
            .database()
            .ref("collegeadmins/" + id)
            .on("value", function (snapshot) {
              let value = snapshot.val();

              AsyncStorage.setItem("profile", JSON.stringify(value)).then(
                () => {
                  callProfile();
                }
              );
            });
        } else {
          this.getData();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  componentDidMount() {
    this.listener();
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.listener();
      }
    );
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }
  //On pulling from the top this function is called
  _onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.listener()
      .then(() => {
        this.setState({ refreshing: false });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.requestsLoaded &&
          this.state.requestsList.length != 0 &&
          ((this.state.profile!=null&&
            this.state.profile.user_type == "college_admin" &&
            this.state.profile.authorized == true) ||
            (this.state.profile==null ||this.state.profile.user_type != "college_admin")) && (
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh.bind(this)}
                />
              }
            >
              {/* Displaying all events */}
              {/*Scheduled */}
              {this.state.requestsScheduledList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}
              {/*Open */}
              {this.state.requestsOpenList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}
              {/*Rejected */}
              {this.state.requestsRejectedList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}
              {/*waiting for feedback */}
              {this.state.requestsWaitingForFeedbackList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}

              {/*Completed */}
              {this.state.requestsCompletedList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}
              {/*cancelled*/}
              {this.state.requestsCancelledList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}
            </ScrollView>
          )}

        {/* When there are no requests */}
        {this.state.requestsLoaded && this.state.requestsList.length == 0 && (
          <ScrollView
            contentContainerStyle={styles.msgContainer}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />
            }
          >
            <Text>No events to show</Text>
          </ScrollView>
        )}
        
        {!this.state.requestsLoaded && (
          <View style={styles.msgContainer}>
            <ActivityIndicator size={40} />
          </View>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0,
    backgroundColor: "#fff",
  },
  msgContainer: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
});
