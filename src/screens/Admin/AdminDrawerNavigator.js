import React, { Component } from "react";
import {
  View,
  StyleSheet,
  AsyncStorage,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Text,
  Alert,
} from "react-native";
import { NavigationActions } from "react-navigation";
import { Avatar, ListItem } from "react-native-elements";
import Constants from "expo-constants";
import firebase from "firebase";

let listToReder = [
  { Home: "Events", icon: "home", title: "Home" },

  { Home: "AddAlumniPage", icon: "user", title: "Add Alumni" },
  { Home: "AdminMentorPage", icon: "", title: "Review Mentors" },
  { Home: "AdminMenteePage", icon: "", title: "Review Mentees" },
];

export default class DrawerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      image:
        "https://firebasestorage.googleapis.com/v0/b/collegekonnect-app.appspot.com/o/profile_pic.png?alt=media&token=4152469c-0a75-4704-9da8-bd948944e278",
    };
  }
  navigateToScreen = (route) => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    this.props.navigation.dispatch(navigateAction);
  };

  componentDidMount() {
    console.log("componentdidmount");

    //getting details from storage and setting details of photo url.
    AsyncStorage.getItem("profile")
      .then((d) => {
        return JSON.parse(d);
      })
      .then((d) => {
        this.setState({ image: d.photo_url });
        this.setState({ user: d });
      })
      .catch((err) => console.log(err));
  }

  keyExtractor = (item, index) => index.toString();

  signout = async () => {
    firebase.auth().signOut();
    AsyncStorage.setItem("signout", "true")
      .then((d) => {
        this.props.navigation.navigate("AuthScreen");
      })
      .catch((err) => console.log(err));
  };

  render() {
    return (
      <ScrollView style={styles.container} stickyHeaderIndices={[0]}>
        <View style={styles.headerContainer}>
          <ImageBackground
            source={require("../../../assets/sideBarBackground.png")}
            style={styles.profilepic}
          >
            <Text style={styles.titleStyle}>MentorKonnect</Text>
            <View style={styles.viewStyle}>
              <Avatar
                onPress={() => {}}
                editButton={{
                  name: "mode-edit",
                  type: "material",
                  color: "#fff",
                  underlayColor: "#0f0",
                }}
                size={Dimensions.get("window").width * 0.35}
                rounded
                source={{ uri: this.state.image }}
                avatarStyle={{}}
                icon={{ name: "user", type: "font-awesome" }}
              />
            </View>
            <Text style={styles.headerText}>{}</Text>
          </ImageBackground>
        </View>
        <ScrollView>
          {listToReder.map((item, index) => {
            return (
              <ListItem
                key={index}
                title={item.title}
                leftAvatar={
                  <Avatar
                    size="small"
                    icon={{ name: item.icon, type: "font-awesome" }}
                    rounded
                    activeOpacity={0.7}
                  />
                }
                containerStyle={{}}
                chevron
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.navigateToScreen(item.Home);
                }}
              />
            );
          })}
          {/* This drawer navigation section is for super admin */}
          {this.state.user.user_type != "college_admin" && (
            <View>
              <ListItem
                key={6}
                title={"Group Events"}
                leftAvatar={
                  <Avatar
                    size="small"
                    icon={{ name: "plus", type: "font-awesome" }}
                    rounded
                    activeOpacity={0.7}
                  />
                }
                containerStyle={{}}
                chevron
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.navigateToScreen("GroupEvents");
                }}
              />
              <ListItem
                key={7}
                title={"Add Admin"}
                leftAvatar={
                  <Avatar
                    size="small"
                    icon={{ name: "plus", type: "font-awesome" }}
                    rounded
                    activeOpacity={0.7}
                  />
                }
                containerStyle={{}}
                chevron
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.navigateToScreen("AddAdminPage");
                }}
              />
              <ListItem
                key={8}
                title={"Add College Admin"}
                leftAvatar={
                  <Avatar
                    size="small"
                    icon={{ name: "plus", type: "font-awesome" }}
                    rounded
                    activeOpacity={0.7}
                  />
                }
                containerStyle={{}}
                chevron
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.navigateToScreen("AddCollegeAdminPage");
                }}
              />
              <ListItem
                key={9}
                title={"Review Colleges"}
                leftAvatar={
                  <Avatar
                    size="small"
                    icon={{ name: "user", type: "font-awesome" }}
                    rounded
                    activeOpacity={0.7}
                  />
                }
                containerStyle={{}}
                chevron
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.navigateToScreen("ReviewColleges");
                }}
              />
              <ListItem
                key={10}
                title={"Signout"}
                leftAvatar={
                  <Avatar
                    size="small"
                    icon={{ name: "sign-out", type: "font-awesome" }}
                    rounded
                    activeOpacity={0.7}
                  />
                }
                containerStyle={{ paddingBottom: 50 }}
                chevron
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  Alert.alert(
                    "Sign out",
                    "Are you sure you want to signout?",
                    [
                      {
                        text: "Cancel",
                        style: "cancel",
                      },
                      { text: "OK", onPress: () => this.signout() },
                    ],
                    { cancelable: false }
                  );
                }}
              ></ListItem>
            </View>
          )}
          {/* This drawer navigation section is for college admin */}

          {this.state.user.user_type == "college_admin" && (
            <View>
              <ListItem
                key={6}
                title={"Group Events"}
                leftAvatar={
                  <Avatar
                    size="small"
                    icon={{ name: "plus", type: "font-awesome" }}
                    rounded
                    activeOpacity={0.7}
                  />
                }
                containerStyle={{}}
                chevron
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.navigateToScreen("CollegeGroupEvents");
                }}
              />
              <ListItem
                key={7}
                title={"Activate Profile"}
                leftAvatar={
                  <Avatar
                    size="small"
                    icon={{ name: "check", type: "font-awesome" }}
                    rounded
                    activeOpacity={0.7}
                  />
                }
                containerStyle={{}}
                chevron
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.navigateToScreen("AdminActivatePage");
                }}
              />
              <ListItem
                key={8}
                title={"My Licenses"}
                leftAvatar={
                  <Avatar
                    size="small"
                    icon={{ name: "check", type: "font-awesome" }}
                    rounded
                    activeOpacity={0.7}
                  />
                }
                containerStyle={{}}
                chevron
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  this.navigateToScreen("MyLicenses");
                }}
              />
              <ListItem
                key={9}
                title={"Signout"}
                leftAvatar={
                  <Avatar
                    size="small"
                    icon={{ name: "sign-out", type: "font-awesome" }}
                    rounded
                    activeOpacity={0.7}
                  />
                }
                containerStyle={{ paddingBottom: 50 }}
                chevron
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                  Alert.alert(
                    "Sign out",
                    "Are you sure you want to signout?",
                    [
                      {
                        text: "Cancel",
                        style: "cancel",
                      },
                      { text: "OK", onPress: () => this.signout() },
                    ],
                    { cancelable: false }
                  );
                }}
              ></ListItem>
            </View>
          )}
        </ScrollView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  titleStyle: {
    fontSize: 25,
    color: "orange",
    fontWeight: "bold",
    margin: 10,
    marginTop: 0,
  },
  profilepic: {
    paddingTop: 15,
    alignItems: "center",
    height: Dimensions.get("window").height * 0.3,
  },
  container: {
    paddingTop: Constants.statusBarHeight,
    flex: 1,
    width: Dimensions.get("window").width * 0.7,
    flexDirection: "column",
  },
  headerText: {
    textTransform: "capitalize",
    fontSize: 30,
    fontWeight: "bold",
    color: "orange",
  },
  headerContainer: {
    flex: 1,
    flexDirection: "column",
    alignContent: "center",
  },
});
