import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Keyboard,
  Image,
  TextInput,
  KeyboardAvoidingView,
  Dimensions,
  ActivityIndicator,
  Platform,
  AsyncStorage,
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { Toast } from "native-base";
import * as firebase from "firebase";
import Logos from "../../../assets/Logos";
import { urls } from "./../../../env.json";

export default class AddAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: false, email: "" };
  }

  reviewDetails = () => {
    Keyboard.dismiss();
    this.setState({ loading: true });
    //checking the auth details.
    if (this.state.email.trim().length == 0) {
      Toast.show({ text: "Enter email id", buttonText: "Okay" });
    }

    fetch(urls.updateRole, {
      method: "POST",
      body: JSON.stringify({ email: this.state.email }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        Toast.show({ text: res.status, buttonText: "Okay" });
        this.setState({ email: "", loading: false });
      })
      .catch((err) => {
        this.setState({ email: "", loading: false });
        console.log(err);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView
          behavior={Platform.Os == "ios" ? "padding" : "height"}
          style={styles.keyboardView}
        >
          <ScrollView keyboardShouldPersistTaps="handled">
            <View style={styles.logoContainer}>
              <Image
                style={styles.imageStyle}
                source={{ uri: Logos.jntukLogo }}
              />
              <Text style={styles.titleStyle}> Welcome Admin</Text>
            </View>
            <View style={styles.detailsContainer}>
              <Text style={styles.subtitle}>
                Email Id <Text style={{ color: "red" }}>*</Text>
              </Text>
              <TextInput
                selectTextOnFocus={true}
                autoCapitalize="none"
                placeholder="Email"
                underlineColorAndroid="transparent"
                style={styles.TextInputStyle}
                value={this.state.email}
                onChangeText={(text) => this.setState({ email: text })}
              />
            </View>
            <View style={styles.grantButtonView}>
              <Button
                icon={
                  <Icon
                    type="font-awesome"
                    name="user-plus"
                    size={20}
                    color="white"
                  />
                }
                containerStyle={styles.grantButtonContainer}
                buttonStyle={styles.grantButtonStyle}
                onPress={() => {
                  this.setState({ loading: false });
                  this.reviewDetails();
                }}
                titleStyle={styles.buttonText}
                title="Grant Admin Privileges"
              />
              {this.state.loading && <ActivityIndicator size="large" />}
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonText: { padding: 5 },
  grantButtonStyle: {
    borderRadius: 30,
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    width: Dimensions.get("window").width * 0.7,
  },
  grantButtonContainer: { padding: 0 },
  grantButtonView: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: 10,
  },
  imageStyle: {
    height: 120,
    width: 120,
  },
  keyboardView: {
    flex: 1,
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "bold",
  },
  detailsContainer: {
    flexDirection: "column",
    justifyContent: "center",
    margin: 30,
    padding: 30,
    alignContent: "center",
    borderWidth: 0.5,
    borderRadius: 2,
  },
  TextInputStyle: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.7,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
  },
  logoContainer: {
    marginTop: 50,
    alignItems: "center",
  },
  titleStyle: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  logoStyle: {
    borderRadius: 5,
    height: 175,
    width: 300,
  },
});
