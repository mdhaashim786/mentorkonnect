import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Keyboard,
  Image,
  TextInput,
  KeyboardAvoidingView,
  Dimensions,
  ActivityIndicator,
  Platform,
  AsyncStorage,
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import { Toast } from "native-base";
import * as firebase from "firebase";
import Logos from "../../../assets/Logos";
import { urls } from "./../../../env.json";

export default class AddAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      institutionsList: [],
      loading: false,
      email: "",
      institutionName: "None",
      collegeName: "None",
      menteecount: null,
      license_bucket: [],
    };
  }
  async componentDidMount() {
    fetch(urls.getInstitutions)
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        let d = Array.from(res);
        this.setState({ institutionsList: d });
      })
      .catch((error) => {
        console.log(error);
        Toast.show({ text: "Unable to show institutions", buttonText: "Okay" });
      });
  }
  reviewDetails = () => {
    Keyboard.dismiss();
    this.setState({ loading: true });
    //checking the auth details.
    if (this.state.email.trim().length == 0) {
      Toast.show({ text: "Enter email id", buttonText: "Okay" });
    }
    if (this.state.institutionName == "None") {
      Toast.show({ text: "Enter institutionName", buttonText: "Okay" });
    } else {
      if (this.state.collegeName == "None") {
        Toast.show({ text: "Enter collegeName", buttonText: "Okay" });
      }
    }
    if (this.state.menteecount == null) {
      Toast.show({ text: "Enter no of mentees", buttonText: "Okay" });
    }
    if (this.state.collegesList.find((x) => x.name == this.state.collegeName)) {
      var collegeId = this.state.collegesList.find(
        (x) => x.name == this.state.collegeName
      ).id;
    }
    if (
      this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      )
    ) {
      var institutionId = this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      ).id;
    }
    //to push the license bucket details
    let bucket = {
      days_left_to_expire: 365,
      no_of_mentees_present: 0,
      created_date: Date().toLocaleString(),
      license_status: true,
      mentee_limit: this.state.menteecount,
      bucket_id: 0,
    };
    this.state.license_bucket.push(bucket);
    console.log("data", this.state.license_bucket[0].license_status);
    //
    let clgadmin = {
      email: this.state.email,
      institute: institutionId,
      college: collegeId,
      license_code: " ",
      license_buckets: this.state.license_bucket,
    };
    //To add license details in the institutions branch of db
    fetch(urls.addLicenseAdmin, {
      method: "POST",
      body: JSON.stringify(clgadmin),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then(() => {
        /*1. This function adds a dummy data in the collegeadmin section of database, whenever a license is created for a college for the first time
          2. When we add license for a college from the 2nd time the license details will be pushed into collegeadmins profile  */
        fetch(urls.addLicenseAdminReference, {
          method: "POST",
          body: JSON.stringify(clgadmin),
          headers: {
            "Content-type": "application/json; charset=UTF-8",
          },
        })
          .then((res) => {
            return res.json();
          })
          .then((res) => {
            if (res.status == true) {
              Toast.show({ text: "License added", buttonText: "Okay" });
              this.setState({
                loading: false,
                email: "",
                institutionName: "None",
                collegeName: "None",
                menteecount: null,
                license_bucket: [],
              });
            } else {
              Toast.show({
                text: "Something error occured",
                buttonText: "Okay",
              });
            }
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView
          behavior={Platform.Os == "ios" ? "padding" : "height"}
          style={styles.keyboardView}
        >
          <ScrollView keyboardShouldPersistTaps="handled">
            <View style={styles.logoContainer}>
              <Image
                style={styles.imageStyle}
                source={{ uri: Logos.mentorKonnectIcon }}
              />
              <Text style={styles.titleStyle}> Add College Admin</Text>
            </View>
            <View style={styles.detailsContainer}>
              <Text style={styles.subtitle}>
                Email Id <Text style={{ color: "red" }}>*</Text>
              </Text>
              <TextInput
                selectTextOnFocus={true}
                autoCapitalize="none"
                placeholder="Email"
                underlineColorAndroid="transparent"
                style={styles.TextInputStyle}
                value={this.state.email}
                onChangeText={(text) => this.setState({ email: text })}
              />
              <Text style={styles.subtitle}>
                Institution <Text style={{ color: "red" }}>*</Text>
              </Text>
              <Dropdown
                itemCount={6}
                labelTextStyle={styles.dropDownStyle}
                animationDuration={0}
                data={this.state.institutionsList}
                value={this.state.institutionName}
                valueExtractor={(item, index) => {
                  return item.name;
                }}
                onChangeText={async (v, i, d) => {
                  await this.setState({ institutionName: v });
                  await this.setState({
                    collegeName: "None",
                  });
                  await this.setState({
                    collegesList: this.state.institutionsList.find(
                      (x) => x.name == this.state.institutionName
                    )
                      ? this.state.institutionsList.find(
                          (x) => x.name == this.state.institutionName
                        ).colleges
                      : [],
                  });
                }}
              />
              <Text style={styles.subtitle}>
                College <Text style={{ color: "red" }}>*</Text>
              </Text>
              <Dropdown
                itemCount={6}
                animationDuration={0}
                dropdownPosition={3}
                labelTextStyle={styles.dropDownStyle}
                data={this.state.collegesList}
                value={this.state.collegeName}
                valueExtractor={(item, index) => {
                  return item.name;
                }}
                onChangeText={async (v, i, d) => {
                  await this.setState({ collegeName: v });
                  await this.setState({ course: "None", branch: "None" });
                  await this.setState({
                    coursesList: this.state.collegesList.find(
                      (x) => x.name == this.state.collegeName
                    )
                      ? this.state.collegesList.find(
                          (x) => x.name == this.state.collegeName
                        ).courses
                      : [],
                  });
                  // console.log(this.state.coursesList);
                  await this.setState({
                    branchesList: this.state.coursesList.find(
                      (x) => x.course == this.state.course
                    )
                      ? this.state.coursesList.find(
                          (x) => x.course == this.state.course
                        ).branches
                      : [],
                  });
                }}
              />
              <Text style={styles.subtitle}>
                No of Mentees <Text style={{ color: "red" }}>*</Text>
              </Text>
              <TextInput
                selectTextOnFocus={true}
                autoCapitalize="none"
                placeholder="No of Mentees"
                underlineColorAndroid="transparent"
                style={styles.TextInputStyle}
                value={this.state.menteecount}
                onChangeText={(text) => this.setState({ menteecount: text })}
              />
            </View>
            <View style={styles.grantButtonView}>
              <Button
                icon={
                  <Icon
                    type="font-awesome"
                    name="user-plus"
                    size={20}
                    color="white"
                  />
                }
                containerStyle={styles.grantButtonContainer}
                buttonStyle={styles.grantButtonStyle}
                onPress={() => {
                  this.setState({ loading: false });
                  this.reviewDetails();
                }}
                titleStyle={styles.buttonText}
                title="Create License"
              />
              {this.state.loading && <ActivityIndicator size="large" />}
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonText: { padding: 5 },
  grantButtonStyle: {
    borderRadius: 30,
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    width: Dimensions.get("window").width * 0.7,
  },
  grantButtonContainer: { padding: 0 },
  grantButtonView: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: 10,
  },
  imageStyle: {
    height: 120,
    width: 120,
  },
  keyboardView: {
    flex: 1,
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "bold",
  },
  detailsContainer: {
    flexDirection: "column",
    justifyContent: "center",
    margin: 30,
    padding: 30,
    alignContent: "center",
    borderWidth: 0.5,
    borderRadius: 2,
  },
  TextInputStyle: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.7,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 10,
  },
  logoContainer: {
    marginTop: 20,
    alignItems: "center",
  },
  titleStyle: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  logoStyle: {
    borderRadius: 5,
    height: 175,
    width: 300,
  },
  dropDownStyle: {
    paddingBottom: 5,
    fontWeight: "bold",
  },
});
