//This page is for collegeadmin group events
import React from "react";
import {
  StyleSheet,
  View,
  RefreshControl,
  ActivityIndicator,
  Text,
  AsyncStorage,
} from "react-native";
import { FlatList, ScrollView } from "react-native-gesture-handler";
import { SearchBar } from "react-native-elements";
import { Toast } from "native-base";
import AdminEventCard from "./AdminEventCard";
import { Avatar } from "react-native-elements";
import { urls } from "./../../../env.json";
import firebase from "firebase";

export default class CollegeGroupEvents extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      requestsLoaded: false,
      refreshing: false,
      text: "",
      requestsList: [], //used to store all the events
      requestsOpenList: [], //used to store all the open events
      requestsScheduledList: [], //used to store all  scheduled  events
      requestsRejectedList: [], //used to store all  the Rejected events
      requestsCancelledList: [], //used to store all the Cancelled events
      requestsCompletedList: [], //used to store all the  Completed events
      requestsWaitingForFeedbackList: [], //used to store all the  waiting for feedback events
      profile: null,
    };
  }
  getCollegeGroupEvents = async (value) => {
    let response = value;
    await this.setState({
      requestsLoaded: true,
      requestsList: response,
    });
    await this.setState({
      requestsOpenList: this.state.requestsList.filter(
        (item) => item.status == "Open"
      ),
      requestsScheduledList: this.state.requestsList.filter(
        (item) => item.status == "Scheduled"
      ),
      requestsRejectedList: this.state.requestsList.filter(
        (item) => item.status == "Rejected"
      ),
      requestsCancelledList: this.state.requestsList.filter(
        (item) => item.status == "Cancelled"
      ),
      requestsCompletedList: this.state.requestsList.filter(
        (item) => item.status == "Completed"
      ),
      requestsWaitingForFeedbackList: this.state.requestsList.filter(
        (item) => item.status == "Waiting for feedback"
      ),
    });
  };

  getData = async () => {
    AsyncStorage.getItem("profile")
      .then(async (data) => {
        return JSON.parse(data);
      })
      .then(async (d) => {
        await this.setState({
          profile: d,
        });
        if (d.authorized == true) {
          let college = d.college_id;

          var call = (value) => {
            this.getCollegeGroupEvents(value);
          };

          await firebase
            .database()
            .ref("events/")
            .orderByChild("college_id")
            .equalTo(college)
            .on("value", function (snapshot) {
              let list = [];
              snapshot.forEach((item) => {
                if (
                  item.val().group_event === true &&
                  item.val().institution_id === d.institution_id
                ) {
                  list.push(item.val());
                }
              });
              list.filter((item) => {
                return item.group_event === false;
              });
              call(list);
            });
        } else {
          Toast.show({
            text:
              "You can only perform tasks when you activate your profile with your given license code.",
            buttonText: "Okay",
          });
          this.setState({
            requestsLoaded: true,
            dataloaded: true,
            refreshing: false,
          });
        }
      })
      .catch((error) => console.log(error));
  };

  componentDidMount() {
    this.getData();
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getData();
      }
    );
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  _onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.getData()
      .then(() => {
        this.setState({ refreshing: false });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    if (this.state.requestsLoaded && this.state.profile.authorized) {
      return (
        <View style={styles.container}>
          {this.state.requestsLoaded && this.state.requestsList.length != 0 && (
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh.bind(this)}
                />
              }
            >
              {this.state.requestsScheduledList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}
              {this.state.requestsOpenList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}
              {this.state.requestsRejectedList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}
              {this.state.requestsWaitingForFeedbackList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}

              {this.state.requestsCompletedList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}
              {this.state.requestsCancelledList.map((item, index) => {
                return (
                  <AdminEventCard
                    event={item}
                    navigation={this.props.navigation}
                    profile={this.state.profile}
                    key={item.event_id}
                    getData={this.getData.bind(this)}
                  />
                );
              })}
            </ScrollView>
          )}

          {this.state.requestsLoaded && (
            <View style={styles.floatingButtonContainer}>
              <Avatar
                size="medium"
                rounded
                title="+"
                onPress={() => {
                  this.props.navigation.navigate("CreateNewEventRoute", {
                    profile: this.state.profile,
                  });
                }}
                overlayContainerStyle={{ backgroundColor: "#106CC8" }}
              />
            </View>
          )}

          {this.state.requestsLoaded && this.state.requestsList.length == 0 && (
            <ScrollView
              contentContainerStyle={styles.msgContainer}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh.bind(this)}
                />
              }
            >
              <Text>No group events to show</Text>

              <View style={styles.floatingButtonContainer}>
                <Avatar
                  size="medium"
                  rounded
                  title="+"
                  onPress={() => {
                    this.props.navigation.navigate("CreateNewEventRoute", {
                      profile: this.state.profile,
                    });
                  }}
                  overlayContainerStyle={{ backgroundColor: "#106CC8" }}
                />
              </View>
            </ScrollView>
          )}
        </View>
      );
    } else if (this.state.requestsLoaded) {
      return (
        <View style={styles.msgContainer}>
          <Text>
            You can only perform tasks when you are activated by Mentoring association.
          </Text>
        </View>
      );
    } else {
      return (
        <View style={styles.msgContainer}>
          <ActivityIndicator size={40} />
        </View>
      );
    }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0,
    backgroundColor: "#fff",
  },
  msgContainer: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  floatingButtonContainer: {
    alignItems: "flex-end",
    position: "absolute",
    right: 20,
    bottom: 20,
    justifyContent: "flex-end",
  },
});
