import React from "react";
import {
  StyleSheet,
  ActivityIndicator,
  Text,
  View,
  ImageBackground,
  Image,
  FlatList,
  Button,
  AsyncStorage,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { SearchBar } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import { AntDesign, FontAwesome } from "@expo/vector-icons";
import { Toast } from "native-base";
import MentorCard from "./AdminMentorCard";
import { urls } from "./../../../env.json";

export default class CollegeMentors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      dataloaded: true,
      refreshing: false,
      institutionName: "None",
      collegeName: "None",
      institutionsList: [],
      toggle: false,
      //
    };
  }
  SearchFilterFunction(text) {
    const newData = this.state.data.filter(function (item) {
      const itemData = item.name ? item.name.toUpperCase() : "".toUpperCase(); //checking if the search string is substring of any of the names of mentors.
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      search: text,
      dupdata: newData,
    });
  }
  getData = async () => {
    if (this.state.collegesList.find((x) => x.name == this.state.collegeName)) {
      var collegeId = this.state.collegesList.find(
        (x) => x.name == this.state.collegeName
      ).id;
    }
    if (
      this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      )
    ) {
      var institutionId = this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      ).id;
    }
    let obj = { college: collegeId, institute: institutionId };
    fetch(urls.getCollegeAlumni, {
      method: "POST",
      body: JSON.stringify(obj),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    }) //fetching all the alumni data.
      .then((response) => {
        let d = response.json();
        return d;
      })
      .then((responseJSON) => {
        let d = Array.from(responseJSON);
        this.setState({
          data: d,
          dataloaded: true,
          dupdata: d,
          refreshing: false,
        });
        console.log("list", this.state.dupdata);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        fetch(urls.getInstitutions)
          .then((res) => {
            return res.json();
          })
          .then((res) => {
            let d = Array.from(res);
            this.setState({ institutionsList: d });
          })
          .catch((error) => {
            console.log(error);
            Toast.show({
              text: "Unable to show institutions",
              buttonText: "Okay",
            });
          });
      }
    );
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }
  render() {
    if (this.state.dataloaded) {
      return (
        <View style={styles.container}>
          {this.state.toggle == true && (
            <View style={{ borderColor: "black", borderWidth: 1 }}>
              <TouchableOpacity
                onPress={() => {
                  this.getData();
                  if (this.state.toggle == false) {
                    this.setState({ toggle: true });
                  } else {
                    this.setState({ toggle: false });
                  }
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    borderBottomColor: "black",
                    borderWidth: 1,
                    margin: 4,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: "bold",
                      marginLeft: 10,
                      marginTop: 5,
                    }}
                  >
                    Please select College
                  </Text>
                  <AntDesign
                    name="up"
                    size={20}
                    color="black"
                    style={{
                      marginTop: 5,
                      marginBottom: 10,
                      marginLeft: Dimensions.get("window").width * 0.33,
                    }}
                  />
                </View>
              </TouchableOpacity>
              <View
                style={{
                  width: Dimensions.get("window").width * 0.9,
                  marginLeft: Dimensions.get("window").width * 0.04,
                }}
              >
                <Text style={{ ...styles.subtitle, marginTop: 10 }}>
                  Institution <Text style={{ color: "red" }}>*</Text>
                </Text>
                <Dropdown
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  animationDuration={0}
                  data={this.state.institutionsList}
                  value={this.state.institutionName}
                  valueExtractor={(item, index) => {
                    return item.name;
                  }}
                  onChangeText={async (v, i, d) => {
                    await this.setState({ institutionName: v });
                    await this.setState({
                      collegeName: "None",
                    });
                    await this.setState({
                      collegesList: this.state.institutionsList.find(
                        (x) => x.name == this.state.institutionName
                      )
                        ? this.state.institutionsList.find(
                            (x) => x.name == this.state.institutionName
                          ).colleges
                        : [],
                    });
                  }}
                />
                <Text style={{ ...styles.subtitle, marginTop: 10 }}>
                  College <Text style={{ color: "red" }}>*</Text>
                </Text>
                <Dropdown
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.collegesList}
                  value={this.state.collegeName}
                  valueExtractor={(item, index) => {
                    return item.name;
                  }}
                  onChangeText={async (v, i, d) => {
                    await this.setState({ collegeName: v });
                    await this.setState({ course: "None", branch: "None" });
                    await this.setState({
                      coursesList: this.state.collegesList.find(
                        (x) => x.name == this.state.collegeName
                      )
                        ? this.state.collegesList.find(
                            (x) => x.name == this.state.collegeName
                          ).courses
                        : [],
                    });
                  }}
                />
              </View>
              <View
                style={{
                  width: 160,
                  marginLeft: Dimensions.get("window").width * 0.5 - 80,
                  marginTop: 10,
                  marginBottom: 10,
                }}
              >
                <Button
                  title="Show Mentors"
                  buttonStyle={{
                    alignItems: "center",
                    justifyContent: "center",
                    width: 50,
                  }}
                  containerStyle={styles.buttonContainer}
                  onPress={() => {
                    if (this.state.institutionName === "None") {
                      Toast.show({
                        text: "Please select instituion",
                        buttonText: "Okay",
                      });
                    } else {
                      if (this.state.collegeName === "None") {
                        Toast.show({
                          text: "Please select College",
                          buttonText: "Okay",
                        });
                      } else {
                        this.getData();
                        this.setState({ dataloaded: false });
                        if (this.state.toggle == false) {
                          this.setState({ toggle: true });
                        } else {
                          this.setState({ toggle: false });
                        }
                      }
                    }
                  }}
                />
              </View>
            </View>
          )}
          {this.state.toggle == false && this.state.collegeName == "None" && (
            <TouchableOpacity
              onPress={() => {
                this.getData();
                if (this.state.toggle == false) {
                  this.setState({
                    toggle: true,
                    institutionName: "None",
                    collegeName: "None",
                  });
                } else {
                  this.setState({ toggle: false });
                }
              }}
            >
              <View
                style={{
                  height: 40,
                  borderColor: "black",
                  borderWidth: 1,
                  flexDirection: "row",
                }}
              >
                {this.state.collegeName == "None" && (
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: "bold",
                      marginLeft: 10,
                      marginTop: 5,
                    }}
                  >
                    Please Select College
                  </Text>
                )}
                <View
                  style={{
                    justifyContent: "flex-end",
                    marginLeft: Dimensions.get("window").width * 0.35,
                  }}
                >
                  <AntDesign
                    name="down"
                    size={20}
                    color="black"
                    style={{ marginTop: 5, marginBottom: 10 }}
                  />
                </View>
              </View>
            </TouchableOpacity>
          )}
          {this.state.toggle == false && this.state.collegeName != "None" && (
            <TouchableOpacity
              onPress={() => {
                if (this.state.toggle == false) {
                  this.setState({
                    toggle: true,
                    institutionName: "None",
                    collegeName: "None",
                  });
                } else {
                  this.setState({ toggle: false });
                }
              }}
            >
              <View
                style={{
                  height: 40,
                  borderColor: "black",
                  borderWidth: 1,
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: "bold",
                    marginLeft: 10,
                    marginTop: 5,
                  }}
                >
                  {this.state.collegeName}
                </Text>
                <View style={{ justifyContent: "flex-end", marginRight: 10 }}>
                  <AntDesign
                    name="down"
                    size={20}
                    color="black"
                    style={{ marginTop: 5, marginBottom: 10 }}
                  />
                </View>
              </View>
            </TouchableOpacity>
          )}
          <SearchBar
            round
            inputContainerStyle={styles.searchContainer}
            showLoadingIcon={true}
            inputStyle={styles.searchinput}
            containerStyle={styles.searchSubContainer}
            searchIcon={{ size: 20 }}
            onChangeText={(text) => this.SearchFilterFunction(text)}
            onClear={(text) => this.SearchFilterFunction("")}
            placeholder="Enter mentor name to search..."
            value={this.state.search}
          ></SearchBar>
          <FlatList
            refreshing={this.state.refreshing}
            data={this.state.dupdata}
            onRefresh={this._onRefresh}
            renderItem={({ item }) => {
              console.log(item.name);
              return (
                <MentorCard
                  mentor={item}
                  navigation={this.props.navigation}
                  getData={this.getData}
                />
              );
            }}
            keyExtractor={(item, index) => item.email}
          />
        </View>
      );
    } else {
      return (
        <View style={styles.indicator}>
          <ActivityIndicator size="large"></ActivityIndicator>
        </View>
      );
    }
  }
}
const styles = StyleSheet.create({
  subtitle: {
    fontSize: 18,
    fontWeight: "bold",
  },
  dropDownStyle: { paddingBottom: 5, fontWeight: "bold" },
  buttonContainer: {
    marginBottom: 5,
    width: Dimensions.get("window").width * 0.4,
  },
  searchSubContainer: {
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "white",
    borderBottomWidth: 0,
    borderTopWidth: 0,
  },
  searchinput: {
    color: "black",
  },
  searchContainer: {
    backgroundColor: "white",
    padding: 0,
    marginRight: 6,
    width: "100%",
    borderWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 5,
    marginTop: 10,
  },
  indicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  search: {
    margin: 0,
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 7,
    paddingTop: 10,
    justifyContent: "center",
  },
  signOut: {
    alignSelf: "flex-end",
    marginRight: 20,
  },
  listItem: {
    flexDirection: "row",
    margin: 10,
    shadowRadius: 0.3,
    shadowOpacity: 0.5,
    elevation: 1,
    borderRadius: 2,
    padding: 20,
  },
});
