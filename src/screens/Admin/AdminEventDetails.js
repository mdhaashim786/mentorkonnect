/* Functionality: For dispalying all the details of a single mentoring event and for scheduling,
   rejecting, canceling, marking for completing and submitting the feedback */
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Alert,
  KeyboardAvoidingView,
  Linking,
  Dimensions,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { Button } from "react-native-elements";
import Feedback from "./Feedback";
import { Toast } from "native-base";
import { urls } from "./../../../env.json";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class AdminEventDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mentee: null,
      mentor: null,
      menteeLoaded: false,
      mentorLoaded: false,
      materials: [],
      rejectFlag: false,
      rejectReason: "",
      cancelFlag: false,
      cancelReason: "",
      addedMentor: null,
      text: "",
      showMaterialTextBox: false,
      materialsCount: 0,
      submitButtonFlag: false,
      completed: false,
      userType: null,
    };
  }

  getMentor() {
    let flag = this.props.navigation.getParam("flag");
    if (flag) {
      let mentor = this.props.navigation.getParam("mentor");
      // console.log(mentor);
      this.setState({
        addedMentor: mentor != undefined ? mentor : null,
      });
    }
  }

  componentDidMount() {
    const event = this.props.navigation.getParam("event");

    let userId = event.mentor_id;
    const url = `${urls.getRole}?id=${userId}`;
    fetch(url, { method: "GET" })
      .then((res) => res.json())
      .then((res) => {
        console.log(event.mentor_id);
        if (event.mentor_id != undefined) {
          if (res.role === "alumni") {
            this.loadMentor(event.mentor_id, "mentor");
            console.log("mentor");
          } else {
            this.loadMentor(event.mentor_id, "mentee");
            console.log("mentee");
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
    let user = event.mentee_id;

    const url1 = `${urls.getRole}?id=${user}`;
    fetch(url1, { method: "GET" })
      .then((res) => res.json())
      .then((res) => {
        console.log("RESPONSE", res);
        console.log(event.mentee_id);
        if (event.mentor_id != undefined) {
          if (res.role === "college_admin") {
            this.loadMentee(event.mentee_id, "college_admin");
            console.log("mentor");
          } else {
            this.loadMentee(event.mentee_id, "mentee");
            console.log("mentee");
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
    //this.loadMentee(event.mentee_id, "mentee");
    //  Materials are stored as object in firebase database. So here we are storing them
    // in array to display them using map()
    let materials = [];
    let materialsCount = 0;
    if (event.materials != null) {
      for (let key in event.materials) {
        materials.push(event.materials[key]);
        materialsCount++;
      }
    }
    this.setState({
      materials: materials,
      materialsCount: materialsCount,
    });
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getMentor();
      }
    );
  }

  componentWillUnmount() {
    if (this.willFocusSubscription != undefined)
      this.willFocusSubscription.remove();
  }

  loadMentee = async (userId, type) => {
    const url = `${urls.loadProfile}?id=${userId}&userType=${type}`;
    await fetch(url, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((response) => {
        // Mentee profile loaded but it is not present in db, then response is null.
        if (response.profile != null) {
          this.setState({
            mentee: response.profile,
            menteeLoaded: true,
          });
        }
      })
      .catch((err) => {
        console.error("Error", err);
      });
  };

  loadMentor = async (userId, type) => {
    this.setState({ userType: type }); //assigning type of the user to variable userType
    const url = `${urls.loadProfile}?id=${userId}&userType=${type}`;
    await fetch(url, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((response) => {
        // Mentor profile loaded but it is not present in db, then response is null.
        if (response.profile != null) {
          this.setState({
            mentor: response.profile,
            mentorLoaded: true,
          });
        }
      })
      .catch((err) => {
        console.error("Error", err);
      });
  };

  cancel = () => {
    if (this.state.cancelReason.trim() == "")
      Toast.show({ text: "Please enter cancel reason", buttonText: "Okay" });
    else this.cancelAlert();
  };

  cancelAlert = () => {
    Alert.alert(
      "Event Cancellation",
      "Are you sure you want to cancel the event?",
      [
        {
          text: "No",
          onPress: () => console.log("No Pressed"),
          style: "cancel",
        },
        { text: "Yes", onPress: () => this.cancelRequest() },
      ]
    );
  };

  cancelRequest = async () => {
    const event = this.props.navigation.getParam("event");
    let eventObject = {
      ...event,
      date_of_last_action: Date.now(),
      status: "Cancelled",
      cancel_reason: this.state.cancelReason.trim(),
      materials: this.state.materials,
    };
    this.updateEvent(eventObject);
    if (event.group_event) this.props.navigation.navigate("GroupEvents");
    else this.props.navigation.navigate("AdminLaunch");
    if (event.mentor_id !== undefined) {
      let mentor_message = `Hi ${this.state.mentor.name} , the mentoring event ${eventObject.event_id} on ${eventObject.event_name} has been cancelled by ${this.state.mentee.name} `;
      await this.sendPushNotification(
        mentor_message,
        eventObject.mentor_push_token,
        eventObject.status
      );
    }
    let mentee_message = `Hi ${this.state.mentee.name} , you have cancelled mentoring event request ${eventObject.event_id} that has been sent to ${this.state.mentor.name} on ${eventObject.event_name}`;
    await this.sendPushNotification(
      mentee_message,
      eventObject.mentee_push_token,
      eventObject.status
    );
  };

  reject = () => {
    if (this.state.rejectReason.trim() == "")
      Toast.show({ text: "Please enter reject reason", buttonText: "Okay" });
    else {
      this.rejectAlert();
    }
  };

  rejectAlert = () => {
    Alert.alert(
      "Event Rejection",
      "Are you sure you want to reject the event?",
      [
        {
          text: "No",
          onPress: () => console.log("No Pressed"),
          style: "cancel",
        },
        { text: "Yes", onPress: () => this.rejectRequest() },
      ]
    );
  };

  rejectRequest = async () => {
    const event = this.props.navigation.getParam("event");
    let eventObject = {
      ...event,
      date_of_last_action: Date.now(),
      status: "Rejected",
      reject_reason: this.state.rejectReason.trim(),
      materials: this.state.materials,
    };
    this.updateEvent(eventObject);
    if (event.group_event) this.props.navigation.navigate("GroupEvents");
    else this.props.navigation.navigate("AdminLaunch");
    let mentor_message = `Hi ${this.state.mentor.name} , you have rejected mentoring event ${eventObject.event_id} on ${eventObject.event_name} by ${this.state.mentee.name} `;
    let mentee_message = `Hi ${this.state.mentee.name} , your mentoring event ${eventObject.event_id} on ${eventObject.event_name} has been rejected by ${this.state.mentor.name} `;
    await this.sendPushNotification(
      mentee_message,
      eventObject.mentee_push_token,
      eventObject.status
    );
    await this.sendPushNotification(
      mentor_message,
      eventObject.mentor_push_token,
      eventObject.status
    );
  };

  completedAlert = () => {
    Alert.alert("Event Completed", "Are you sure event is completed?", [
      {
        text: "No",
        onPress: () => console.log("No Pressed"),
        style: "cancel",
      },
      { text: "Yes", onPress: () => this.complete() },
    ]);
  };

  complete = async () => {
    const event = this.props.navigation.getParam("event");

    let eventObject = {
      ...event,
      date_of_last_action: Date.now(),
      status: "Waiting for feedback",
      materials: this.state.materials,
    };
    this.setState({
      completed: true,
    });

    this.updateEvent(eventObject);
    if (event.group_event) this.props.navigation.navigate("GroupEvents");
    else this.props.navigation.navigate("AdminLaunch");
    let mentor_message = `Hi ${this.state.mentor.name} , your mentoring event ${eventObject.event_id} on ${eventObject.event_name} has been marked as completed by ${this.state.mentee.name} and waiting for feedback.`;
    let mentee_message = `Hi ${this.state.mentee.name} , your mentoring event ${eventObject.event_id} on ${eventObject.event_name} has been marked as completed and provide feedback as soon as possible `;
    await this.sendPushNotification(
      mentee_message,
      eventObject.mentee_push_token,
      eventObject.status
    );
    await this.sendPushNotification(
      mentor_message,
      eventObject.mentor_push_token,
      eventObject.status
    );
  };

  // For updating ratings from Feedback class
  updateRating = (ratings) => {
    this.setState({
      ratings: ratings,
    });
  };

  submitFeedback = async () => {
    const event = this.props.navigation.getParam("event");
    let userType = this.state.userType;
    let eventType = event.event_type;
    let userId = this.state.mentor.uid;
    // console.log(this.state.feedback, this.state.ratings);
    if (this.state.feedback.trim() == "")
      Toast.show({ text: "Enter feedback message", buttonText: "Okay" });
    else if (this.state.ratings == 0)
      Toast.show({ text: "Please provide rating", buttonText: "Okay" });
    else {
      const event = this.props.navigation.getParam("event");
      let eventObject = {
        ...event,
        date_of_last_action: Date.now(),
        feedback: this.state.feedback.trim(),
        materials: this.state.materials,
        ratings: this.state.ratings,
        status: "Completed",
      };
      this.updateEvent(eventObject);
      this.props.navigation.goBack();
      //cloud functiond to increments no_of_overview_events and no_of_bootcamp_events based on eventType.

      fetch(urls.updateBadge, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: userId,
          userType: userType,
          eventType: eventType,
        }),
      })
        .then((response) => response.json())
        .catch((err) => {
          console.error("Error", err);
        });
      let mentor_message = `Hi ${this.state.mentor.name} , your mentoring event ${eventObject.event_id} on ${eventObject.event_name} has been given feed back by ${this.state.mentee.name}. Please open the MentorKonnect App to go through feedback.`;
      let mentee_message = `Hi ${this.state.mentee.name} , you have provided feedback for mentoring event ${eventObject.event_id} on ${eventObject.event_name}.Thanks for using our App. `;
      await this.sendPushNotification(
        mentee_message,
        eventObject.mentee_push_token,
        eventObject.status
      );
      await this.sendPushNotification(
        mentor_message,
        eventObject.mentor_push_token,
        eventObject.status
      );
    }
  };

  goToScheduleScreen = () => {
    const event = this.props.navigation.getParam("event");
    if (this.state.menteeLoaded && this.state.mentorLoaded) {
      this.props.navigation.navigate("ScheduleRoute", {
        event: event,
        getData: this.props.navigation.state.params.getData.bind(this),
        user: "Admin",
        mentor: this.state.mentor,
        mentee: this.state.mentee,
      });
    } else {
      Toast.show({
        text: "Please wait details are loading!",
        buttonText: "Okay",
      });
    }
  };

  addMaterial = () => {
    if (!this.state.showMaterialTextBox) {
      this.setState({
        showMaterialTextBox: true,
      });
    } else {
      let materials = this.state.materials;
      let newMaterial = this.state.text;
      if (newMaterial.length == 0) {
        Toast.show({ text: "Cannot add empty material", buttonText: "Okay" });
        this.setState({
          text: "",
          showMaterialTextBox: false,
        });
      } else {
        materials.push(
          ++this.state.materialsCount + ". " + this.state.text.trim()
        );
        this.setState({
          materials: materials,
          text: "",
          showMaterialTextBox: false,
        });
        const event = this.props.navigation.getParam("event");
        let eventObject = {
          ...event,
          date_of_last_action: Date.now(),
          materials: this.state.materials,
        };
        this.updateEvent(eventObject);
      }
    }
  };

  // Navigating back from selecting the mentor we need to capture the selected mentor
  refresh = async (data) => {
    await this.setState({
      addedMentor: data,
    });
  };

  goToEditDetailsScreen = (user) => {
    if (this.state.menteeLoaded && this.state.mentorLoaded) {
      const event = this.props.navigation.getParam("event");
      this.props.navigation.navigate("AdminEditEventDetails", {
        event: event,
        mentor: this.state.mentor,
        mentee: this.state.mentee,
        user: user,
        userType: "admin",
      });
    } else {
      Toast.show({
        text: "Please wait details are loading",
        buttonText: "Okay",
      });
    }
  };

  addNewMentor = async () => {
    const event = this.props.navigation.getParam("event");
    let eventObject = {
      ...event,
      date_of_last_action: Date.now(),
      mentor_id:
        this.state.addedMentor != null
          ? this.state.addedMentor.mentoring_interested != undefined
            ? this.state.addedMentor.uid
            : this.state.addedMentor.uid
          : null,
      mentor_push_token: this.state.addedMentor.expo_push_token,
      // If request is rejected then after selecting new mentor, we need to change the status
      // to Open and erase the reject_reason from main data
      status: "Open",
      reject_reason: null,
    };
    await this.updateEvent(eventObject);
    this.props.navigation.goBack();
    let mentee_message = `Hi ${this.state.mentee.name} , you have successfully sent a mentoring event request to ${this.state.addedMentor.name} on ${eventObject.event_name}`;
    let mentor_message = `Hi ${this.state.addedMentor.name} , you have received a mentoring event request ${eventObject.event_id} from ${this.state.mentee.name} on ${eventObject.event_name}`;
    await this.sendPushNotification(
      mentee_message,
      eventObject.mentee_push_token,
      "Open"
    );
    await this.sendPushNotification(
      mentor_message,
      this.state.addedMentor.expo_push_token,
      "Open"
    );
  };

  // Used for updating whenever a change is made
  updateEvent = async (eventObject) => {
    eventObject = {
      ...eventObject,
      last_updated_by: "admin",
    };
    await fetch(urls.updateEvent, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(eventObject),
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(response.message);
      })
      .catch((err) => {
        console.error("Error", err);
      });
  };

  sendPushNotification = async (msg, token, status) => {
    const message = {
      to: token,
      sound: "default",
      title: "Mentoring Event",
      body: msg,
      data: { data: status },
      _displayInForeground: true,
    };
    // console.log(message);
    fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Accept-encoding": "gzip, deflate",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(message),
    }).catch((err) => {
      console.log(err);
    });
  };

  render() {
    const event = this.props.navigation.getParam("event");
    return (
      <KeyboardAvoidingView behavior="height" style={styles.container}>
        <ScrollView keyboardShouldPersistTaps="handled">
          {/* For displaying event_id, mentor and mentee names */}
          <View style={styles.eventIdContainer}>
            {/* Event ID */}
            <View style={styles.eventIdTextContainer}>
              <Text style={styles.titleStyle}>Event Id : </Text>
              <Text style={{ ...styles.fieldStyle, marginBottom: 3 }}>
                {event.event_id}
              </Text>
            </View>
            {/* Mentor name */}
            <View style={styles.eventIdTextContainer}>
              <Text style={styles.titleStyle}>Mentor : </Text>
              {this.state.mentorLoaded && (
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("MentorDetailsRoute", {
                      mentor: this.state.mentor,
                      admin: true,
                    });
                  }}
                >
                  <Text style={styles.fieldLinkStyle}>
                    {this.state.mentor != null ? this.state.mentor.name : ""}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
            {/* Mentee Name */}
            <View style={styles.eventIdTextContainer}>
              {event.group_event == false && (
                <Text style={styles.titleStyle}>Mentee : </Text>
              )}
              {event.group_event == true && (
                <Text style={styles.titleStyle}>College : </Text>
              )}
              {this.state.menteeLoaded && (
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("MenteeDetailsRoute", {
                      mentee: this.state.mentee,
                    });
                  }}
                >
                  {event.group_event == false && (
                    <Text style={styles.fieldLinkStyle}>
                      {this.state.mentee.name}
                    </Text>
                  )}
                  {event.group_event == true && (
                    <Text style={styles.fieldLinkStyle}>
                      {this.state.mentee.college_name.substring(0, 30)}
                    </Text>
                  )}
                </TouchableOpacity>
              )}
            </View>
            {event.group_event == false && (
              <View style={styles.eventIdTextContainer}>
                <Text style={styles.titleStyle}>College : </Text>
                {this.state.menteeLoaded && (
                  <Text style={styles.fieldLinkStyle}>
                    {this.state.mentee.college_name.substring(0, 30)}
                  </Text>
                )}
              </View>
            )}
            {event.group_event && (
              <View style={styles.eventIdTextContainer}>
                <Text style={styles.titleStyle}>Google ClassRoom : </Text>

                <View>
                  {event.google_classroom_code &&
                    event.google_classroom_code != undefined && (
                      <Text
                        style={styles.fieldLinkStyle}
                        onPress={() => {
                          Linking.openURL(event.google_classroom_link);
                        }}
                      >
                        {event.google_classroom_code}
                      </Text>
                    )}
                </View>
              </View>
            )}
          </View>

          {/*  For dispalying event details */}
          <View style={styles.detailsContainer}>
            <Text style={styles.titleStyle}>Event name</Text>
            <Text style={styles.fieldStyle} selectable={true}>
              {event.event_name}
            </Text>
            {event.event_type != undefined && (
              <View>
                <Text style={styles.titleStyle}>Event Type</Text>
                <Text style={styles.fieldStyle}>{event.event_type}</Text>
              </View>
            )}
            <Text style={styles.titleStyle}>Specific learning objective</Text>
            <Text style={styles.fieldStyle} selectable={true}>
              {event.event_summary}
            </Text>
            <Text style={styles.titleStyle}>Pre-work done</Text>
            <Text style={styles.fieldStyle} selectable={true}>
              {event.prework_done}
            </Text>
            {event.group_event &&
              event.google_classroom_code &&
              event.google_classroom_code != undefined && (
                <View>
                  <Text style={styles.titleStyle}>Google Classroom Link</Text>
                  <Text style={styles.fieldStyle} selectable={true}>
                    {event.google_classroom_link}
                  </Text>
                </View>
              )}

            {/*  For displaying schedule preferences if provided */}
            {event.schedule_preferences !== undefined && (
              <View>
                <Text style={styles.titleStyle}>Schedule preferences</Text>
                <Text style={styles.fieldStyle}>
                  {event.schedule_preferences}
                </Text>
              </View>
            )}

            {/*  For displaying additional comments of mentee if provided */}
            {(event.additional_details_mentee !== undefined ||
              event.status != "Completed") && (
              <View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={styles.titleStyle}>
                    Addtional details by mentee
                  </Text>
                  {(event.status == "Open" || event.status == "Scheduled") && (
                    <Icon
                      name="edit"
                      size={25}
                      style={{ marginStart: 10 }}
                      onPress={() => {
                        this.goToEditDetailsScreen("mentee");
                      }}
                    />
                  )}
                </View>
                <Text style={styles.fieldStyle} selectable={true}>
                  {event.additional_details_mentee}
                </Text>
              </View>
            )}
            {(event.additional_details_mentee == undefined ||
              event.additional_details_mentee == "") &&
              event.status == "Completed" && <View></View>}

            {/*  For displaying additional comments of mentor if provided */}
            {(event.additional_details_mentor !== undefined ||
              event.status != "Completed") && (
              <View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={styles.titleStyle}>
                    Addtional details by mentor
                  </Text>
                  {(event.status == "Open" || event.status == "Scheduled") && (
                    <Icon
                      name="edit"
                      size={25}
                      style={{ marginStart: 10 }}
                      onPress={() => {
                        this.goToEditDetailsScreen("mentor");
                      }}
                    />
                  )}
                </View>
                <Text style={styles.fieldStyle} selectable={true}>
                  {event.additional_details_mentor}
                </Text>
              </View>
            )}
            {(event.additional_details_mentor == undefined ||
              event.additional_details_mentor == "") &&
              event.status == "Completed" && <View></View>}

            {/* Dispalying event date when it is scheduled */}
            {event.status != "Open" &&
              event.status != "Cancelled" &&
              event.status != "Rejected" && (
                <View>
                  <Text style={styles.titleStyle}>Event date</Text>
                  <Text style={styles.fieldStyle}>{event.event_date}</Text>
                </View>
              )}

            {/* Dispalying event mode of mentoring when it is scheduled */}
            {event.status != "Open" &&
              event.status != "Cancelled" &&
              event.status != "Rejected" && (
                <View>
                  <Text style={styles.titleStyle}>Mode of mentoring</Text>
                  <Text selectable={true} style={styles.fieldStyle}>
                    {event.mode_of_mentoring}
                  </Text>
                </View>
              )}
            {/* Displaying registered mentees */}
            {event.group_event && event.registered_mentees!=undefined &&(
              <View>
                <View>
                  <Text style={{ ...styles.titleStyle, marginTop: 5 }}>
                    Number of registered mentees
                  </Text>

                  <View style={{ paddingEnd: 20 }}>
                    <Text style={{ ...styles.fieldStyle, marginBottom: 5 }}>
                      {event.registered_mentees.length - 1}
                    </Text>
                  </View>
                </View>
              </View>
            )}
            {/* Displaying reject reason when the event is rejected */}
            {event.status == "Rejected" && (
              <View>
                <Text style={styles.titleStyle}>Rejected Reason</Text>
                <Text style={styles.fieldStyle}>{event.reject_reason}</Text>
              </View>
            )}

            {/* Displaying cancel reason when the event is cancelled */}
            {event.status == "Cancelled" && (
              <View>
                <Text style={styles.titleStyle}>Cancel Reason</Text>
                <Text style={styles.fieldStyle}>{event.cancel_reason}</Text>
              </View>
            )}

            {/* If mentor is not selected by mentee or mentor rejects request
                 providing option for admin to add mentor */}
            {!this.state.cancelFlag &&
              !this.state.rejectFlag &&
              ((event.mentor_id == undefined && event.status == "Open") ||
                event.status == "Rejected") && (
                <View>
                  <Text style={{ ...styles.titleStyle, marginBottom: 5 }}>
                    Add Mentor
                  </Text>
                  {/* Button for selecting mentors */}
                  {this.state.addedMentor == null && (
                    <View
                      style={{
                        ...styles.mentorButtonContainer,
                        marginStart: 10,
                      }}
                    >
                      <Button
                        title="Show mentors"
                        onPress={() => {
                          this.props.navigation.navigate("SelectMentorRoute", {
                            // When mentee navigates back from "SelectMentor" screen,
                            // refresh() will be called
                            onGoBack: this.refresh,
                            page: "AdminEventDetails",
                          });
                        }}
                        containerStyle={styles.bigButton}
                      />
                    </View>
                  )}

                  {/* Button for changing selected mentor */}
                  {this.state.addedMentor != null && (
                    <View>
                      <Text style={styles.addedMentorText}>
                        {this.state.addedMentor.name}
                      </Text>
                      <View style={{ flexDirection: "row" }}>
                        <View style={{ marginEnd: 10 }}>
                          <Button
                            title="Remove"
                            onPress={() => {
                              this.setState({
                                addedMentor: null,
                              });
                            }}
                            containerStyle={styles.smallButton}
                            buttonStyle={{ backgroundColor: "#90A4AE" }}
                          />
                        </View>
                        <Button
                          title="Change Mentor"
                          onPress={() => {
                            this.props.navigation.navigate(
                              "SelectMentorRoute",
                              {
                                // When mentee navigates back from "SelectMentor" screen, refresh() will be called
                                onGoBack: this.refresh,
                                page: "AdminEventDetails",
                              }
                            );
                          }}
                          containerStyle={styles.bigButton}
                          buttonStyle={{ backgroundColor: "#03A9F4" }}
                        />
                      </View>
                    </View>
                  )}

                  {/* Submit Button */}
                  {this.state.addedMentor != null && (
                    <View style={styles.buttonContainer}>
                      <Button
                        loading={this.state.submitButtonFlag}
                        title="Submit"
                        onPress={() => {
                          this.setState({ submitButtonFlag: true });
                          this.addNewMentor();
                        }}
                        containerStyle={styles.smallButton}
                        buttonStyle={{ backgroundColor: "#66BB6A" }}
                      />
                    </View>
                  )}
                </View>
              )}

            {/*  Providing option for adding materials and to display them */}
            {event.status == "Scheduled" &&
              !this.state.rejectFlag &&
              !this.state.cancelFlag &&
              !this.state.completed && (
                <View>
                  {this.state.materials.length != 0 && (
                    <View>
                      <Text style={{ ...styles.titleStyle, marginTop: 5 }}>
                        Materials reviewed during the event
                      </Text>
                      {this.state.materials.map((item, key) => (
                        <View key={item} style={{ paddingEnd: 20 }}>
                          <Text
                            style={{ ...styles.fieldStyle, marginBottom: 5 }}
                            selectable={true}
                            onPress={() => {
                              Linking.openURL(item);
                            }}
                          >
                            {item}
                          </Text>
                        </View>
                      ))}
                    </View>
                  )}
                  {this.state.showMaterialTextBox && (
                    <TextInput
                      placeholder={"Enter material"}
                      style={styles.fieldBoxStyle}
                      value={this.state.text}
                      autoFocus={true}
                      onChangeText={async (material) => {
                        await this.setState({ text: material.trim() });
                      }}
                    ></TextInput>
                  )}
                  <View style={styles.buttonContainer}>
                    <Button
                      title="Add Materials"
                      onPress={() => {
                        this.addMaterial();
                      }}
                      containerStyle={styles.bigButton}
                    />
                  </View>
                </View>
              )}

            {/* Displaying materials when event is in waiting for feedback or completed state
                Here materials are only displayed and cannot be added */}
            {event.status != "Open" &&
              (event.status != "Scheduled" || this.state.completed) &&
              event.status != "Rejected" &&
              this.state.materials.length != 0 && (
                <View style={{ marginBottom: 5 }}>
                  <Text style={styles.titleStyle}>
                    Materials reviewed during the event
                  </Text>
                  {this.state.materials.map((item, key) => (
                    <View key={item} style={{ paddingEnd: 20 }}>
                      <Text
                        style={{ ...styles.fieldStyle, marginBottom: 5 }}
                        selectable={true}
                        onPress={() => {
                          Linking.openURL(item);
                        }}
                      >
                        {item}
                      </Text>
                    </View>
                  ))}
                </View>
              )}

            {/* When an event is scheduled, application admin can cancel, reject, reschedule
                or mark the event as completed  */}
            {(event.status == "Scheduled" ||
              event.status == "Open" ||
              event.status == "Rejected") && (
              <View>
                {!this.state.rejectFlag &&
                  !this.state.cancelFlag &&
                  this.state.addedMentor == null && (
                    <View style={styles.doubleButtonContainer}>
                      {/* Cancel button */}
                      <View style={styles.buttonContainer}>
                        <Button
                          title="Cancel"
                          onPress={() => {
                            this.setState({
                              cancelFlag: true,
                            });
                          }}
                          containerStyle={styles.smallButton}
                          buttonStyle={{ backgroundColor: "#90A4AE" }}
                        />
                      </View>
                      {/* Reject button */}
                      {event.status != "Rejected" &&
                        event.status != "Scheduled" &&
                        event.mentor_id != undefined && (
                          <View
                            style={{
                              ...styles.buttonContainer,
                              marginStart: 20,
                            }}
                          >
                            <Button
                              title="Reject"
                              onPress={() => {
                                this.setState({
                                  rejectFlag: true,
                                });
                              }}
                              containerStyle={styles.smallButton}
                              buttonStyle={{ backgroundColor: "#E57373" }}
                            />
                          </View>
                        )}
                    </View>
                  )}
                {/* Cancel Reason */}
                {this.state.cancelFlag && (
                  <View>
                    <Text style={{ ...styles.titleStyle }}>
                      Cancel reason <Text style={{ color: "red" }}>*</Text>
                    </Text>
                    {/* Textbox for capturing reject reason */}
                    <TextInput
                      multiline
                      placeholder={"Enter cancel reason"}
                      style={styles.fieldBoxLargeStyle}
                      value={this.state.cancelReason}
                      onChangeText={(cancel) => {
                        this.setState({ cancelReason: cancel });
                      }}
                    ></TextInput>
                    {/* Buttons for sending the cancel reason*/}
                    <View style={styles.doubleButtonContainer}>
                      <View>
                        <Button
                          title="Go Back"
                          onPress={() => {
                            this.setState({
                              cancelFlag: false,
                            });
                          }}
                          containerStyle={styles.smallButton}
                          buttonStyle={{ backgroundColor: "#90A4AE" }}
                        />
                      </View>
                      <View style={{ marginStart: 10 }}>
                        <Button
                          title="Cancel Request"
                          onPress={() => {
                            this.cancel();
                          }}
                          containerStyle={styles.bigButton}
                          buttonStyle={{ backgroundColor: "#E57373" }}
                        />
                      </View>
                    </View>
                  </View>
                )}
                {/* Reject reason */}
                {this.state.rejectFlag && (
                  <View>
                    <Text style={{ ...styles.titleStyle, marginTop: 5 }}>
                      Reject reason <Text style={{ color: "red" }}>*</Text>
                    </Text>
                    {/* Textbox for capturing reject reason */}
                    <TextInput
                      multiline
                      placeholder={"Enter Reject reason"}
                      style={styles.fieldBoxLargeStyle}
                      value={this.state.rejectReason}
                      onChangeText={(reject) => {
                        this.setState({ rejectReason: reject });
                      }}
                    ></TextInput>
                    {/* Buttons for Cancelling and rejecting the request */}
                    <View style={styles.doubleButtonContainer}>
                      <View>
                        <Button
                          title="Go back"
                          onPress={() => {
                            this.setState({
                              rejectFlag: false,
                            });
                          }}
                          containerStyle={styles.smallButton}
                          buttonStyle={{ backgroundColor: "#90A4AE" }}
                        />
                      </View>
                      <View style={{ marginStart: 10 }}>
                        <Button
                          title="Reject"
                          onPress={() => {
                            this.reject();
                          }}
                          containerStyle={styles.smallButton}
                          buttonStyle={{ backgroundColor: "#E57373" }}
                        />
                      </View>
                    </View>
                  </View>
                )}
                {/* Marking event as completed */}
                {!this.state.rejectFlag &&
                  !this.state.cancelFlag &&
                  event.status == "Scheduled" && (
                    <View style={styles.buttonContainer}>
                      <Button
                        loading={this.state.completeButtonFlag}
                        title="Mark for completion"
                        onPress={() => {
                          this.setState({ completeButtonFlag: true });
                          this.completedAlert();
                        }}
                        containerStyle={styles.bigButton}
                        buttonStyle={{ backgroundColor: "#3FDA96" }}
                      />
                    </View>
                  )}

                {/* Providing option for scheduling */}
                {event.status == "Open" && event.mentor_id != undefined && (
                  <View style={styles.buttonContainer}>
                    <Button
                      title="Schedule"
                      onPress={() => {
                        this.goToScheduleScreen();
                      }}
                      containerStyle={styles.smallButton}
                      buttonStyle={{ backgroundColor: "#3FDA96" }}
                    />
                  </View>
                )}
              </View>
            )}
            {/* Providing options for mentee to enter feedback */}
            {event.group_event &&
              (event.status == "Waiting for feedback" ||
                this.state.completed) && (
                <View>
                  <Text style={styles.titleStyle}>
                    Provide feedback <Text style={{ color: "red" }}>*</Text>
                  </Text>
                  <TextInput
                    multiline
                    placeholder={"Enter feedback"}
                    style={styles.fieldBoxLargeStyle}
                    onChangeText={(feedback) => {
                      this.setState({ feedback: feedback });
                    }}
                  ></TextInput>
                  <Text style={styles.titleStyle}>
                    Ratings <Text style={{ color: "red" }}>*</Text>
                  </Text>
                  <Feedback
                    updateRating={this.updateRating}
                    status={event.status}
                  />
                  <View style={styles.buttonContainer}>
                    <Button
                      title="Submit Feedback"
                      onPress={() => {
                        this.submitFeedback();
                      }}
                      containerStyle={styles.bigButton}
                      buttonStyle={{ backgroundColor: "#90A4AE" }}
                    />
                  </View>
                </View>
              )}

            {/* Displaying feedback when the event is completed */}
            {event.status == "Completed" && (
              <View style={{ marginBottom: 20 }}>
                <Text style={styles.titleStyle}>Feedback</Text>
                <Text style={styles.fieldStyle}>{event.feedback}</Text>
                <Text style={styles.titleStyle}>Ratings</Text>
                <Feedback ratings={event.ratings} status={event.status} />
              </View>
            )}
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
    paddingBottom: 10,
  },
  eventIdContainer: {
    backgroundColor: "#C9E1F4",
    padding: 15,
  },
  eventIdTextContainer: {
    flexDirection: "row",
    marginBottom: 3,
    flexWrap:"wrap"
  },
  infoStyle: {
    marginStart: 5,
    flex: 1,
    marginTop: 5,
  },
  imageStyle: {
    borderRadius: 3,
    width: 130,
    height: 130,
    margin: 10,
    alignSelf: "center",
  },
  detailsContainer: {
    margin: 10,
    alignContent: "space-around",
    marginStart: 10,
  },
  titleStyle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 3,
  },
  fieldStyle: {
    fontSize: 18,
    marginBottom: 10,
  },
  fieldLinkStyle: {
    fontSize: 19,
    marginBottom: 3,
    textDecorationLine: "underline",
    color: "blue",
  },
  fieldBoxStyle: {
    fontSize: 18,
    marginBottom: 15,
    borderWidth: 1,
    padding: 10,
    marginTop: 5,
  },
  fieldBoxLargeStyle: {
    fontSize: 18,
    marginBottom: 15,
    borderWidth: 1,
    padding: 10,
    marginTop: 5,
    height: 150,
    textAlignVertical: "top",
    marginTop: 5,
  },
  addedMentorText: {
    fontSize: 20,
    marginBottom: 15,
    marginStart: 10,
  },
  smallButton: {
    width: Dimensions.get("window").width * 0.3,
  },
  bigButton: {
    width: Dimensions.get("window").width * 0.5,
  },
  buttonContainer: {
    alignSelf: "center",
    marginTop: 20,
  },
  doubleButtonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20,
  },
  buttonText: {
    fontSize: 17,
    padding: 10,
  },
  mentorButtonContainer: {
    backgroundColor: "#03A9F4",
    borderRadius: 25,
    alignItems: "center",
    width: 175,
    marginBottom: 15,
  },
});
