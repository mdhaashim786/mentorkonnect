import React from "react";
import { StyleSheet, Text, View, Dimensions, Linking } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class LicenseBucketCard extends React.Component {
  render() {
    const { bucket, navigation, bucketno } = this.props;
    let timestamp = bucket.created_date;
    let date = new Date(timestamp);
    return (
      <View style={styles.container}>
        <TouchableOpacity>
          <View style={styles.cardStyle}>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.fieldBoldStyle}>Bucket no: </Text>
              <Text style={styles.fieldStyle}>{bucket.bucket_id + 1}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.fieldBoldStyle}>License added date: </Text>
              <Text style={styles.fieldStyle}>
                {date.getDate() +
                  "-" +
                  (date.getMonth() + 1) +
                  "-" +
                  date.getFullYear()}
              </Text>
            </View>

            <View style={{ flexDirection: "row" }}>
              <Text style={styles.fieldBoldStyle}>Days left to expire: </Text>
              <Text style={styles.fieldStyle}>
                {bucket.days_left_to_expire}
              </Text>
            </View>
            {bucket.license_status == true && (
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.fieldBoldStyle}>License status: </Text>
                <Text style={{ ...styles.fieldBoldStyle }}>active</Text>
              </View>
            )}
            {bucket.license_status == false && (
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.fieldBoldStyle}>License status: </Text>
                <Text style={{ ...styles.fieldBoldStyle }}>deactivated</Text>
              </View>
            )}
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.fieldBoldStyle}>No of mentee licenses: </Text>
              <Text style={styles.fieldStyle}>{bucket.mentee_limit}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.fieldBoldStyle}>No of active mentees: </Text>
              <Text style={styles.fieldStyle}>
                {bucket.no_of_mentees_present}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    margin: 7,
    elevation: 2,
    borderWidth: 0.5,
    borderTopWidth: 0,
  },
  cardStyle: {
    paddingHorizontal: 15,
    paddingBottom: 10,
    paddingTop: 10,
    alignItems: "flex-start",
    alignContent: "space-between",
  },
  titleStyle: {
    fontSize: 22,
    fontWeight: "bold",
    marginBottom: 5,
  },
  fieldBoldStyle: {
    fontSize: 18,
    marginBottom: 3,
    color: "#212121",
    fontWeight: "700",
  },
  fieldStyle: {
    fontSize: 18,
    marginBottom: 3,
    color: "#212121",
  },
});
