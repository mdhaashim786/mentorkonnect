// Functionality: For Scheduling the event by mentor asked to enter event date, mode of mentoring
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  Dimensions,
} from "react-native";
import { TextInput, ScrollView } from "react-native-gesture-handler";
import DatePicker from "react-native-datepicker";
import { Button } from "react-native-elements";
import {} from "@react-native-community/datetimepicker";
import { Toast } from "native-base";
import { urls } from "./../../../env.json";
import firebase from "firebase";

export default class ScheduleScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      eventDate: "",
      modeOfMentoring: "",
      materials: "",
      additionalDetails: "",
      scheduleButtonFlag: false,
    };
  }

  schedule = () => {
    // Check whether mentor entered required fields or not
    if (this.state.eventDate == "")
      Toast.show({ text: "Select event start date", buttonText: "Okay" });
    else if (this.state.modeOfMentoring == "")
      Toast.show({ text: "Enter mode of mentoring", buttonText: "Okay" });
    else {
      this.setState({ scheduleButtonFlag: true });
      this.scheduleEvent();
    }
  };

  scheduleEvent = async () => {
    const event = this.props.navigation.getParam("event");
    const user = this.props.navigation.getParam("user");
    let userType = null;
    if (user == "Admin") userType = "admin";
    else {
      const profile = this.props.navigation.getParam("profile");
      const isMentor = this.props.navigation.getParam("isMentor");
      if (isMentor) userType = "mentor";
      else userType = profile.user_type;
    }
    let eventObject = {
      ...event,
      date_of_last_action: Date.now(),
      status: "Scheduled",
      event_date: this.state.eventDate,
      mode_of_mentoring: this.state.modeOfMentoring.trim(),
      last_updated_by: userType,
      additional_details_mentor:
        this.state.additionalDetails.trim() !== ""
          ? this.state.additionalDetails.trim()
          : null,
    };

    fetch(urls.updateEvent, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(eventObject),
    })
      .then((response) => response.json())
      .then(() => {
        const user = this.props.navigation.getParam("user");
        // If user logged in, after scheduling the event navigate to user launch screen
        if (user == "User") {
          this.props.navigation.navigate("LaunchRoute");
        }
        // If admin logged in, after scheduling the event navigate to admin launch screen
        else {
          if (event.group_event) this.props.navigation.navigate("GroupEvents");
          else this.props.navigation.navigate("AdminLaunch");
        }
      })
      .then(async (response) => {
        // console.log(response);
        const mentor = this.props.navigation.getParam("mentor");
        const mentee = this.props.navigation.getParam("mentee");
        let mentor_message = `Hi ${mentor.name} , you have scheduled the mentoring event ${eventObject.event_id} from ${mentee.name} on ${eventObject.event_name}`;
        await this.sendPushNotification(
          mentor_message,
          mentor.expo_push_token,
          eventObject.status
        );
        let mentee_message = `Hi ${mentee.name} , your mentoring event ${eventObject.event_id} on ${eventObject.event_name} has been scheduled by ${mentor.name}`;
        await this.sendPushNotification(
          mentee_message,
          mentee.expo_push_token,
          eventObject.status
        );

        //Sending PushNotifications when GEvent is created-->
        //if event is a group event send notification to all mentees that a new group event is created
        let adminData = null; //used to store admins data
        let menteeVal = null; // used to temporarily store each mentees data
        var menteesList = []; //contains all the list of mentees present in admin college
        /* if the admin/mentor schedules a groupevent then pushnotifiactions are sent to All the mentees of the admin's clg if the expoPushtokens exist , 
   saying that a new group event is created.*/

        if (eventObject.group_event) {
          let path;
          if (eventObject.college_id == 1 && eventObject.institution_id=="INST1") {
            path = "mentees/";
          } else {
            path = "collegeadmins/";
          }
          //console.log("group");
          await firebase
            .database()
            .ref(path + eventObject.mentee_id)
            .once("value")
            .then((menteeSnapShot) => {
              adminData = menteeSnapShot.val();
            })
            .then(() => {
              console.log(adminData);
            }); //now admin data is stored in 'adminData'

          await firebase
            .database()
            .ref("mentees/")
            .once("value")
            .then((snap) => {
              snap.forEach((childSnapshot) => {
                if (
                  childSnapshot.val().institution_id ===
                    adminData.institution_id &&
                  childSnapshot.val().college_id === adminData.college_id &&
                  eventObject.mentor_id !== childSnapshot.val().uid
                ) {
                  menteeVal = childSnapshot.val();
                  menteesList.push(
                    menteeVal
                  ); /* we are checking all the mentees who belong to admins college and pushing them into mentee list except admin's data */
                }
              });

              //menteesList which belong to the same college as admin contains all mentees data but we send push notifications only if expotoken exists
            });
          let all_mentees_message = `A group mentoring event: ${eventObject.event_name} with id ${eventObject.event_id}, has been created. If you are interested, register for the event using the register button.`;
          for (let i = 0; i < menteesList.length; i++) {
            if (menteesList[i].expo_push_token) {
              this.sendPushNotification(
                all_mentees_message,
                menteesList[i].expo_push_token,
                eventObject.status
              ); //menteesList which belong to the same college as admin contains all mentees data but we send push notifications only if expotoken exists
            }
          }
        }
      })
      .catch((err) => {
        console.error("Error", err);
      });
  };

  sendPushNotification = async (msg, token, status) => {
    const message = {
      to: token,
      sound: "default",
      title: "Mentoring Event",
      body: msg,
      data: { data: status },
      _displayInForeground: true,
    };
    // console.log(message);
    fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Accept-encoding": "gzip, deflate",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(message),
    }).catch((err) => {
      console.log(err);
    });
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="height">
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
        >
          {/* Event date heading */}
          <Text style={styles.titleStyle}>
            Pick event date <Text style={{ color: "red" }}>*</Text>
          </Text>

          {/* Component for taking date input */}
          <DatePicker
            placeholder="Select date"
            style={styles.datePickerStyle}
            date={this.state.eventDate}
            mode="date"
            onDateChange={(date) =>
              this.setState({
                eventDate: date,
              })
            }
            format="DD-MM-YYYY"
            minDate={new Date()}
            confirmBtnText={"OK"}
            cancelBtnText={"Cancel"}
          />

          {/* Mode of mentoring heading */}
          <Text style={styles.titleStyle}>
            Mode of mentoring <Text style={{ color: "red" }}>*</Text>
          </Text>

          {/* Text input for entering mode of mentoring */}
          <TextInput
            multiline
            placeholder={"Ex: Zoom call"}
            style={styles.fieldStyle}
            onChangeText={(mode) => {
              this.setState({ modeOfMentoring: mode });
            }}
          ></TextInput>

          {/* Additonal comments heading */}
          <Text style={styles.titleStyle}>Additional Comments</Text>

          {/* Text input for entering additional details */}
          <TextInput
            multiline
            placeholder={"Any additional details..."}
            style={styles.fieldLargeStyle}
            onChangeText={(additional) => {
              this.setState({ additionalDetails: additional });
            }}
          ></TextInput>
          <View style={styles.acceptContainer}>
            <Button
              title="Schedule"
              loading={this.state.scheduleButtonFlag}
              onPress={() => {
                this.schedule();
              }}
              containerStyle={{ width: Dimensions.get("window").width * 0.5 }}
              buttonStyle={{ backgroundColor: "#3FDA96" }}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    paddingHorizontal: 10,
  },
  titleStyle: {
    fontSize: 20,
    fontWeight: "700",
    marginBottom: 5,
  },
  fieldStyle: {
    fontSize: 15,
    paddingStart: 13,
    borderWidth: 1,
    marginBottom: 15,
    height: 40,
  },
  fieldLargeStyle: {
    fontSize: 17,
    padding: 5,
    paddingStart: 13,
    borderWidth: 1,
    marginBottom: 15,
    height: 120,
    textAlignVertical: "top",
  },
  acceptContainer: {
    borderRadius: 10,
    alignSelf: "center",
    alignItems: "center",
    width: 120,
    marginBottom: 15,
    marginTop: 15,
    marginEnd: 20,
  },
  buttonText: {
    fontSize: 17,
    padding: 10,
  },
  datePickerStyle: {
    width: 250,
    borderRadius: 10,
    marginLeft: 3,
    marginBottom: 15,
  },
});
