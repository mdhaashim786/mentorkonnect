import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Linking,
  ImageBackground,
  ActivityIndicator,
} from "react-native";
import { Avatar } from "react-native-elements";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { urls } from "./../../../env.json";
import PreBuiltEventTypes from "./PreBuiltEventTypes";
import Logos from "../../../assets/Logos";
import * as Font from "expo-font";

var data;
let customFonts = {
  LobsterRegular: require("../../../assets/fonts/LobsterRegular-rR8O.ttf"),
};
export default class MenteeDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: null,
      fontsLoaded: false,
    };
  }
  async _loadFontsAsync() {
    await Font.loadAsync(customFonts);
    this.setState({ fontsLoaded: true });
  }

  getData = async () => {
    let d = await this.props.navigation.getParam("mentee");
    await this.setState({ profile: d });
  };

  componentDidMount() {
    //TO load the fonts
    this._loadFontsAsync();
    this.getData();
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getData();
      }
    );
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  render() {
    const { navigation } = this.props;
    if (this.state.profile != null && this.state.fontsLoaded) {
      var badge = null;
      if (this.state.profile.no_of_events != undefined) {
        const simple = this.state.profile.no_of_events["no_of_simple_events"]; //getting no of simple evnts of the mentor.
        const overview = this.state.profile.no_of_events[
          "no_of_overview_events"
        ]; //getting no of overview evnts of the mentor.
        const bootcamp = this.state.profile.no_of_events[
          "no_of_bootcamp_events"
        ]; //getting no of bootcamp evnts of the mentor.
        // based on the conditions,respective badge is stored in badge variable.
        if (simple >= 20 || overview >= 7 || bootcamp >= 3)
          badge = Logos.platinum;
        else if (
          (simple >= 10 && simple < 20) ||
          (overview >= 4 && overview < 7) ||
          bootcamp == 2
        )
          badge = Logos.gold;
        else if (
          (simple >= 5 && simple < 10) ||
          (overview >= 1 && overview < 4) ||
          bootcamp == 1
        )
          badge = Logos.silver;
        else if (simple >= 1 && simple < 5) badge = Logos.bronze;
        else badge = null;
      }
      return (
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.imageContainer}>
              <View style={styles.imageViewStyle}>
                <Avatar
                  size={100}
                  rounded
                  source={{ uri: this.state.profile.photo_url }}
                  icon={{ name: "user", type: "font-awesome" }}
                />
              </View>
              <View style={styles.imageBesideStyle}>
                <Text style={styles.imageBesideTextStyle}>
                  {this.state.profile.name}
                </Text>
                <Text
                  style={{
                    ...styles.imageBesideTextStyle,
                    textTransform: "uppercase",
                  }}
                >
                  {this.state.profile.roll_number}
                </Text>
                <Text style={styles.imageBesideTextStyle}>
                  {this.state.profile.branch}
                </Text>
                <Text style={styles.imageBesideTextStyle}>
                  {this.state.profile.college_name}
                </Text>
              </View>
              {badge != null && (

              <View style={styles.badgeViewStyle}>
                  <ImageBackground
                    style={{
                      width: 70,
                      height: 140,
                    }}
                    resizeMode="contain"
                    source={{ uri: badge }}
                  >
                    <View style={styles.badge_1stview}>
                      <View style={styles.badge_2ndview}>
                        <Text style={styles.nameonbadge}>
                          {this.state.profile.name.split(" ", 1)}
                        </Text>
                      </View>
                    </View>
                  </ImageBackground>
                </View>
              )}
            </View>
          </View>
          
          <View style={styles.details}>
          <View style={{ flexDirection: "row" }}>
              <Text style={{ ...styles.titleStyle, marginEnd: 10 }}>
                {this.state.profile.name}
              </Text>
              <TouchableOpacity
                  style={{
                    borderWidth: 2,
                    borderColor: "#4682b4",
                    borderRadius: 6,
                  }}
                  onPress={() =>
                    Linking.openURL(`${this.state.profile.linkedin}`)
                  }
                >
                  <Image
                    source={require("../../../assets/linkedin-icon.png")}
                    style={{
                      width: 28,
                      height: 28,
                      margin: 0.4,
                    }}
                  />
                </TouchableOpacity>
            </View>

            {this.state.profile.about != "" &&
              this.state.profile.about != undefined && (
                <View>
                  <Text style={styles.subHeading}>About</Text>
                  <Text style={styles.about}>{this.state.profile.about}</Text>
                </View>
              )}

            {this.state.profile.email != undefined &&
              this.state.profile.email != "" && (
                <View>
                  <Text style={styles.subHeading}>Email</Text>
                  <Text style={styles.about} selectable={true}>
                    {this.state.profile.email}
                  </Text>
                </View>
              )}

            {this.state.profile.gpa != undefined &&
              this.state.profile.gpa != "" && (
                <View>
                  <Text style={styles.subHeading}>GPA</Text>
                  <Text style={styles.about}> {this.state.profile.gpa}</Text>
                </View>
              )}

            {this.state.profile.year_passed != "" &&
              this.state.profile.year_passed != undefined && (
                <View>
                  <Text style={styles.subHeading}>Year of Graduation</Text>
                  <Text style={styles.about}>
                    {this.state.profile.year_passed}
                  </Text>
                </View>
              )}

            {this.state.profile.college_after_BTECH != "" &&
              this.state.profile.college_after_BTECH != undefined && (
                <View>
                  <Text style={styles.subHeading}>College After B.TECH</Text>
                  <Text style={styles.about}>
                    {this.state.profile.college_after_BTECH}
                  </Text>
                </View>
              )}

            {this.state.profile.higher_education != undefined &&
              this.state.profile.higher_education != "" && (
                <View>
                  <Text style={styles.subHeading}>Higher Education</Text>
                  <Text style={styles.about}>
                    {this.state.profile.higher_education}
                  </Text>
                </View>
              )}

            {this.state.profile.specialization != undefined &&
              this.state.profile.specialization != "" && (
                <View>
                  <Text style={styles.subHeading}>Specialization</Text>
                  <Text style={styles.about}>
                    {this.state.profile.specialization}
                  </Text>
                </View>
              )}

            {this.state.profile.available != undefined &&
              this.state.profile.available != "" && (
                <View>
                  <Text style={styles.subHeading}>Available Time</Text>
                  <Text style={styles.about}>
                    {this.state.profile.available}
                  </Text>
                </View>
              )}

            {this.state.profile.phone_number != undefined &&
              this.state.profile.phone_number != "" && (
                <View>
                  <Text style={styles.subHeading}>Phone Number</Text>
                  <Text style={styles.about}>
                    {this.state.profile.phone_number}
                  </Text>
                </View>
              )}
            {this.state.profile.interests_list != undefined && (
              <View>
                <Text style={styles.subHeading}>Interests</Text>

                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  {this.state.profile.interests_list.map((l, i) => (
                    <View key={i} style={styles.listViewStyle}>
                      <Text style={styles.listTextStyle}>{l.name}</Text>
                    </View>
                  ))}
                </View>
              </View>
            )}
            {this.state.profile.mentoring_interest_list != undefined && (
              <View>
                <Text style={styles.subHeading}>Mentoring Interests</Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  {this.state.profile.mentoring_interest_list.map((l, i) => (
                    <View key={i} style={styles.listViewStyle}>
                      <Text style={styles.listTextStyle}>{l.name}</Text>
                    </View>
                  ))}

                  {/* Used to display preBuilt Events when menteedetails are opned */}

                  {this.state.profile.pre_built_events_indices != undefined &&
                    this.state.profile.pre_built_events_indices.length != 0 &&
                    this.state.profile.pre_built_events_indices.map((l, i) => (
                      <View key={i} style={styles.listViewStyle}>
                        {PreBuiltEventTypes.map((item, id) => (
                          <View key={id}>
                            {l == id && (
                              <Text style={styles.listTextStyle}>
                                {item.name}
                              </Text>
                            )}
                          </View>
                        ))}
                      </View>
                    ))}
                </View>
              </View>
            )}

            {this.state.profile.patents != undefined && (
              <View>
                <Text style={styles.subHeading}>Patents</Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  {this.state.profile.patents.map((l, i) => (
                    <View key={i} style={styles.listViewStyle}>
                      <Text style={styles.listTextStyle}>{l.name}</Text>
                    </View>
                  ))}
                </View>
              </View>
            )}

            {this.state.profile.extra_curricular_activities != undefined && (
              <View>
                <Text style={styles.subHeading}>
                  Extra Curricular Activites
                </Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  {this.state.profile.extra_curricular_activities.map(
                    (l, i) => (
                      <View key={i} style={styles.listViewStyle}>
                        <Text style={styles.listTextStyle}>{l.name}</Text>
                      </View>
                    )
                  )}
                </View>
              </View>
            )}

            {this.state.profile.short_term_goals != undefined && (
              <View>
                <Text style={styles.subHeading}>Short Term Goals</Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  {this.state.profile.short_term_goals.map((l, i) => (
                    <View key={i} style={styles.listViewStyle}>
                      <Text style={styles.listTextStyle}>{l.name}</Text>
                    </View>
                  ))}
                </View>
              </View>
            )}

            {this.state.profile.long_term_goals != undefined &&
              this.state.profile.long_term_goals.length != 0 && (
                <View>
                  <Text style={styles.subHeading}>Long Term Goals</Text>
                  <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                    {this.state.profile.long_term_goals.map((l, i) => (
                      <View key={i} style={styles.listViewStyle}>
                        <Text style={styles.listTextStyle}>{l.name}</Text>
                      </View>
                    ))}
                  </View>
                </View>
              )}

            {this.state.profile.subjects_liked != undefined &&
              this.state.profile.subjects_liked.length != 0 && (
                <View>
                  <Text style={styles.subHeading}>
                    Subjects Liked at your college
                  </Text>
                  <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                    {this.state.profile.subjects_liked.map((l, i) => (
                      <View key={i} style={styles.listViewStyle}>
                        <Text style={styles.listTextStyle}>{l.name}</Text>
                      </View>
                    ))}
                  </View>
                </View>
              )}

            {this.state.profile.faculty_recommended != undefined &&
              this.state.profile.faculty_recommended.length != 0 && (
                <View>
                  <Text style={styles.subHeading}>
                    Faculty Recommended at your college
                  </Text>
                  <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                    {this.state.profile.faculty_recommended.map((l, i) => (
                      <View key={i} style={styles.listViewStyle}>
                        <Text style={styles.listTextStyle}>{l.name}</Text>
                      </View>
                    ))}
                  </View>
                </View>
              )}
          </View>
        </ScrollView>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <ActivityIndicator size="large"></ActivityIndicator>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: Dimensions.get("window").height * 0.25,
    backgroundColor: "#a3c0e1",
  },
  subHeading: {
    fontSize: 20,
    fontWeight: "bold",
    marginVertical: 5,
  },
  about: {
    fontSize: 17,
    marginStart: 3,
  },
  imageContainer: {
    height: Dimensions.get("window").height * 0.25,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  listViewStyle: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    borderRadius: 10,
    borderWidth: 2,
    alignItems: "center",
  },
  listTextStyle: {
    fontSize: 17,
    fontWeight: "bold",
  },
  details: {
    flexDirection: "column",
    padding: 10,
  },
  badgeViewStyle: {
    paddingLeft: 5,
    paddingRight: 5,
  },
  badge_2ndview: {
    height: 35,
    width: 35,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
  },
  badge_1stview: {
    borderRadius: 50,
    flexWrap: "wrap",
    marginLeft: 12.8,
    marginTop: 36.8,
    height: 45,
    width: 45,
    justifyContent: "center",
    alignItems: "center",
  },
  nameonbadge: {
    fontSize: 8,
    color: "#fffff0",
    fontWeight: "900",
    textAlign: "center",
    fontFamily: "LobsterRegular",
    textTransform: "capitalize",
  },
  imageViewStyle: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: Dimensions.get("window").height * 0.01,
    paddingLeft: 10,
    paddingRight: 10,
    marginRight: Dimensions.get("window").height * 0.01,
  },
  titleStyle: {
    fontSize: 23,
    fontWeight: "bold",
    textTransform: "capitalize",
  },
  emailStyle: {
    color: "rgba(200,0,0,0.8)",
    fontSize: 15,
  },
  imageBesideStyle: {
    flex: 2,
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "center",
  },
  imageBesideTextStyle: {
    color: "white",
    fontWeight: "bold",
    marginBottom: 5,
    fontSize: 16,
  },
});
