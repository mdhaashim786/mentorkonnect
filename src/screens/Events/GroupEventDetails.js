/* Functionality: For dispalying all the details of a single mentoring event and for scheduling,
   rejecting, canceling, marking for completing and submitting the feedback */
import React from "react";
import {
  StyleSheet,
  Text,
  ActivityIndicator,
  View,
  ScrollView,
  TouchableOpacity,
  Linking,
  KeyboardAvoidingView,
  Dimensions,
} from "react-native";
import Feedback from "./Feedback";
import { Button } from "react-native-elements";
import { urls } from "./../../../env.json";
import firebase from "firebase";
import Constants from "expo-constants";
import { Toast } from "native-base";

export default class EventDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      mentee: null,
      mentor: null,
      menteeLoaded: false,
      mentorLoaded: false,
      materials: [],
      profile_uid:null,
      materialsCount: 0,
      alreadyRegistered: false, //to check if the mentee has already registered for a particular Gevent
    };
  }

  componentDidMount() {
    const event = this.props.navigation.getParam("event");
    const profile = this.props.navigation.getParam("profile");
    this.setState({profile_uid:profile.uid})
    let userId = event.mentor_id;
    const url = `${urls.getRole}?id=${userId}`;
    fetch(url, { method: "GET" })
      .then((res) => res.json())
      .then((res) => {
        console.log(event.mentor_id);
        if (event.mentor_id != undefined) {
          if (res.role === "alumni") {
            this.loadMentor(event.mentor_id, "mentor");
            console.log("mentor");
          } else {
            this.loadMentor(event.mentor_id, "mentee");
            console.log("mentee");
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });

    let user = event.mentee_id;

    const url1 = `${urls.getRole}?id=${user}`;
    fetch(url1, { method: "GET" })
      .then((res) => res.json())
      .then((res) => {
        console.log("RESPONSE", res);
        console.log(event.mentee_id);
        if (event.mentor_id != undefined) {
          if (res.role === "college_admin") {
            this.loadMentee(event.mentee_id, "college_admin");
            console.log("mentor");
          } else {
            this.loadMentee(event.mentee_id, "mentee");
            console.log("mentee");
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });

    // this.loadMentee(event.mentee_id, "mentee");

    //  Materials are stored as object in firebase database. So here we are storing them
    // in array to display them using map()
    let materials = [];
    let materialsCount = 0;
    if (event.materials != null) {
      for (let key in event.materials) {
        materials.push(event.materials[key]);
        materialsCount++;
      }
    }
    this.setState({
      materials: materials,
      materialsCount: materialsCount,
    });
    if (event.registered_mentees !== undefined) {
      for (let i = 0; i < event.registered_mentees.length; i++) {
        if (event.registered_mentees[i] == profile.uid) {
          /*  we are checking whether the mentee has already registered in a GEvent */
          this.setState({ alreadyRegistered: true }); //if already registered the flag is set to true
          break;
        }
      }
    }
  }

  componentWillUnmount() {
    if (this.willFocusSubscription != undefined)
      this.willFocusSubscription.remove();
  }

  loadMentee = async (userId, type) => {
    const url = `${urls.loadProfile}?id=${userId}&userType=${type}`;
    await fetch(url,{
      method:"GET"
    })
      .then((response) => response.json())
      .then(async (response) => {
        this.setState({
          mentee: response.profile,
          menteeLoaded: true,
        });
      })
      .catch((err) => {
        console.error("Error", err);
      });
  };

  loadMentor = async (userId, type) => {
    const url = `${urls.loadProfile}?id=${userId}&userType=${type}`;
    await fetch(url,{
      method:"GET"
    })
      .then((response) => response.json())
      .then((response) => {
        this.setState({
          mentor: response.profile,
          mentorLoaded: true,
        });
      })
      .catch((err) => {
        console.error("Error", err);
      });
  };

  updateRegisteredGEvent = (eventObject) => {
    this.setState({ loading: true });
    /*to update the details of the mentee in  registered_mentees[] and the automated mails are send to both admin and that particular mentee.*/
    const userType = this.props.navigation.getParam("userType");
    const profile = this.props.navigation.getParam("profile");
    eventObject = {
      ...eventObject,
      last_updated_by: userType,
    };
    const url = `${urls.updateRegisteredGEvent}?email=${profile.email}&name=${profile.name}`;
    return fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(eventObject),
    })
      .then((response) => response.json())
      .then((response) => {
        // console.log(response.message);
        this.setState({ alreadyRegistered: true, loading: false });
      })
      .catch((err) => {
        console.error("Error", err); // error
      });
  };
  register = (event, profile) => {
    /*  const subject = "I want to register in " + event.event_name + " mentoring event";
             Linking.openURL(
               'mailto:jntukmentoringassociation@gmail.com?subject=' + subject + `&body=Instructions: Enter your WhatsApp number for further updates: `
             ) --->Removed this functionality*/
    event.registered_mentees.push(profile.uid); //pushing the registered mentee uid into the array
    //console.log(event.registered_mentees)

    this.updateRegisteredGEvent(event); //calling the fuction to update in database and send mails to  mentee and admin
  };

  render() {
    const event = this.props.navigation.getParam("event");
    const profile = this.props.navigation.getParam("profile");
    const isMentor = this.props.navigation.getParam("isMentor");
    let userType = profile.user_type;
    // If mentee is also a mentor and when he opens his events in which he is a mentor then
    // he need to see mentor options instead of mentee options
    if (isMentor == true) userType = "mentor";
    // We get userType based on who logged in from AsyncStorage.
    return (
      <KeyboardAvoidingView behavior="height" style={styles.container}>
        <ScrollView keyboardShouldPersistTaps="handled">
          {/* For displaying event_id, mentor and mentee names */}
          <View style={styles.eventIdContainer}>
            {/* Event ID */}
            <View style={styles.eventIdTextContainer}>
              <Text style={styles.titleStyle}>Event Id : </Text>
              <Text style={{ ...styles.fieldStyle, marginBottom: 0 }}>
                {event.event_id}
              </Text>
            </View>
            {/* Mentor name */}
            <View style={styles.eventIdTextContainer}>
              <Text style={styles.titleStyle}>Mentor : </Text>
              {this.state.mentorLoaded &&
                !(userType == "mentee" && event.status == "Rejected") && (
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("MentorDetailsRoute", {
                        mentor: this.state.mentor,
                        // Sending admin as true so that connect button will not display
                        admin: true,
                      });
                    }}
                  >
                    <Text style={styles.fieldLinkStyle}>
                      {this.state.mentor != null ? this.state.mentor.name : ""}
                    </Text>
                  </TouchableOpacity>
                )}
            </View>
            {/* Mentee name */}
            <View style={styles.eventIdTextContainer}>
              <Text style={styles.titleStyle}>Mentee : </Text>
              {this.state.mentee != null && this.state.menteeLoaded && (
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("MenteeDetailsRoute", {
                      mentee: this.state.mentee,
                    });
                  }}
                >
                  <Text style={styles.fieldLinkStyle}>
                    {this.state.mentee.college_name.substring(0, 22)}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
            {/* If the mentee has already registered we have to provide googleclass Room Link */}
            {event.group_event && this.state.alreadyRegistered && (
              <View style={styles.eventIdTextContainer}>
                <Text style={styles.titleStyle}>Google ClassRoom : </Text>

                <View>
                  {event.google_classroom_code &&
                    event.google_classroom_code != undefined && (
                      <Text
                        style={styles.fieldLinkStyle}
                        onPress={() => {
                          Linking.openURL(event.google_classroom_link);
                        }}
                      >
                        {event.google_classroom_code}
                      </Text>
                    )}
                </View>
              </View>
            )}
          </View>

          {/*  For dispalying event details */}
          <View style={styles.eventDetailsContainer}>
            <Text style={styles.titleStyle}>Event name</Text>
            <Text style={styles.fieldStyle}>{event.event_name}</Text>
            <Text style={styles.titleStyle}>Specific learning objective</Text>
            <Text style={styles.fieldStyle} selectable={true}>
              {event.event_summary}
            </Text>
            <Text style={styles.titleStyle}>Pre-work need to be done</Text>
            <Text style={styles.fieldStyle} selectable={true}>
              {event.prework_done}
            </Text>
            {event.group_event &&
              this.state.alreadyRegistered &&
              event.google_classroom_code &&
              event.google_classroom_code != undefined && (
                <View>
                  <Text style={styles.titleStyle}>Google Classroom Link</Text>
                  <Text style={styles.fieldStyle} selectable={true}>
                    {event.google_classroom_link}
                  </Text>
                </View>
              )}

            {/*  For displaying schedule preferences if provided */}
            {event.schedule_preferences !== undefined && (
              <View>
                <Text style={styles.titleStyle}>Timings</Text>
                <Text style={styles.fieldStyle}>
                  {event.schedule_preferences}
                </Text>
              </View>
            )}

            {/*  For displaying additional comments of mentee if provided */}
            {event.additional_details_mentee !== undefined && (
              <View>
                <Text style={styles.titleStyle}>
                  Addtional details by mentee
                </Text>
                <Text style={styles.fieldStyle} selectable={true}>
                  {event.additional_details_mentee}
                </Text>
              </View>
            )}

            {/*  For displaying additional comments of mentor if provided */}
            {event.additional_details_mentor !== undefined && (
              <View>
                <Text style={styles.titleStyle}>
                  Addtional details by mentor
                </Text>
                <Text style={styles.fieldStyle} selectable={true}>
                  {event.additional_details_mentor}
                </Text>
              </View>
            )}

            {/* Dispalying event date when it is scheduled */}
            {event.status != "Open" &&
              event.status != "Cancelled" &&
              event.status != "Rejected" && (
                <View>
                  <Text style={styles.titleStyle}>Event date</Text>
                  <Text style={styles.fieldStyle}>{event.event_date}</Text>
                </View>
              )}

            {/* Dispalying event mode of mentoring when it is scheduled */}
            {event.status != "Open" &&
              event.status != "Cancelled" &&
              event.status != "Rejected" && (
                <View>
                  <Text style={styles.titleStyle}>Mode of mentoring</Text>
                  <Text selectable={true} style={styles.fieldStyle}>
                    {event.mode_of_mentoring}
                  </Text>
                </View>
              )}

            {/* Displaying materials when event is in waiting for feedback or completed state
                            Here materials are only displayed and cannot be added */}
            {event.status != "Open" &&
              (event.status != "Scheduled" || this.state.completed) &&
              event.status != "Rejected" &&
              this.state.materials.length != 0 && (
                <View style={{ marginBottom: 5 }}>
                  <Text style={styles.titleStyle}>
                    Materials reviewed during the event
                  </Text>
                  {this.state.materials.map((item, key) => (
                    <View key={item} style={{ paddingEnd: 20 }}>
                      <Text
                        selectable={true}
                        style={{ ...styles.fieldStyle, marginBottom: 5 }}
                      >
                        {item}
                      </Text>
                    </View>
                  ))}
                </View>
              )}

            {/* Displaying cancel reason when the event is cancelled */}
            {event.status == "Cancelled" && (
              <View>
                <Text style={styles.titleStyle}>Cancel Reason</Text>
                <Text style={styles.fieldStyle}>{event.cancel_reason}</Text>
              </View>
            )}

            {/* Displaying feedback when the event is completed */}
            {event.status == "Completed" && (
              <View>
                <Text style={styles.titleStyle}>Feedback</Text>
                <Text style={styles.fieldStyle}>{event.feedback}</Text>
                <Text style={styles.titleStyle}>Ratings</Text>
                <Feedback ratings={event.ratings} status={event.status} />
              </View>
            )}
            <View style={{ flexDirection: "row", justifyContent: "center" }}>
              {/* this button will be showed only if the person is not registered */}
              {userType !== "mentor" && event.mentor_id!=this.state.profile_uid &&
                (event.status == "Open" || event.status == "Scheduled") &&
                !this.state.alreadyRegistered && (
                  <View>
                    <Button
                      title="Register"
                      containerStyle={styles.buttonContainer}
                      onPress={async () => {
                        if (
                          Constants.deviceId != this.state.device_id &&
                          this.state.device_id != undefined
                        ) {
                          // //signout

                          await firebase
                            .auth()
                            .signOut()
                            .then(() => {
                              this.props.navigation.navigate("AuthScreen");
                            })
                            .then(() => {
                              Toast.show({
                                text:
                                  "You have been logged out as you have logged in another device!  ",
                                buttonText: "Okay",
                              });
                            })
                            .catch((err) => {
                              console.log(err);
                            });
                        } else {
                          this.register(event, profile);
                        }
                      }}
                    />
                  </View>
                )}
              {this.state.loading && <ActivityIndicator size="large" />}
            </View>

            {/* if the mentee has already registered then he should be provided with the text that he will be soon added into google classroom*/}
            {this.state.alreadyRegistered && (
              <View>
                <Text style={{ color: "green", fontWeight: "bold" }}>
                  Your request for registering in this Group event has been
                  sent!!<Text style={{ color: "red" }}> HAPPY LEARNING</Text>
                </Text>
              </View>
            )}
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
    paddingBottom: 5,
  },
  eventIdContainer: {
    backgroundColor: "#C9E1F4",
    padding: 15,
  },
  eventIdTextContainer: {
    flexDirection: "row",
    marginBottom: 3,
  },
  eventDetailsContainer: {
    margin: 10,
    alignContent: "space-around",
    marginStart: 10,
  },
  titleStyle: {
    fontSize: 20,
    fontWeight: "bold",
  },
  fieldStyle: {
    fontSize: 18,
    marginBottom: 7,
  },
  fieldLinkStyle: {
    fontSize: 19,
    marginBottom: 3,
    textDecorationLine: "underline",
    color: "blue",
  },
  codeLinkStyle: {
    fontSize: 18,
    marginBottom: 3,
    textDecorationLine: "underline",
    color: "blue",
    marginTop: -4,
  },
  buttonContainer: {
    alignSelf: "center",
    width: Dimensions.get("window").width * 0.4,
    marginTop: 30,
    marginBottom: 10,
  },
});
