export default PreBuiltEventTypes = [
  (TYPE0 = {
    id: 0,
    name: "GATE",
  }),
  (TYPE1 = {
    id: 1,
    name: "GRE",
  }),
  (TYPE2 = {
    id: 2,
    name: "CAT",
  }),
  (TYPE3 = {
    id: 3,
    name: "Placement",
  }),
  (TYPE4 = {
    id: 4,
    name: "GMAT",
  }),
];
