// Functionality: For displaying the group events for mentee
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  RefreshControl,
  ActivityIndicator,
  AsyncStorage,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import EventCard from "./EventCard";
import { urls } from "./../../../env.json";
export default class LaunchScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      requestsList: [],
      requestsLoaded: false,
      refreshing: false,
      profile: null,
      search: "",
      status: false,
    };
  }

  // getData = async () => {
  //   let requests = [];
  //   AsyncStorage.getItem("profile")
  //     .then((data) => {
  //       this.setState({
  //         profile: JSON.parse(data),
  //       });
  //     })
  //     .then(async () => {
  //       const URL = urls.getGroupEvents;
  //       await fetch(URL)
  //         .then((response) => response.json())
  //         .then(async (response) => {
  //           await this.setState({
  //             requestsList: response.requests,
  //             searchData: response.requests,
  //           });
  //           await this.setState({
  //             requestsList: this.state.requestsList.filter(
  //               (item) =>
  //                 item.status == "Scheduled" &&
  //                 item.mentor_id != this.state.profile.uid
  //             ),
  //           });
  //           // console.log("req", this.state.requestsList)
  //         })
  //         .then(() => {
  //           this.setState({ requestsLoaded: true });
  //         })
  //         .catch((error) => console.log(error));
  //     })
  //     .catch((error) => console.log(error));
  // };

  getProfile = async () => {
    AsyncStorage.getItem("profile")
      .then((data) => {
        return JSON.parse(data);
      })
      .then(async (data) => {
        // console.log("Came here1", data);
        let id;
        if (data.user_type == "mentee") {
          id = data.uid;
        } else {
          id = data.uid;
        }
        const URL = `${urls.loadProfile}?id=${id}&userType=${data.user_type}`;
        await fetch(URL, {
          method: "GET",
        })
          .then((response) => response.json())
          .then(async (response) => {
            await AsyncStorage.setItem(
              "profile",
              JSON.stringify(response.profile)
            );
            await this.setState({ profile: response.profile });
          })
          .then(() => {
            // Get requests only when mentee/mentor is approved by council team
            if (this.state.profile.user_type === "license_mentee") {
              if (
                this.state.profile.status === "approved" &&
                this.state.profile.super_admin_verified === true &&
                this.state.profile.License_code_verified == true
              ) {
                console.log("get called");
                this.getMenteeData();
                this.setState({ status: true });
              } else this.setState({ requestsLoaded: true });
            } else {
              if (this.state.profile.status === "approved") {
                this.getMenteeData();
                this.setState({ status: true });
              } else this.setState({ requestsLoaded: true });
            }
          })
          .catch((err) => console.log(err));
      })
      .catch((err) => console.log(err));
  };
  getMenteeData = async () => {
    let requests = [];
    AsyncStorage.getItem("profile")
      .then((data) => {
        this.setState({
          profile: JSON.parse(data),
        });
        return JSON.parse(data);
      })
      .then(async (d) => {
        let obj = {
          college: d.college_id,
          institute: d.institution_id,
        };
        const URL = urls.getCollegeGroupEvents;
        await fetch(URL, {
          method: "POST",
          body: JSON.stringify(obj),
          headers: {
            "Content-type": "application/json; charset=UTF-8",
          },
        })
          .then((response) => response.json())
          .then(async (response) => {
            console.log(response.requests);
            await this.setState({
              requestsList: response.requests,
              searchData: response.requests,
            });
            await this.setState({
              requestsList: this.state.requestsList.filter(
                (item) => item.status == "Scheduled" //&&
                //item.mentor_id != this.state.profile.uid
              ),
            });
            console.log("req", this.state.requestsList);
          })
          .then(() => {
            this.setState({ requestsLoaded: true });
          })
          .catch((error) => console.log(error));
      })
      .catch((error) => console.log(error));
  };

  componentDidMount() {
    this.getProfile();
    // Adding listener, by doing this whenever launch screen is opened getData() method
    // will be called
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getProfile();
      }
    );
  }

  componentWillUnmount() {
    if (this.willFocusSubscription != undefined)
      this.willFocusSubscription.remove();
  }

  _onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.getProfile()
      //this.getData()
      .then(() => {
        this.setState({ refreshing: false });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }
        >
          {this.state.requestsLoaded && (
            <View>
              {/* Displaying mentee requests when mentee logged in */}
              {this.state.requestsList.length != 0 && (
                <View>
                  {(this.state.profile.user_type == "mentee" ||
                    this.state.profile.user_type == "license_mentee") && (
                    <View>
                      <Text style={styles.titleStyle}>
                        If you are interested in any of these following events,
                        click on the event, review details and register.
                      </Text>
                    </View>
                  )}
                  {this.state.requestsList.map((item, index) => {
                    return (
                      <EventCard
                        event={item}
                        navigation={this.props.navigation}
                        profile={this.state.profile}
                        isMentor={false}
                        key={item.event_id}
                        groupEvent={true}
                      />
                    );
                  })}
                </View>
              )}
            </View>
          )}
        </ScrollView>

        {/* Requests loaded but there are no events to show */}
        {this.state.requestsLoaded && (
          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled"
          >
            {this.state.requestsList.length == 0 && this.state.status === true && (
              <View style={styles.msgContainer}>
                <Text>No group events to show</Text>
              </View>
            )}
            {/* When mentee/mentor not approved by council team */}
            {this.state.profile &&
              this.state.profile.status === "not_approved" &&
              this.state.profile.user_type === "mentee" && (
                <View style={styles.msgContainer}>
                  <Text>
                    Your application as mentee is under review by our Mentoring
                    association.
                  </Text>
                </View>
              )}
            {this.state.profile &&
              this.state.profile.user_type === "license_mentee" &&
              this.state.profile.super_admin_verified === false &&
              this.state.profile.status === "approved" && (
                <View style={styles.msgContainer}>
                  <Text>
                    Activate your profile with the License code sent by your
                    College Admin! and please wait untill the Mentoring
                    association validates your profile.
                  </Text>
                </View>
              )}
            {this.state.profile &&
              this.state.profile.user_type === "license_mentee" &&
              this.state.profile.status === "not_approved" && (
                <View style={styles.msgContainer}>
                  <Text>
                    Your application as mentee is under review by your college
                    admin.
                  </Text>
                </View>
              )}
          </ScrollView>
        )}

        {/* If requests not loaded display ActivityIndicator */}
        {!this.state.requestsLoaded && (
          <View style={styles.msgContainer}>
            <ActivityIndicator size={40} />
          </View>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  msgContainer: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    paddingHorizontal: 20,
  },
  titleStyle: {
    fontSize: 18,
    fontWeight: "bold",
    marginStart: 10,
    marginTop: 5,
  },
  floatingButtonContainer: {
    alignItems: "flex-end",
    position: "absolute",
    right: 20,
    bottom: 20,
    justifyContent: "flex-end",
  },
});
