/* Functionality: For dispalying all the details of a single mentoring event and for scheduling,
   rejecting, canceling, marking for completing and submitting the feedback */
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Alert,
  KeyboardAvoidingView,
  Dimensions,
  Linking,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { Button } from "react-native-elements";
import { urls } from "./../../../env.json";
import { Toast } from "native-base";

export default class EditEventDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mentee: null,
      mentor: null,
      event: null,
    };
  }

  componentDidMount() {
    const event = this.props.navigation.getParam("event");
    this.setState({
      additional_details_mentee: event.additional_details_mentee,
      additional_details_mentor: event.additional_details_mentor,
    });
  }

  // Used for updating whenever a change is made
  updateEvent = (eventObject) => {
    const userType = this.props.navigation.getParam("userType");
    eventObject = {
      ...eventObject,
      last_updated_by: userType,
    };
    const url = urls.updateEvent;
    return fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(eventObject),
    })
      .then((response) => response.json())
      .then((response) => {
        // console.log(response.message);
      })
      .catch((err) => {
        console.error("Error", err);
      });
  };

  done = () => {
    let event = this.props.navigation.getParam("event");
    event = {
      ...event,
      additional_details_mentor:
        this.state.additional_details_mentor != undefined
          ? this.state.additional_details_mentor.trim()
          : undefined,
      additional_details_mentee:
        this.state.additional_details_mentee != undefined
          ? this.state.additional_details_mentee.trim()
          : undefined,
    };
    // console.log(event);
    this.updateEvent(event);
    this.props.navigation.navigate("LaunchRoute");
  };

  render() {
    const event = this.props.navigation.getParam("event");
    const mentor = this.props.navigation.getParam("mentor");
    const mentee = this.props.navigation.getParam("mentee");
    const userType = this.props.navigation.getParam("userType");
    return (
      <KeyboardAvoidingView behavior="height" style={styles.container}>
        <ScrollView keyboardShouldPersistTaps="handled">
          {/* For displaying event_id, mentor and mentee names */}
          <View style={styles.eventIdContainer}>
            {/* Event ID */}
            <View style={styles.eventIdTextContainer}>
              <Text style={styles.titleStyle}>Event Id : </Text>
              <Text
                style={{ ...styles.fieldStyle, marginBottom: 0, marginTop: 2 }}
              >
                {event.event_id}
              </Text>
            </View>
            {/* Mentor name */}
            <View style={styles.eventIdTextContainer}>
              <Text style={styles.titleStyle}>Mentor : </Text>
              {mentor != undefined && mentor != null && (
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("MentorDetailsRoute", {
                      mentor: mentor,
                      // Sending admin as true so that connect button will not display
                      admin: true,
                    });
                  }}
                >
                  <Text style={styles.fieldLinkStyle}>
                    {mentor != null ? mentor.name : ""}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
            {/* Mentee name */}
            <View style={styles.eventIdTextContainer}>
              <Text style={styles.titleStyle}>Mentee : </Text>
              {mentee != null && (
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("MenteeDetailsRoute", {
                      mentee: mentee,
                    });
                  }}
                >
                  <Text style={styles.fieldLinkStyle}>{mentee.name}</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>

          {/*  For dispalying event details */}
          <View style={styles.eventDetailsContainer}>
            <Text style={styles.titleStyle}>Event name</Text>
            <Text style={styles.fieldStyle}>{event.event_name}</Text>
            <Text style={styles.titleStyle}>Specific learning objective</Text>
            <Text style={styles.fieldStyle} selectable={true}>
              {event.event_summary}
            </Text>
            <Text style={styles.titleStyle}>Pre-work done</Text>
            <Text style={styles.fieldStyle} selectable={true}>
              {event.prework_done}
            </Text>

            {/*  For displaying additional comments of mentee if provided */}
            {userType == "mentee" && (
              <View>
                <View>
                  <Text style={styles.titleStyle} selectable={true}>
                    Addtional details by mentee
                  </Text>
                  <TextInput
                    value={this.state.additional_details_mentee}
                    multiline={true}
                    selectTextOnFocus={true}
                    textAlignVertical="top"
                    onChangeText={(text) => {
                      this.setState({ additional_details_mentee: text });
                    }}
                    style={styles.fieldEditStyle}
                  ></TextInput>
                </View>
                {event.additional_details_mentor !== undefined && (
                  <View>
                    <Text style={styles.titleStyle} selectable={true}>
                      Addtional details by mentor
                    </Text>
                    <Text style={styles.fieldStyle}>
                      {event.additional_details_mentor}
                    </Text>
                  </View>
                )}
              </View>
            )}

            {/*  For displaying additional comments of mentor if provided */}
            {userType == "mentor" && (
              <View>
                <View>
                  <Text style={styles.titleStyle} selectable={true}>
                    Addtional details by mentor
                  </Text>
                  <TextInput
                    multiline={true}
                    numberOfLines={10}
                    selectTextOnFocus={true}
                    value={this.state.additional_details_mentor}
                    onChangeText={(text) => {
                      this.setState({ additional_details_mentor: text });
                    }}
                    textAlignVertical="top"
                    style={styles.fieldEditStyle}
                  ></TextInput>
                  {/* <Text style={styles.fieldStyle}>{event.additional_details_mentor}</Text> */}
                </View>
                {event.additional_details_mentee !== undefined && (
                  <View>
                    <Text style={styles.titleStyle} selectable={true}>
                      Addtional details by mentee
                    </Text>
                    <Text style={styles.fieldStyle}>
                      {event.additional_details_mentee}
                    </Text>
                  </View>
                )}
              </View>
            )}

            {/* Dispalying event date when it is scheduled */}
            {event.status != "Open" &&
              event.status != "Cancelled" &&
              event.status != "Rejected" && (
                <View>
                  <Text style={styles.titleStyle}>Event date</Text>
                  <Text style={styles.fieldStyle}>{event.event_date}</Text>
                </View>
              )}

            {/* Dispalying event mode of mentoring when it is scheduled */}
            {event.status != "Open" &&
              event.status != "Cancelled" &&
              event.status != "Rejected" && (
                <View>
                  <Text style={styles.titleStyle}>Mode of mentoring</Text>
                  <Text selectable style={styles.fieldStyle} selectable={true}>
                    {event.mode_of_mentoring}
                  </Text>
                </View>
              )}

            {/* Displaying rejected reason */}
            {event.status == "Rejected" && (
              <View>
                <Text style={styles.titleStyle}>Rejected Reason</Text>
                <Text style={styles.fieldStyle}>{event.reject_reason}</Text>
              </View>
            )}
            {/* Displaying materials when event is in waiting for feedback or completed state
                   Here materials are only displayed and cannot be added */}
            {event.status != "Open" &&
              (event.status != "Scheduled" || this.state.completed) &&
              this.state.materials.length != 0 && (
                <View style={{ marginBottom: 5 }}>
                  <Text style={styles.titleStyle}>
                    Materials reviewed during the event
                  </Text>
                  {this.state.materials.map((item, key) => (
                    <View key={item} style={{ paddingEnd: 20 }}>
                      <Text
                        selectable
                        style={{ ...styles.fieldStyle, marginBottom: 5 }}
                      >
                        {item}
                      </Text>
                    </View>
                  ))}
                </View>
              )}
            <View style={styles.buttonContainer}>
              <Button
                title="Done"
                onPress={() => {
                  this.done();
                }}
                containerStyle={styles.bigButton}
              />
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
    paddingBottom: 5,
  },
  eventIdContainer: {
    backgroundColor: "#C9E1F4",
    padding: 15,
  },
  eventIdTextContainer: {
    flexDirection: "row",
    marginBottom: 3,
  },
  eventDetailsContainer: {
    margin: 10,
    alignContent: "space-around",
    marginStart: 10,
  },
  addedMentorTextStyle: {
    fontSize: 20,
    marginBottom: 15,
    marginStart: 10,
  },
  titleStyle: {
    fontSize: 20,
    fontWeight: "bold",
  },
  fieldStyle: {
    fontSize: 18,
    marginBottom: 7,
  },
  fieldEditStyle: {
    fontSize: 17,
    padding: 5,
    paddingStart: 13,
    borderWidth: 1,
    marginBottom: 10,
    marginTop: 5,
    height: 150,
  },

  fieldLinkStyle: {
    fontSize: 19,
    marginBottom: 3,
    textDecorationLine: "underline",
    color: "blue",
  },
  fieldBoxStyle: {
    fontSize: 18,
    marginBottom: 15,
    borderWidth: 1,
    padding: 10,
    marginTop: 5,
  },
  fieldBoxLargeStyle: {
    height: 120,
    textAlignVertical: "top",
    borderWidth: 1,
    padding: 10,
    fontSize: 18,
    marginBottom: 10,
    marginTop: 5,
  },
  buttonContainer: {
    alignSelf: "center",
    marginTop: 30,
    marginBottom: 10,
  },
  doubleButtonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 10,
    marginTop: 10,
  },
  buttonText: {
    fontSize: 17,
    padding: 10,
  },
  mentorButtonContainer: {
    backgroundColor: "#03A9F4",
    borderRadius: 25,
    alignItems: "center",
    width: 175,
    marginBottom: 15,
    marginStart: 10,
    marginTop: 5,
  },
  smallButton: {
    width: Dimensions.get("window").width * 0.4,
  },
  bigButton: {
    width: Dimensions.get("window").width * 0.5,
  },
});
