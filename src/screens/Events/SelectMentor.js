// Functionality: Displaying all the mentors and providing checkbox to select a mentor for mentoring
import React from "react";
import {
  StyleSheet,
  FlatList,
  View,
  Text,
  StatusBar,
  Platform,
  ActivityIndicator,
  AsyncStorage,
} from "react-native";
import { SearchBar, Avatar } from "react-native-elements";
import { CheckBox } from "native-base";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/Ionicons";
import { Toast } from "native-base";
import { urls } from "./../../../env.json";
import PreBuiltEventTypes from "./../Events/PreBuiltEventTypes";
var event_index;
var event_type;
export default class SelectMentor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mentorsList: [],
      listLoaded: false,
      checked: false,
      checkedIndex: -1,
      searchindex: false,
      refreshing: false,
      eventType: "",
      search: "",
    };
  }
  SearchFilterFunction(text) {
    const newData = this.state.data.filter(function (item) {
      const nameData = item.name ? item.name.toUpperCase() : "".toUpperCase(); //checking if the search string is substring of any of the names of mentors.
      const textnameData = text.toUpperCase();
      if (nameData.indexOf(textnameData) != -1) {
        //console.log(nameData, textnameData, nameData.indexOf(textnameData));
        return nameData.indexOf(textnameData) > -1;
      } else {
        for (var i = 0; i < item.mentoring_interest_list.length; i++) {
          const itemData = item.mentoring_interest_list[i].name
            ? item.mentoring_interest_list[i].name.toUpperCase()
            : "".toUpperCase();
          const textData = text.toUpperCase();
          if (itemData.indexOf(textData) != -1) {
            return itemData.indexOf(textData) > -1;
          }
        }
      }
    });
    const preData = this.state.data.filter((item) => {
      if (
        item.pre_built_events_indices !== undefined &&
        item.pre_built_events_indices.length != 0
      ) {
        for (var x = 0; x < item.pre_built_events_indices.length; x++) {
          for (var y = 0; y < PreBuiltEventTypes.length; y++) {
            if (item.pre_built_events_indices[x] == y) {
              const XData = PreBuiltEventTypes[y].name
                ? PreBuiltEventTypes[y].name.toUpperCase()
                : "".toUpperCase();
              const tData = text.toUpperCase();
              //console.log(XData+"    "+tData);
              if (XData.indexOf(tData) != -1) {
                //console.log(XData.indexOf(tData) >-1);
                return XData.indexOf(tData) > -1;
              }
            }
          }
        }
      }
    });
    let searchArray = [...preData, ...newData]; //can contain values which can be duplicate
    let myset = new Set(searchArray); //removing the duplicate values
    this.setState({
      search: text,
      mentorsList: [...myset],
      searchindex: true,
    });
  }
  getMentors = () => {
    // console.log(filterType);
    let url = urls.getMentors;
    return fetch(url, {
      method: "GET",
    })
      .then((response) => {
        return response.json();
      })

      .then(async (responseJSON) => {
        //let mentors = this.state.mentorsList;
        let mentors = Array.from(responseJSON);
        // Adding id and isChecked property to each mentor object

        let profile = await AsyncStorage.getItem("profile").then((data) =>
          JSON.parse(data)
        );
        // console.log("prof", mentors);
        let idToRemove = null;

        if (profile.user_type == "mentee") {
          idToRemove = profile.uid;
          mentors = mentors.filter((item) => item.uid !== idToRemove);
        } else {
          idToRemove = profile.uid;
          mentors = mentors.filter((item) => item.uid !== idToRemove);
        }
        //console.log("AFTER REMOVING OUR ID" + mentors);
        let k = mentors.filter((item) => {
          if (
            item.user_type === "mentor" &&
            item.institution_id != profile.institution_id &&
            item.mentoring_interest_other_college !== undefined
          ) {
            return item.mentoring_interest_other_college === true;
          }
        });

         let l = mentors.filter((item) => {
          if (
            item.user_type === "mentor" &&
            item.college_id != profile.college_id &&
            item.institution_id == profile.institution_id &&
            item.mentoring_interest_other_college !== undefined
          ) {
            return item.mentoring_interest_other_college === true;
          }
        });
        l = l.filter((item) => item.display_profile === true);
        //console.log("whose mentoring interest to other clg is true" + k);

        k = k.filter((item) => item.display_profile == true);

        mentors = mentors.filter(
          (item) =>
            item.college_id == profile.college_id &&
            item.institution_id === profile.institution_id
        );

        mentors = mentors.filter((item) => item.display_profile === true);

        mentors = [...mentors, ...k,...l];

        //console.log("Mentors List" + mentors);
        console.log("my" + event_type);
        if (event_type != undefined) {
          if (event_type !== "Others" && event_type !== null) {
            mentors = mentors.filter((item) => {
              if (
                typeof item.pre_built_events_indices !== null &&
                item.pre_built_events_indices !== undefined &&
                item.pre_built_events_indices.length != 0
              ) {
                if (item.pre_built_events_indices.includes(event_index)) {
                  return true;
                }
              }
            });
          }
        }
        let id = 0;
        mentors = mentors.map((data) => {
          data = { ...data, isChecked: false, checkId: id++ };
          return data;
        });

        this.setState({
          data: mentors,
          mentorsList: mentors,
          listLoaded: true,
          refreshing: false,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  componentDidMount = async () => {
    event_index = this.props.navigation.getParam("eventIndex");
    event_type = this.props.navigation.getParam("eventType");

    await this.getMentors();
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.getMentors();
      }
    );
  };
  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }
  _onRefresh = async () => {
    this.setState({ refreshing: true, search: "" });
    await this.componentDidMount();
  };
  toggleCheck = (index) => {
    let foundIndex = -1;
    let mentors = null;
    if (!this.state.searchindex) {
      mentors = this.state.mentorsList;
    } else {
      // this.toggleCheck1(index);
      mentors = this.state.mentorsList;
      this.state.searchindex = false;
    }
    // Finding the index of selected mentor
    // for (let i = 0; i < mentors.length; i++) {
    //   if (mentors[i].checkId == index)
    //     foundIndex = i;
    // }

    foundIndex = index;
    // If no mentor is selected, check the selected mentor.
    if (!this.state.checked) {
      mentors[foundIndex].isChecked = !mentors[foundIndex].isChecked;
      this.setState({
        checked: true,
        checkedIndex: foundIndex,
        mentorsList: mentors,
      });
    } else if (this.state.checked && foundIndex == this.state.checkedIndex) {
      /* If a mentor is selected and again the same mentor is selected
       then we need to uncheck the mentor */
      mentors[foundIndex].isChecked = !mentors[foundIndex].isChecked;
      this.setState({
        checked: false,
        checkedIndex: -1,
        mentorsList: mentors,
      });
    } else {
      /* If a mentor is selected and user selects another then we need to
    uncheck the previous selected mentor and check the newly selected mentor */
      mentors[foundIndex].isChecked = !mentors[foundIndex].isChecked;
      mentors[this.state.checkedIndex].isChecked = !mentors[
        this.state.checkedIndex
      ].isChecked;
      this.setState({
        checked: true,
        checkedIndex: foundIndex,
        mentorsList: mentors,
      });
    }
  };

  done = () => {
    // If no mentor is selected then ask user to select a mentor
    if (this.state.checkedIndex == -1)
      Toast.show({ text: "Select a mentor", buttonText: "Okay" });
    else {
      // Send the selected mentor data to previous screen
      this.props.navigation.state.params.onGoBack(
        this.state.mentorsList[this.state.checkedIndex]
      );

      this.props.navigation.goBack();
    }
  };

  render() {
    if (this.state.listLoaded) {
      //&& this.state.mentorsList.length != 0
      return (
        <View style={styles.container}>
          {/* Displaying header with back and done button */}
          <View style={styles.headerStyle}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                name="md-arrow-back"
                color="#2F55C0"
                size={25}
                style={styles.headerIconStyle}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.done();
              }}
            >
              <Text style={styles.headerTextStyle}>Done</Text>
            </TouchableOpacity>
          </View>
          <SearchBar
            round
            inputContainerStyle={styles.searchContainer}
            showLoadingIcon={true}
            inputStyle={styles.searchinput}
            containerStyle={styles.searchSubContainer}
            searchIcon={{ size: 20 }}
            onChangeText={(text) => this.SearchFilterFunction(text)}
            onClear={(text) => this.SearchFilterFunction("")}
            placeholder="Enter name/interest to search..."
            value={this.state.search}
          ></SearchBar>
          {/* Displaying all mentors */}
          <FlatList
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            data={this.state.mentorsList}
            renderItem={({ item, index }) => {
              return (
                <View style={styles.listItemStyle}>
                  {/* Checkbox for selecting mentor */}
                  <View style={{ justifyContent: "center", marginRight: 20 }}>
                    <CheckBox
                      checked={item.isChecked}
                      onPress={() => {
                        this.toggleCheck(index);
                      }}
                    />
                  </View>
                  <Avatar
                    size={70}
                    rounded
                    source={{ uri: item.photo_url }}
                    icon={{ name: "user", type: "font-awesome" }}
                  />

                  {/* Displaying mentor name and his info */}
                  <View style={styles.cardRight}>
                    <View style={styles.cardRightSub}>
                      <Text style={styles.text}>{item.name}</Text>
                    </View>
                    {item.current_working_title != undefined &&
                      item.current_working_title != "" && (
                        <Text style={{ fontSize: 17, fontWeight: "500" }}>
                          {item.current_working_title}{" "}
                        </Text>
                      )}
                    <Text style={{ fontSize: 15, fontWeight: "bold" }}>
                      Mentoring Interests:
                    </Text>
                    {item.mentoring_interest_list.map((item1, index) => {
                      return (
                        <Text style={{ fontSize: 14 }} key={index}>
                          {item1.name}
                        </Text>
                      );
                    })}
                    {item.pre_built_events_indices != undefined &&
                      item.pre_built_events_indices.length != 0 &&
                      item.pre_built_events_indices.map((l, i) => (
                        <View key={i}>
                          {PreBuiltEventTypes.map((item, id) => (
                            <View key={id}>
                              {l == id && (
                                <Text style={styles.subTexttitle}>
                                  {item.name}
                                </Text>
                              )}
                            </View>
                          ))}
                        </View>
                      ))}
                  </View>
                  {/* Button for seeing mentor details */}
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("MentorDetailsRoute", {
                        mentor: item,
                        page: this.props.navigation.getParam("page"),
                        onGoBack: this.props.navigation.getParam("onGoBack"),
                      });
                    }}
                  >
                    <Icon
                      name="ios-arrow-forward"
                      color="black"
                      size={20}
                      style={styles.iconStyle}
                    />
                  </TouchableOpacity>
                </View>
              );
            }}
            keyExtractor={(item) => item.email}
            extraData={this.state}
          />
        </View>
      );
    } else {
      return (
        <View style={styles.indicator}>
          <ActivityIndicator size="large"></ActivityIndicator>
        </View>
      );
    }
  }
}
const styles = StyleSheet.create({
  indicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  searchSubContainer: {
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "white",
    borderBottomWidth: 0,
    borderTopWidth: 0,
  },
  searchinput: {
    color: "black",
  },
  searchContainer: {
    backgroundColor: "white",
    marginRight: 6,
    width: "100%",
    borderWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 5,
    marginTop: 5,
  },
  container: {
    backgroundColor: "#fff",
    flex: 1,
    paddingTop: Platform.OS == "ios" ? 20 : 0,
    paddingHorizontal: 3,
  },
  msgContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    width: "100%",
    backgroundColor: "#fff",
  },
  headerStyle: {
    marginTop: StatusBar.currentHeight,
    backgroundColor: "#fff",
    height: 60,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 0.5,
  },
  headerTextStyle: {
    fontSize: 20,
    color: "#2F55C0",
    justifyContent: "flex-end",
    marginEnd: 20,
    alignContent: "center",
  },
  headerIconStyle: {
    marginStart: 20,
  },
  cardRight: { flex: 1, alignItems: "flex-start", marginLeft: 15 },
  cardRightSub: { flexDirection: "row", alignItems: "center" },
  text: {
    fontWeight: "bold",
    textTransform: "capitalize",
    fontSize: 18,
    marginEnd: 5,
  },
  listItemStyle: {
    flexDirection: "row",
    alignItems: "center",
    margin: 5,
    borderRadius: 1,
    padding: 10,
    borderWidth: 0.5,
    marginBottom: 2,
  },

  iconStyle: {
    justifyContent: "flex-end",
    marginEnd: 35,
    marginTop: 10,
  },
});
