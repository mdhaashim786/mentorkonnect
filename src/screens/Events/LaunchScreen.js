// Functionality: For displaying the events of mentor/mentee
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  RefreshControl,
  ActivityIndicator,
  AsyncStorage,
} from "react-native";
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";
import EventCard from "./EventCard";
import { Avatar } from "react-native-elements";
import { urls } from "./../../../env.json";
import firebase from "firebase";
import Constants from "expo-constants";
import { Toast } from "native-base";

export default class LaunchScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      requestsList: [], //contains events when logged as mentee
      requestsOpenList: [], //contains opened events when logged as mentee
      requestsScheduledList: [], //contains scheduled events when logged as mentee
      requestsCompletedList: [], //contains Completed events when logged as mentee
      requestsRejectedList: [], //contains Rejected events when logged as mentee
      requestsCancelledList: [], //contains Cancelled events when logged as mentee
      requestsWaitingForFeedBackList: [], //contains waiting for feedback events when logged as mentee
      menteeRequestsList: [], //contains events when logged as mentee but intrested as mentor( acting as mentor for these events)
      menteeScheduledRequestsList: [], //contains Scheduled events when logged as mentee but intrested as mentor( acting as mentor for these events)
      menteeOpenRequestsList: [], //contains Opened events when logged as mentee but intrested as mentor( acting as mentor for these events)
      menteeRejectedRequestsList: [], //contains Rejected events when logged as mentee but intrested as mentor( acting as mentor for these events)
      menteeCompletedRequestsList: [], //contains Completed events when logged as mentee but intrested as mentor( acting as mentor for these events)
      menteeWaitingForFeedBackRequestsList: [], //contains  waiting for  feedback events  when logged as mentee but intrested as mentor( acting as mentor for these events)
      mentorRequestsList: [], //contains events when logged as mentor
      mentorScheduledRequestsList: [], //contains Scheduled events when logged as mentor
      mentorOpenRequestsList: [], //contains Opened events when logged as mentor
      mentorRejectedRequestsList: [], //contains Rejected events when logged as mentor
      mentorCompletedRequestsList: [], //contains Completed events when logged as mentor
      mentorWaitingForFeedBackRequestsList: [], //contains  waiting for  feedback  events when logged as mentor
      requestsLoaded: false,
      menteeRequestsLoaded: false,
      refreshing: false,
      profile: null,
      search: "",
      status: false,
    };
  }
  Menteeeventlistener = async (id, userType) => {
    let filter = null;
    if (userType === "mentor") filter = "mentor_id";
    else filter = "mentee_id";
    var callfunction = (value) => {
      this.getMenteeRequests(value);
    };

    await firebase
      .database()
      .ref("events/")
      .orderByChild(filter)
      .equalTo(id)
      .on("value", function (snapshot) {
        let list = [];
        snapshot.forEach((item) => {
          list.push(item.val());
        });

        callfunction(list);
      });
  };

  getMenteeRequests = async (value) => {
    let response = value;

    await this.setState({
      menteeRequestsList: response,
    });
    await this.setState({
      // For mentors, no need to display cancelled events, so filtering them(helping mentees --> mentees acting as mentors)
      menteeRequestsList: this.state.menteeRequestsList.filter(
        (item) => item.status !== "Cancelled"
      ),
      menteeOpenRequestsList: this.state.menteeRequestsList.filter(
        (item) => item.status == "Open"
      ), //stores opened events
      menteeScheduledRequestsList: this.state.menteeRequestsList.filter(
        (item) => item.status == "Scheduled"
      ), //stores scheduled events
      menteeRejectedRequestsList: this.state.menteeRequestsList.filter(
        (item) => item.status == "Rejected"
      ), //stores rejected events
      menteeCompletedRequestsList: this.state.menteeRequestsList.filter(
        (item) => item.status == "Completed"
      ), //stores completed events
      menteeWaitingForFeedBackRequestsList: this.state.menteeRequestsList.filter(
        (item) => item.status == "Waiting for feedback"
      ), //stores waiting for feedback events)
    });

    this.setState({ menteeRequestsLoaded: true });
  };
  eventlistener = async (id, userType) => {
    let filter = null;
    if (userType === "mentor") filter = "mentor_id";
    else filter = "mentee_id";
    var callfunction = (value) => {
      this.getRequests(value);
    };
    await firebase
      .database()
      .ref("events/")
      .orderByChild(filter)
      .equalTo(id)
      .on("value", function (snapshot) {
        let list = [];
        snapshot.forEach((item) => {
          list.push(item.val());
        });
        callfunction(list);
      });
  };

  getRequests = async (value) => {
    // let requests = [];
    let response = value;
    console.log("requeest", response);

    await this.setState({
      requestsList: response,
      mentorRequestsList: response,
    });
    await this.setState({
      mentorRequestsList: this.state.mentorRequestsList.filter(
        (item) => item.status !== "Cancelled"
      ), //no need to show cancelled events-->alumni
      requestsScheduledList: this.state.requestsList.filter(
        (item) => item.status == "Scheduled"
      ), //to store scheduled events for the mentees
      requestsOpenList: this.state.requestsList.filter(
        (item) => item.status == "Open"
      ), //to store opened events for the mentees
      requestsRejectedList: this.state.requestsList.filter(
        (item) => item.status == "Rejected"
      ), //to store rejected events for the mentees
      requestsCompletedList: this.state.requestsList.filter(
        (item) => item.status == "Completed"
      ), //to store completed events for the mentees
      requestsWaitingForFeedBackList: this.state.requestsList.filter(
        (item) => item.status == "Waiting for feedback"
      ), //to store completed events for the mentees
      requestsCancelledList: this.state.requestsList.filter(
        (item) => item.status == "Cancelled"
      ), //to store cancelled events for the mentees
      mentorScheduledRequestsList: this.state.mentorRequestsList.filter(
        (item) => item.status == "Scheduled"
      ), //stores opened events-->alumni
      mentorOpenRequestsList: this.state.mentorRequestsList.filter(
        (item) => item.status == "Open"
      ), //stores opened events-->alumni
      mentorRejectedRequestsList: this.state.mentorRequestsList.filter(
        (item) => item.status == "Rejected"
      ), //stores rejected events-->alumni
      mentorCompletedRequestsList: this.state.mentorRequestsList.filter(
        (item) => item.status == "Completed"
      ), //stores completed events-->alumni
      mentorWaitingForFeedBackRequestsList: this.state.mentorRequestsList.filter(
        (item) => item.status == "Waiting for feedback"
      ), //stores waiting for feedback events-->alumni
    });

    this.setState({ requestsLoaded: true });
  };

  getData = async () => {
    let user_type = this.state.profile.user_type;

    // For alumni, user_type will be mentor,
    // Retrieving all the events for alumni
    if (user_type == "mentor")
      await this.eventlistener(this.state.profile.uid, user_type);
    // For mentee, user_type will be mentee
    // Here we need to retreive two type of events.
    // 1. Events in which he is mentee
    // 2. Events in which he is mentor
    else {
      await this.eventlistener(this.state.profile.uid, user_type);
      if (this.state.profile.mentoring_interested) {
        await this.Menteeeventlistener(this.state.profile.uid, "mentor");
      }
    }
  };
  listener = async () => {
    console.log("listener called");
    await AsyncStorage.getItem("profile")
      .then((data) => {
        return JSON.parse(data);
      })
      .then(async (data) => {
        let usertype;
        if (data.user_type === "mentor") {
          usertype = "mentors";
        } else {
          usertype = "mentees";
        }
        let id = data.uid;
        var callProfile = () => {
          this.getProfile();
        };

        await firebase
          .database()
          .ref(usertype + "/" + id)
          .on("value", function (snapshot) {
            let value = snapshot.val();

            AsyncStorage.setItem("profile", JSON.stringify(value)).then(() => {
              callProfile();
            });
          });
      });
  };
  getProfile = async () => {
    await AsyncStorage.getItem("profile")
      .then((data) => {
        return JSON.parse(data);
      })
      .then(async (data) => {
        await this.setState({ profile: data });
      })
      .then(() => {
        // Get requests only when mentee/mentor is approved by council team
        if (this.state.profile.user_type === "license_mentee") {
          if (
            this.state.profile.status === "approved" &&
            this.state.profile.super_admin_verified === true &&
            this.state.profile.License_code_verified == true
          ) {
            this.getData();
            console.log("getdata");
            this.setState({ status: true });
          } else this.setState({ requestsLoaded: true, status: false });
        } else {
          if (this.state.profile.status === "approved") {
            this.getData();
            this.setState({ status: true });
          } else this.setState({ requestsLoaded: true, status: false });
        }
      })
      .catch((err) => console.log(err));
  };
  componentDidMount() {
    this.listener();
    // Adding listener, by doing this whenever launch screen is opened getData() method
    // will be called
    this.willFocusSubscription = this.props.navigation.addListener(
      "willFocus",
      () => {
        this.listener();
        console.log("listener calling");
      }
    );
  }

  componentWillUnmount() {
    if (this.willFocusSubscription != undefined)
      this.willFocusSubscription.remove();
  }

  _onRefresh = () => {
    let user_type = this.state.profile.user_type;
    this.setState({
      refreshing: true,
    });
    this.eventlistener(this.state.profile.uid, user_type)

      .then(() => {
        this.setState({ refreshing: false });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }
        >
          {this.state.profile &&
            this.state.status === true &&
            this.state.requestsLoaded && (
              <View>
                {/* Displaying mentee requests when mentee logged in */}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.requestsList.length != 0 && (
                    <View>
                      <Text style={styles.titleStyle}>
                        Requested mentoring events
                      </Text>
                    </View>
                  )}
                {/*scheduled state*/}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.requestsScheduledList.length != 0 && (
                    <View>
                      {this.state.requestsScheduledList.map((item, index) => {
                        return (
                          <EventCard
                            event={item}
                            navigation={this.props.navigation}
                            profile={this.state.profile}
                            isMentor={false}
                            key={item.event_id}
                            groupEvent={false}
                          />
                        );
                      })}
                    </View>
                  )}
                {/*open state*/}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.requestsOpenList.length != 0 && (
                    <View>
                      {this.state.requestsOpenList.map((item, index) => {
                        return (
                          <EventCard
                            event={item}
                            navigation={this.props.navigation}
                            profile={this.state.profile}
                            isMentor={false}
                            key={item.event_id}
                            groupEvent={false}
                          />
                        );
                      })}
                    </View>
                  )}
                {/*rejected state which are converted as Open state in Event Card*/}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.requestsRejectedList.length != 0 && (
                    <View>
                      {this.state.requestsRejectedList.map((item, index) => {
                        return (
                          <EventCard
                            event={item}
                            navigation={this.props.navigation}
                            profile={this.state.profile}
                            isMentor={false}
                            key={item.event_id}
                            groupEvent={false}
                          />
                        );
                      })}
                    </View>
                  )}
                {/*waiting for feedback*/}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.requestsWaitingForFeedBackList.length != 0 && (
                    <View>
                      {this.state.requestsWaitingForFeedBackList.map(
                        (item, index) => {
                          return (
                            <EventCard
                              event={item}
                              navigation={this.props.navigation}
                              profile={this.state.profile}
                              isMentor={false}
                              key={item.event_id}
                              groupEvent={false}
                            />
                          );
                        }
                      )}
                    </View>
                  )}

                {/*Completed*/}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.requestsCompletedList.length != 0 && (
                    <View>
                      {this.state.requestsCompletedList.map((item, index) => {
                        return (
                          <EventCard
                            event={item}
                            navigation={this.props.navigation}
                            profile={this.state.profile}
                            isMentor={false}
                            key={item.event_id}
                            groupEvent={false}
                          />
                        );
                      })}
                    </View>
                  )}
                {/*Cancelled*/}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.requestsCancelledList.length != 0 && (
                    <View>
                      {this.state.requestsCancelledList.map((item, index) => {
                        return (
                          <EventCard
                            event={item}
                            navigation={this.props.navigation}
                            profile={this.state.profile}
                            isMentor={false}
                            key={item.event_id}
                            groupEvent={false}
                          />
                        );
                      })}
                    </View>
                  )}

                {/* Displaying mentor requests when mentor logged in */}
                {this.state.profile.user_type == "mentor" &&
                  this.state.mentorRequestsList.length != 0 && (
                    <View>
                      <Text style={styles.titleStyle}>Mentoring events</Text>
                    </View>
                  )}
                {/*scheduled*/}
                {this.state.profile.user_type == "mentor" &&
                  this.state.mentorScheduledRequestsList.length != 0 && (
                    <View>
                      {this.state.mentorScheduledRequestsList.map(
                        (item, index) => {
                          return (
                            <EventCard
                              event={item}
                              navigation={this.props.navigation}
                              profile={this.state.profile}
                              isMentor={false}
                              key={index}
                              groupEvent={false}
                            />
                          );
                        }
                      )}
                    </View>
                  )}
                {/*open*/}
                {this.state.profile.user_type == "mentor" &&
                  this.state.mentorOpenRequestsList.length != 0 && (
                    <View>
                      {this.state.mentorOpenRequestsList.map((item, index) => {
                        return (
                          <EventCard
                            event={item}
                            navigation={this.props.navigation}
                            profile={this.state.profile}
                            isMentor={false}
                            key={index}
                            groupEvent={false}
                          />
                        );
                      })}
                    </View>
                  )}
                {/*rejected*/}
                {this.state.profile.user_type == "mentor" &&
                  this.state.mentorRejectedRequestsList.length != 0 && (
                    <View>
                      {this.state.mentorRejectedRequestsList.map(
                        (item, index) => {
                          return (
                            <EventCard
                              event={item}
                              navigation={this.props.navigation}
                              profile={this.state.profile}
                              isMentor={false}
                              key={index}
                              groupEvent={false}
                            />
                          );
                        }
                      )}
                    </View>
                  )}
                {/*waiting for feedback*/}
                {this.state.profile.user_type == "mentor" &&
                  this.state.mentorWaitingForFeedBackRequestsList.length !=
                    0 && (
                    <View>
                      {this.state.mentorWaitingForFeedBackRequestsList.map(
                        (item, index) => {
                          return (
                            <EventCard
                              event={item}
                              navigation={this.props.navigation}
                              profile={this.state.profile}
                              isMentor={false}
                              key={index}
                              groupEvent={false}
                            />
                          );
                        }
                      )}
                    </View>
                  )}
                {/*completed*/}
                {this.state.profile.user_type == "mentor" &&
                  this.state.mentorCompletedRequestsList.length != 0 && (
                    <View>
                      {this.state.mentorCompletedRequestsList.map(
                        (item, index) => {
                          return (
                            <EventCard
                              event={item}
                              navigation={this.props.navigation}
                              profile={this.state.profile}
                              isMentor={false}
                              key={index}
                              groupEvent={false}
                            />
                          );
                        }
                      )}
                    </View>
                  )}
                {/* Displaying mentee as mentor requests when he selected mentoring interested */}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.profile.mentoring_interested &&
                  this.state.menteeRequestsLoaded &&
                  this.state.menteeRequestsList.length != 0 && (
                    <View style={{ marginBottom: 10 }}>
                      <Text style={styles.titleStyle}>Helping mentees...</Text>
                    </View>
                  )}
                {/*Scheduled */}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.profile.mentoring_interested &&
                  this.state.menteeRequestsLoaded &&
                  this.state.menteeScheduledRequestsList.length != 0 && (
                    <View style={{ marginBottom: 10 }}>
                      {this.state.menteeScheduledRequestsList.map(
                        (item, index) => {
                          if (
                            this.state.profile.user_type !== "mentor" &&
                            item.status != "Cancelled"
                          ) {
                            return (
                              <EventCard
                                event={item}
                                navigation={this.props.navigation}
                                profile={this.state.profile}
                                isMentor={true}
                                key={index}
                                groupEvent={false}
                              />
                            );
                          }
                        }
                      )}
                    </View>
                  )}
                {/*Open */}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.profile.mentoring_interested &&
                  this.state.menteeRequestsLoaded &&
                  this.state.menteeOpenRequestsList.length != 0 && (
                    <View>
                      {this.state.menteeOpenRequestsList.map((item, index) => {
                        if (
                          this.state.profile.user_type !== "mentor" &&
                          item.status != "Cancelled"
                        ) {
                          return (
                            <EventCard
                              event={item}
                              navigation={this.props.navigation}
                              profile={this.state.profile}
                              isMentor={true}
                              key={index}
                              groupEvent={false}
                            />
                          );
                        }
                      })}
                    </View>
                  )}
                {/*Rejected*/}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.profile.mentoring_interested &&
                  this.state.menteeRequestsLoaded &&
                  this.state.menteeRejectedRequestsList.length != 0 && (
                    <View>
                      {this.state.menteeRejectedRequestsList.map(
                        (item, index) => {
                          if (
                            this.state.profile.user_type !== "mentor" &&
                            item.status != "Cancelled"
                          ) {
                            return (
                              <EventCard
                                event={item}
                                navigation={this.props.navigation}
                                profile={this.state.profile}
                                isMentor={true}
                                key={index}
                                groupEvent={false}
                              />
                            );
                          }
                        }
                      )}
                    </View>
                  )}

                {/*waiting for feedback*/}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.profile.mentoring_interested &&
                  this.state.menteeRequestsLoaded &&
                  this.state.menteeWaitingForFeedBackRequestsList.length !=
                    0 && (
                    <View>
                      {this.state.menteeWaitingForFeedBackRequestsList.map(
                        (item, index) => {
                          if (
                            this.state.profile.user_type !== "mentor" &&
                            item.status != "Cancelled"
                          ) {
                            return (
                              <EventCard
                                event={item}
                                navigation={this.props.navigation}
                                profile={this.state.profile}
                                isMentor={true}
                                key={index}
                                groupEvent={false}
                              />
                            );
                          }
                        }
                      )}
                    </View>
                  )}
                {/*Completed*/}
                {this.state.profile.user_type !== "mentor" &&
                  this.state.profile.mentoring_interested &&
                  this.state.menteeRequestsLoaded &&
                  this.state.menteeCompletedRequestsList.length != 0 && (
                    <View>
                      {this.state.menteeCompletedRequestsList.map(
                        (item, index) => {
                          if (
                            this.state.profile.user_type !== "mentor" &&
                            item.status != "Cancelled"
                          ) {
                            return (
                              <EventCard
                                event={item}
                                navigation={this.props.navigation}
                                profile={this.state.profile}
                                isMentor={true}
                                key={index}
                                groupEvent={false}
                              />
                            );
                          }
                        }
                      )}
                    </View>
                  )}
              </View>
            )}
        </ScrollView>

        {/* Requests loaded but there are no events to show */}
        {this.state.requestsLoaded && (
          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled"
          >
            {/* Message to display when mentee logged in */}
            {this.state.profile.user_type !== "mentor" &&
              this.state.status === true &&
              this.state.requestsList.length == 0 &&
              this.state.menteeRequestsList.length == 0 && (
                <View style={styles.msgContainer}>
                  <Text>Click + button to add new mentoring event</Text>
                </View>
              )}
            {/* Message to dispaly when mentor logged in */}
            {this.state.profile.user_type == "mentor" &&
              this.state.profile.status == "approved" &&
              this.state.requestsList.length == 0 && (
                <View style={styles.msgContainer}>
                  <Text>You have no requests to show</Text>
                </View>
              )}
            {/* When mentee/mentor not approved by council team */}
            {this.state.profile &&
              this.state.profile.status === "not_approved" &&
              this.state.profile.user_type === "mentee" && (
                <View style={styles.msgContainer}>
                  <Text>
                    Your application as mentee is under review by our Mentoring
                    association.
                  </Text>
                </View>
              )}
            {this.state.profile &&
              this.state.profile.user_type === "license_mentee" &&
              this.state.profile.super_admin_verified === false &&
              this.state.profile.status === "approved" && (
                <View style={styles.msgContainer}>
                  <Text>
                    Activate your profile with the License code sent
                    by your College Admin! and please wait untill the Mentoring
                    association validates your profile.
                  </Text>
                </View>
              )}
            {this.state.profile &&
              this.state.profile.user_type === "license_mentee" &&
              this.state.profile.status === "not_approved" && (
                <View style={styles.msgContainer}>
                  <Text>
                    Your application as mentee is under review by your college
                    admin.
                  </Text>
                </View>
              )}
          </ScrollView>
        )}

        {/* If requests not loaded display ActivityIndicator */}
        {!this.state.requestsLoaded && (
          <View style={styles.msgContainer}>
            <ActivityIndicator size={40} />
          </View>
        )}

        {/* Dispalying Add event button. It should appear only for mentees */}
        {this.state.requestsLoaded &&
          this.state.status == true &&
          this.state.profile.user_type != "mentor" && (
            <View style={styles.floatingButtonContainer}>
              <Avatar
                size="medium"
                rounded
                title="+"
                onPress={async () => {
                  if (
                    Constants.deviceId != this.state.profile["device_id"] &&
                    this.state.profile["device_id"] != undefined
                  ) {
                    // //signout
                    await firebase
                      .auth()
                      .signOut()
                      .then(() => {
                        this.props.navigation.navigate("AuthScreen");
                      })
                      .then(() => {
                        Toast.show({
                          text:
                            "You have been logged out as you have logged in another device!  ",
                          buttonText: "Okay",
                        });
                      })
                      .catch((err) => {
                        console.log(err);
                      });
                  } else {
                    this.props.navigation.navigate("AddMentorRoute", {
                      profile: this.state.profile,
                    });
                  }
                }}
                overlayContainerStyle={{ backgroundColor: "#106CC8" }}
              />
            </View>
          )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  msgContainer: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    paddingHorizontal: 20,
  },
  titleStyle: {
    fontSize: 20,
    fontWeight: "bold",
    marginStart: 10,
    marginTop: 5,
  },
  floatingButtonContainer: {
    alignItems: "flex-end",
    position: "absolute",
    right: 20,
    bottom: 20,
    justifyContent: "flex-end",
  },
});
