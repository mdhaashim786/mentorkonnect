// Functionality: Displaying events as cards on Launch screen
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class EventCard extends React.Component {

  render() {
    const { event } = this.props;
    const { profile } = this.props;
    const { isMentor } = this.props;
    let description = event.event_summary;
    let status = event.status;
    let statusColor = null;

    // Changing date from epoch to normal
    let timestamp = event.date_of_last_action;
    let date = new Date(timestamp);

    // We get userType based on who logged in from AsyncStorage
    let userType = profile.user_type;

    // If length of description is greater than 25, displaying ellipsis
    if (description.length > 20) {
      description = description.substring(0, 20);
      description += "...";
    }

    // For mentee we need to show status as open even though the request is rejected.
    if (isMentor == false && userType == "mentee" && status == "Rejected")
      status = "Open";

    // Displaying colors based on the event status
    if (status == "Open")
      statusColor = "#FFA726";
    else if (status == "Scheduled")
      statusColor = "#03D1A4";
    else if (status == "Rejected")
      statusColor = "#EF5350";
    else if (status == "Waiting for feedback")
      statusColor = "#90A4AE";
    else if (status == "Completed")
      statusColor = "#2E7D32";
    else if (status == "Cancelled")
      statusColor = "#E57373";

    // Checking whether it is group event or not and navigating accordingly to the eventdetails screen
    const { groupEvent } = this.props;

    return (
      <View style={styles.container}>
        {/* When event card is clicked, navigating to event details */}
        <TouchableOpacity
          onPress={() => {
            if (groupEvent == false) {
              this.props.navigation.navigate("EventDetailsRoute", {
                event: event,
                profile: profile,
                isMentor: isMentor
              })
            }
            else {
              this.props.navigation.navigate("GroupEventDetailsRoute", {
                event: event,
                profile: profile,
                isMentor: isMentor
              })
            }
          }}>

          {/* Displaying color strip on top */}
          <View style={{ backgroundColor: statusColor, elevation: 2 }}>
            <Text> </Text>
          </View>

          {/* Dispalying event_id, name, summary, status, last_action_date */}
          <View style={styles.cardStyle}>
            <Text style={styles.titleStyle}>{event.event_name}</Text>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.fieldBoldStyle}>Event ID: </Text>
              <Text style={styles.fieldStyle}>{event.event_id}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.fieldBoldStyle}>Description: </Text>
              <Text style={styles.fieldStyle}>{description}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.fieldBoldStyle}>Status: </Text>
              <Text style={{ ...styles.fieldBoldStyle, color: statusColor, }}>{status}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.fieldBoldStyle}>Last action date: </Text>
              <Text style={styles.fieldStyle}>{date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear()}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View >
    )
  }
}
const styles = StyleSheet.create({
  container: {
    margin: 7,
    elevation: 2,
    borderWidth: 0.5,
    borderTopWidth: 0
  },
  cardStyle: {
    paddingHorizontal: 15,
    paddingBottom: 10,
    paddingTop: 10,
    alignItems: "flex-start",
    alignContent: "space-between",
  },
  titleStyle: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  fieldBoldStyle: {
    fontSize: 18,
    marginBottom: 3,
    color: "#212121",
    fontWeight: "700"
  },
  fieldStyle: {
    fontSize: 18,
    marginBottom: 3,
    color: "#212121"
  },
});