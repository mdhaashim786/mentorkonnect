import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  ScrollView,
  Platform,
  Image,
  Text,
  Picker,
  TextInput,
  KeyboardAvoidingView,
  Alert,
} from "react-native";
import { Button, CheckBox, Avatar, Icon } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import { Notifications } from "expo";
import DatePicker from "react-native-datepicker";
import firebase from "firebase";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import { Toast } from "native-base";
import { urls } from "../../../env.json";
import Logos from "../../../assets/Logos";
import { FlatList } from "react-native-gesture-handler";
import PreBuiltEventTypes from "./../Events/PreBuiltEventTypes";
export default class NonAlumniUserLogin extends Component {
  constructor(props) {
    super(props);
    this.interestInputRef = React.createRef();
    this.multiSelect = React.createRef();
    this.state = {
      loading: false,
      interestmanip: false,
      mentoringInterested: false,
      image: Logos.defaultProfilePic,
      available: "",
      linkedIn: "",
      about: "",
      college_after_BTECH: "",
      collegeName: "None",
      professor: "",
      specialization: "",
      longTermGoal: "",
      credential: "",
      name: "",
      course: "None",
      yearOfGraduation: "",
      gpa: "",
      phoneNumber: "",
      result: "",
      subject: "",
      mentoringInterest: "",
      interest: "",
      activity: "",
      shortTermGoal: "",
      institutionName: "None",
      email: "",
      rollNumber: "",
      higherEducation: "None",
      branch: "None",
      branchesList: [],
      coursesList: [],
      interestsList: [],
      shortTermGoalsList: [],
      activityList: [],
      longTermGoalsList: [],
      mentoringInterestList: [],
      higherEducationList: [],
      subjectsList: [],
      professorList: [],
      collegesList: [],
      institutionsList: [],
      expo_push_token: "", //used to store expo push token

      pre_built_event_type: "",
      pre_built_event_index: null, //used to store the no.of prebuilt event types available
      pre_built_list: [], //storing values of events the mentor/mentee is intrested to mentor
      device_id: "", //used to store device id
    };
  }

  async componentDidMount() {
    //we temporarily delete the user because he may not fill the sign up form and leave only if he presses save then we login him again.
    var signedInUser = firebase.auth().currentUser; //we get details of user from navigation
    //setting all the details
    console.log("signedInUser", signedInUser);

    await this.setState({
      image:
        signedInUser.photoURL != null
          ? signedInUser.photoURL
          : this.state.image,
    });
    await this.setState({
      name: signedInUser.displayName != null ? signedInUser.displayName : "",
    });
    await this.setState({ email: signedInUser.email });
    await this.setState({
      phoneNumber:
        signedInUser.phoneNumber === null ? "" : signedInUser.phoneNumber,
    });
    //getting all the details from database of different academics.
    fetch(urls.getHigherEducation, { method: "GET" })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        let d = Array.from(res);
        this.setState({ higherEducationList: d });
      })
      .catch((error) => {
        console.log(error);
        Toast.show({
          text: "Unable to show higher educations list",
          buttonText: "Okay",
        });
      });
    fetch(urls.getInstitutions)
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        let d = Array.from(res);
        this.setState({ institutionsList: d });
      })
      .catch((error) => {
        console.log(error);
        Toast.show({ text: "Unable to show institutions", buttonText: "Okay" });
      });
    this.setState({ pre_built_event_index: PreBuiltEventTypes.length });
    this.setFalseForAllPreBuiltEvents();
    await this.registerForPushNotificationsAsync(); //to create a new pushToken for every user
    this.getdeviceId(); //To call device id
  }

  setFalseForAllPreBuiltEvents = () => {
    let count = this.state.pre_built_event_index;
    for (let i = 0; i < count; i++) this.state.pre_built_list.push(false); //pushing all values as false intitially
    // console.log("setFalse"+this.state.pre_built_list);
  };
  setFalseWhenNo = () => {
    //console.log("No");
    let pre_List = this.state.pre_built_list;
    for (let i = 0; i < pre_List.length; i++) {
      //used to set all the values of checkBoxes as false when NO is clicked for mentoringInterest
      pre_List[i] = false;
    }
    //console.log(pre_List);
    this.setState({ pre_built_list: pre_List });
    //console.log(this.state.pre_built_list);
  };

  toggle = async (index) => {
    let pre_list = this.state.pre_built_list; //used to temporarily store pre_built_list
    pre_list[index] = !pre_list[index]; //toggling the value at the index selected
    await this.setState({ pre_built_list: pre_list }); //assigning 'pre_list' back into 'pre_built_list'
  };

  getdeviceId = () => {
    //Getting the Unique Id from here
    var id = Constants.deviceId;
    //var id = DeviceInfo.getUniqueID();
    this.setState({ device_id: id });
  };
  addMentoringInterest = () => {
    if (this.state.mentoringInterest.trim() == "") {
      Toast.show({
        text: "No mentoring Interests to add.",
        buttonText: "Okay",
      });
      return;
    }
    let flag = 0;
    let s = this.state.mentoringInterest.trim();
    let l = this.state.mentoringInterestList;
    let i = 0;
    //checking mentor interest whether already exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      // if mentor interest not present then add into the list.
      let a = this.state.mentoringInterestList;
      a.push({ id: l.length, name: this.state.mentoringInterest.trim() });
      this.setState({ mentoringInterestList: a, mentoringInterest: "" });
    } else {
      Toast.show({
        text: "Mentoring Interest already added",
        buttonText: "Okay",
      });
    }
  };

  deleteMentoringInterest = (name) => {
    if (this.state.mentoringInterestList.length == 0) {
      Toast.show({
        text: "No mentoring Interests to delete",
        buttonText: "Okay",
      });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.mentoringInterestList;
    var i = 0;
    //check if mentor interest already present
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1); //if present deleting the mentor interest
      this.setState({ mentoringInterestList: l });
      this.setState({ mentoringInterest: "" });
    } else {
      Toast.show({ text: "Mentoring Interest not exists", buttonText: "Okay" });
      this.setState({ mentoringInterest: "" });
    }
  };

  addLongGoal = () => {
    if (this.state.longTermGoal.trim() == "") {
      Toast.show({ text: "No long term goal to add.", buttonText: "Okay" });
      return;
    }
    let flag = 0;
    let s = this.state.longTermGoal.trim();
    let l = this.state.longTermGoalsList;
    let i = 0;
    //checking if goal already present.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      // if goal not already present in list add it
      let a = this.state.longTermGoalsList;
      a.push({ id: l.length, name: this.state.longTermGoal.trim() });
      this.setState({ longTermGoalsList: a });
      this.setState({ longTermGoal: "" });
    } else {
      Toast.show({ text: "Long Term Goal already added", buttonText: "Okay" });
      this.setState({ longTermGoal: "" });
    }
  };

  deleteLongGoal = (name) => {
    if (this.state.longTermGoalsList.length == 0) {
      Toast.show({ text: "No long term goals to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.longTermGoalsList;
    var i = 0;
    //checking if goal present.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ longTermGoalsList: l });
    } else {
      Toast.show({ text: "Long term Goal not exists", buttonText: "Okay" });
    }
  };

  addShortGoal = () => {
    if (this.state.shortTermGoal.trim() == "") {
      Toast.show({ text: "No short term goal to add.", buttonText: "Okay" });
      return;
    }
    let flag = 0;
    let s = this.state.shortTermGoal.trim();
    let l = this.state.shortTermGoalsList;
    let i = 0;
    //checking if goal already present.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      // if goal not already present in list add it
      let a = this.state.shortTermGoalsList;
      a.push({ id: l.length, name: this.state.shortTermGoal.trim() });
      this.setState({ shortTermGoalsList: a });
      this.setState({ shortTermGoal: "" });
    } else {
      Toast.show({ text: "Short Term Goal already added", buttonText: "Okay" });
      this.setState({ shortTermGoal: "" });
    }
  };

  deleteShortGoal = (name) => {
    if (this.state.shortTermGoalsList.length == 0) {
      Toast.show({ text: "No short term goals to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.shortTermGoalsList;
    var i = 0;
    //check if goal exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ shortTermGoalsList: l });
    } else {
      Toast.show({ text: "Short term Goal not exists", buttonText: "Okay" });
    }
  };

  addActivity = () => {
    if (this.state.activity.trim() == "") {
      Toast.show({ text: "No activity to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.activity.trim();
    var l = this.state.activityList;
    var i = 0;
    //check if activity exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      //if activity does not exist.
      var a = this.state.activityList;
      a.push({ id: l.length, name: this.state.activity.trim() });
      this.setState({ activityList: a });
      this.setState({ activity: "" });
    } else {
      Toast.show({
        text: "Extra curricular activity already added",
        buttonText: "Okay",
      });
      this.setState({ activity: "" });
    }
  };

  addSubject = () => {
    if (this.state.subject.trim() == "") {
      Toast.show({ text: "No subject to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.subject.trim();
    var l = this.state.subjectsList;
    var i = 0;
    //check if subject already present in list.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      //if subject not present.
      var a = this.state.subjectsList;
      a.push({ id: l.length, name: this.state.subject.trim() });
      this.setState({ subjectsList: a });
      this.setState({ subject: "" });
    } else {
      this.setState({ subject: "" });
      Toast.show({ text: "Subject already added", buttonText: "Okay" });
    }
  };

  addInterest = () => {
    if (this.state.interest.trim() == "") {
      Toast.show({ text: "No interest to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.interest.trim();
    var l = this.state.interestsList;
    var i = 0;
    //check if interest already present.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) {
      //if interest not present
      var a = this.state.interestsList;
      a.push({ id: l.length, name: this.state.interest.trim() });
      this.setState({ interestsList: a });
      this.setState({ interest: "" });
    } else {
      this.setState({ interest: "" });
      Toast.show({ text: "Academic interests added", buttonText: "Okay" });
    }
  };

  addFaculty = () => {
    if (this.state.professor.trim() == "") {
      Toast.show({ text: "No faculty to add.", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = this.state.professor.trim();
    var l = this.state.professorList;
    var i = 0;
    //check if faculty already present
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    //if faculty not present.
    if (flag == 0) {
      var a = this.state.professorList;
      a.push({ id: l.length, name: this.state.professor.trim() });
      this.setState({ professorList: a });
      this.setState({ professor: "" });
    } else {
      this.setState({ professor: "" });
      Toast.show({ text: "Faculty already added", buttonText: "Okay" });
    }
  };

  deleteActivity = (name) => {
    if (this.state.activityList.length == 0) {
      Toast.show({ text: "No activities to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.activityList;
    var i = 0;
    //check if activity exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ activityList: l });
      this.setState({ activity: "" });
    } else {
      this.setState({ activity: "" });
      Toast.show({
        text: "Extra curricular activity not exists",
        buttonText: "Okay",
      });
    }
  };

  deleteSubject = (name) => {
    if (this.state.subjectsList.length == 0) {
      Toast.show({ text: "No subjects to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.subjectsList;
    var i = 0;
    //check if subject exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ subjectList: l });
      this.setState({ subject: "" });
    } else {
      this.setState({ subject: "" });
      Toast.show({ text: "Subject not exists", buttonText: "Okay" });
    }
  };

  deleteInterest = (name) => {
    if (this.state.interestsList.length == 0) {
      Toast.show({ text: "No Interests to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.interestsList;
    var i = 0;
    //check if interest exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ interestsList: l });
    } else {
      Toast.show({ text: "Academic interest not exists", buttonText: "Okay" });
    }
  };

  deleteFaculty = (name) => {
    if (this.state.professorList.length == 0) {
      Toast.show({ text: "No faculty to delete", buttonText: "Okay" });
      return;
    }
    var flag = 0;
    var s = name.trim();
    var l = this.state.professorList;
    var i = 0;
    //check if faculty exists.
    for (i = 0; i < l.length; i++) {
      if (l[i]["name"] == s) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      l.splice(i, 1);
      this.setState({ professorList: l });
      this.setState({ professor: "" });
    } else {
      Toast.show({ text: "Professor not exists", buttonText: "Okay" });
      this.setState({ professor: "" });
    }
  };

  validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      //used to create expoPushToken for every user
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        alert("Failed to get push token for push notification!");
        return;
      }
      try {
        var token = await Notifications.getExpoPushTokenAsync();
        console.log(token);
        await this.setState({
          expo_push_token: token,
        });
      } catch (err) {
        console.log(err);
      }
    } else alert("Must use physical device for Push Notifications");

    if (Platform.OS === "android") {
      Notifications.createChannelAndroidAsync("default", {
        name: "default",
        sound: true,
        priority: "max",
        vibrate: [0, 250, 250, 250],
      });
    }
    // return token;
  };
  signOut = async () => {
    await firebase.auth().signOut()
   
      .then((d) => {
        this.props.navigation.navigate("AuthScreen");
      })
      .catch((err) => console.log(err));
  };
  saveButton = async () => {
    //They check whether all required fields are filled or not
    if (this.state.name.trim() == "") {
      Toast.show({ text: "Enter name", buttonText: "Okay" });
      return;
    }
    if (this.state.email.trim() == "") {
      Toast.show({ text: "Enter email id", buttonText: "Okay" });
      return;
    }
    if (!this.validateEmail(this.state.email)) {
      Toast.show({ text: "Enter valid email id", buttonText: "Okay" });
      return;
    }
    if (this.state.phoneNumber.trim().length == 0) {
      Toast.show({ text: "Enter valid phone Number", buttonText: "Okay" });
      return;
    }
    if (this.state.linkedIn.trim() == "") {
      Toast.show({ text: "Enter your linkedIn Profile", buttonText: "Okay" });
      return;
    }
    if (this.state.linkedIn.trim().indexOf("linkedin.com/in/") == -1) {
      Toast.show({
        text: "LinkedIn url must be of format linkedin.com/in/username",
        buttonText: "Okay",
        duration: 2000,
      });
      return;
    }
    if (this.state.rollNumber.trim() == "") {
      Toast.show({ text: "Enter Roll number", buttonText: "Okay" });
      return;
    }
    if (this.state.institutionName.trim() == "None") {
      Toast.show({ text: "Select valid instituion", buttonText: "Okay" });
      return;
    }
    if (this.state.collegeName.trim() == "None") {
      Toast.show({ text: "Select valid college", buttonText: "Okay" });
      return;
    }
    if (this.state.branch.trim() == "None") {
      Toast.show({ text: "Select valid branch", buttonText: "Okay" });
      return;
    }
    if (this.state.course.trim() == "None") {
      Toast.show({ text: "Select valid course", buttonText: "Okay" });
      return;
    }
    if (this.state.yearOfGraduation.trim() === "") {
      Toast.show({ text: "Enter year of graduation", buttonText: "Okay" });
      return;
    }

    if (this.state.interestsList.length == 0) {
      Toast.show({ text: "Enter atleast one interest", buttonText: "Okay" });
      return;
    }
    if (this.state.shortTermGoalsList.length == 0) {
      Toast.show({
        text: "Enter atleast one short term goal",
        buttonText: "Okay",
      });
      return;
    }

    if (this.state.longTermGoalsList.length == 0) {
      Toast.show({
        text: "Enter atleast one long term goal",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.activityList.length == 0) {
      Toast.show({
        text: "Enter atleast one extra curricular activity",
        buttonText: "Okay",
      });
      return;
    }
    if (
      this.state.mentoringInterested == true &&
      this.state.mentoringInterestList.length == 0
    ) {
      Toast.show({
        text: "Enter atleast one Mentoring Interest",
        buttonText: "Okay",
      });
      return;
    }
    if (this.state.collegesList.find((x) => x.name == this.state.collegeName)) {
      var collegeId = this.state.collegesList.find(
        (x) => x.name == this.state.collegeName
      ).id;
    }
    if (
      this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      )
    ) {
      var institutionId = this.state.institutionsList.find(
        (x) => x.name == this.state.institutionName
      ).id;
    }

    const navigation = await this.props.navigation.getParam("navigation");
    // console.log(this.state.pre_built_list);
    let pre_list = this.state.pre_built_list; //used to temporarily store pre_built_list
    let event_indices_list = []; //keeps track of all indices which are selected
    for (let i = 0; i < pre_list.length; i++) {
      if (pre_list[i] == true)
        //if selected i.e true
        event_indices_list.push(i); //then we are pushing the index into the array
    }

    let d = {
      last_logged_in: Date().toLocaleString(),
      phone_number: this.state.phoneNumber.trim(),
      available: this.state.available.trim(),
      name: this.state.name.trim(),
      email: this.state.email,
      branch: this.state.branch.trim(),
      photo_url: this.state.image,
      course: this.state.course.trim(),
      year_passed: this.state.yearOfGraduation.trim(),
      gpa: this.state.gpa.trim() === "" ? null : this.state.gpa.trim(),

      interests_list: this.state.interestsList,
      short_term_goals: this.state.shortTermGoalsList,
      long_term_goals: this.state.longTermGoalsList,
      extra_curricular_activities: this.state.activityList,
      mentoring_interest_list: this.state.mentoringInterestList,
      mentoring_interested: this.state.mentoringInterested,
      subjects_liked:
        this.state.subjectsList.length === 0 ? null : this.state.subjectsList,
      faculty_recommended:
        this.state.professorList === 0 ? null : this.state.professorList,
      college_after_BTECH:
        this.state.college_after_BTECH.trim() === ""
          ? null
          : this.state.college_after_BTECH.trim(),
      about: this.state.about.trim(),
      specialization:
        this.state.specialization.trim() === ""
          ? null
          : this.state.specialization.trim(),
      linkedin: this.state.linkedIn.trim(),
      status: "not_approved",
      college_name: this.state.collegeName,
      college_id: collegeId,
      institution_id: institutionId,
      expo_push_token: this.state.expo_push_token, //token is stored into the
      last_updated_by: "user",
      institution_name: this.state.institutionName,
      display_profile: true,
      roll_number: this.state.rollNumber,
      device_id: this.state.device_id,
      pre_built_events_indices:
        event_indices_list.length === 0 ? null : event_indices_list, //storing the indices values into db
      // storing no_of_events when mentoringInterested is true.
      no_of_events: {
        no_of_simple_events: this.state.mentoringInterested ? 0 : null,
        no_of_bootcamp_events: this.state.mentoringInterested ? 0 : null,
        no_of_overview_events: this.state.mentoringInterested ? 0 : null,
      },
    };
    //Other than JNTUK college mentees
    if (d.college_id == 1 && d.institution_id == "INST1") {
      d = { ...d, user_type: "mentee" };

    } else {
      d = {
        ...d,
        user_type: "license_mentee",
        status: "not_approved",
        super_admin_verified: false,
        License_code_verified: false,
        license_code: "will be recieved after activation",
        bucket_id: "null",
      };
    }
    this.setState({ loading: true });
    d.uid = firebase.auth().currentUser.uid;
    await AsyncStorage.setItem("profile", JSON.stringify(d)); //set the profile in async storage
    await AsyncStorage.getItem("profile").then((d) =>
      console.log("data saved", d)
    );
    console.log("User logged in succesfully");
    fetch(
      urls.newMenteeSignUp, //To save mentee details to database.
      {
        method: "POST",
        body: JSON.stringify(d),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      }
    ).then((res) => {
      // To send mail to mentee
      let obj = { roles: ["mentee"] };
      fetch(urls.setUserRole, {
        method: "POST",
        body: JSON.stringify({
          role: obj,
          uid: firebase.auth().currentUser.uid,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      })
        .then((res) => {
          console.log(res.text);
          Alert.alert(
            "Notifications",
            "You will get frequent updates from MentorKonnect App. If you don’t like to receive notifications, please turn off notifications in settings.",
            [
              {
                text: "OK",
                onPress: () => {
                 //this.signOut()
                 console.log("OK")
                },
              },
            ],
            // { cancelable: false }
          );
          this.props.navigation.navigate("MenteeAppPage"); //navigating to the appPage.
        })
        .catch((err) => {
          console.log(err);
        });
    });
    const url = `${urls.sendWelcomeMail}?name=${d.name}&email=${d.email}&type=mentee`;
    fetch(url)
      .then(() => {
        console.log("registration confirmation sent as mail.");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  uploadPhoto = async () => {
    try {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL); //Asking user for storage permissions.
      if (status == "granted") {
        let result = await ImagePicker.launchImageLibraryAsync({
          //Selecting user from local storage.
          mediaTypes: ImagePicker.MediaTypeOptions.Image,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
        if (!result.cancelled) {
          this.setState({ image: result.uri, imageLoading: true });
          const response = await fetch(result.uri);
          const blob = await response.blob(); //getting the binary form of image.
          var ref = firebase
            .storage()
            .ref()
            .child(`profile_images/${this.state.email}`)
            .put(blob); //storing the image binary data in cloud storage.
          Toast.show({ text: "Uploading Image", buttonText: "Okay" });
          return ref.on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
              // Observe state change events such as progress, pause, and resume
              // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
              progress =
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              console.log("Upload is " + progress + "% done");
              switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                  console.log("Upload is paused");
                  break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                  console.log("Upload is running");
                  break;
              }
            },
            (error) => {
              // Handle unsuccessful uploads
              console.log("Upload unsuccessfull");
            },
            () => {
              // Handle successful uploads on complete
              // For instance, get the download URL: https://firebasestorage.googleapis.com/...
              Toast.show({ text: "Uploaded Image", buttonText: "Okay" });
              ref.snapshot.ref.getDownloadURL().then((downloadURL) => {
                // console.log('File available at', downloadURL);
                this.setState({ image: downloadURL, imageLoading: false });
              });
            }
          );
        }
        // console.log(result);
      } else {
        Alert.alert("Required storage permissions.");
      }
    } catch (E) {
      console.log(E);
    }
  };

  render() {
    return (
      <View style={styles.overall}>
        <KeyboardAvoidingView
          behavior={Platform.Os == "ios" ? "padding" : "height"}
          style={{ flex: 1 }}
        >
          <ScrollView
            style={styles.container}
            keyboardShouldPersistTaps="handled"
          >
            <View style={styles.detailsContainer}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignContent: "center",
                }}
              >
                <Avatar
                  onPress={() => {
                    this.uploadPhoto();
                  }}
                  editButton={{
                    name: "mode-edit",
                    type: "material",
                    color: "#fff",
                    underlayColor: "#0f0",
                  }}
                  showEditButton
                  size={100}
                  rounded
                  source={{ uri: this.state.image }}
                  avatarStyle={{}}
                  icon={{ name: "user", type: "font-awesome" }}
                />
              </View>

              <Text style={styles.heading}>Contact Info:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Name <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Name"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.name}
                  onChangeText={(text) => this.setState({ name: text })}
                />
                <Text style={styles.subtitle}>
                  Email Id <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  editable={false}
                  autoCapitalize="none"
                  placeholder="Email"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.email}
                  onChangeText={(text) => this.setState({ email: text })}
                />
                <Text style={styles.subtitle}>
                  Phone Number <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Mobile Number"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  keyboardType={"numeric"}
                  value={this.state.phoneNumber}
                  onChangeText={(text) => this.setState({ phoneNumber: text })}
                />
                <Text style={styles.subtitle}>
                  LinkedIn Profile <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  autoCapitalize="none"
                  placeholder="LinkedIn profile url..linkedin.com/in/username"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.linkedIn.toLowerCase()}
                  onChangeText={(text) =>
                    this.setState({ linkedIn: text.toLowerCase() })
                  }
                />
              </View>
              <Text style={styles.heading}>College Info:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Roll No <Text style={styles.req}>*</Text>
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  autoCapitalize="characters"
                  placeholder="Enter roll number in block letters"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.rollNumber}
                  onChangeText={(text) => this.setState({ rollNumber: text })}
                />
                <Dropdown
                  label="Institution *"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  animationDuration={0}
                  data={this.state.institutionsList}
                  value={this.state.institutionName}
                  valueExtractor={(item, index) => {
                    return item.name;
                  }}
                  onChangeText={async (v, i, d) => {
                    await this.setState({ institutionName: v });
                    await this.setState({
                      collegeName: "None",
                      course: "None",
                      branch: "None",
                    });
                    await this.setState({
                      collegesList: this.state.institutionsList.find(
                        (x) => x.name == this.state.institutionName
                      )
                        ? this.state.institutionsList.find(
                            (x) => x.name == this.state.institutionName
                          ).colleges
                        : [],
                    });
                    await this.setState({
                      coursesList: this.state.collegesList.find(
                        (x) => x.name == this.state.collegeName
                      )
                        ? this.state.collegesList.find(
                            (x) => x.name == this.state.collegeName
                          ).courses
                        : [],
                    });
                    await this.setState({
                      branchesList: this.state.coursesList.find(
                        (x) => x.course == this.state.course
                      )
                        ? this.state.coursesList.find(
                            (x) => x.course == this.state.course
                          ).branches
                        : [],
                    });
                  }}
                />
                <Dropdown
                  label="College *"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.collegesList}
                  value={this.state.collegeName}
                  valueExtractor={(item, index) => {
                    return item.name;
                  }}
                  onChangeText={async (v, i, d) => {
                    await this.setState({ collegeName: v });
                    await this.setState({ course: "None", branch: "None" });
                    await this.setState({
                      coursesList: this.state.collegesList.find(
                        (x) => x.name == this.state.collegeName
                      )
                        ? this.state.collegesList.find(
                            (x) => x.name == this.state.collegeName
                          ).courses
                        : [],
                    });
                    // console.log(this.state.coursesList);
                    await this.setState({
                      branchesList: this.state.coursesList.find(
                        (x) => x.course == this.state.course
                      )
                        ? this.state.coursesList.find(
                            (x) => x.course == this.state.course
                          ).branches
                        : [],
                    });
                  }}
                />
                <Dropdown
                  label="Course *"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.coursesList}
                  value={this.state.course}
                  valueExtractor={(item, index) => {
                    return item.course;
                  }}
                  onChangeText={async (v, i, d) => {
                    await this.setState({ course: v });
                    await this.setState({ branch: "None" });
                    await this.setState({
                      branchesList: this.state.coursesList.find(
                        (x) => x.course == this.state.course
                      )
                        ? this.state.coursesList.find(
                            (x) => x.course == this.state.course
                          ).branches
                        : [],
                    });
                    // console.log(this.state.branchesList)
                  }}
                />
                <Dropdown
                  label="Branch *"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.branchesList}
                  value={this.state.branch}
                  valueExtractor={(item, index) => {
                    return item.branch;
                  }}
                  onChangeText={async (v, i, d) => {
                    this.setState({ branch: v });
                  }}
                />
                <Dropdown
                  label="Highest Education"
                  labelFontSize={18}
                  itemCount={6}
                  labelTextStyle={styles.dropDownStyle}
                  data={this.state.higherEducationList}
                  value={this.state.higherEducation}
                  valueExtractor={(item, index) => {
                    return item.name;
                  }}
                  onChangeText={async (v, i, d) => {
                    this.setState({ higherEducation: v });
                  }}
                />
                <Text style={styles.subtitle}>Specialization</Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Specialization"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.specialization}
                  onChangeText={(text) =>
                    this.setState({ specialization: text })
                  }
                />
                <Text style={styles.subtitle}>
                  Year of Graduation <Text style={styles.req}>*</Text>
                </Text>
                <DatePicker
                  style={styles.dateStyle}
                  date={this.state.yearOfGraduation}
                  mode="date"
                  placeholder="select date"
                  format="YYYY-MM-DD"
                  minDate="1800-01-01"
                  // maxDate="2030-01-01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 4,
                      marginLeft: 0,
                    },
                    dateInput: {
                      marginLeft: 36,
                    },
                  }}
                  onDateChange={(date) => {
                    this.setState({ yearOfGraduation: date });
                  }}
                />
                <Text style={styles.subtitle}>GPA/Percentage</Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="GPA/Percentage"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  keyboardType={"numeric"}
                  value={this.state.gpa}
                  onChangeText={(text) => this.setState({ gpa: text })}
                />
                <Text style={styles.subtitle}>College After B.TECH</Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="College name"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.college_after_BTECH}
                  onChangeText={(text) =>
                    this.setState({ college_after_BTECH: text })
                  }
                />
                <Text style={styles.subtitle}>
                  Subjects liked at your college
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Subject name and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.subject}
                    onChangeText={(text) => this.setState({ subject: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={styles.deleteOverlayContainer}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addSubject()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.subjectsList.map((l, i) => (
                    <View key={i} style={styles.interestsSubContent}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Icon
                        name={"times-circle"}
                        type={"font-awesome"}
                        onPress={() => this.deleteSubject(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
              </View>
              <Text style={styles.heading}>Goal Setting:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Short Term Goals <Text style={styles.req}>*</Text>
                </Text>
                <Text>Goal expected to be completed beyond one year</Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Short Term Goals and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.shortTermGoal}
                    onChangeText={(text) =>
                      this.setState({ shortTermGoal: text })
                    }
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={styles.deleteOverlayContainer}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addShortGoal()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.shortTermGoalsList.map((l, i) => (
                    <View key={i} style={styles.interestsSubContent}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Icon
                        name={"times-circle"}
                        type={"font-awesome"}
                        onPress={() => this.deleteShortGoal(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
                <Text style={styles.subtitle}>
                  Long Term Goals <Text style={styles.req}>*</Text>
                </Text>
                <Text>
                  Something you want to achieve for your successful career{" "}
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Long Term Goals and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.longTermGoal}
                    onChangeText={(text) =>
                      this.setState({ longTermGoal: text })
                    }
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={styles.deleteOverlayContainer}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addLongGoal()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.longTermGoalsList.map((l, i) => (
                    <View key={i} style={styles.interestsSubContent}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Icon
                        name={"times-circle"}
                        type={"font-awesome"}
                        onPress={() => this.deleteLongGoal(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
              </View>

              <Text style={styles.heading}>Mentoring Interests:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Mentoring Interest Areas
                  <Text style={{ fontSize: 15 }}>
                    (If mentoring Interested)
                  </Text>
                </Text>
                <Text>
                  Topics you are proficient and passionate to share with others
                </Text>
                <View style={styles.interestsView}>
                  <CheckBox
                    center
                    title="Yes"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    containerStyle={{ backgroundColor: "white" }}
                    checked={this.state.mentoringInterested}
                    onPress={() => {
                      this.setState((prev) => {
                        return {
                          mentoringInterested: !prev.mentoringInterested,
                        };
                      });
                    }}
                  />
                  <CheckBox
                    center
                    title="No"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    containerStyle={{ backgroundColor: "white" }}
                    checked={!this.state.mentoringInterested}
                    onPress={() => {
                      this.setFalseWhenNo();
                      //this.setState({ mentoringInterestList: [v] });
                      this.setState((prev) => {
                        return {
                          mentoringInterested: !prev.mentoringInterested,
                        };
                      });
                    }}
                  />
                </View>
                {/* If mentoring interesed is true then we provide checkboxes to select preBuiltEvents*/}
                {this.state.mentoringInterested && (
                  <View>
                    <FlatList
                      data={PreBuiltEventTypes}
                      style={{ flex: 1 }}
                      renderItem={({ item, index }) => {
                        return (
                          <CheckBox
                            checked={this.state.pre_built_list[index]}
                            containerStyle={{ backgroundColor: "white" }}
                            title={item.name}
                            onPress={() => {
                              this.toggle(index);
                            }} //toggle function is called to keep track of checked event types
                          />
                        );
                      }}
                    />

                    <View style={styles.interestsStyle}>
                      <TextInput
                        selectTextOnFocus={true}
                        placeholder="Enter Other Mentoring Interests and press + "
                        underlineColorAndroid="transparent"
                        style={styles.adderLists}
                        value={this.state.mentoringInterest}
                        onChangeText={(text) =>
                          this.setState({ mentoringInterest: text })
                        }
                      />
                      <Avatar
                        size="medium"
                        overlayContainerStyle={styles.deleteOverlayContainer}
                        rounded
                        icon={{
                          name: "plus-circle",
                          color: "white",
                          type: "font-awesome",
                        }}
                        onPress={() => this.addMentoringInterest()}
                        activeOpacity={0.7}
                      />
                    </View>
                    <View style={styles.interestsContentContainer}>
                      {this.state.mentoringInterestList.map((l, i) => (
                        <View key={i} style={styles.interestsSubContent}>
                          <Text style={styles.interestsTextStyle}>
                            {l.name}
                          </Text>
                          <Icon
                            name={"times-circle"}
                            type={"font-awesome"}
                            onPress={() => this.deleteMentoringInterest(l.name)}
                            containerStyle={styles.deleteContainerStyle}
                            activeOpacity={0.7}
                          />
                        </View>
                      ))}
                    </View>
                  </View>
                )}
              </View>
              <Text style={styles.heading}>Additional Info:</Text>
              <View style={styles.headingView}>
                <Text style={styles.subtitle}>
                  Academic Interests <Text style={styles.req}>*</Text>
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Interests and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.interest}
                    onChangeText={(text) => this.setState({ interest: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={styles.deleteOverlayContainer}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addInterest()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.interestsList.map((l, i) => (
                    <View key={i} style={styles.interestsSubContent}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Icon
                        name={"times-circle"}
                        type={"font-awesome"}
                        onPress={() => this.deleteInterest(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
                <Text style={styles.subtitle}>
                  Extra Curricular Activities <Text style={styles.req}>*</Text>
                </Text>
                <View style={styles.interestsStyle}>
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter Extra Curricular Activities and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.activity}
                    onChangeText={(text) => this.setState({ activity: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={styles.deleteOverlayContainer}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addActivity()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.activityList.map((l, i) => (
                    <View key={i} style={styles.interestsSubContent}>
                      <Text style={styles.interestsTextStyle}>{l.name}</Text>
                      <Icon
                        name={"times-circle"}
                        type={"font-awesome"}
                        onPress={() => this.deleteActivity(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>

                <Text style={styles.subtitle}>
                  Faculty you would recommend at your college
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <TextInput
                    selectTextOnFocus={true}
                    placeholder="Enter faculty name and press +"
                    underlineColorAndroid="transparent"
                    style={styles.adderLists}
                    value={this.state.professor}
                    onChangeText={(text) => this.setState({ professor: text })}
                  />
                  <Avatar
                    size="medium"
                    overlayContainerStyle={styles.deleteOverlayContainer}
                    rounded
                    icon={{
                      name: "plus-circle",
                      color: "white",
                      type: "font-awesome",
                    }}
                    onPress={() => this.addFaculty()}
                    activeOpacity={0.7}
                  />
                </View>
                <View style={styles.interestsContentContainer}>
                  {this.state.professorList.map((l, i) => (
                    <View key={i} style={styles.interestsSubContent}>
                      <Text style={{ fontSize: 15, fontWeight: "bold" }}>
                        {l.name}
                      </Text>
                      <Icon
                        name={"times-circle"}
                        type={"font-awesome"}
                        onPress={() => this.deleteFaculty(l.name)}
                        containerStyle={styles.deleteContainerStyle}
                        activeOpacity={0.7}
                      />
                    </View>
                  ))}
                </View>
                <Text style={styles.subtitle}>Available Timings </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Available timings"
                  underlineColorAndroid="transparent"
                  style={styles.TextInputStyle}
                  value={this.state.available}
                  onChangeText={(text) => this.setState({ available: text })}
                />
                <Text style={styles.subtitle}>
                  Additional information about yourself{" "}
                </Text>
                <TextInput
                  selectTextOnFocus={true}
                  placeholder="Additional information about yourself"
                  underlineColorAndroid="transparent"
                  multiline={true}
                  numberOfLines={10}
                  textAlignVertical="top"
                  style={styles.about}
                  value={this.state.about}
                  onChangeText={(text) => this.setState({ about: text })}
                />
              </View>
              <View style={styles.saveView}>
                <Button
                  icon={
                    <Icon
                      type="font-awesome"
                      name="save"
                      size={20}
                      color="white"
                    />
                  }
                  containerStyle={styles.saveContainerStyle}
                  buttonStyle={styles.saveButtonStyle}
                  onPress={async() => {
                    await this.saveButton();
                  }}
                  titleStyle={styles.saveButtonTitleStyle}
                  title="Save "
                />
                {this.state.loading && <ActivityIndicator size="large" />}
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  overall: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 10,
  },
  interestsView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  deleteOverlayContainer: {
    backgroundColor: "rgba(25,100,200,1)",
  },
  deleteContainerStyle: {
    padding: 7,
  },
  saveButtonTitleStyle: {
    padding: 5,
  },
  saveContainerStyle: {
    padding: 10,
  },
  about: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.9,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    height: 100,
    textAlign: "left",
    backgroundColor: "white",
  },
  saveButtonStyle: {
    backgroundColor: "rgba(200,175,0,1)",
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    width: Dimensions.get("window").width * 0.4,
  },
  saveView: {
    flexDirection: "row",
    justifyContent: "center",
  },
  subView: {
    flexDirection: "row",
    borderRadius: 10,
    borderWidth: 2,
    backgroundColor: "rgba(100,100,255,0.4)",
    alignItems: "center",
  },
  adderLists: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.76,
    borderRadius: 10,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    backgroundColor: "white",
  },
  TextInputStyle: {
    padding: 10,
    marginTop: 5,
    height: 40,
    width: Dimensions.get("window").width * 0.9,
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "black",
    marginBottom: 5,
    backgroundColor: "white",
  },
  heading: {
    fontSize: 20,
    fontWeight: "bold",
    marginTop: 10,
    color: "black",
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 5,
    color: "black",
  },
  detailsContainer: {
    flexDirection: "column",
    justifyContent: "flex-start",
    margin: Dimensions.get("window").width * 0.03,
    alignContent: "flex-start",
  },
  titleStyle: {
    fontSize: 25,
    fontWeight: "bold",
    textAlign: "center",
  },
  logoContainer: {
    flex: 0.75,
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: "white",
  },
  container: {
    flex: 1,
    flexDirection: "column",
  },
  logoStyle: {
    borderRadius: 5,
    height: 110,
    width: 200,
  },
  dropDownStyle: {
    paddingBottom: 5,
    fontWeight: "bold",
  },
  req: {
    color: "red",
  },
  dateStyle: {
    width: Dimensions.get("window").width * 0.9,
    marginTop: 10,
    marginBottom: 10,
  },
  interestsStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  interestsContentContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  interestsSubContent: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    paddingStart: 10,
    borderRadius: 10,
    borderWidth: 2,
    backgroundColor: "rgba(100,100,255,0.4)",
    alignItems: "center",
  },
  interestsTextStyle: {
    fontSize: 15,
    fontWeight: "bold",
  },
  deleteOverlay: {
    backgroundColor: "rgba(100,255,255,0.1)",
    padding: 0,
  },
  headingView: {
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "grey",
    padding: 5,
    width: Dimensions.get("window").width * 0.9375,
    backgroundColor: "whitesmoke",
    marginBottom: 5,
  },
});
