import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Dimensions,
  TouchableOpacity,
  Image,
  Linking,
} from "react-native";

export default class Mentors extends React.Component {
  constructor(props) {
    super(props);
  }
  sendMail = () => {
    const subject = "Please register me as mentor";
		Linking.openURL(
			`mailto:myCollegekonnect.app@gmail.com?subject=` + subject + `&body=Instructions: For registering you as mentor, please enter your LinkedIn profile and phone number:`
    )
  };

  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <View
          style={{
            
            backgroundColor:"white",
            margin: 20,
            borderRadius: 10,
            borderWidth: 5,
            padding:6
          }}
        >
          <Text
            style={{
              fontSize: 17,
              fontWeight: "bold",
              alignSelf: "center",
              marginVertical: 10,
            }}
          >
            Instructions :
          </Text>
          <View style={{ flexDirection: "row", fontSize: 20, margin: 15 }}>
            <Text>1. </Text>
            <Text style={{ fontSize: 16 }}>
              If you want to continue as a mentee or mentee/mentor in MentorKonnect. Please fill
              the profile.
            </Text>
          </View>
          <View style={{ width: 150, alignSelf: "center" }}>
            <Button
              title="Fill Profile"
              onPress={async () => {
                this.props.navigation.navigate("ProfilePage");
              }}
            />
          </View>

          <View style={{ flexDirection: "row", fontSize: 20, margin: 15 }}>
            <Text>2. </Text>
            <Text style={{ fontSize: 16 }}>
              If you want to join as mentor or for more information please contact us.
            </Text>
          </View>

          <View style={{ width: 100, marginLeft: 5, alignSelf: "center" }}>
            <TouchableOpacity
              onPress={() => this.sendMail()}
              style={{ justifyContent: "center", alignItems: "center" }}
            >
              <Image
                source={require("../../../assets/mail.png")}
                style={{
                  width: 35,
                  height: 35,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              />

              <Text style={{ color: "red" }}>Contact us</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: "row", fontSize: 20, margin: 15 }}>
            <Text>3. </Text>
            <Text style={{ fontSize: 16 }}>
              Mentors will be shown after user's profile is created in
              MentorKonnect.
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
  },
});
