import React from "react";
import { Text, View, StyleSheet, Dimensions, ScrollView } from "react-native";


export default class AppContainer extends React.Component {
  render() {
    return (
      <View style={styles.overAllView}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            {(
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <Text style={{ paddingRight: 15, fontWeight: "700" }}>
                      1.{""}
                    </Text>
                  </View>
                  <Text
                    style={{
                      ...styles.fieldBoldStyle,
                      marginRight: Dimensions.get("window").width * 0.23,
                    }}
                  >
                    What is the MentorKonnect ?
                  </Text>

                 
                </View>
                <Text style={styles.fieldStyle}>
                  MentorKonnect is a platform that connects Mentors and Mentees.
                  In the world of MentorKonnect, mentors are people who offer
                  mentoring to current students (mentees).
                </Text>
              </View>
            )}
     
          </View>
          <View style={styles.container}>
            { (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <View>
                      <Text style={{ fontWeight: "700", paddingRight: 15 }}>
                        2.
                      </Text>
                    </View>
                  </View>
                  <Text
                    style={{
                      ...styles.fieldBoldStyle,
                      marginRight: Dimensions.get("window").width * 0.21,
                    }}
                  >
                    Mentors in MentorKonnect:
                  </Text>
                 
                </View>
                <Text style={styles.fieldStyle}>
                  MentorKonnect has alumni of any college, industry experienced
                  professionals in their field as mentors.
                </Text>
              </View>
            )}
      
          </View>
          <View style={styles.container}>
            { (
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <View>
                      <Text style={{ fontWeight: "700", paddingRight: 15 }}>
                        3.
                      </Text>
                    </View>
                  </View>
                  <Text style={{
                       ...styles.fieldBoldStyle,
                        marginRight: Dimensions.get("window").width *0.21,
                      }}>
                    Events in MentorKonnect:
                  </Text>
            
                </View>
                <Text style={styles.fieldStyle}>
                  Mentoring events are 3 types.
                </Text>
                <View
                  style={{
                    paddingRight: 15,
                    marginRight: 5,
                  }}
                >
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 1. </Text>
                    <Text style={styles.mattertext}>
                      Simple event is a one-to-one type. Example: First/second
                      year student (mentee in this case) asking final year
                      students (mentors in this case) for guidance while
                      choosing electives.
                      {"\n"}
                    </Text>
                  </View>
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 2. </Text>
                    <Text style={styles.mattertext}>
                      Orientation/ Guest lecture event type is generally a
                      one-time group meeting where a mentor gives an overview of
                      a topic/subject. Example: “Trends in computer networks
                      industry” – A mentoring event by an alumni student
                      graduated in 1990 - 1994.
                      {"\n"}
                    </Text>
                  </View>
                  <View style={styles.eventstype}>
                    <Text style={styles.mattertext}> 3. </Text>

                    <Text style={styles.mattertext}>
                      Bootcamp event type is a recurring intensive training to
                      improve a specific skill significantly. This usually spans
                      across 5-15 weeks. Example: “Mobile Application
                      development using ReactJS” – A mentoring event by an
                      alumni student graduated in 1990 - 1994.
                    </Text>
                  </View>
                </View>
              </View>
            )}
            
          </View>
         
          <View style={styles.container}>
            {(
              //<View style={{flexDirection:"row"}}>
              <View style={styles.containerselected}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                  }}
                >
                  <View style={{ marginRight: 2, marginLeft: 3 }}>
                    <Text style={{ paddingRight: 15, fontWeight: "700" }}>
                      4.{""}
                    </Text>
                  </View>
                  <Text style={{
                         ...styles.fieldBoldStyle,
                        marginRight: Dimensions.get("window").width * 0.05,
                      }}>
                   Goal of MentorKonnect :
                  </Text>

                 
                </View>
                <Text style={styles.fieldStyle}>
                  Main goal of the MentorKonnect platform is to promote
                  collaboration between industry and academia. It is a platform
                  that connects experts (mentors) with current students
                  (mentees). For mentors, this is one more way to share their
                  knowledge with current students. For mentees, this platform
                  provides opportunities to learn from / work with seniors.
                </Text>
              </View>
            )}
           
          
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  feedback: {
    fontSize: 25,
    color: "black",
    fontWeight: "bold",
  },
  icons1: {
    flexDirection: "row",
    paddingTop: 10,
  },
  container1: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 20,
    marginBottom: 20,
  },
  eventstype: { flexDirection: "row", paddingRight: 10 },
  overAllView: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    marginLeft: Dimensions.get("window").width * 0.03,
    marginRight: Dimensions.get("window").width * 0.03,
  },
  container: {
    margin: 5,
    flexDirection: "column",
    borderBottomWidth: 2,
    borderBottomColor: "#afeeee",
  },
  containerselected: {
    elevation: 3,
    marginBottom: 2,
    shadowColor: "#00ffff",
    backgroundColor: "#f0f8ff",
    shadowOpacity: 0.5,
    borderWidth: 0,
    paddingHorizontal: 17,
    paddingBottom: 10,
    paddingTop: 10,
    alignItems: "flex-start",
    alignContent: "space-between",
    justifyContent: "flex-start",
  },
  cardStyle: {
    paddingHorizontal: 15,
    paddingBottom: 0,
    paddingTop: 10,
  },
  orstyle: {
    alignSelf: "center",
    margin: 10,
    marginTop: -17,
    fontSize: 17,
    color: "black",
  },
  titleStyle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 5,
  },
  mattertext: {
    fontSize: 16,
    color: "#2f4f4f",
    fontWeight: "600",
  },
  fieldBoldStyle: {
    fontSize: 16,
    marginBottom: 3,
    color: "#212121",
    fontWeight: "700",
   
  },
  fieldStyle: {
    fontSize: 15,
    marginBottom: 3,
    color: "#2f4f4f",
    paddingHorizontal: 3,
  },
})