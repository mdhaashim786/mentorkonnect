import React from "react";
import { Text, View, StyleSheet, Image, Linking } from "react-native";
import Logos from "./../../../assets/Logos";
import Constants from "expo-constants";
export default class AppContainer extends React.Component {
  sendMail = () => {
    Linking.openURL(`mailto:myCollegekonnect.app@gmail.com`);
  };

  render() {
    return (
      <View style={styles.overAllView}>
        <View style={styles.aboutContainer}>
          <Text style={styles.titleStyle}>MentorKonnect</Text>
        </View>
        <View style={styles.aboutView}>
          <Image
            style={styles.logoStyle}
            source={{ uri: Logos.mentorkonnectLogo }}
          />
          <Text style={styles.textStyle}>
            Version: {Constants.manifest.version}
          </Text>
          <Text style={styles.footerStyle}>
            mycollegekonnect.com © All rights reserved.
          </Text>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View>
              <Text
                onPress={() => this.sendMail()}
                style={{
                  ...styles.linkurl,
                  textDecorationLine: "underline",
                  color: "blue",
                }}
              >
                Contact us
              </Text>
            </View>
            <View>
              <Text
                style={styles.linkurl}
                onPress={() => {
                  Linking.openURL("https://www.mycollegekonnect.com/");
                }}
              >
                Visit us
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  aboutView: {
    flex: 5,
    flexDirection: "column",
    alignItems: "center",
  },
  linkurl: {
    fontSize: 17,
    margin: 10,
    color: "blue",
    textDecorationLine: "underline",
    fontWeight: "bold",
  },
  textStyle: {
    fontSize: 17,
    margin: 10,
  },
  titleStyle: {
    fontSize: 25,
    fontWeight: "bold",
  },
  overAllView: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
  },
  aboutContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    margin: 20,
  },
  logoStyle: {
    borderRadius: 5,
    height: 175,
    width: 300,
  },
  footerStyle: {
    fontSize: 15,
    color: "gray",
    alignSelf: "center",
    fontWeight: "bold",
  },
});
