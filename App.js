import _ from "lodash";
import React from "react";
import { Root } from "native-base";
import { Image, Dimensions, YellowBox } from "react-native";
import { Avatar } from "react-native-elements";
import { createSwitchNavigator, createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createDrawerNavigator } from "react-navigation-drawer";
import * as firebase from "firebase";
import { Ionicons } from '@expo/vector-icons';
import { firebaseConfig } from "./env.json";
import * as Font from 'expo-font';
import Header from "./src/views/headerComponent.js";
import AuthScreen from "./src/screens/Authentication/AuthScreen.js";
import NonAlumniUserSignUp from "./src/screens/Authentication/NonAlumniUserSignUp";
import AdminUserSignUp from "./src/screens/Authentication/AdminUserSignUp";
import AlumniDetailsSignUp from "./src/screens/Authentication/AlumniDetailsSignUp.js";
import DrawerComponent from "./src/screens/DrawerNavigation/DrawerComponent.js";
import About from "./src/screens/DrawerNavigation/About.js";
import Profile from "./src/screens/DrawerNavigation/profile.js";
import FAQ from "./src/screens/DrawerNavigation/FAQ.js";
import Activate from "./src/screens/DrawerNavigation/Activate.js";
import AdminActivate from "./src/screens/Admin/AdminActivate.js";
import Settings from "./src/screens/DrawerNavigation/Settings.js";
import Mentors from "./src/screens/Mentors/Mentors.js";
import MentorDetails from "./src/screens/Mentors/MentorDetails.js";
import LaunchScreen from "./src/screens/Events/LaunchScreen";
import EventDetails from "./src/screens/Events/EventDetails";
import SelectMentor from "./src/screens/Events/SelectMentor";
import AddMentorEvent from "./src/screens/Events/AddMentorEvent";
import ScheduleScreen from "./src/screens/Events/ScheduleScreen";
import MenteeUserSignUp from "./src/screens/PreviewPage/MenteeUserSignUp";
import PreviewAbout from "./src/screens/PreviewPage/PreviewAbout";
import PreviewDrawerComponent from "./src/screens/PreviewPage/PreviewDrawerComponent";
import PreviewLaunchScreen from "./src/screens/PreviewPage/PreviewLaunchScreen";
import PreviewMenteeGroupEvents from "./src/screens/PreviewPage/PreviewMenteeGroupEvents";
import PreviewMentors from "./src/screens/PreviewPage/PreviewMentors";
import PreviewFAQ from "./src/screens/PreviewPage/PreviewFAQ.js";

import AddAlumni from "./src/screens/Admin/AddAlumni";
import AdminLaunch from "./src/screens/Admin/AdminLaunch";
import CollegeAdmins from "./src/screens/Admin/CollegeAdmins";
import CollegeMentors from "./src/screens/Admin/CollegeMentors";
import CollegeMentee from "./src/screens/Admin/CollegeMentee";
import AdminEventDetails from "./src/screens/Admin/AdminEventDetails";
import AdminEditMenteeProfile from "./src/screens/Admin/AdminEditMenteeProfile";
import AdminEditMentorProfile from "./src/screens/Admin/AdminEditMentorProfile";
import AdminProfile from "./src/screens/Admin/AdminProfile";
import EditProfileAlumni from "./src/screens/DrawerNavigation/EditProfileAlumni";
import EditProfileMentee from "./src/screens/DrawerNavigation/EditProfileMentee";
import MenteeDetails from "./src/screens/Events/MenteeDetails";
import AdminHeader from "./src/screens/Admin/AdminHeaderComponent";
import AdminDrawerComponent from "./src/screens/Admin/AdminDrawerNavigator";
import AddAdmin from "./src/screens/Admin/AddAdmin";
import AddCollegeAdmin from "./src/screens/Admin/AddCollegeAdmin";
import MyLicenses from "./src/screens/Admin/MyLicenses";
import LicenseHistory from "./src/screens/Admin/LicenseHistory";
import CollegeLicenseDetails from "./src/screens/Admin/CollegeLicenseDetails";
import AdminMentors from "./src/screens/Admin/AdminMentors";
import AdminMentee from "./src/screens/Admin/AdminMentee";
import CreateNewEvent from "./src/screens/Admin/CreateNewEvent";
import CollegeGroupEvents from "./src/screens/Admin/CollegeGroupEvents";
import GroupEvents from "./src/screens/Admin/GroupEvents";
import MenteeGroupEvents from "./src/screens/Events/MenteeGroupEvents";
import GroupEventDetails from "./src/screens/Events/GroupEventDetails";
import EditEventDetails from "./src/screens/Events/EditEventDetails";
import AdminEditEventDetails from "./src/screens/Admin/AdminEditEventDetails";

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}

// All the authentication screens are in AuthStack
const AuthStack = createStackNavigator(
  {
    initial: {
      screen: AuthScreen,
      navigationOptions: () => ({
        headerShown: false,
      }),
    },

    NonAlumniUserSignUp: {
      screen: NonAlumniUserSignUp,
      navigationOptions: () => ({
        headerTitle: "MentorKonnect Sign up",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            rounded
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
    AlumniDetailsSignUp: {
      screen: AlumniDetailsSignUp,
      navigationOptions: () => ({
        headerTitle: "MentorKonnect Sign up",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            rounded
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
    AdminUserSignUp: {
      screen: AdminUserSignUp,
      navigationOptions: () => ({
        headerTitle: "Admin Sign up",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            rounded
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
  },
  {
    initialRouteName: "initial",
  }
);

// Mentor Screens are present in MentorsStack
const MentorsStack = createStackNavigator({
  MentorsPage: {
    screen: Mentors,
    navigationOptions: ({ navigation }) => ({
      headerTitle: () => (
        <Header navigation={navigation} text={"MentorKonnect"} />
      ),
      headerLeft: () => null,
      headerRight: () => null,
      headerBackTitle: false,
    }),
  },
  MentorDetailsPage: {
    screen: MentorDetails,
    navigationOptions: () => ({
      headerTitle: "Mentor Details",
      headerBackTitle: false,
      headerRight: () => (
        <Avatar
          size={40}
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          rounded
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  },
});
const AdminsStack = createStackNavigator(
  {
    LaunchRoute: {
      screen: CollegeAdmins,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <Header navigation={navigation} text={"MentorKonnect"} />
        ),
        headerRight: () => null,
        headerBackTitle: false,
      }),
    },
    AdminDetailsValidation: {
      screen: AdminProfile,
      navigationOptions: () => ({
        headerTitle: "Admin Profile",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            rounded
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
  },
  {
    defaultNavigationOptions: () => ({
      headerRight: () => (
        <Avatar
          size={40}
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          rounded
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  }
);
const AdminMentorsStack = createStackNavigator(
  {
    LaunchRoute: {
      screen: CollegeMentors,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <Header navigation={navigation} text={"MentorKonnect"} />
        ),
        headerRight: () => null,
        headerBackTitle: false,
      }),
    },
    MentorDetailsValidation: {
      screen: AdminProfile,
      navigationOptions: () => ({
        headerTitle: "Mentor Profile",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            rounded
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
    AdminMentorEditProfileRoute: {
      screen: AdminEditMentorProfile,
      navigationOptions: () => ({
        headerTitle: "Edit Mentor Profile ",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            rounded
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
  },
  {
    defaultNavigationOptions: () => ({
      headerRight: () => (
        <Avatar
          size={40}
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          rounded
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  }
);
const AdminMenteeStack = createStackNavigator(
  {
    LaunchRoute: {
      screen: CollegeMentee,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <Header navigation={navigation} text={"MentorKonnect"} />
        ),
        headerRight: () => null,
        headerBackTitle: false,
      }),
    },
    MenteeDetailsValidation: {
      screen: AdminProfile,
      navigationOptions: ({ navigation }) => ({
        headerTitle: "Mentee Profile",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            rounded
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
    AdminMenteeEditProfileRoute: {
      screen: AdminEditMenteeProfile,
      navigationOptions: () => ({
        headerTitle: "Edit Mentee Profile ",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            rounded
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
  },
  {
    defaultNavigationOptions: () => ({
      headerRight: () => (
        <Avatar
          size={40}
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          rounded
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  }
);
// Event Screens are present in RequestsStack
const EventsStack = createStackNavigator(
  {
    LaunchRoute: {
      screen: LaunchScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <Header navigation={navigation} text={"MentorKonnect"} />
        ),
        headerRight: () => null,
        headerBackTitle: false,
      }),
    },
    EventDetailsRoute: {
      screen: EventDetails,
      navigationOptions: () => ({
        headerTitle: "Event Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    AddMentorRoute: {
      screen: AddMentorEvent,
      navigationOptions: () => ({
        headerTitle: "Create new Request",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    SelectMentorRoute: {
      screen: SelectMentor,
      navigationOptions: () => ({
        headerTitle: "Select Mentor",
        headerTintColor: "#2F55C0",
        headerShown: false,
      }),
    },
    ScheduleRoute: {
      screen: ScheduleScreen,
      navigationOptions: () => ({
        headerTitle: "Schedule details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    MentorDetailsRoute: {
      screen: MentorDetails,
      navigationOptions: () => ({
        headerTitle: "Mentor Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    MenteeDetailsRoute: {
      screen: MenteeDetails,
      navigationOptions: () => ({
        headerTitle: "Mentee Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    EditEventDetails: {
      screen: EditEventDetails,
      navigationOptions: () => ({
        headerTitle: "Edit Additional Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
  },
  {
    defaultNavigationOptions: () => ({
      headerRight: () => (
        <Avatar
          size={40}
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          rounded
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  }
);

const MenteeGroupEventStack = createStackNavigator({
  MenteeGroupEvents: {
    screen: MenteeGroupEvents,
    navigationOptions: ({ navigation }) => ({
      headerTitle: () => (
        <Header navigation={navigation} text={"MentorKonnect"} />
      ),
      headerRight: () => null,
      headerBackTitle: false,
    }),
  },
  GroupEventDetailsRoute: {
    screen: GroupEventDetails,
    navigationOptions: () => ({
      headerTitle: "Event Details",
      headerTintColor: "#2F55C0",
      headerBackTitle: false,
      headerRight: () => (
        <Avatar
          size={40}
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          rounded
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  },
});

// MentorTabNavigator combines the screens of MentorAppStack and RequestsAppStack into one TabNavigator
const MentorTabNavigator = createBottomTabNavigator(
  {
    Events: EventsStack,
    MentorsStack: {
      screen: MentorsStack,
      navigationOptions: {
        title: "Mentors",
      },
    },
    GroupEvents: {
      screen: MenteeGroupEventStack,
      navigationOptions: {
        title: "Group Events",
      },
    },
  },
  {
    initialRouteName: "Events", // initially on login requests page must appear for mentor or mentee or admin
    labelStyle: { fontSize: 10 },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: () => {
        const { routeName } = navigation.state;
        if (routeName === "MentorsStack") {
          return (
            <Image
              source={require("./assets/alumni.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        } else if (routeName == "Events") {
          return (
            <Image
              source={require("./assets/events.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        } else if (routeName == "GroupEvents") {
          return (
            <Image
              source={require("./assets/group-events.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        }
      },
    }),
    tabBarOptions: {
      // keyboardHidesTabBar: true,
      activeBackgroundColor: "lightgrey",
      activeTintColor: "black",
      inactiveTintColor: "darkgray",
      labelStyle: {
        textAlignVertical: "bottom",
        fontSize: 14,
        marginBottom: Dimensions.get("window").height * 0.01,
      },
      style: {
        alignContent: "center",
        height: Dimensions.get("window").height * 0.09,
      },
    },
  }
);
const AdminTabNavigator = createBottomTabNavigator(
  {
    Admins: AdminsStack,
    Mentors: {
      screen: AdminMentorsStack,
      navigationOptions: {
        title: "Mentors",
      },
    },
    Mentees: {
      screen: AdminMenteeStack,
      navigationOptions: {
        title: "Mentees",
      },
    },
  },
  {
    initialRouteName: "Admins", // initially on login requests page must appear for mentor or mentee or admin
    labelStyle: { fontSize: 10 },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: () => {
        const { routeName } = navigation.state;
        if (routeName === "Admins") {
          return (
            <Image
              source={require("./assets/admin.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        } else if (routeName == "Mentors") {
          return (
            <Image
              source={require("./assets/classroom.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        } else if (routeName == "Mentees") {
          return (
            <Image
              source={require("./assets/mentee.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        }
      },
    }),
    tabBarOptions: {
      // keyboardHidesTabBar: true,
      activeBackgroundColor: "lightgrey",
      activeTintColor: "black",
      inactiveTintColor: "darkgray",
      labelStyle: {
        textAlignVertical: "bottom",
        fontSize: 14,
        marginBottom: Dimensions.get("window").height * 0.01,
      },
      style: {
        alignContent: "center",
        height: Dimensions.get("window").height * 0.09,
      },
    },
  }
);
// MentorTabNavigator combines the screens of MentorAppStack and RequestsAppStack into one TabNavigator
const MenteeTabNavigator = createBottomTabNavigator(
  {
    MentorsStack: {
      screen: MentorsStack,
      navigationOptions: {
        title: "Mentors",
      },
    },
    Events: {
      screen: EventsStack,
      navigationOptions: {
        title: "Events",
      },
    },
    GroupEvents: {
      screen: MenteeGroupEventStack,
      navigationOptions: {
        title: "Group Events",
      },
    },
  },
  {
    // initially on login requests page must appear for mentor or mentee or admin
    initialRouteName: "MentorsStack",
    labelStyle: { fontSize: 10 },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: () => {
        const { routeName } = navigation.state;
        if (routeName === "MentorsStack") {
          return (
            <Image
              source={require("./assets/alumni.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        } else if (routeName == "Events") {
          return (
            <Image
              source={require("./assets/events.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        } else if (routeName == "GroupEvents") {
          return (
            <Image
              source={require("./assets/group-events.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        }
      },
    }),
    tabBarOptions: {
      // keyboardHidesTabBar: false,
      activeBackgroundColor: "lightgrey",
      activeTintColor: "black",
      inactiveTintColor: "darkgray",
      labelStyle: {
        textAlignVertical: "bottom",
        fontSize: 14,
        marginBottom: Dimensions.get("window").height * 0.01,
      },
      style: {
        alignContent: "center",
        height: Dimensions.get("window").height * 0.09,
      },
    },
  }
);

//Profile Screen is in ProfileStack
const ProfileStack = createStackNavigator({
  ProfilePage: {
    screen: Profile,
    navigationOptions: ({ navigation }) => ({
      headerTitle: () => <Header navigation={navigation} text="Profile" />,
      headerLeft: () => null,
      headerBackTitle: false,
    }),
  },
  EditProfileAlumniPage: {
    screen: EditProfileAlumni,
    navigationOptions: () => ({
      headerTitle: "Edit Profile",
      headerBackTitle: false,
      headerRight: () => (
        <Avatar
          size={40}
          rounded
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  },
  EditProfileMenteePage: {
    screen: EditProfileMentee,
    navigationOptions: () => ({
      headerTitle: "Edit Profile",
      headerBackTitle: false,
      headerRight: () => (
        <Avatar
          size={40}
          rounded
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  },
});

// Setting Screen is in SettingsStack
const SettingsStack = createStackNavigator(
  {
    SettingView: Settings,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: () => <Header navigation={navigation} text={"Settings"} />,
      headerLeft: () => null,
      headerBackTitle: false,
    }),
  }
);

// About Screen is in AboutStack
const AboutStack = createStackNavigator(
  {
    AboutView: About,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: () => <Header navigation={navigation} text={"About"} />,
      headerLeft: () => null,
      headerBackTitle: false,
    }),
  }
);
// FAQ Screen is in AboutStack
const FaqStack = createStackNavigator(
  {
    FaqView: FAQ,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: () => <Header navigation={navigation} text={"FAQ's"} />,
      headerLeft: () => null,
      headerBackTitle: false,
    }),
  }
);
const ActivateStack = createStackNavigator(
  {
    Activate: Activate,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: () => (
        <Header navigation={navigation} text={"Activate Profile"} />
      ),
      headerLeft: () => null,
      headerBackTitle: false,
    }),
  }
);
const AdminActivateStack = createStackNavigator(
  {
    AdminActivate: AdminActivate,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: () => (
        <Header navigation={navigation} text={"Activate Profile"} />
      ),
      headerLeft: () => null,
      headerBackTitle: false,
    }),
  }
);
// DrawerNavigator aggregates UserTabNavigator,ProfilePage,SettingPage,AboutPage
const MentorDrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: MentorTabNavigator,
    },
    ProfilePage: ProfileStack,
    SettingsPage: SettingsStack,
    AboutPage: AboutStack,
    FaqPage: FaqStack,
  },
  {
    contentComponent: DrawerComponent,
    drawerType: "front",
    drawerWidth: () => {
      return Dimensions.get("window").width * 0.7;
    },
  }
);
const MenteeSignUpStack = createStackNavigator({
  ProfilePage: {
    screen: MenteeUserSignUp,
    navigationOptions: ({ navigation }) => ({
      headerTitle: () => <Header navigation={navigation} text="Mentee Login" />,
      headerLeft: () => null,
      headerBackTitle: false,
    }),
  },
  
  
});
const PreviewAboutStack = createStackNavigator(
  {
    AboutView: PreviewAbout,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: () => <Header navigation={navigation} text={"About"} />,
      headerLeft: () => null,
      headerBackTitle: false,
    }),
  }
);
const PreviewMentorsStack = createStackNavigator({
  MentorsPage: {
    screen: PreviewMentors,
    navigationOptions: ({ navigation }) => ({
      headerTitle: () => (
        <Header navigation={navigation} text={"MentorKonnect"} />
      ),
      headerLeft: () => null,
      headerRight: () => null,
      headerBackTitle: false,
    }),
  },
 
});
const PreviewEventsStack = createStackNavigator(
  {
    LaunchRoute: {
      screen: PreviewLaunchScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <Header navigation={navigation} text={"MentorKonnect"} />
        ),
        headerRight: () => null,
        headerBackTitle: false,
      }),
    },
    
  },
  {
    defaultNavigationOptions: () => ({
      headerRight: () => (
        <Avatar
          size={40}
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          rounded
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  }
);
const PreviewMenteeGroupEventStack = createStackNavigator({
  MenteeGroupEvents: {
    screen: PreviewMenteeGroupEvents,
    navigationOptions: ({ navigation }) => ({
      headerTitle: () => (
        <Header navigation={navigation} text={"MentorKonnect"} />
      ),
      headerRight: () => null,
      headerBackTitle: false,
    }),
  },
  
});
const PreviewMenteeTabNavigator = createBottomTabNavigator(
  {
    MentorsStack: {
      screen: PreviewMentorsStack,
      navigationOptions: {
        title: "Mentors",
      },
    },
    Events: {
      screen: PreviewEventsStack,
      navigationOptions: {
        title: "Events",
      },
    },
    GroupEvents: {
      screen: PreviewMenteeGroupEventStack,
      navigationOptions: {
        title: "Group Events",
      },
    },
  },
  {
    // initially on login requests page must appear for mentor or mentee or admin
    initialRouteName: "MentorsStack",
    labelStyle: { fontSize: 10 },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: () => {
        const { routeName } = navigation.state;
        if (routeName === "MentorsStack") {
          return (
            <Image
              source={require("./assets/alumni.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        } else if (routeName == "Events") {
          return (
            <Image
              source={require("./assets/events.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        } else if (routeName == "GroupEvents") {
          return (
            <Image
              source={require("./assets/group-events.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        }
      },
    }),
    tabBarOptions: {
      // keyboardHidesTabBar: false,
      activeBackgroundColor: "lightgrey",
      activeTintColor: "black",
      inactiveTintColor: "darkgray",
      labelStyle: {
        textAlignVertical: "bottom",
        fontSize: 14,
        marginBottom: Dimensions.get("window").height * 0.01,
      },
      style: {
        alignContent: "center",
        height: Dimensions.get("window").height * 0.09,
      },
    },
  }
);
const PreviewFaqStack = createStackNavigator(
  {
    FaqView: PreviewFAQ,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: () => <Header navigation={navigation} text={"FAQ's"} />,
      headerLeft: () => null,
      headerBackTitle: false,
    }),
  }
);
const PreviewDrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: PreviewMenteeTabNavigator,
    },
    ProfilePage: MenteeSignUpStack,
    AboutPage: PreviewAboutStack,
    FaqPage: PreviewFaqStack,
  },
  {
    contentComponent: PreviewDrawerComponent,
    drawerType: "front",
    drawerWidth: () => {
      return Dimensions.get("window").width * 0.7;
    },
  }
);

const MenteeDrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: MenteeTabNavigator,
    },
    ProfilePage: ProfileStack,
    SettingsPage: SettingsStack,
    AboutPage: AboutStack,
    FaqPage: FaqStack,
    ActivatePage: ActivateStack,
  },
  {
    contentComponent: DrawerComponent,
    drawerType: "front",
    drawerWidth: () => {
      return Dimensions.get("window").width * 0.7;
    },
  }
);

const AdminEventsStack = createStackNavigator(
  {
    AdminLaunch: {
      screen: AdminLaunch,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <AdminHeader text="Events" navigation={navigation} />
        ),
        headerRight: () => null,
        headerBackTitle: false,
      }),
    },
    AdminEventDetails: {
      screen: AdminEventDetails,
      navigationOptions: () => ({
        headerTitle: "Event Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    AdminEditEventDetails: {
      screen: AdminEditEventDetails,
      navigationOptions: () => ({
        headerTitle: "Edit Additional Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    MenteeDetailsRoute: {
      screen: MenteeDetails,
      navigationOptions: () => ({
        headerTitle: "Mentee Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    MentorDetailsRoute: {
      screen: MentorDetails,
      navigationOptions: () => ({
        headerTitle: "Mentor Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    ScheduleRoute: {
      screen: ScheduleScreen,
      navigationOptions: () => ({
        headerTitle: "Schedule details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    SelectMentorRoute: {
      screen: SelectMentor,
      navigationOptions: () => ({
        headerTitle: "Select Mentor",
        headerTintColor: "#2F55C0",
        headerShown: false,
      }),
    },
  },
  {
    defaultNavigationOptions: ({}) => ({
      headerRight: () => (
        <Avatar
          size={40}
          rounded
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 20 }}
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  }
);

const AddAlumniStack = createStackNavigator(
  {
    AddAlumniRoute: AddAlumni,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: () => (
        <AdminHeader text="Add Alumni" navigation={navigation} />
      ),
      headerLeft: () => null,
    }),
  }
);

const AdminMentorValidationStack = createStackNavigator(
  {
    AdminMentorView: AdminMentors,
    MentorDetailsValidation: {
      screen: AdminProfile,
      navigationOptions: () => ({
        headerTitle: "Mentor Profile",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            rounded
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
    AdminMentorEditProfileRoute: {
      screen: AdminEditMentorProfile,
      navigationOptions: () => ({
        headerTitle: "Edit Mentor Profile ",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            rounded
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: () => (
        <AdminHeader text="Mentor Validation" navigation={navigation} />
      ),
    }),
  }
);

const AdminMenteeValidationStack = createStackNavigator(
  {
    AdminMenteeView: AdminMentee,
    MenteeDetailsValidation: {
      screen: AdminProfile,
      navigationOptions: ({ navigation }) => ({
        headerTitle: "Mentee Profile",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            rounded
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
    AdminMenteeEditProfileRoute: {
      screen: AdminEditMenteeProfile,
      navigationOptions: () => ({
        headerTitle: "Edit Mentee Profile ",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            rounded
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: () => (
        <AdminHeader text="Mentee Validation" navigation={navigation} />
      ),
    }),
  }
);

const AddAdminStack = createStackNavigator(
  {
    AddAdminRoute: AddAdmin,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: () => (
        <AdminHeader text="Add Admin" navigation={navigation} />
      ),
      headerLeft: () => null,
      headerBackTitle: false,
    }),
  }
);
const AddCollegeAdminStack = createStackNavigator(
  {
    LaunchRoute: {
      screen: AddCollegeAdmin,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <Header navigation={navigation} text={"MentorKonnect"} />
        ),
        headerRight: () => null,
        headerBackTitle: false,
      }),
    },
  },
  {
    defaultNavigationOptions: () => ({
      headerRight: () => (
        <Avatar
          size={40}
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          rounded
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  }
);
const MyLicensesStack = createStackNavigator(
  {
    LaunchRoute: {
      screen: MyLicenses,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <Header navigation={navigation} text={"My Licenses"} />
        ),
        headerRight: () => null,
        headerBackTitle: false,
      }),
    },
  },
  {
    defaultNavigationOptions: () => ({
      headerRight: () => (
        <Avatar
          size={40}
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          rounded
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  }
);

const LicenseHistoryStack = createStackNavigator(
  {
    LaunchRoute: {
      screen: LicenseHistory,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <Header navigation={navigation} text={"MentorKonnect"} />
        ),
        headerRight: () => null,
        headerBackTitle: false,
      }),
    },
    LicenseDetails: {
      screen: CollegeLicenseDetails,
      navigationOptions: () => ({
        headerTitle: "License History",
        headerBackTitle: false,
        headerRight: () => (
          <Avatar
            size={40}
            rounded
            containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
            source={require("./assets/mentorkonnect-icon.png")}
          />
        ),
      }),
    },
  },
  {
    defaultNavigationOptions: () => ({
      headerRight: () => (
        <Avatar
          size={40}
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          rounded
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  }
);
const AddCollegeAdminTabNavigator = createBottomTabNavigator(
  {
    AddCollegeAdminRoute: {
      screen: AddCollegeAdminStack,
      navigationOptions: {
        title: "Add License",
      },
    },
    LicenseHistoryRoute: {
      screen: LicenseHistoryStack,
      navigationOptions: {
        title: "License History",
      },
    },
  },
  {
    initialRouteName: "AddCollegeAdminRoute", // initially on login requests page must appear for mentor or mentee or admin
    labelStyle: { fontSize: 10 },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: () => {
        const { routeName } = navigation.state;
        if (routeName === "AddCollegeAdminRoute") {
          return (
            <Image
              source={require("./assets/license.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        } else if (routeName == "LicenseHistoryRoute") {
          return (
            <Image
              source={require("./assets/history.png")}
              style={{ width: 30, height: 30 }}
            />
          );
        }
      },
    }),
    tabBarOptions: {
      // keyboardHidesTabBar: true,
      activeBackgroundColor: "lightgrey",
      activeTintColor: "black",
      inactiveTintColor: "darkgray",
      labelStyle: {
        textAlignVertical: "bottom",
        fontSize: 14,
        marginBottom: Dimensions.get("window").height * 0.01,
      },
      style: {
        alignContent: "center",
        height: Dimensions.get("window").height * 0.09,
      },
    },
  }
);

const CollegeGroupEventsStack = createStackNavigator(
  {
    CollegeGroupEvents: {
      screen: CollegeGroupEvents,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <AdminHeader text="College Group Events" navigation={navigation} />
        ),
        headerLeft: () => null,
        headerBackTitle: false,
        headerRight: () => null,
      }),
    },
    CreateNewEventRoute: {
      screen: CreateNewEvent,
      navigationOptions: () => ({
        headerTitle: "Create new event",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    AdminEventDetails: {
      screen: AdminEventDetails,
      navigationOptions: () => ({
        headerTitle: "Event Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    MenteeDetailsRoute: {
      screen: MenteeDetails,
      navigationOptions: () => ({
        headerTitle: "Mentee Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    MentorDetailsRoute: {
      screen: MentorDetails,
      navigationOptions: () => ({
        headerTitle: "Mentor Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    ScheduleRoute: {
      screen: ScheduleScreen,
      navigationOptions: () => ({
        headerTitle: "Schedule details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    SelectMentorRoute: {
      screen: SelectMentor,
      navigationOptions: () => ({
        headerTitle: "Select Mentor",
        headerTintColor: "#2F55C0",
        headerShown: false,
      }),
    },
  },
  {
    defaultNavigationOptions: ({}) => ({
      headerRight: () => (
        <Avatar
          size={40}
          rounded
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  }
);
const GroupEventStack = createStackNavigator(
  {
    GroupEvents: {
      screen: GroupEvents,
      navigationOptions: ({ navigation }) => ({
        headerTitle: () => (
          <AdminHeader text="Group Events" navigation={navigation} />
        ),
        headerLeft: () => null,
        headerBackTitle: false,
        headerRight: () => null,
      }),
    },
    CreateNewEventRoute: {
      screen: CreateNewEvent,
      navigationOptions: () => ({
        headerTitle: "Create new event",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    AdminEventDetails: {
      screen: AdminEventDetails,
      navigationOptions: () => ({
        headerTitle: "Event Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    MenteeDetailsRoute: {
      screen: MenteeDetails,
      navigationOptions: () => ({
        headerTitle: "Mentee Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    MentorDetailsRoute: {
      screen: MentorDetails,
      navigationOptions: () => ({
        headerTitle: "Mentor Details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    ScheduleRoute: {
      screen: ScheduleScreen,
      navigationOptions: () => ({
        headerTitle: "Schedule details",
        headerTintColor: "#2F55C0",
        headerBackTitle: false,
      }),
    },
    SelectMentorRoute: {
      screen: SelectMentor,
      navigationOptions: () => ({
        headerTitle: "Select Mentor",
        headerTintColor: "#2F55C0",
        headerShown: false,
      }),
    },
  },
  {
    defaultNavigationOptions: ({}) => ({
      headerRight: () => (
        <Avatar
          size={40}
          rounded
          containerStyle={{ marginEnd: Platform.OS == "ios" ? 0 : 10 }}
          source={require("./assets/mentorkonnect-icon.png")}
        />
      ),
    }),
  }
);

const AdminDrawerNavigator = createDrawerNavigator(
  {
    Events: AdminEventsStack,
    GroupEvents: GroupEvents,
    ReviewColleges: AdminTabNavigator,
    AddAlumniPage: AddAlumniStack,
    AddAdminPage: AddAdminStack,
    AdminMentorPage: AdminMentorValidationStack,
    AdminMenteePage: AdminMenteeValidationStack,
    GroupEvents: GroupEventStack,
    MyLicenses: MyLicensesStack,
    CollegeGroupEvents: CollegeGroupEventsStack,
    AddCollegeAdminPage: AddCollegeAdminTabNavigator,
    AdminActivatePage: AdminActivateStack,
  },
  {
    contentComponent: AdminDrawerComponent,
    drawerType: "front",
    drawerWidth: () => {
      return Dimensions.get("window").width * 0.7;
    },
  }
);

// SwitchNavigator contains AuthStack and the DrawerNavigator
const SwitchScreens = createSwitchNavigator(
  {
    
    AuthScreen: AuthStack,
    PreviewAppPage:PreviewDrawerNavigator,
    MentorAppPage: MentorDrawerNavigator,
    MenteeAppPage: MenteeDrawerNavigator,
    adminPage: AdminDrawerNavigator,
  },
  {
    initialRouteName: "AuthScreen",
  }
);

//This is used to ignore the warnings.

YellowBox.ignoreWarnings(["Setting a timer"]);
const _console = _.clone(console);
console.warn = (message) => {
  if (message.indexOf("Setting a timer") <= -1) {
    _console.warn(message);
  }
};

// All of the screens are wrapped up into AppContainer and that is exported.
const AppContainer = createAppContainer(SwitchScreens);

export default class App extends React.Component {
  
    async componentDidMount() {
      await Font.loadAsync({
        Roboto: require('native-base/Fonts/Roboto.ttf'),
        Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
        ...Ionicons.font,
      });
      this.setState({ isReady: true });
    }
  
    
  

  render() {
    return (
      <Root>
        <AppContainer />
      </Root>
    );
  }
}
