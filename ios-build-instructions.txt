1. Check out the branch from SourceTree
2. From terminal cd to the project folder
3. execute npm install
4. execute expo build:ios -t archive
    - This builds the ios archive file on expo
5. Download the archive file from expo
6. Upload the build using Transporter application 
    - This will upload the build
7. Login to appstoreconnect.apple.com    
8. Release the build to Testflight users once it is available