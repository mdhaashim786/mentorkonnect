export default googleConfig={
    behaviour:"web",
    androidClientId:"733266237532-l5pc9bscjogh16hfm5bn1i3od3hjg1jc.apps.googleusercontent.com",
    androidStandaloneAppClientId:"733266237532-9c999d5oe7p1qqq4ds0njep41coap63e.apps.googleusercontent.com",
    iosClientId: "733266237532-vdq4cdkbn95n72hrc9pnaooijb8pb9lm.apps.googleusercontent.com",
    iosStandaloneAppClientId : "733266237532-oqjlpc8op1qvo0h6g5li09694kskubfg.apps.googleusercontent.com",
    scopes:["profile","email"]
};