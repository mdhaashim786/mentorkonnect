const FIREBASE_URL =
  "https://us-central1-mycollegekonnect-app-efd5d.cloudfunctions.net/";
export default urls = {
  addAlumniJNTUK: FIREBASE_URL + "addAlumniJNTUK",
  createNewMentoringEvent: FIREBASE_URL + "createNewMentoringEvent",
  getAllAlumni: FIREBASE_URL + "getAllAlumni",
  getAllMentees: FIREBASE_URL + "getAllMentees",
  getAllRequests: FIREBASE_URL + "getAllRequests",
  getAlumniByRegCode: FIREBASE_URL + "getAlumniByRegCode",
  getBranches: FIREBASE_URL + "getBranches",
  getCourses: FIREBASE_URL + "getCourses",
  getHigherEducation: FIREBASE_URL + "getHigherEducation",
  getEventRequests: FIREBASE_URL + "getEventRequests",
  getMentors: FIREBASE_URL + "getMentors",
  getSubjects: FIREBASE_URL + "getSubjects", 
  handleApprovals: FIREBASE_URL + "handleApprovals",
  loadProfile: FIREBASE_URL + "loadProfile",
  newMenteeSignUp: FIREBASE_URL + "newMenteeSignUp",
  sendWelcomeMail: FIREBASE_URL + "sendWelcomeMail",
  updateAlumniJNTUK: FIREBASE_URL + "updateAlumniJNTUK",
  updateEvent: FIREBASE_URL + "updateEvent",
  updateMentee: FIREBASE_URL + "updateMentee",
  uploadProfilePic: FIREBASE_URL + "uploadProfilePic",
  getColleges: FIREBASE_URL + "getColleges",
  getInstitutions: FIREBASE_URL + "getInstitutions",
  resendRegsitrationCode: FIREBASE_URL + "resendRegsitrationCode",
  isAdmin: FIREBASE_URL + "isAdmin",
  isEmailMentor: FIREBASE_URL + "isEmailMentor",
  setUserRole: FIREBASE_URL + "setUserRole",
  isAlumniPresent: FIREBASE_URL + "isAlumniPresent",
  updateRole: FIREBASE_URL + "updateRole",
  getGroupEvents: FIREBASE_URL + "getGroupEvents",
  getAllEvents: FIREBASE_URL + "getAllEvents",
  updateRegisteredGEvent: FIREBASE_URL + "updateRegisteredGEvent",
  updateBadge: FIREBASE_URL + "updateBadge",
  handleApprovalsv2: FIREBASE_URL+"handleApprovalsv2",
  isAdminv2: FIREBASE_URL+"isAdminv2",
  updateAlumniEventRequests: FIREBASE_URL+"updateAlumniEventRequests",
  getRole: FIREBASE_URL+"getRole",
  getAllCollegeEvents: FIREBASE_URL+"getAllCollegeEvents",
  getCollegeGroupEvents: FIREBASE_URL+"getCollegeGroupEvents",
  CheckMenteeInBucket: FIREBASE_URL+"CheckMenteeInBucket",
  SendLicenseCodeMentee: FIREBASE_URL+"SendLicenseCodeMentee",
  SendToLicenseBucketMentee: FIREBASE_URL +"SendToLicenseBucketMentee",
  getCollegeMentees: FIREBASE_URL+"getCollegeMentees",
  getCollegeAlumni: FIREBASE_URL+"getCollegeAlumni",
  addLicenseAdminReference: FIREBASE_URL+"addLicenseAdminReference",
  getAllCollegeAdmins: FIREBASE_URL+"getAllCollegeAdmins",
  findAdmin: FIREBASE_URL+"findAdmin",
  addLicenseadmin: FIREBASE_URL+"addLicenseadmin",
  updateLicenseadmin: FIREBASE_URL+"updateLicenseadmin",
  updateDeviceId: FIREBASE_URL+"updateDeviceId",
  addAlumniJNTUKadmin: FIREBASE_URL+"addAlumniJNTUKadmin",
  updateAlumniJNTUKadmin: FIREBASE_URL+"updateAlumniJNTUKadmin",
  addAlumniJNTUKMentor: FIREBASE_URL+"addAlumniJNTUKMentor",
  getAlumnibyEmail: FIREBASE_URL+"getAlumnibyEmail",
  updateAlumniJNTUKMentor: FIREBASE_URL+"updateAlumniJNTUKMentor",
  addLicenseAdmin: FIREBASE_URL +"addLicenseAdmin",
};
